<?php

namespace Vermal\Ecommerce\Defaults;


use Vermal\Admin\View;

class Controller extends \Vermal\Admin\Defaults\Controller
{

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        parent::__construct();

        // Set propper paths for View
        if (isset($this->defaultViewPath) && !$this->defaultViewPath) {
            $this->setViewPath();
        }

		$this->addToBreadcrumb('ecommerce', 'admin.dashboard');
    }

    /**
     * New view path for ecommerce modules
     */
    public function setViewPath()
    {
        $currentModule = str_replace('\\', '/', get_called_class());
        $currentModule = substr($currentModule, 0, strrpos( $currentModule, '/'));
        $currentModule = str_replace('Vermal/Ecommerce/Modules/', '', $currentModule);
        $currentModule = str_replace('App/Admin/Modules/', '', $currentModule);
        View::setModulePath($currentModule);
        View::setViewPath(__DIR__ . '/../Modules/' . $currentModule . '/templates/');
    }

}
