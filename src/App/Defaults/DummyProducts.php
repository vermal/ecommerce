<?php

namespace Vermal\Ecommerce\Defaults;

use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Defaults\Model;
use Vermal\Admin\Modules\Auth\Entities\Resource\Resource;
use Vermal\Admin\Modules\Auth\Entities\Role\Role;
use Vermal\Admin\Modules\Menu\Entities\MenuItem;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductAttribute;
use Vermal\Ecommerce\Modules\Tax\Entities\Tax;

class DummyProducts
{

	/**
	 * @var array $categories
	 */
	private static $categories = [];

	/**
	 * @var array $attributes
	 */
	private static $attributes = [
		'color' => [
			'blue', 'red', 'green', 'yellow'
		],
		'material' => [
			'leather', 'aluminium', 'textil', 'wood', 'plastic'
		],
		'size' => [
			'XS', 'S', 'M', 'L', 'XL', 'XXL'
		],
		'brand' => [
			'Adidas', 'Nike', 'Puma', 'Lotto'
		]
	];

	/**
	 * @var bool $isVariation
	 */
	private static $isVariation = false;

    /**
     * Main function
     */
    public static function main()
    {
    	\Vermal\Admin\Defaults\Controller::$langs = [
    		'sk', 'en'
		];

//    	self::destroyAttributes();
//    	self::destroyProducts();

		//$dummyCategories = file_get_contents(__DIR__ . '/dummy/dummy-categories.json');
		$dummyProducts = json_decode(file_get_contents(__DIR__ . '/dummy/dummy-products.json'));

		// Import categories if not created
		$categories = Database::Model('ProductCategory')->get();
		if (empty($categories)) {
			// todo
//			foreach ($dummyCategories as $key => $data) {
//				$category = Entity::getEntity('ProductCategory');
//				self::importCategory($category, $key, $data);
//			}
		} else {
			$categories_ = [];
			foreach ($categories as $category) {
				$categories_[$category->id] = $category->title;
			}
			self::$categories = $categories_;
		}
		echo 'Categories imported!' . PHP_EOL . PHP_EOL;

		// Import attributes
		// Get attribues
		$attributes = Database::Model('ProductAttribute')
			->where('p.parent', '!=', null)->get();
		// Set attributes
		$attributes_ = [];
		foreach ($attributes as $attribute) {
			$attributes_[$attribute->id] = $attribute->value;
		}
		self::$attributes = $attributes_;
//		self::importAttributes();
//		echo 'Attributes imported!' . PHP_EOL . PHP_EOL;

		// Import products
		foreach ($dummyProducts as $data) {
			$product = Entity::getEntity('Product');
			$product = self::hydrateProduct($product, $data);
			$product = Database::saveAndFlush($product);
			echo 'Product with id: ' . $product->id . ' created!' . PHP_EOL;
			self::productVariation($data, $product->id);
			echo 'Product variations created!' . PHP_EOL . PHP_EOL . PHP_EOL;
		}
    }

	/**
	 * Import product
	 *
	 * @param Product $product
	 * @param $data
	 * @return Product
	 */
	public static function hydrateProduct($product, $data)
	{
		$product->ean = rand(1000000000000, 9999999999999);
		$product->status = 1;
		$product->order = 1;
		$product->active = 1;

		$product->weight = Sanitizer::applyFilter('trim|float', $data->weight);
		$product->length = Sanitizer::applyFilter('trim|float', $data->length);
		$product->width = Sanitizer::applyFilter('trim|float', $data->width);
		$product->height = Sanitizer::applyFilter('trim|float', $data->height);

		$product->stock = Sanitizer::applyFilter('trim|float', $data->stock);

		// Add translation to content
		foreach (Languages::get() as $lang)
		{
			// Content
			$title = Sanitizer::applyFilter('trim|escape|strip_tags', $data->title);

			$entity = 'ProductTr';
			Database::translate($product, $entity, 'title', $lang, $title);
			Database::translate($product, $entity, 'te', $lang, $data->te);
			Database::translate($product, $entity, 'shortTe', $lang, $data->shortTe);
			Database::translate($product, $entity, 'slug', $lang, Controller::slugify($title));

			// SEO
			Database::translate($product, $entity, 'keywords', $lang, $data->keywords);
		}

		// Add categories to product
		if (!self::$isVariation) {
			$data->categories = array_rand(self::$categories, 2);
			foreach ($data->categories as $category) {
				$category = Sanitizer::applyFilter('trim|digit', $category);
				$newCategory = Database::Model('ProductCategory')->find($category);

				// Check if category exist
				if ($newCategory === null) continue;

				$newCategory->product = $product;
				$product->addCategory($newCategory);
				Database::save($newCategory);
			}
		}

		// Add attributes to product
		if (!self::$isVariation) {
			$data->attributes = array_rand(self::$attributes, 3);
			foreach ($data->attributes as $attribute) {
				$attribute = Sanitizer::applyFilter('trim|digit', $attribute);
				$newAttribute = Database::Model('ProductAttribute')->find($attribute);

				// Check if category exist
				if ($newAttribute === null) continue;

				$newAttribute->product = $product;
				$product->addAttribute($newAttribute);
				Database::save($newAttribute);
			}
		}

		// Add prices
		foreach (['EUR'] as $curency) {
			$productPrice = Entity::getEntity('ProductPrice');

			// Currency
			$productPrice->currency = $curency;

			// Price settings
			$productPrice->price = Sanitizer::applyFilter('trim|digit', rand(5, 2000));
			$productPrice->salePrice = Sanitizer::applyFilter('trim|digit', null);
			$productPrice->isSale = Sanitizer::applyFilter('cast:bool', false);

			// Save
			$productPrice->product = $product;
			$product->addPrice($productPrice);
			Database::save($productPrice);
		}

		return $product;
    }

	/**
	 * Generate product variation
	 *
	 * @param $post
	 * @param $id
	 * @throws \Exception
	 */
	private static function productVariation($post, $id)
	{
		self::$isVariation = true;

		/** @var Product $product */
		$product = Database::Model('Product')->find($id);

		$variations = [];
		$attributes = $product->getAttributes();
		foreach ($attributes as $attr) {
			$variations_ = [];
			foreach ($attr->children as $a) {
				$variations_[] = $a->id;
			}
			$variations[] = $variations_;
		}
		$variations = generate_combinations($variations);

		foreach ($variations as $variation) {
			// Product variation validation
			if (self::productHasVariation($id, $variation)) {
				continue;
			}

			/** @var Product $childProduct */
			$childProduct = Entity::getEntity('Product');
			$productTitleByAttribute = [];
			foreach ($variation as $attribute) {
				/** @var ProductAttribute $attribute */
				$attribute = Database::Model('ProductAttribute')->find(Sanitizer::applyFilter('trim|digit', $attribute));
				if ($attribute === null || empty($attribute->parent)) continue;

				foreach (Languages::get() as $lang) {
					if (!isset($productTitleByAttribute[$lang])) $productTitleByAttribute[$lang] = [];
					$productTitleByAttribute[$lang][] = $attribute->getTr($lang, 'value');
				}

				$childProduct->addAttribute($attribute);
				Database::save($attribute);
			}

			// Copy values from parent product
			foreach (Languages::get() as $lang) {
				$title = $product->getTr($lang, 'title') . ' -  ' . implode(', ', $productTitleByAttribute[$lang]);
				$post->{$lang . '_title'} = '';
				$post->{$lang . '_te'} = '';
				$post->{$lang . '_shortTe'} = '';
				$post->{$lang . '_slug'} = '';
				$post->{$lang . '_keywords'} = '';
			}

			$post->ean = $product->ean;
			$post->status = 1;
			$post->sort = 1;

			$post->weight = $product->weight;
			$post->length = $product->length;
			$post->width = $product->width;
			$post->height = $product->height;

			$post->stock = $product->stock;

			// Prevetn from creating attributes for each variation
			$post->categories = ['111'];
			$post->attributes = ['111'];

			// Hydrate child
			$childProduct = self::hydrateProduct($childProduct, $post);

			$childProduct->parent = $product;
			$product->addChild($childProduct);
			Database::save($childProduct);
		}
		Database::saveAndFlush($product);
		self::$isVariation = false;
	}

	/**
	 * Check if product has variation
	 *
	 * @param $id
	 * @param $variation
	 * @return bool
	 */
	private static function productHasVariation($id, $variation)
	{
		// Check if variation already exist
		$qb = Database::Model('Product')
			->where('p.parent', '=', $id);

		foreach (array_values((array)$variation) as $key => $value) {
			$alias = 'pm' . $key;
			$qb
				->join('p.attributes', $alias)
				->where($alias . '.id', $value);
		}

		return empty($qb->get()) ? false : true;
	}

	/**
	 * Import attributes
	 */
    private static function importAttributes()
	{
		foreach (self::$attributes as $attribute => $values) {
			$entity = Entity::getEntity('ProductAttribute');

			$entity->key = Sanitizer::applyFilter('trim|escape|strip_tags', $attribute);

			// Base attribute
			foreach (Languages::get() as $lang) {
				$tr = 'ProductAttributeTr';
				Database::translate($entity, $tr, 'value', $lang, $attribute);
			}

			// Child values
			$data = [];
			foreach ($values as $k => $value) {
				foreach (Languages::get() as $lang) {
					$data[$lang] = [
						'value' => Sanitizer::applyFilter('trim|escape|strip_tags', $value)
					];
				}
				$child = Entity::getEntity('ProductAttribute');

				foreach ($data as $lang => $v) {
					$tr = 'ProductAttributeTr';
					Database::translate($child, $tr, 'value', $lang, $v['value']);
				}
				$child->parent = $entity;
				Database::save($child);
			}

			Database::saveAndFlush($entity);
		}

		// Get attribues
		$attributes = Database::Model('ProductAttribute')
			->where('p.parent', '!=', null)->get();

		// Set attributes
		$attributes_ = [];
		foreach ($attributes as $attribute) {
			$attributes_[$attribute->id] = $attribute->value;
		}
		self::$attributes = $attributes_;
	}


	/**
	 * Import category
	 *
	 * @param $category
	 * @param $key
	 * @param $data
	 */
	private static function importCategory($category, $key, $data)
	{
		if (!empty($id)) {
			$entity = Database::Model('ProductCategory')->find((int)$id);
			$entity->clearTr();
			Database::saveAndFlush($entity);
		} else
			$entity = Entity::getEntity('ProductCategory');

		foreach (Languages::get() as $lang) {
			$tr = 'ProductCategoryTr';

			$value = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});
			Database::translate($entity, $tr, 'title', $lang, $value);
			Database::translate($entity, $tr, 'te', $lang, Sanitizer::applyFilter('trim', $post->{$lang . '_te'}));
			Database::translate($entity, $tr, 'slug', $lang, self::slugify($value));
		}

		// Set parent
		if (!empty($post->parent)) {
			$entity->parent = Database::Model('ProductCategory')->find($post->parent);
		}

		Database::saveAndFlush($entity);
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public static function destroyProducts()
	{
		foreach (Database::Model('Product')->where('p.parent', '=')->get() as $product) {
			$id = $product->id;
			$product = Database::Model('Product')->find($id);
			$product->clearTr();
			$product->clearMeta();
			$product->clearPrices();
			$product->clearCategories();
			$product->clearAttributes();
			$product->removeAllChildren();
			Database::saveAndFlush($product);
			Database::Model('Product')->delete($id);
		}
	}

	/**
	 * Remove the specified resource from storage.
	 */
	public static function destroyAttributes()
	{
		foreach (Database::Model('ProductAttribute')->where('p.parent', '=')->get() as $attribute) {
			$id = $attribute->id;
			$entity = Database::Model('ProductAttribute')->find($id);
			$entity->clearTr();
			$entity->removeAllChildren();
			Database::saveAndFlush($entity);
			Database::Model('ProductAttribute')->delete($id);
		}
	}

}
