<?php

namespace Vermal\Ecommerce\Defaults;

use Vermal\Admin\Defaults\Model;
use Vermal\Admin\Modules\Auth\Entities\Resource\Resource;
use Vermal\Admin\Modules\Auth\Entities\Role\Role;
use Vermal\Admin\Modules\Menu\Entities\MenuItem;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Tax\Entities\Tax;

class Seeder
{

    /**
     * Main function
     */
    public static function main()
    {
        self::resources();
        self::roles();
        self::menus();
		self::tax();
        // self::settings();

        // Finally save all datas
        Database::flush();
    }

    /**
     * Add default roles
     */
    private static function roles()
    {
        // Create developer role
		/** @var $role Role */
        $role = Database::Model('Role')
			->where('r.name', 'developer')->first();

        // Get permission
        $permissions = Database::Model('Permission')->get();

        foreach ($permissions as $permission) {
			$role->addPermission($permission);
		}

        // Add role to permissions
        Database::saveAndFlush($role);
    }

    /**
     * Add all default resources
     */
    private static function resources()
    {
		self::addResource("Ecommerce", 1);
        self::addResource("Orders", 1);
		self::addResource("Customers", 1);
		self::addResource("Addresses", 1);
		self::addResource("Products", 1);
		self::addResource("Product categories", 1);
		self::addResource("Product attributes", 1);
		self::addResource("Tax", 1);
		self::addResource("Shipping methods", 1);
		self::addResource("Payment methods", 1);
		self::addResource("Expeditions", 1);

        Database::flush();
    }

    /**
     * Add resource
     *
     * @param $name
     * @param $authority
     */
    private static function addResource($name, $authority)
    {
        $resource = Entity::getEntity('Resource');
        $resource->name = Controller::slugify($name);
        $resource->displayName = $name;
        $resource->authority = $authority;

        Database::saveAndFlush($resource);
		self::addPermission($resource, 1, 1,1, 1);
    }

    /**
     * Add permission
     *
     * @param $resource
     * @param $c
     * @param $r
     * @param $u
     * @param $d
     */
    private static function addPermission($resource, $c, $r, $u, $d)
    {
        $permission = Entity::getEntity('Permission');
        $permission->resource = $resource;
        $permission->create = $c;
        $permission->read = $r;
        $permission->update = $u;
        $permission->delete = $d;

        Database::save($permission);
    }

    /**
     * Add menus
     */
    private static function menus()
    {
        $menu = Database::Model('Menu')->where('m.name', 'admin')->first();
        self::menuItems($menu);
    }

    /**
     * Add menu items
     *
     * @param $menu
     */
    private static function menuItems($menu)
    {
        // Add all parents
        self::addMenuItem($menu, "__resources.ecommerce", "static", '#', "", "", "_self", "fas fa-store", 'ecommerce', 3, [
			self::addMenuItem($menu, '__resources.order', 'dynamic', '', 'admin.order.index', '', '_self', '', 'orders', 1),
			self::addMenuItem($menu, '__resources.customer', 'dynamic', '', 'admin.customer.index', '', '_self', '', 'customers', 2),
			self::addMenuItem($menu, '__resources.address', 'dynamic', '', 'admin.address.index', '', '_self', '', 'addresses', 3),
			self::addMenuItem($menu, '__resources.expedition', 'dynamic', '', 'admin.expedition.index', '', '_self', '', 'expeditions', 4),
			self::addMenuItem($menu, '__resources.product', 'dynamic', '', 'admin.product.index', '', '_self', '', 'products', 5),
			self::addMenuItem($menu, '__resources.product_category', 'dynamic', '', 'admin.product-category.index', '', '_self', '', 'product-categories', 6),
			self::addMenuItem($menu, '__resources.product_tag', 'dynamic', '', 'admin.product-tag.index', '', '_self', '', 'products', 7),
			self::addMenuItem($menu, '__resources.product_attribute', 'dynamic', '', 'admin.product-attribute.index', '', '_self', '', 'product-attributes', 8),
			self::addMenuItem($menu, '__resources.productReviews', 'dynamic', '', 'admin.product-review.index', '', '_self', '', 'products', 9),
			self::addMenuItem($menu, '__resources.tax', 'dynamic', '', 'admin.tax.index', '', '_self', '', 'tax', 8),
			self::addMenuItem($menu, '__resources.shippingMethod', 'dynamic', '', 'admin.shippingMethod.index', '', '_self', '', 'shipping-methods', 10),
			self::addMenuItem($menu, '__resources.paymentMethod', 'dynamic', '', 'admin.paymentMethod.index', '', '_self', '', 'payment-methods', 11),
		]);
        Database::flush();
    }

    /**
     * Add menu item
     *
     * @param $menu
     * @param $title
     * @param $type
     * @param $url
     * @param $route
     * @param $parameters
     * @param $target
     * @param $icon
     * @param $resource
     * @param $sort
	 * @param $children
	 *
	 * @return MenuItem
     */
    private static function addMenuItem($menu, $title, $type, $url, $route, $parameters, $target, $icon, $resource, $sort, $children = [])
    {
    	/** @var MenuItem $item */
        $item = Entity::getEntity('MenuItem');

        /** @var Resource $resource */
        $resource = Database::Model('Resource')->where('r.name', $resource)->first();
		if (empty($resource)) $resource = null;
		else $resource = $resource->getId();

        $item->menu = $menu;
        $item->title = $title;
        $item->type = $type;
        $item->url = $url;
        $item->route = $route;
        $item->parameters = $parameters;
        $item->target = $target;
        $item->iconClass = $icon;
        $item->note = $resource;
        $item->order = $sort;

		foreach ($children as $child) {
			/** @var $child MenuItem */
			$child->parent = $item;
			$item->addChild($child);
			Database::save($child);
		}

        return Database::save($item);
    }

    private static function settings()
    {
        $setting = Entity::getEntity('Setting');

        // Save settings
        Database::saveAndFlush($setting);
    }

    private static function tax()
	{
		/** @var Tax $tax */
		$tax = Entity::getEntity('Tax');

		$tax->title = 'Default';
		$tax->countryCode = '*';
		$tax->postcode = '*';
		$tax->rate = 20;
		$tax->priority = 1;

		Database::saveAndFlush($tax);
	}

}
