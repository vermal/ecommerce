<?php

namespace Vermal\Ecommerce\Modules\Checkout;

use Tree\Fixture\Transport\Car;
use Vermal\Admin\Defaults\ControllerExtension;
use Vermal\Admin\View;
use Vermal\App;
use Vermal\Controller;
use Vermal\Database\Database;
use Vermal\Ecommerce\Modules\Cart\Cart;
use Vermal\Ecommerce\Modules\Customers\Models\CustomerModel;
use Vermal\Ecommerce\Modules\Orders\OrderModel;
use Vermal\Ecommerce\Modules\Orders\Orders;
use Vermal\Form\Form;
use Vermal\Router;

class Checkout extends Controller
{

	use ControllerExtension;

	protected $defaultViewPath = false;

	private $cart;

    public function __construct()
    {
        parent::__construct();
		View::setViewPath(__DIR__ . '/templates/');
		View::setExtendsPath(View::getViewPath());

		// Setup settings
		$this->settingsSetup();
		View::setVariable('settings', $this->settings);

		View::setVariable('documents', App::get('documents'));

		// Cart
		$this->cart = Cart::getInstance()->getCart();
	}

    /**
     * checkout.form.
     */
    public function index()
    {
    	if ($this->cart->items->isEmpty()) {
			Router::redirect(appGet('ecommerce.shop-path', 'ecommerce.shop'));
		}

    	$form = $this->form();

		Cart::getInstance()->setShipping();
		Cart::getInstance()->setPayment();

		// Checkout header
		View::setVariable('checkoutHeader', $this->settings->checkout_brand_title);
		View::setVariable('checkoutHeaderImage', $this->settings->checkout_brand_image);

		View::view('checkout', [
			'form' => $form->build(),
			'shippingMethods' => self::getShippingMethodsView(),
			'paymentMethods' => self::getPaymentMethodsView(),
			'currency' => $this->currency,
			'cart' => $this->cart
		]);
    }

	public static function getShippingMethodsView()
	{
		$cart = Cart::getInstance();
		View::setViewPath(__DIR__ . '/templates/');
		return View::view('parts.shippings', [
			'shippingMethods' => $cart->getShippingMethods(),
			'currency' => self::$currencyStatic,
			'selectedMethod' => !empty($cart->getCart()->shipping) ? $cart->getCart()->shipping->id : null
		], true);
    }

	public static function getPaymentMethodsView()
	{
		$cart = Cart::getInstance();
		View::setViewPath(__DIR__ . '/templates/');
		return View::view('parts.payments', [
			'paymentMethods' => $cart->getPaymentMethods(),
			'currency' => self::$currencyStatic,
			'selectedMethod' => !empty($cart->getCart()->payment) ? $cart->getCart()->payment->id : null
		], true);
	}

	public static function getCouponsView()
	{
		$cart = Cart::getInstance();
		View::setViewPath(__DIR__ . '/templates/');
		return View::view('parts.coupons', [
			'cart' => $cart->getCart()
		], true);
	}

	public function orderReceived($id)
	{
		$id = Router::$params['id'];
		$orderId = App::sessionGet('orderId');

		// Redirect to shop if this is not order for current user
		if (empty($orderId) || $orderId != $id)
			Router::redirect(appGet('ecommerce.shop-path', 'ecommerce.shop'));

		// Checkout header
		View::setVariable('checkoutHeader', $this->settings->checkout_brand_title);
		View::setVariable('checkoutHeaderImage', $this->settings->checkout_brand_image);

		$order = Database::Model('Order')->find($id);

		// Extend functionality
		do_action('ecommerce_after_order_received', [$order, $this->settings]);

		View::view('thankyou', [
			'order' => $order
		]);
    }

	/**
	 * Handle button click in checkout
	 *
	 * @param $post
	 */
	public function make($post)
	{
		if (isset($post->form)) {
			$action = $post->action;
			parse_str($post->form, $form);
			$form = (object)$form;
		} else {
			$this->order($post);
			die();
		}

		$data = [];
		if ($action == 'change-step-1')
			$data = $this->step1($form);
		else if ($action == 'change-step-2')
			$data = $this->step2($form);
		else if ($action == 'change-step-3')
			$data = $this->step3($form);
		else if ($action == 'change-shipping-method')
			$data = $this->shippingMethod($form);
		else if ($action == 'change-payment-method')
			$data = $this->paymentMethod($form);

		$this->cart = Cart::getInstance()->getCart();

		$shipping = '-';
		if (!empty($this->cart->shipping))
			$shipping = $this->cart->getFormattedPrice('shipping_amount');

		$payment = '-';
		if (!empty($this->cart->payment))
			$payment = $this->cart->getFormattedPrice('payment_amount');

		$data = [
			'fields' => [
				'price-shipping' => $shipping,
				'price-payment' => $payment,
				'price-total' => $this->cart->getFormattedPrice('price'),
				'price-subtotal' => $this->cart->getFormattedPrice('price_items'),
				'price-discount' => $this->cart->getFormattedPrice('discount'),
				'shipping-methods' => Checkout::getShippingMethodsView(),
				'payment-methods' => Checkout::getPaymentMethodsView(),
			],
			'data' => $data
		];

		echo json_encode($data);
    }

	/**
	 * Create order
	 *
	 * @param $post
	 */
    private function order($post)
	{
		$cart = $this->cart;

		// Set post data
		$post->items = [];
		foreach ($cart->items as $item) {
			$post->items[] = [
				'id' => $item->product->id,
				'qty' => $item->qty
			];
		}
		$post->currency = $this->currency;

		$post->shippingMethod = $post->shipping_method;
		$post->paymentMethod = $post->payment_method;
		$post->customer = 'guest';

		// Create fields if not created
		$fields = [
			'email', 'shipping_address_toggle',

			'billing_name', 'billing_surname', 'billing_address', 'billing_city', 'billing_postcode',
			'billing_country', 'billing_phone', 'billing_company', 'billing_ico', 'billing_dic',
			'billing_ic_dph',

			'shipping_name', 'shipping_surname', 'shipping_address', 'shipping_city', 'shipping_postcode',
			'shipping_country',

			'shipping_method'
		];
		foreach ($fields as $field) {
			if (!isset($post->{$field})) {
				$post->{$field} = '';
			}
		}

		$orderModel = new OrderModel((array)$post, $this->form());
		$order = $orderModel->store();

		if (!$orderModel->form->hasError()) {
			// Delete cart
			$this->cart->clearItems();
			$this->cart->clearCoupons();
			Database::saveAndFlush($this->cart);
			Database::Model('Cart')->delete($this->cart->id);
			App::sessionSet('orderId', $order->id);
			App::sessionSet('checkout', []);

			// Send new order email
			Orders::sendNewOrderEmail($order, $this->settings);

			$order->customer->unfinishedOrder = 0;
			Database::saveAndFlush($order->customer);

			// Extend functionality
			do_action('ecommerce_after_order_created', [$order, $this->settings]);

			echo json_encode([
				'location' => routerLink('ecommerce.order-received', ['id' => $order->id])
			]);
		} else {
			echo json_encode([
				'location' => routerLink('ecommerce.checkout')
			]);
		}
	}

	/**
	 * @param $data
	 */
    private function step1($data)
	{
		Cart::getInstance()->setShipping();
		Cart::getInstance()->setPayment();
	}

	/**
	 * @param $data
	 */
	private function step2($data)
	{
		Cart::getInstance()->setPayment();
		if (!empty($data->shipping_method))
			Cart::getInstance()->setShipping($data->shipping_method);
		else Cart::getInstance()->setShipping();
		$this->saveSessionData([
			'email', 'shipping_address_toggle',

			'billing_name', 'billing_surname', 'billing_address', 'billing_city', 'billing_postcode',
			'billing_country', 'billing_phone', 'billing_company', 'billing_ico', 'billing_dic',
			'billing_ic_dph',

			'shipping_name', 'shipping_surname', 'shipping_address', 'shipping_city', 'shipping_postcode',
			'shipping_country',

			'shipping_method'
		], $data);

		// Create customer
		$customerModel = (new CustomerModel([
			'name' => self::getField('billing_name'),
			'surname' => self::getField('billing_surname'),
			'phone' => self::getField('billing_phone'),
			'email' => self::getField('email'),
			'unfinishedOrder' => true
		]))->store();
	}

	/**
	 * @param $data
	 */
	private function step3($data)
	{
		Cart::getInstance()->setPayment($data->payment_method);
		$this->saveSessionData([
			'shipping_method', 'payment_method'
		], $data);
	}

	/**
	 * Set shipping method
	 *
	 * @param $data
	 */
	private function shippingMethod($data)
	{
		$this->saveSessionData([
			'shipping_method'
		], $data);
		Cart::getInstance()->setPayment();
		Cart::getInstance()->setShipping($data->shipping_method);
	}

	/**
	 * Set payment method
	 *
	 * @param $data
	 */
	private function paymentMethod($data)
	{
		$this->saveSessionData([
			'payment_method'
		], $data);
		Cart::getInstance()->setPayment($data->payment_method);
	}

	/**
	 * @param $key
	 * @return string|false
	 */
	public static function getField($key)
	{
		$checkout = App::sessionGet('checkout');
		if (isset($checkout[$key]))
			return $checkout[$key];
		else return false;
	}

	/**
	 * @param $key
	 * @param $value
	 */
	public static function setField($key, $value)
	{
		$checkout = App::sessionGet('checkout');
		$checkout[$key] = $value;
		App::sessionSet('checkout', $checkout);
	}

	private function saveSessionData($save, $data)
	{
		$sessionData = App::sessionGet('checkout');
		if (empty($sessionData)) $sessionData = [];
		foreach ($save as $key) {
			if (isset($data->{$key}))
				$sessionData[$key] = $data->{$key};
		}
		App::sessionSet('checkout', $sessionData);
	}

	private function form()
	{
		$form = new Form('checkout', routerLink('ecommerce.checkout.make'));
		$form->setAjax(true);
//		$form->JSvalidate();

		$form->addEmail('email', $this->_('checkout.form.email'))
			->required();

		// Todo: countries selection
		$countries = [];

		// Billing/Shipping data
		View::setVariable('billing_fields', apply_filters('ecommerce_checkout_billing_fields', [
			'name', 'surname', 'address', 'city', 'postcode', 'country', 'phone', 'company', 'ico', 'dic', 'ic_dph'
		]));
		View::setVariable('shipping_fields', apply_filters('ecommerce_checkout_shipping_fields', [
			'name', 'surname', 'address', 'city', 'postcode', 'country'
		]));

		$form->addText('billing_name', $this->_('checkout.form.name'))->setCol('col-md-6')->required();
		$form->addText('billing_surname', $this->_('checkout.form.surname'))->setCol('col-md-6')->required();
		$form->addText('billing_phone', $this->_('checkout.form.phone'))->required();
		$form->addText('billing_address', $this->_('checkout.form.address'))
			->setClass('here-maps-suggest')
			->addAttribute('autocomplete', 'offf')
			->required();
		$form->addText('billing_city', $this->_('checkout.form.city'))->setCol('col-md-8')->required();
		$form->addText('billing_postcode', $this->_('checkout.form.psc'))->setCol('col-md-4')->required();
		$form->addText('billing_country', $this->_('checkout.form.country'))->required();
		$form->addText('billing_company', $this->_('checkout.form.companyName'));
		$form->addText('billing_ico', $this->_('checkout.form.ico'))->setCol('col-lg-4');
		$form->addText('billing_dic', $this->_('checkout.form.dic'))->setCol('col-lg-4');
		$form->addText('billing_ic_dph', $this->_('checkout.form.ic_dph'))->setCol('col-lg-4');

		$form->addText('shipping_name', $this->_('checkout.form.name'))->setCol('col-md-6');
		$form->addText('shipping_surname', $this->_('checkout.form.surname'))->setCol('col-md-6');
		$form->addText('shipping_address', $this->_('checkout.form.address'))
			->addAttribute('autocomplete', 'offf')
			->setClass('here-maps-suggest');
		$form->addText('shipping_city', $this->_('checkout.form.city'))->setCol('col-md-8');
		$form->addText('shipping_postcode', $this->_('checkout.form.psc'))->setCol('col-md-4');
		$form->addText('shipping_country', $this->_('checkout.form.country'));

		$form->addCheckboxSwitch('shipping_address_toggle', $this->_('checkout.form.differentAddress'))
			->toggle('shipping-info', '=', 1);

		$form->addHidden('predicted_delivery');

		$form->setValues(App::sessionGet('checkout'));

		$form = apply_filters('ecommerce_checkout_form', $form);

		return $form;
    }
}
