<div class="inner pb-0 text-center change-step-nav-wrapper">
    <div class="brand mb-3">
        @if(empty($checkoutHeaderImage))
            <h2>{{ $checkoutHeader }}</h2>
        @else
            <img class="logo mb-0" src="{{ $checkoutHeaderImage->getMiniFile() }}" alt="">
        @endif
    </div>
    <p class="change-step-nav">
        <a href="{{ routerLink(appGet('ecommerce.shop-path', 'ecommerce.shop')) }}" class="enabled">@__(checkout.shop)</a>
        <span class="chevron">></span>
        <a href="#step-1" data-step="1" class="change-step enabled active">@__(checkout.customerInfo)</a>
        <span class="chevron">></span>
        <a href="#step-2" data-step="2" class="change-step">@__(checkout.deliveryInfo)</a>
        <span class="chevron">></span>
        <a href="#step-3" data-step="3" class="change-step">@__(checkout.paymentInfo)</a>
    </p>
</div>
