<div class="methods shipping">

    @php($someActive = false)
    @foreach($shippingMethods as $shipping)
        @php($active = ($loop->first && empty($selectedMethod) || $selectedMethod == $shipping->id) ? true : false)
        <input id="shipping_method_{{ $shipping->id }}"
               type="radio" class="input-radio shipping-method" name="shipping_method"  value="{{ $shipping->id }}"
               {!! !empty($active) ? 'checked="true"' : '' !!} data-type="{{ $shipping->type }}">
        <label for="shipping_method_{{  $shipping->id }}">
            <span>
                @if(!empty($shipping->icon))
                    <img src="{!! $shipping->icon->getFile() !!}" alt="">
                @endif
                {{ $shipping->title }}
            </span>
            <span class="float-right" id="shipping-cost-{{ $shipping->id }}">
                @php($price = $shipping->getPrice($currency))
                @if($price->price != 0)
                    {!! $price->getFormattedPrice() !!}
                @else
                    ZDARMA
                @endif
            </span>
            @if(!empty($shipping->shortDescription))
                <br>
                <span style="font-size: 0.6rem;font-weight: normal;">
                    {{ $shipping->shortDescription }}
                </span>
            @endif
        </label>
    @endforeach
</div>

{{-- Description --}}
@foreach($shippingMethods as $shipping)
    @php($active = ($loop->first && empty($selectedMethod) || $selectedMethod == $shipping->id) ? true : false)
    @if(!empty($shipping->description))
        <div class="shipping_desc mt-4 {{ (!$active ? 'd-none' : 'active') }}" id="desc_shipping_method_{{ $shipping->id }}">
            {!! nl2br($shipping->description) !!}
        </div>
    @endif
@endforeach

<input type="hidden" name="predicted_delivery">

@php(do_action('ecommerce_checkout_shipping_predicted_delivery'))

<script>
    $(document).ready(function() {
        $('.input-radio.shipping-method').change(function() {
            $('.shipping_desc').hide();
            $('#desc_shipping_method_' + $(this).val()).show();
        });
    });
</script>
