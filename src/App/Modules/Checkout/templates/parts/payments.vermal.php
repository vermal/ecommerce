<div class="methods payments">
    @foreach($paymentMethods as $paymentMethod)
        @php($active = ($loop->first && empty($selectedMethod) || $selectedMethod == $paymentMethod->id) ? true : false)
        <input class="payment-method input-radio" id="payment_method_{{ $paymentMethod->id }}"
                data-type="{{ $paymentMethod->id }}" type="radio"
                name="payment_method" value="{{ $paymentMethod->id }}"
                {{ ($active ? 'checked' : '') }}/>

        <label for="payment_method_{{ $paymentMethod->id }}">
            <span>
                @if(!empty($paymentMethod->icon))
                    <img src="{!! $paymentMethod->icon->getFile() !!}" alt="">
                @endif
                {{ $paymentMethod->title }}
            </span>
            <span class="float-right" id="payment-cost-{{ $paymentMethod->id }}">
                @php($price = $paymentMethod->getPrice($currency))
                @if($price->price != 0)
                    {!! $price->getFormattedPrice() !!}
                @else
                    ZDARMA
                @endif
            </span>
        </label>

        @if(!empty($paymentMethod->description))
            <div class="description {{ (!$active ? 'd-none' : '') }}"
                 data-id="payment_method_{{ $paymentMethod->id }}">
                {!! $paymentMethod->description !!}
            </div>
        @endif

    @endforeach
</div>


<script>
    $('.methods.payments input').change(function() {
        $('.methods.payments .description').addClass('d-none');
        $('.methods.payments input:checked').next().next().removeClass('d-none');
    });
</script>
