<div class="inner pb-0 text-center change-step-nav-wrapper">
    <div class="brand mb-3">
        @if(empty($checkoutHeaderImage))
            <h2>{{ $checkoutHeader }}</h2>
        @else
            <img class="logo mb-0" src="{{ $checkoutHeaderImage->getMiniFile() }}" alt="">
        @endif
    </div>
    <h2 class="thank-you-header text-left">
        <small>Objednávka #{!! $order->getInvoiceNumber($settings->invoice_number_zeros) !!}</small><br>
        Děkujeme!
        <img v:admin:ecommerce:Checkout:src="assets/img/check-circle.png" alt="">
    </h2>
</div>
