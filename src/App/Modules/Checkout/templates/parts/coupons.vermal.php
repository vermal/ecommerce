<tr class="field-r-summary-coupons">
	@if(!$cart->coupons->isEmpty())
		<td colspan="5">
			<strong>Slevové kupóny:</strong><br>
			@foreach($cart->coupons as $coupon)
				<div class="mb-1">
					{{ $coupon->code }}
					<a href="#" data-id="{{ $coupon->id }}"
					   class="btn btn-outline-secondary btn-icon remove-coupon-from-checkout float-right">
						<i class="fas fa-times"></i>
					</a>
				</div>
			@endforeach
		</td>
	@endif
</tr>
