<div class="step" id="step-1">

    <div class="mb-4">
        <span class="mb-2 d-block">@__(checkout.contactInfo)</span>
        <div class="row floating-label">
            @label('email')
            @control('email')
        </div>
    </div>

    <span class="mb-2 d-block">@__(checkout.billingInfo)</span>
    <div class="billing floating-label">
        <div class="row">
            @foreach($billing_fields as $field)
                @label("billing_{$field}")
                @control("billing_{$field}")
            @endforeach
        </div>
    </div>

    <div class="shipping floating-label mt-4" id="shipping-info" style="display: none;">
        <span class="mb-2 d-block">@__(checkout.shippingInfo)</span>
        <div class="row">
            @foreach($shipping_fields as $field)
                @label("shipping_{$field}")
                @control("shipping_{$field}")
            @endforeach
        </div>
    </div>

    <div class="row">
        @control("shipping_address_toggle")
    </div>

    <div class="row align-items-center">
        <div class="col-lg-5 order-last order-lg-first mt-4 mt-lg-0">
            <a href="{{ routerLink(appGet('ecommerce.shop-path', 'ecommerce.shop')) }}" class="btn btn-link btn-sm text-dark pl-0">
                <i class="fas fa-chevron-left mr-2"></i> @__(checkout.buttons.shopBack)
            </a>
        </div>
        <div class="col-lg-7 text-right">
            <button class="btn btn-success change-step disable-auto-ajax" data-step="2" name="action" value="shipping">
                @__(checkout.buttons.shipping)
            </button>
        </div>
    </div>

</div>
