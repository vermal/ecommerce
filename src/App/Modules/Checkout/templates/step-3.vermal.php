<div class="step" id="step-3" style="display: none">

    <strong class="mb-1 d-block">@__(checkout.paymentInfo)</strong>
    <div id="payments" class="field-payment-methods">
        {{--@include('parts.payments', ['paymentMethods' => $paymentMethods, 'currency' => $currency])--}}
        {!! $paymentMethods !!}
    </div>

    <div class="form-group mb-0 mt-4">
        <input type="hidden" name="order_obchodni_podminky" value="">
        <input type="checkbox" id="order_obchodni_podminky" name="order_obchodni_podminky" value="1" checked>
        <label for="order_obchodni_podminky">
            Souhlasím s <a href="{{ $documents['TERMS_AND_CODITIONS'] }}" target="_blank">obchodními podmínkami</a>
            <span class="checkmark"></span>
        </label>
    </div>

    <div class="form-group mt-0">
        <input type="hidden" name="order_zivy_produkt" value="">
        <input type="checkbox" id="order_zivy_produkt" name="order_zivy_produkt" value="1" checked>
        <label for="order_zivy_produkt">
            Rozumím, že MAGU KOMBUCHA je živý produkt, který musí být skladován v lednici.
            <span class="checkmark"></span>
        </label>
    </div>

    <div class="row align-items-center mt-5">
        <div class="col-lg-5 order-last order-lg-first mt-4 mt-lg-0">
            <a class="back-btn change-step" href="#step-2" data-step="2">
                <i class="fas fa-chevron-left mr-2"></i> @__(checkout.buttons.shippingBack)
            </a>
        </div>
        <div class="col-lg-7 text-right">
            <button class="btn btn-success" name="action" value="make-order">
                @__(checkout.buttons.finish)
            </button>
        </div>
    </div>

</div>
