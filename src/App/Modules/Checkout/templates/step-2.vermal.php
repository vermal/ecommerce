<div class="step" id="step-2" style="display: none">

    <strong class="mb-1 d-block">@__(checkout.deliveryInfo)</strong>
    <div id="shippings" class="field-shipping-methods">
        {{--@include('parts.shippings', ['shippingMethods' => $shippingMethods, 'currency' => $currency])--}}
        {!! $shippingMethods !!}
    </div>

    <div class="row align-items-center mt-5">
        <div class="col-lg-5 order-last order-lg-first mt-4 mt-lg-0">
            <a href="#step-1" class="btn btn-link btn-sm text-dark pl-0 change-step" data-step="1">
                <i class="fas fa-chevron-left mr-2"></i>
                @__(checkout.buttons.infoBack)
            </a>
        </div>
        <div class="col-lg-7 text-right">
            <button class="btn btn-success change-step disable-auto-ajax" data-step="3" name="action" value="payment">
                @__(checkout.buttons.payment)
            </button>
        </div>
    </div>

</div>
