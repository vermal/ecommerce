<table class="table" id="cart-items" style="table-layout: fixed">

    @foreach($cart->items as $item)
        <tr class="product-item">
            <td>
                <div class="image">
                    @if(!empty($item->product->simpleMultimedia))
                        <img src="{{ $item->product->getSimpleMultimedia()->getFile() }}" alt="">
                    @endif
                </div>
            </td>
            <td class="text-center">
                {{ $item->product->getTitle() }}
            </td>
            <td class="text-center p-0 cart-qty-handler">
                <a href="#" class="btn btn-outline-secondary btn-icon" data-operation="substract">
                    <i class="fas fa-minus"></i>
                </a>
                <span class="cart-item-value" data-id="{{ $item->id }}" data-qty="{{ $item->qty }}">
                    {{ $item->qty }}
                </span>
                <a href="#" class="btn btn-outline-secondary btn-icon" data-operation="add">
                    <i class="fas fa-plus"></i>
                </a>
            </td>
            <td class="text-right font-weight-bold pl-0 pr-1">
                <div>
                    {{ $item->product->getPrice($currency)->getFormattedPrice() }}
                </div>
            </td>
            <td class="text-right" style="width: 50px">
                <a href="#" class="remove-from-cart btn btn-outline-secondary btn-icon" data-id="{{ $item->id }}">
                    <i class="fas fa-times"></i>
                </a>
            </td>
        </tr>
    @endforeach

    <tfoot>
        <tr>
            <td colspan="5">
                <div id="coupon">
                    <div class="form-group">
                        <div class="d-flex">
                            <input type="text" name="coupon" class="form-control" placeholder="@__(checkout.items.coupon)">
                            <button class="btn btn-outline-secondary disable-auto-ajax ml-2">@__(checkout.buttons.addCoupon)</button>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        <tr class="small-padding">
            <td colspan="3">
                @__(checkout.items.subtotal)<br>
            </td>
            <th class="text-right field-price-subtotal" id="price-subtotal" colspan="2">
                {{ $cart->getFormattedPrice('price_items') }}
            </th>
        </tr>
        <tr class="small-padding">
            <td colspan="3">
                @__(checkout.items.discount)<br>
            </td>
            <th class="text-right field-price-discount" id="price-payment" colspan="2">
                {{ (!empty($cart->discount) ? $cart->getFormattedPrice('discount') : '-')  }}
            </th>
        </tr>
        @include('parts.coupons', ['cart' => $cart])
        <tr class="small-padding">
            <td colspan="3">
                @__(checkout.items.shipping)<br>
            </td>
            <th class="text-right field-price-shipping" id="price-shipping" colspan="2">
                -
            </th>
        </tr>
        <tr class="small-padding">
            <td colspan="3">
                @__(checkout.items.payment)<br>
            </td>
            <th class="text-right field-price-payment" id="price-payment" colspan="2">
                -
            </th>
        </tr>
        <tr class="bigger">
            <th colspan="3">
                @__(checkout.items.total)<br>
                {{-- Coupon --}}
                {{-- TAX --}}
            </th>
            <th class="text-right field-price-total" id="price-total" colspan="2">
                {{ $cart->getFormattedPrice('price') }}
            </th>
        </tr>
    </tfoot>

</table>
