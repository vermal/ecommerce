<table class="table" id="cart-items" style="table-layout: fixed">

    @foreach($order->items as $item)
        <tr class="product-item">
            <td>
                <div class="image">
                    @if(!empty($item->product->simpleMultimedia))
                        <img src="{{ $item->getSimpleMultimedia()->getFile() }}" alt="">
                    @endif
                </div>
            </td>
            <td class="text-center" colspan="2">
                {{ $item->getTitle() }}
            </td>
            <td class="text-right font-weight-bold pl-0 pr-1" colspan="2">
                <div>
                    {{ $item->getFormattedPrice('price') }}
                </div>
            </td>
        </tr>
    @endforeach

    <tfoot>
        <tr>
            <td colspan="5"></td>
        </tr>
        <tr class="small-padding">
            <td colspan="3">
                Mezisoučet<br>
            </td>
            <th class="text-right field-price-subtotal" id="price-subtotal" colspan="2">
                {{ $order->formatPrice($order->price_items, true, true) }}
            </th>
        </tr>
        <tr class="small-padding">
            <td colspan="3">
                Doprava<br>
            </td>
            <th class="text-right field-price-shipping" id="price-shipping" colspan="2">
                {{ $order->formatPrice($order->shipping_amount, true, true) }}
            </th>
        </tr>
        <tr class="small-padding">
            <td colspan="3">
                Platba<br>
            </td>
            <th class="text-right field-price-payment" id="price-payment" colspan="2">
                {{ $order->formatPrice($order->payment_amount, true, true) }}
            </th>
        </tr>
        <tr class="bigger">
            <th colspan="3">
                Celkem k úhradě<br>
                {{-- Coupon --}}
                {{-- TAX --}}
            </th>
            <th class="text-right field-price-total" id="price-total" colspan="2">
                {{ $order->formatPrice($order->price, true, true) }}
            </th>
        </tr>
    </tfoot>

</table>
