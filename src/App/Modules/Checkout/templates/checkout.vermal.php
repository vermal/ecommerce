@extends('layout')

@section("main")

    <div id="loader">
        <img v:admin:ecommerce:Checkout:src="assets/img/spinner.svg" alt="">
    </div>
    <div class="container-fluid" id="checkout">
        @form(checkout)
            <div class="col-12">
                <div class="container-fluid px-0">
                    <div class="row">
                        <div class="col-lg-7 d-lg-none">
                            @include('parts.nav')
                        </div>
                        <div class="col-lg-7" id="form">
                            <div class="d-lg-block d-none">
                                @include('parts.nav')
                            </div>
                            <div class="inner personal-data" id="steps">
                                @include('step-1')
                                @include('step-2', ['shippingMethods' => $shippingMethods])
                                @include('step-3', ['paymentMethods' => $paymentMethods])
                                <div class="footer mt-5">
                                    <a href="{{ $documents['RETURNS'] }}">@__(checkout.documents.refund)</a>
                                    <a href="{{ $documents['PRIVACY_POLICY'] }}">@__(checkout.documents.privacyPolicy)</a>
                                    <a href="{{ $documents['TERMS_AND_CODITIONS'] }}">@__(checkout.documents.terms)</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5" id="items">
                            <div class="d-lg-none" id="items-overview-button">
                                <div class="icon-wrapper">
                                    <svg width="14px" height="14px" viewBox="0 0 14 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                        <g id="Symbolss_" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <g id="shopping-cartt" transform="translate(-5.000000, -6.000000)" fill="#000000" fill-rule="nonzero">
                                                <g transform="translate(5.000000, 6.000000)" id="Shape">
                                                    <path d="M3.98888742,9.20978645 L12.2745386,9.20978645 C13.214053,9.20978645 13.9804989,8.4968501 13.9804989,7.62292813 L13.9804989,4.38021766 C13.9804989,4.37734292 13.9804989,4.37159343 13.9804989,4.36871869 C13.9804989,4.36009446 13.9804989,4.35434497 13.9804989,4.34572074 C13.9804989,4.33997125 13.9804989,4.33422177 13.9774084,4.32847228 C13.9774084,4.32272279 13.9743179,4.31409856 13.9743179,4.30834908 C13.9743179,4.30259959 13.9712274,4.2968501 13.9712274,4.29110062 C13.9681369,4.28535113 13.9681369,4.27960164 13.9650464,4.27097741 C13.9619558,4.26522793 13.9619558,4.25947844 13.9588653,4.25372895 C13.9557748,4.24797947 13.9557748,4.24222998 13.9526843,4.23648049 C13.9495938,4.23073101 13.9465033,4.22498152 13.9434128,4.21635729 C13.9403223,4.2106078 13.9372318,4.20485832 13.9341413,4.20198357 C13.9310508,4.19623409 13.9279603,4.1904846 13.9248698,4.18473511 C13.9217792,4.17898563 13.9186887,4.17611088 13.9155982,4.1703614 C13.9125077,4.16461191 13.9063267,4.15886242 13.9032362,4.15311294 C13.9001457,4.14736345 13.8970552,4.14448871 13.8908742,4.13873922 C13.8877837,4.13298973 13.8816026,4.13011499 13.8785121,4.1243655 C13.8754216,4.11861602 13.8692406,4.11574127 13.8661501,4.10999179 C13.8630596,4.1042423 13.8568786,4.10136756 13.8537881,4.09849281 C13.8476071,4.09274333 13.8445166,4.08986858 13.8383355,4.0841191 C13.8321545,4.08124435 13.829064,4.07549487 13.822883,4.07262012 C13.816702,4.06974538 13.810521,4.06399589 13.80434,4.06112115 C13.7981589,4.05824641 13.7950684,4.05537166 13.7888874,4.05249692 C13.7827064,4.04962218 13.7765254,4.04674743 13.7703444,4.04099795 C13.7641634,4.0381232 13.7579823,4.03524846 13.7518013,4.03237372 C13.7456203,4.02949897 13.7394393,4.02662423 13.7332583,4.02374949 C13.7270773,4.02087474 13.7208962,4.018 13.7147152,4.01512526 C13.7085342,4.01225051 13.7023532,4.01225051 13.6961722,4.00937577 C13.6899912,4.00650103 13.6807196,4.00362628 13.6745386,4.00362628 C13.6683576,4.00362628 13.6621766,4.00075154 13.6590861,4.00075154 C13.6498146,3.9978768 13.6436336,3.9978768 13.634362,3.9978768 C13.6312715,3.9978768 13.628181,3.99500205 13.622,3.99500205 L3.12045475,2.64674743 L3.12045475,1.2841191 C3.12045475,1.26974538 3.12045475,1.25537166 3.11736424,1.24387269 C3.11736424,1.24099795 3.11736424,1.2381232 3.11427373,1.23237372 C3.11427373,1.22374949 3.11118322,1.21512526 3.11118322,1.20650103 C3.10809272,1.1978768 3.10809272,1.19212731 3.10500221,1.18350308 C3.10500221,1.17775359 3.1019117,1.17487885 3.1019117,1.16912936 C3.09882119,1.16050513 3.09573068,1.1518809 3.09264018,1.14325667 C3.09264018,1.14038193 3.08954967,1.13463244 3.08954967,1.1317577 C3.08645916,1.12313347 3.08336865,1.11738398 3.07718764,1.10875975 C3.07409713,1.10588501 3.07409713,1.10013552 3.07100662,1.09726078 C3.06791611,1.09151129 3.06482561,1.08576181 3.05864459,1.08001232 C3.05555408,1.07426283 3.05246358,1.07138809 3.04937307,1.0656386 C3.04628256,1.05988912 3.04319205,1.05701437 3.04010155,1.05126489 C3.03701104,1.0455154 3.03083002,1.03976591 3.02773951,1.03401643 C3.02464901,1.03114168 3.0215585,1.02826694 3.01846799,1.0253922 C3.01228698,1.01964271 3.00610596,1.01389322 2.99992494,1.00814374 C2.99683444,1.00526899 2.99374393,1.00239425 2.99065342,0.999519507 C2.98447241,0.993770021 2.97829139,0.988020534 2.96901987,0.982271047 C2.96592936,0.979396304 2.95974834,0.976521561 2.95665784,0.973646817 C2.95047682,0.967897331 2.94429581,0.965022587 2.93811479,0.959273101 C2.92884327,0.953523614 2.91957174,0.947774127 2.91339073,0.944899384 C2.91030022,0.942024641 2.90720971,0.942024641 2.90411921,0.939149897 C2.89175717,0.933400411 2.87630464,0.927650924 2.8639426,0.921901437 L0.586238411,0.0307310062 C0.372993377,-0.0526365503 0.128843267,0.0393552361 0.039218543,0.237712526 C-0.050406181,0.436069815 0.0484900662,0.663174538 0.261735099,0.746542094 L2.28292715,1.53997125 L2.28292715,3.29931417 L2.28292715,3.57816427 L2.28292715,5.71697331 L2.28292715,7.62867762 L2.28292715,10.043462 C2.28292715,10.8483901 2.93193377,11.5153306 3.76946137,11.6159466 C3.61802649,11.8516756 3.52840177,12.1276509 3.52840177,12.4208747 C3.52840177,13.2861725 4.28557616,13.9876099 5.21272848,13.9876099 C6.13988079,13.9876099 6.89705519,13.2832977 6.89705519,12.4208747 C6.89705519,12.1334004 6.81361148,11.8602998 6.66526711,11.6303203 L10.4171435,11.6303203 C10.2687991,11.8631745 10.1853554,12.1334004 10.1853554,12.4208747 C10.1853554,13.2861725 10.9425298,13.9876099 11.8696821,13.9876099 C12.7968344,13.9876099 13.5540088,13.2832977 13.5540088,12.4208747 C13.5540088,11.5584517 12.7968344,10.8541396 11.8696821,10.8541396 L3.98888742,10.8541396 C3.50676821,10.8541396 3.11736424,10.4890472 3.11736424,10.043462 L3.11736424,8.99130595 C3.37078587,9.12929363 3.67056512,9.20978645 3.98888742,9.20978645 Z M6.06570861,12.418 C6.06570861,12.854961 5.68248565,13.2085544 5.21581898,13.2085544 C4.74915232,13.2085544 4.36592936,12.8520862 4.36592936,12.418 C4.36592936,11.9839138 4.74915232,11.6274456 5.21581898,11.6274456 C5.68248565,11.6274456 6.06570861,11.981039 6.06570861,12.418 Z M12.7226623,12.418 C12.7226623,12.854961 12.3394393,13.2085544 11.8727726,13.2085544 C11.406106,13.2085544 11.022883,12.8520862 11.022883,12.418 C11.022883,11.9839138 11.406106,11.6274456 11.8727726,11.6274456 C12.3394393,11.6274456 12.7226623,11.981039 12.7226623,12.418 Z M12.2745386,8.43360575 L3.98888742,8.43360575 C3.50676821,8.43360575 3.11736424,8.06851335 3.11736424,7.62292813 L3.11736424,5.71122382 L3.11736424,3.57241478 L3.11736424,3.42867762 L13.1460618,4.71368789 L13.1460618,7.62005339 C13.1460618,8.07138809 12.7535673,8.43360575 12.2745386,8.43360575 Z"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </svg>
                                </div>
                                <div class="item show">
                                    @__(checkout.showDetails)
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                                <div class="item hide d-none">
                                    @__(checkout.hideDetails)
                                    <i class="fas fa-chevron-up"></i>
                                </div>
                                <div class="price">
                                    <strong id="price-total2" class="field-price-total">{{ $cart->getFormattedPrice('price') }}</strong>
                                </div>
                            </div>
                            <div class="inner">
                                @include('items', ['cart' => $cart, 'currency' => $currency])
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endform
    </div>

    <script>
        $(document).ready(function() {

            $('#items .show').click(function(e) {
                e.preventDefault();
                $(this).addClass('d-none');
                $(this).next().removeClass('d-none');
                $('#items .inner').slideDown(100);
            });
            $('#items .hide').click(function(e) {
                e.preventDefault();
                $(this).addClass('d-none');
                $(this).prev().removeClass('d-none');
                $('#items .inner').slideUp(100);
            });

            let formObj = $('#checkout form');
            let loader = $('#loader');

            /**
             * Default init handler for ajax forms
             */
            formObj.on('start', function(item, data) {
                let btn = data.button;
                vermalButton(btn, 'disable', true);
            });

            /**
             * Default success handler for ajax forms
             */
            formObj.on('success', function(item, data) {
                let response = data.response;
                try {
                    let obj = JSON.parse(response);
                    if (typeof obj.location !== "undefined") {
                        window.location.href = obj.location;
                    }
                } catch (e) {}
            });

            // Switch step
            $('.change-step').click(function(e) {
                e.preventDefault();
                let steps = $('#steps');
                let stepsNav = $('.change-step-nav');
                let newStep = $(this).data('step');

                // Validate current step
                let activeStepId = stepsNav.find('.active').data('step');
                let activeStep = steps.find('#step-' + activeStepId);
                let hasError = false;
                if (formObj.hasClass('validate')) {
                    activeStep.find('.form-control, .custom-control-input.custom-control-input-switch').each(function() {
                        let hasError_ = vermalValidateControl($(this).closest('.form-group'), {});
                        if (hasError_) hasError = true;
                    });
                }
                if (hasError) return;

                // Do ajax
                ajax(formObj.attr('action'), { form: formObj.serialize(), action: 'change-step-' + newStep });

                // Change step
                steps.find('.step').hide();
                steps.find('#step-' + newStep).show();
                stepsNav.find('.active').removeClass('active');
                stepsNav.find('a[href="#step-' + newStep + '"]').addClass('active');
            });

            $(document).on('change', '.shipping .shipping-method', function() {
                ajax(formObj.attr('action'), { form: formObj.serialize(), action: 'change-shipping-method' });
            });

            $(document).on('change', '.payments .payment-method', function() {
                ajax(formObj.attr('action'), { form: formObj.serialize(), action: 'change-payment-method' });
            });

            // Cart qty handle
            $('.product-item .cart-qty-handler a').click(function(e) {
                e.preventDefault();
                let operation = 'substract';
                if ($(this).data('operation') === 'add')
                    operation = 'add';
                let item = $(this).parent().find('.cart-item-value');
                let value = item.data('qty');
                let id = item.data('id');
                if (operation === 'add')
                    value++;
                else {
                    // Substract only if value is more than 1
                    if (value === 1) return;
                    value--;
                }
                item.text(value);
                item.data('qty', value);

                // Change qty in cart
                ajax('{{ routerLink('ecommerce.cart.change-qty') }}', { id: id, qty: value });
                $('.change-step-nav .active').click();
            });

            // Remove item
            $('.product-item .remove-from-cart').click(function(e) {
                e.preventDefault();
                let dis = $(this);
                ajax('{{ routerLink('ecommerce.cart.remove') }}', { id: dis.data('id') }, function() {
                    dis.closest('.product-item').remove();
                });
            });

            // Add coupon
            $('#coupon button').click(function(e) {
                e.preventDefault();
                let input = $(this).parent().find('input');
                let val = input.val();
                ajax('{{ routerLink('ecommerce.cart.add-coupon') }}', { code: val }, function() {
                    input.val('');
                });
            });

            $(document).on('click', '.remove-coupon-from-checkout', function(e) {
                e.preventDefault();
                ajax('{{ routerLink('ecommerce.cart.remove-coupon') }}', { id: $(this).data('id') });
            });

            let ajaxRequest;
            function ajax(url, data, callback) {
                loader.fadeIn(150);
                if (typeof ajaxRequest !== "undefined")
                    ajaxRequest.abort();
                ajaxRequest = $.ajax({
                    url: url,
                    method: 'POST',
                    data: data,
                    success: function(resp) {
                        try {
                            let obj = JSON.parse(resp);
                            updateFields(obj.fields);
                            if (typeof callback !== "undefined")
                                callback(obj);
                        } catch (e) {
                            if (typeof callback !== "undefined")
                                callback(resp);
                        }
                        loader.fadeOut(150);
                    }
                });
            }

            /**
             * Update fields
             *
             * @param fields
             */
            function updateFields(fields) {
                $.each(fields, function(key, value) {
                    $('.field-' + key).html(value);
                    $('.field-r-' + key).replaceWith(value);
                });
            }

        });
    </script>

@endsection

@section('head')
    @if(!empty(appGet('hereMapsJSApi')))
        <script src="https://js.api.here.com/v3/3.1/mapsjs-core.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://js.api.here.com/v3/3.1/mapsjs-service.js" type="text/javascript" charset="utf-8"></script>
        <script src="https://js.api.here.com/v3/3.1/mapsjs-ui.js" type="text/javascript" charset="utf-8"></script>
        <script type="text/javascript" src="https://js.api.here.com/v3/3.1/mapsjs-mapevents.js"></script>
        <link rel="stylesheet" type="text/css" href="https://js.api.here.com/v3/3.1/mapsjs-ui.css" />
        <script>
            var platform = new H.service.Platform({ 'apikey': '{{ appGet('hereMapsJSApi') }}' });
            var service = platform.getSearchService();

            $(document).ready(function() {
                var timeout;
                let enabledCountryCodes = {!! json_encode(appGet('hereMapsJSEnabledCountryCodes', []), JSON_PRETTY_PRINT) !!};
                $('.here-maps-suggest').keyup(function() {
                    if (typeof timeout !== "undefined") clearTimeout(timeout);
                    var value = $(this).val();
                    var autocomplete = $(this).parent().find('.autocomplete');
                    $(this).addClass('autocomplete-content');
                    if (autocomplete.length < 1) {
                        $(this).parent().append($('<div>').addClass('autocomplete'));
                        autocomplete = $(this).parent().find('.autocomplete');
                    }
                    timeout = setTimeout(function() {
                        service.geocode({
                            q: value
                        }, function(result) {
                            autocomplete.html('');
                            autocomplete.show();
                            result.items.forEach(function(item) {
                                var houseNumber = typeof item.address.houseNumber !== "undefined" ? ' ' + item.address.houseNumber : '';
                                if (enabledCountryCodes.indexOf(item.address.countryCode) === -1) {
                                    return;
                                }
                                if (typeof item.address.street === "undefined") {
                                    item.address.street = '';
                                }
                                autocomplete.append(
                                        $('<div>').addClass('item')
                                            .html(item.title)
                                            .data('street', item.address.street + houseNumber)
                                            .data('city', item.address.city)
                                            .data('zip', item.address.postalCode)
                                            .data('country', item.address.countryName)
                                )
                            });
                        })
                    }, 500);
                });

                // On select item
                $(document).on('click', '.autocomplete .item', function() {
                    let wrapper = $(this).closest('.row').parent();
                    if (wrapper.hasClass('billing')) {
                        wrapper.find('input[name="billing_city"]').val($(this).data('city'));
                        wrapper.find('input[name="billing_address"]').val($(this).data('street'));
                        wrapper.find('input[name="billing_postcode"]').val($(this).data('zip'));
                        wrapper.find('input[name="billing_country"]').val($(this).data('country'));
                    } else {
                        wrapper.find('input[name="shipping_city"]').val($(this).data('city'));
                        wrapper.find('input[name="shipping_address"]').val($(this).data('street'));
                        wrapper.find('input[name="shipping_postcode"]').val($(this).data('zip'));
                        wrapper.find('input[name="shipping_country"]').val($(this).data('country'));
                    }
                    $(this).parent().hide();
                    $(this).closest('.form-group').find('input').removeClass('autocomplete-content');
                });
            });

        </script>
    @endif

@endsection
