<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Checkout - {{ $settings->checkout_brand_title }}</title>

	<link rel="stylesheet" v:admin:ecommerce:Checkout:href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" integrity="sha256-h20CPZ0QyXlBuAw7A+KluUYx/3pK+c7lYEpqLTlxjYQ=" crossorigin="anonymous" />
	<link rel="stylesheet" v:admin:ecommerce:Checkout:href="assets/css/checkout.css">

	<script v:admin:ecommerce:Checkout:src="assets/js/jquery.min.js"></script>

	@yield('head')

	@php(do_action('ecommerce_checkout_head'))

</head>
<body>

	@yield('main')

	<script v:admin:ecommerce:Checkout:src="assets/js/popper.min.js"></script>
	<script v:admin:ecommerce:Checkout:src="assets/js/bootstrap.min.js"></script>
	<script v:vermal:src="js/vermal.js"></script>

	@php(do_action('ecommerce_checkout_footer'))

</body>
</html>
