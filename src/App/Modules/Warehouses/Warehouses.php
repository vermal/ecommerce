<?php

namespace Vermal\Ecommerce\Modules\Warehouses;

use PHPMailer\PHPMailer\Exception;
use Vermal\Admin\Upload;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Expeditions\ExpeditionsModel;
use Vermal\Ecommerce\Modules\Warehouses\Entities\Warehouse;
use Vermal\Form\Form;
use Vermal\Router;

class Warehouses extends Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('warehouses', $this->CRUDAction);
        $this->addToBreadcrumb('warehouse', 'admin.warehouse.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    	$paymentMethods = Database::Model('PaymentMethod')->setPair('id', 'title')->get();
		$shippingMethods = Database::Model('ShippingMethod')->setPair('id', 'title')->get();

        $this->setDefaultViewPath();

        $data = Database::Model('Warehouse');
        $datagrid = new DataGrid($data);

		$datagrid->addColumn('created_at', $this->_('warehouse.date'))->setRenderer(function ($entity) {
			return $entity->getFormattedCreatedAt();
		});


        // Add actions
		$datagrid->addAction('invoice', ['admin.warehouse.invoice', ['id']], 'fas fa-file-invoice', '', 'btn btn-sm btn-info');
        $datagrid->addEdit(['admin.warehouse.edit', ['id']]);
        $datagrid->addDelete(['admin.warehouse.destroy_', ['id']]);

		$datagrid->addAction('payed1', ['admin.warehouse.payed', ['id']], 'fas fa-toggle-off', 'Nezaplatená',
			'btn btn-danger btn-sm', [['payed', '=', 0]]);
		$datagrid->addAction('payed0', ['admin.warehouse.payed', ['id']], 'fas fa-toggle-on', 'Zaplatená',
			'btn btn-success btn-sm', [['payed', '=', 1]]);

        $datagrid->addSearch('id', $this->_('warehouse.id'), 'col-lg-1');
		$datagrid->addFilter('customer', [null => $this->_('warehouse.customer')], $this->_('warehouse.customer'),
			'col-lg-3', 'js-select2 search', ['data-controller' => self::class, 'data-method' => 'searchCustomer']);
		$datagrid->addFilter('status', $this->warehouseStatuses, $this->_('warehouse.status'), 'col-lg-1');
		$datagrid->addFilter('payment', $paymentMethods, $this->_('warehouse.payment'), 'col-lg-2');
		$datagrid->addFilter('shipping', $shippingMethods, $this->_('warehouse.shipping'), 'col-lg-2');
		$datagrid->addInputFilter('created_at', 'daterange', $this->_('warehouse.date'), 'col-lg-2', 'daterange');

		$datagrid->addBulkAction('createExpedition', 'Expedovať objednávky', [self::class, 'createExpedition']);
		foreach ($this->warehouseStatuses as $key => $status) {
			$datagrid->addBulkAction('changeStatus' . ucfirst($key), 'Zmena stavu: ' . $status, [self::class, 'changeStatus' . ucfirst($key)]);
		}

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        $this->getPersistedItems();
        View::view('warehouse', [
            "form" => $form->build(),
            "formName" => $form->getName()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
		$form = $this->form();
		if ($form->hasError()) {
			$this->persistItems($post);
			Router::redirect('admin.warehouse.create');
		}

		// Set post items ids
		$post->items = $this->handleItems($post);
		$post->currency = 'EUR';

		(new WarehouseModel((array)$post, $form))->store();
		Router::redirect('admin.warehouse.index');
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 */
    public function edit($id)
    {
		$form = $this->form();
		$form->setAjax(true);
		$form->setMethod('PUT');
		$form->setAction(routerLink('admin.warehouse.update', ['id' => $id]));
		$this->getPersistedItems();

		/** @var Warehouse $warehouse */
		$warehouse = Database::Model('Warehouse')->find($id);

		$form->setValues($warehouse, ['items', 'email', 'billing', 'shipping', 'shippingMethod', 'paymentMethod', 'customer']);

		$form->getComponent('customer')->setSelect([
			$warehouse->customer->id => $warehouse->customer->name . ' ' . $warehouse->customer->surname
		])->setValue($warehouse->customer->id);
		$form->getComponent('shippingMethod')->setValue($warehouse->shipping->id);
		$form->getComponent('paymentMethod')->setValue($warehouse->payment->id);
		$form->getComponent('email')
			->addAttribute('disabled', 'disabled')
			->setValue($warehouse->customer->email);

		$items = [];
		foreach ($warehouse->items as $item) {
			$items[] = [
				'id' => !empty($item->product) ? $item->product->id : "",
				'title' => $item->title,
				'qty' => $item->qty,
				'price' => $item->price_per_unit,
				'currency' => $item->currency
			];
		}
		View::setVariable('warehouseItems', $items);

		View::view('warehouse', [
			"form" => $form->build(),
			"formName" => $form->getName(),
			"warehouse" => $warehouse
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
		$form = $this->form();
		if ($form->hasError()) {
			$this->alert($this->_('error.form.empty'), 'danger');
			die();
		}

		// Set post items ids
		$post->items = $this->handleItems($post);
		$post->currency = 'EUR';

		$warehouseModel = new WarehouseModel((array)$post, $form);
		$warehouse = $warehouseModel->save((int)$id);
		if (!$warehouse) {
			foreach ($warehouseModel->form->getErrors() as $error) {
				$this->alert($this->_($error['msg']), $error['type']);
				break;
			}
		} else {
			$this->alert($this->_('warehouse.edited'));
		}
    }

	/**
	 * Send new warehouse email
	 *
	 * @param $warehouse
	 * @param $settings
	 *
	 * @throws Exception
	 * @throws \Mpdf\MpdfException
	 * @throws \Whoops\Exception\ErrorException
	 */
	public static function sendNewWarehouseEmail($warehouse, $settings)
	{
		$mailer = self::mailer($settings);

		// Subject
		$mailer->subject('Nová objednávka');

		// Create template
		$template = apply_filters('ecommerce_warehouse_email_new_warehouse', '', $warehouse);
		if (empty($template)){
			$oldViewPath = View::getViewPath();
			View::setViewPath(__DIR__ . '/templates/');
			$template = \Vermal\View::view('email.new-warehouse', ['warehouse' => $warehouse], true);
			// Set view path back to original
			View::setViewPath($oldViewPath);
		}

		// Setup
		$mailer->body($template);
		$mailer->addAddress($warehouse->customer->email, $warehouse->customer->name . ' ' . $warehouse->customer->surname);

		// Generate invoice if it doesn't exist as file
		if (!is_file(self::INVOICE_DIR . 'magu-warehouse-' . $warehouse->invoice->number . '.pdf')) {
			self::generateInvoice($warehouse->id, $settings, true);
		}
		// Add invoice
		$mailer->addAttachment(self::INVOICE_DIR . 'magu-warehouse-' . $warehouse->invoice->number . '.pdf', 'magu-warehouse-' . $warehouse->invoice->number . '.pdf');

		// Send
		$mailer->send();
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
		$warehouse = Database::Model('Warehouse')->find($id);
		$warehouse->softDelete = true;
		Database::saveAndFlush($warehouse);
//    	$warehouse = Database::Model('Warehouse')->find($id);
//    	$warehouse->clearItems();
//    	Database::saveAndFlush($warehouse);
//		Database::Model('Warehouse')->delete($id);
//
//		// Redirect
		$this->alert($this->_('warehouse.deleted') .
			'. <a class="btn btn-primary btn-sm ml-3" href="' . routerLink('admin.warehouse.renew', ['id' => $id]) . '">'
			. $this->_('warehouse.renew') . '</a>');
		Router::redirect('admin.warehouse.index');
    }

	/**
	 * @param $id
	 */
	public function rendewWarehouse($id)
	{
		$warehouse = Database::Model('Warehouse')->find($id);
		$warehouse->softDelete = false;
		Database::saveAndFlush($warehouse);

		$this->alert($this->_('warehouse.renewed'));
		Router::redirect('admin.warehouse.index');
    }

	/**
	 * Create array of items from post object
	 *
	 * @param $post
	 * @return array
	 */
    private function handleItems($post)
	{
		$items = [];
		if (!empty($post->item)) {
			foreach ($post->item->qty as $key => $qty) {
				$items[] = [
					'qty' => $qty,
					'id' => $post->item->id->{$key},
					'title' => $post->item->title->{$key},
					'price' => $post->item->price->{$key},
					'currency' => $post->item->currency->{$key},
				];
			}
		}
		return $items;
	}

	/**
	 * Persist items in session
	 *
	 * @param $post
	 */
    private function persistItems($post)
	{
		$_SESSION['warehousePersistedItems'] = $this->handleItems($post);
	}

	/**
	 * Get items from session
	 */
	private function getPersistedItems()
	{
		$items = [];
		if (!empty($_SESSION['warehousePersistedItems'])) {
			$items = $_SESSION['warehousePersistedItems'];
			unset($_SESSION['warehousePersistedItems']);
		}
		View::setVariable('warehouseItems', $items);
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('warehouse', routerLink('admin.warehouse.store'));

        // Customer
        $form->addSelect('customer', ['guest' => 'Guest'], $this->_('warehouse.customer'));
        $form->addEmail('email', 'E-Mail')->setParentClass('customer-email');

        $form->addSelect('status', $this->warehouseStatuses, $this->_('warehouse.status'));

        // Billing/Shipping data
        $GLOBALS['billing_fields'] = ['name', 'surname', 'address', 'city', 'postcode', 'country', 'phone', 'company', 'ico', 'dic', 'ic_dph'];
        $GLOBALS['shipping_fields'] = ['name', 'surname', 'address', 'city', 'postcode', 'country'];
        for ($i = 0; $i < 2; $i++) {
            if ($i == 0) $type = 'billing';
            else $type = 'shipping';

            $form->addText($type . '_name', $this->_('customer.name'))->setCol('col-md-6')->required();
            $form->addText($type . '_surname', $this->_('customer.surname'))->setCol('col-md-6')->required();
            $form->addText($type . '_address', $this->_('address.address'))->required();
            $form->addText($type . '_city', $this->_('address.city'))->setCol('col-md-8')->required();
            $form->addText($type . '_postcode', $this->_('address.psc'))->setCol('col-md-4')->required();
            $form->addText($type . '_country', $this->_('address.country'))->required();
        }
		$form->addText('billing_company', $this->_('address.companyName'));
		$form->addText('billing_ico', $this->_('address.ico'))->setCol('col-lg-4');
		$form->addText('billing_dic', $this->_('address.dic'))->setCol('col-lg-4');
		$form->addText('billing_ic_dph', $this->_('address.ic_dph'))->setCol('col-lg-4');
        $form->addText('billing_phone', $this->_('address.phone'))->required();
        $form->addSelect('billing', ['new' => 'New'], $this->_('warehouse.billing_address'));
        $form->addSelect('shipping', ['new' => 'New'], $this->_('warehouse.shipping_address'));

        // Items
        $form->addSelect('items', ['0' => $this->_('warehouse.selectItem')], $this->_('warehouse.items'));

        // Shipping methods
	    $form->addSelect('shippingMethod',
		    Database::Model('ShippingMethod')->setPair('id', 'title')->get(), $this->_('warehouse.shipping'))
	        ->setClass('js-select2')
			->required();

		// Shipping methods
		$form->addSelect('paymentMethod',
			Database::Model('PaymentMethod')->setPair('id', 'title')->get(), $this->_('warehouse.payment'))
			->setClass('js-select2')
			->required();

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))
			->setParentClass('mb-0')
			->setClass('btn btn-success btn-block');

        return $form;
    }
}
