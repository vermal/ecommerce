<table>
	<tr>
		<td>
			<table>
				<tr>
					<td>{{ $settings->invoice_phone }}</td>
					<td>{{ $settings->invoice_email }}</td>
				</tr>
				<tr>
					<td>@__(invoice.issued_by) {{ $settings->invoice_company }}</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
					<td style="text-align: right">{{ $settings->invoice_note }}</td>
				</tr>
				<tr>
					<td style="text-align: right">
						@__(invoice.page): <strong>{PAGENO} / {nb}</strong>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
