<!-- HEADER -->
<table style="font-family: 'Open Sans', sans-serif">
	<tr>
		<td style="width: 58%">

		</td>
		<td>
			<table style="border: 1px solid #eee; padding: 5px 10px">
				<tr>
					<td style="text-align: center">@__(invoice.title)</td>
				</tr>
				<tr>
					<th style="text-align: center">{{ $order->getInvoiceNumber($settings->invoice_number_zeros) }}</th>
				</tr>
			</table>
		</td>
		<th style="text-align: right">
			@if(!empty($settings->invoice_logo))
				<img src="var:logo" alt="" style="width: 80px">
			@else
				<h3>{{ $settings->invoice_company }}</h3>
			@endif
		</th>
	</tr>
</table>
<!-- .HEADER -->
