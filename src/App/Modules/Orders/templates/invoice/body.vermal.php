<style>
	table {
		padding: 0;
		margin: 0;
		width: 100%;
		font-size: 12px;
		font-family: 'Open Sans', sans-serif
	}
	table.bordered {
		border-collapse: collapse;
	}
	table.bordered tr.first td {
		border-bottom: 1px solid #ffffff;
	}
	table.bordered tr.second th {
		padding-top: 8px;
	}
	table.bordered.items th {
		font-size: 12px;
	}
	table.bordered.items td {
		border-top: 1px solid #eee;
		padding: 5px 0;
	}
	th, td {
		text-align: left;
		padding: 1px 0;
	}
	th {
		font-size: 14px;
	}
	.heading {
		color: #7F7F7F;
		padding-bottom: 5px;
	}
	.bg {
		background-color: {{ empty($settings->invoice_color) ? '#69CD3D' : $settings->invoice_color }};
	}
	.rotated {
		font-size: 12px;
		color: {{ empty($settings->invoice_color) ? '#69CD3D' : $settings->invoice_color }};
		border-right: 1px solid {{ empty($settings->invoice_color) ? '#69CD3D' : $settings->invoice_color }};
	}
	.left-offset {
		padding-left: 10px;
	}
</style>

<!-- INFO -->
<table style="margin-top: 60px;">
	<tr>
		<th class="rotated" rowspan="2" style="width: 38px; text-align: center;" text-rotate="90">
			@__(invoice.header1)
		</th>
		<td style="padding-right: 10px;" class="left-offset">
			<table>
				<tr>
					<th class="heading">@__(invoice.from):</th>
				</tr>
				<tr>
					<th>{{ $settings->invoice_company }}</th>
				</tr>
				<tr>
					<td>{{ $settings->invoice_address }}</td>
				</tr>
				<tr>
					<td>{{ $settings->invoice_country }}</td>
				</tr>
				<tr>
					<td>
						<table style="margin-left: -1px">
							<tr>
								@if(!empty($settings->invoice_ico))
									<td style="width: 50%;">IČO: {{ $settings->invoice_ico }}</td>
								@endif
								@if(!empty($settings->invoice_dic))
									<td style="width: 50%">DIČ: {{ $settings->invoice_dic }}</td>
								@endif
								@if(!empty($settings->invoice_ic_dph))
									<td style="width: 50%;">IČ DPH: {{ $settings->invoice_ic_dph }}</td>
								@endif
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td>{!! $settings->invoice_company_note !!}</td>
				</tr>
			</table>
		</td>
		<td style="padding-left: 10px; vertical-align: top">
			<table>
				<tr>
					<th class="heading">@__(invoice.to):</th>
				</tr>
				<tr>
					<th>
						@if(empty($order->billing_company))
							{{ $order->billing_name }} {{ $order->billing_surname }}
						@else
							{{ $order->billing_company }}
						@endif
					</th>
				</tr>
				<tr>
					<td>{{ $order->billing_address }}, {{ $order->billing_city }}, {{ $order->billing_postcode }}</td>
				</tr>
				<tr>
					<td>{{ $order->billing_country }}</td>
				</tr>
				<tr>
					<td>
						<table style="width: 100%; margin-left: -1px">
							<tr>
								@if(!empty($order->billing_ico))
									<td style="width: 50%;">IČO: {{ $order->billing_ico }}</td>
								@endif
								@if(!empty($order->billing_dic))
									<td style="width: 50%">DIČ: {{ $order->billing_dic }}</td>
								@endif
								@if(!empty($order->billing_ic_dph))
									<td style="width: 50%;">IČ DPH: {{ $order->billing_ic_dph }}</td>
								@endif
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="padding-top: 15px; padding-bottom: 50px" colspan="2" class="left-offset">
			<table style="width: auto">
				<tr>
					<td style="padding-right: 15px">
						@__(invoice.date_published): <strong>{{ date('d.m.Y', $order->created_at->getTimestamp()) }}</strong>
					</td>
					<td style="padding-right: 15px">
						@__(invoice.date_of_tax): <strong>{{ date('d.m.Y', $order->created_at->getTimestamp()) }}</strong>
					</td>
					<td>
						<?php $dueDateIncrement = $settings->invoice_due_date_increment ?: 14;  ?>
						@__(invoice.date_due): <strong>{{ date('d.m.Y', strtotime($order->getFormattedCreatedAt() . '+ ' . $dueDateIncrement . 'days')) }}</strong>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- .INFO -->

<!-- PAYMENT -->
<table style="margin-top: -5px; color: #fff" class="bg">
	<tr>
		<th class="rotated" style="width: 36px; text-align: center; border-color: #fff; color: #fff" text-rotate="90">
			@__(invoice.header2)
		</th>
		<td style="padding-right: 10px; padding-top: 20px; padding-bottom: 30px" class="left-offset">
			<table class="bordered" style="vertical-align: top">
				<tr class="first">
					<td style="width: 45%">@__(invoice.bank_account)</td>
					<td style="width: 20%">@__(invoice.var_symbol)</td>
					<td style="width: 18%">@__(invoice.konst_symbol)</td>
					<td style="width: 17%; text-align: right">@__(invoice.to_pay)</td>
				</tr>
				<tr class="second">
					<th style="font-weight: normal">
						<strong>{{ $settings->invoice_bank_account }}</strong>
						@if(!empty($settings->getInvoice_iban()))
							<br>
							IBAN: {{ $settings->invoice_iban }}
						@endif
						@if(!empty($settings->getInvoice_swift()))
							<br>
							SWIFT: {{ $settings->invoice_swift }}
						@endif
					</th>
					<th>{{ $order->getInvoiceNumber($settings->invoice_number_zeros) }}</th>
					<th>-</th>
					<th style="text-align: right">{{ $order->formatPrice($order->price, true, true) }}</th>
				</tr>
				<tr>
					<td colspan="4" style="text-align: right; padding-top: 15px">
						@__(invoice.paymentMethod): <strong>{{ $order->payment_title }}</strong>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- .PAYMENT -->


<!-- ITEMS -->
<table style="margin-top: -3px">
	<tr>
		<th class="rotated" style="width: 39px; text-align: center;" text-rotate="90">
			@__(invoice.header3)
		</th>
		<td style="padding-right: 10px; padding-top: 50px; padding-bottom: 30px" class="left-offset">
			<table class="bordered items">
				<tr>
					<td colspan="{{ !empty($settings->invoice_dph_payer) ? 7 : 4 }}" style="border: none; padding-bottom: 15px">@__(invoice.itemsTitle):</td>
				</tr>
				<tr>
					<th style="width: 25%">@__(invoice.item)</th>
					<th style="width: 15%">@__(invoice.perItem)</th>
					<th style="width: 15%">@__(invoice.pricePerItem)</th>
					@if(!empty($settings->invoice_dph_payer))
						<th style="width: 10%">@__(invoice.vat)</th>
						<th style="width: 15%">@__(invoice.withoutVat)</th>
						<th style="width: 10%">@__(invoice.vatTotal)</th>
					@endif
					<th style="width: 10%; text-align: right">@__(invoice.total)</th>
				</tr>
				@foreach($order->items as $item)
					<?php if($item->price <= 0) continue; ?>
					<tr>
						<td>{{ $item->title }}</td>
						<td>{{ $item->qty }}</td>
						<td>{{ $order->formatPrice($item->price_per_unit) }}</td>
						@if(!empty($settings->invoice_dph_payer))
							<td>{{ $order->formatPrice($item->price_tax) }}</td>
							<td>{{ $order->formatPrice($item->price - $item->price_tax_amount) }}</td>
							<td>{{ $order->formatPrice($item->price_tax_amount) }}</td>
						@endif
						<td style="text-align: right">
							<strong>{{ $order->formatPrice($item->price) }}</strong>
						</td>
					</tr>
				@endforeach
				@if($settings->invoice_show_shipping)
					<tr>
						<td>@__(invoice.shipping)</td>
						<td>1</td>
						<td>{{ $order->formatPrice($order->shipping_amount) }}</td>
						@if(!empty($settings->invoice_dph_payer))
							<td>{{ $order->formatPrice($order->shipping_tax) }}</td>
							<td>{{ $order->formatPrice($order->shipping_amount - $order->shipping_tax_amount) }}</td>
							<td>{{ $order->formatPrice($order->shipping_tax_amount) }}</td>
						@endif
						<td style="text-align: right">
							<strong>{{ $order->formatPrice($order->shipping_amount) }}</strong>
						</td>
					</tr>
				@endif
				@if($settings->invoice_show_payment)
					<tr>
						<td>@__(invoice.payment)</td>
						<td>1</td>
						<td>{{ $order->formatPrice($order->payment_amount) }}</td>
						@if(!empty($settings->invoice_dph_payer))
							<td>{{ $order->formatPrice($order->payment_tax) }}</td>
							<td>{{ $order->formatPrice($order->payment_amount - $order->payment_tax_amount) }}</td>
							<td>{{ $order->formatPrice($order->payment_tax_amount) }}</td>
						@endif
						<td style="text-align: right">
							<strong>{{ $order->formatPrice($order->payment_amount) }}</strong>
						</td>
					</tr>
				@endif
				@if(!empty($order->discount))
					<tr>
						<td>@__(invoice.discount)</td>
						<td>1</td>
						<td>{{ $order->formatPrice($order->discount) }}</td>
						@if(!empty($settings->invoice_dph_payer))
							<td>-</td>
							<td>-</td>
							<td>-</td>
						@endif
						<td style="text-align: right">
							<strong>{{ $order->formatPrice($order->discount) }}</strong>
						</td>
					</tr>
				@endif
			</table>
		</td>
	</tr>
</table>
<!-- .ITEMS -->


<!-- RECAP -->
<table style="margin-top: -4px">
	<tr>
		<th class="rotated" style="width: 38px; text-align: center;" text-rotate="90">
			@__(invoice.header4)
		</th>
		<td style="padding-right: 10px; padding-top: 30px; padding-bottom: 10px" class="left-offset">
			<table class="bordered items">
				<tr>
					<th style="width: 45%"></th>
					<th></th>
					@if(!empty($settings->invoice_dph_payer))
						<th style="width: 15%">@__(invoice.default)</th>
						<th>@__(invoice.vatTotal)</th>
					@endif
					<th style="text-align: right">@__(invoice.total)</th>
				</tr>
				<tr>
				<tr>
					<td style="border: none"></td>
					<td><span style="text-transform: uppercase">@__(invoice.total)</span></td>
					@if(!empty($settings->invoice_dph_payer))
						<td>{{ $order->formatPrice($order->price - $order->price_tax_amount) }}</td>
						<td>{{ $order->formatPrice($order->price_tax_amount) }}</td>
					@endif
					<td style="text-align: right">{{ $order->formatPrice($order->price) }}</td>
				</tr>
				<tr>
					<td style="border: none"></td>
					<td colspan="{{ !empty($settings->invoice_dph_payer) ? 4 : 2 }}" style="text-align: right; padding-top: 10px">
						<strong style="font-size: 20px">
							@__(invoice.totalPrice): {{ $order->formatPrice($order->price, true, true) }}
						</strong>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<!-- .RECAP -->
