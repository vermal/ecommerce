<script>
	$(document).ready(function() {
		$('.order-show-preview').click(function(e) {
			e.preventDefault();
			let id = $(this).closest('tr').data('value');
			$.ajax({
				method: 'GET',
				url: '{{ routerLink('admin.order.api.order') }}/' + id,
				success: function(resp) {
					try {
						let obj = JSON.parse(resp);

						// set order details
						$.each(obj.order, function(key, value) {
							$('#orderPreview .order-' + key).html(value);
						});

						// set customer details
						$.each(obj.customer, function(key, value) {
							$('#orderPreview .customer-' + key).html(value);
						});

						// Set items
						let itemsTemplate = $('#orderPreview .items tbody tr').clone();
						itemsTemplate.removeClass('d-none');
						let items = $('#orderPreview .items tbody');
						items.html('');
						let tpl = null;
						$.each(obj.items, function(i, item) {
							tpl = itemsTemplate.clone();
							$.each(item, function(key, value) {
								tpl.find('.item-' + key).html(value);
								items.append(tpl);
							});
						});
						console.log(items);
						console.log(obj.items);

						// Edit button
						let editUrl = "{{ routerLink('admin.order.edit', ['id' => '__id__']) }}/";
						$('#modal-edit-button').attr('href', editUrl.replace('__id__', id));

						// Invoice button
						let invoiceButton = "{{ routerLink('admin.order.invoice', ['id' => '__id__']) }}/";
						$('#modal-invoice-button').attr('href', invoiceButton.replace('__id__', id));

						$('#orderPreview').modal();
					} catch (e) { console.log(e) }
				}
			})
		});
	});
</script>

<!-- Modal -->
<div class="modal fade" id="orderPreview" tabindex="-1" role="dialog" aria-labelledby="orderPreviewLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="orderPreviewLabel">
					Order #<span class="order-id"></span>
				</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<table class="table">
					<tr>
						<th>
							Billing details
						</th>
						<th>
							Shipping details
						</th>
					</tr>
					<tr>
						<td>
							<span class="order-billing_name"></span>
							<span class="order-billing_surname"></span>
							<br>
							<span class="order-billing_address"></span>
							<span class="order-billing_city"></span>
							<span class="order-billing_postcode"></span>
							<br>
							<span class="order-billing_country"></span>
						</td>
						<td>
							<span class="order-shipping_name"></span>
							<span class="order-shipping_surname"></span>
							<br>
							<span class="order-shipping_address"></span>
							<span class="order-shipping_city"></span>
							<span class="order-shipping_postcode"></span>
							<br>
							<span class="order-shipping_country"></span>
						</td>
					</tr>
					<tr>
						<td>
							<strong>Email</strong>
							<br>
							<span class="customer-email"></span>
						</td>
						<td>
							<strong>Shipping method</strong>
							<br>
							<span class="order-shipping_title"></span>
						</td>
					</tr>
					<tr>
						<td>
							<strong>Phone</strong>
							<br>
							<span class="order-billing_phone"></span>
						</td>
						<td>
							<strong>Payment method</strong>
							<br>
							<span class="order-payment_title"></span>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<table class="table items">
								<thead>
									<tr>
										<th>Title</th>
										<th>Quantity</th>
										<th>Tax</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									<tr class="template d-none">
										<td>
											<span class="item-title"></span> <span class="item-categories"></span>
										</td>
										<td class="item-qty"></td>
										<td>
											<span class="item-price_tax_amount"></span>
											<span class="item-currency"></span>
										</td>
										<td>
											<span class="item-price"></span>
											<span class="item-currency"></span>
										</td>
									</tr>
								</tbody>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				<a href="" id="modal-invoice-button" target="_blank" class="btn btn-info">Invoice</a>
				<a href="" id="modal-edit-button" class="btn btn-primary">Edit</a>
			</div>
		</div>
	</div>
</div>
