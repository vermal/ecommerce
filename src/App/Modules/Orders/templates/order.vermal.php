@extends('layout')

@section('body')

    @form($formName)

        {{-- Content --}}
        <div class="col-md-9">
            <div class="card shadow-lg m-b-30 p-4">

                <div class="row mb-3">
                    <div class="col-md-4">
                        <div class="row">
                            @if(isset($order))
                                @label("status")
                                @control("status")
                            @endif

                            @label("customer")
                            @control("customer")
                            @if(isset($order))
                                <div class="col-lg-12 mb-4" style="margin-top: -15px">
                                    <a href="{{ routerLink('admin.customer.edit', ['id' => $order->customer->id]) }}">
                                        @__(order.show_customer)
                                    </a>
                                </div>
                            @endif

                            @if(empty($order))
                                @label("email")
                                @control("email")
                            @endif

                            @label("note")
                            @control("note")
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row" data-type="billing">
                            @if(empty($order))
                                @label("billing")
                                @control("billing")
                            @else
                                <div class="col-12 mb-2">
                                    <h5>@__(order.billing_address)</h5>
                                </div>
                            @endif
                            <div class="col-12">
                                <div id="billing-fields-wrapper">
                                    <div class="row">
                                        @foreach($GLOBALS['billing_fields'] as $billing)
                                            @label("billing_{$billing}")
                                            @control("billing_{$billing}")
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row" data-type="shipping">
                            @if(empty($order))
                                @label("shipping")
                                @control("shipping")
                            @else
                                <div class="col-12 mb-2">
                                    <h5>@__(order.shipping_address)</h5>
                                </div>
                            @endif
                            <div class="col-12">
                                <div id="shipping-fields-wrapper">
                                    <div class="row">
                                        @foreach($GLOBALS['shipping_fields'] as $shipping)
                                            @label("shipping_{$shipping}")
                                            @control("shipping_{$shipping}")
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="card shadow-lg m-b-30 p-4">
                <div class="row">
                    @label("items")
                    @control("items")

                    @label("coupons")
                    @control("coupons")
                    <div class="col-12" id="items">
                        <div class="table-responsive">
                            <table class="table">
                                <tr>
                                    <th>@__(order.item)</th>
                                    <th>@__(order.qty)</th>
                                    <th>@__(order.costPerUnit)</th>
                                    <th>@__(order.itemTotal)</th>
                                    <th></th>
                                    {{--<th>Tax</th>--}}
                                </tr>
                                <tbody id="items-wrapper">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-md-3">
            <div class="card shadow-lg m-b-30 p-4" id="shipping-method">
                @label("shippingMethod")
                @control("shippingMethod")
            </div>

            <div class="card shadow-lg m-b-30 p-4" id="payment-method">
                @label("paymentMethod")
                @control("paymentMethod")
            </div>

            @if(!empty($order))
                <div class="card shadow-lg m-b-30 p-4" id="payment-method">
                    <h5>
                        @__(order.pricing.title)
                        <strong class="text-success">{{ $order->formatPrice($order->price) }} {{ $order->currency }}</strong>
                    </h5>
                    <table class="table">
                        <tr>
                            <th>{{ $order->currency }}</th>
                            <th>@__(order.pricing.default)</th>
                            <th>@__(order.pricing.vat)</th>
                            <th>@__(order.pricing.total)</th>
                        </tr>
                        <tr>
                            <td>@__(order.pricing.items)</td>
                            <td>{{ $order->formatPrice($order->price_items - $order->price_items_tax_amount) }}</td>
                            <td>{{ $order->formatPrice($order->price_items_tax_amount) }}</td>
                            <td>{{ $order->formatPrice($order->price_items) }}</td>
                        </tr>
                        <tr>
                            <td>@__(order.pricing.shipping)</td>
                            <td>{{ $order->formatPrice($order->shipping_amount - $order->shipping_tax_amount) }}</td>
                            <td>{{ $order->formatPrice($order->shipping_tax_amount) }}</td>
                            <td>{{ $order->formatPrice($order->shipping_amount) }}</td>
                        </tr>
                        <tr>
                            <td>@__(order.pricing.payment)</td>
                            <td>{{ $order->formatPrice($order->payment_amount - $order->payment_tax_amount) }}</td>
                            <td>{{ $order->formatPrice($order->payment_tax_amount) }}</td>
                            <td>{{ $order->formatPrice($order->payment_amount) }}</td>
                        </tr>
                        <tr>
                            <th>@__(order.pricing.total)</th>
                            <th>{{ $order->formatPrice($order->price - $order->price_tax_amount) }}</th>
                            <th>{{ $order->formatPrice($order->price_tax_amount) }}</th>
                            <th>{{ $order->formatPrice($order->price) }} {{ $order->currency }}</th>
                        </tr>
                    </table>
                </div>

                @if(!empty($order->invoice))
                    <div class="card shadow-lg m-b-30 p-4" id="payment-method">
                        <h5 class="mb-0">
                            @__(order.invoice)
                            <strong class="text-success">{{ $order->getInvoiceNumber($presenter::$appSettings->invoice_number_zeros) }}</strong>
                            <a href="{{ routerLink('admin.order.invoice', ['id' => $order->id]) }}" target="_blank"
                               class="btn d-inline-block btn-outline-info float-right">@__(order.showInvoice)</a>
                        </h5>
                        <input type="text" class="form-control mt-2"
                               value="{{ routerLink('ecommerce.order.invoice', ['token' => $order->token])  }}">
                    </div>
                @endif
            @endif

            <div class="card shadow-lg m-b-30 p-4">
                @control("submit")
            </div>
        </div>

    @endform

    <script type="text/javascript">
        $(document).ready(function() {
            // Setup select for customers
            let customers = $('#customer_order');
            customers.select2({
                ajax: {
                    url: '{{ routerLink('admin.customer.search') }}',
                    dataType: 'json',
                    minimumInputLength: 2,
                    data: function (params) {
                        return {
                            q: params.term || ' ',
                            page: params.page || 1
                        };
                    }
                }
            });

            // Address select
            let billing = $('#billing_order');
            let shipping = $('#shipping_order');

            // Address select events
            billing.on('change', hydrateAddressFields);
            shipping.on('change', hydrateAddressFields);

            // Get addresses on select
            customers.on('select2:select', function (e) {
                // Remove addresses from select
                billing.find('.customer-address').remove();
                shipping.find('.customer-address').remove();
                let data = e.params.data;
                // Request to server
                $.ajax({
                    url: '{{ routerLink('admin.address.find-by-customer') }}',
                    data: {id: data.id},
                    success: function(response) {
                        try {
                            let addresses = JSON.parse(response);
                            addAddresses(addresses, billing);
                            addAddresses(addresses, shipping);
                        } catch (e) {}
                    }
                });
            });

            /**
             * Append addresses option to select
             *
             * @param addresses
             * @param obj
             */
            function addAddresses(addresses, obj) {
                for (let key in addresses) {
                    if (addresses.hasOwnProperty(key)) {
                        let address = addresses[key];
                        let jsonAddress = JSON.stringify(addresses[key]);
                        address = $('<option class="customer-address" value="' + address.id + '">').text(address.fullAddress)
                            .data('address', jsonAddress);
                        obj.append(address);
                    }
                }
            }

            /**
             * Hydrate address fields
             */
            function hydrateAddressFields() {
                let type = $(this).closest('.row').data('type');
                let address = $(this).find(':selected').data('address');
                try {
                    address = JSON.parse(address);
                    for (let key in address) {
                        if (address.hasOwnProperty(key)) {
                            let value = address[key];
                            if (key === "psc") key = "postcode";
                            else if (key === "street") key = "address";
                            else if (key === "companyName") key = "company";
                            $('#' + type + '_' + key + '_order').val(value);
                        }
                    }
                } catch (e) {}
            }

            // Setup select for items
            let items = $('#items_order');
            items.select2({
                ajax: {
                    url: '{{ routerLink('admin.product.search') }}',
                    dataType: 'json',
                    minimumInputLength: 2,
                    data: function (params) {
                        return {
                            q: params.term || ' ',
                            page: params.page || 1
                        };
                    }
                }
            });

            // Add item on select
            items.on('select2:select', function (e) {
                let data = e.params.data;
                // Request to server
                $.ajax({
                    url: '{{ routerLink('admin.product.find-by-id') }}',
                    data: {id: data.id},
                    success: function(response) {
                        try {
                            let product = JSON.parse(response);
                            buildItem(product);
                        } catch (e) {}
                    }
                });
            });

            // Setup select for items
            let coupons = $('#coupons_order');
            coupons.on('change', function(e) {
                let item = $(this);
                if (item.val() === "") return;
                buildItem({
                    qty: 1,
                    id: item.val(),
                    title: item.find('option:selected').html(),
                    price: {
                        price: 0,
                        currency: ''
                    },
                    formattedPrice: 0,
                    formattedPriceTotal: 0,
                    type: '{{ \Vermal\Ecommerce\Modules\Orders\Entities\OrderItem::DISCOUNT }}'
                });
            });

            /**
             * Buidld item
             *
             * @param data
             */
            function buildItem(data)
            {
                if ($('#items-wrapper tr[data-id="' + data.id + '"]').length > 0) return;
                let qty = typeof data.qty === "undefined" ? 1 : data.qty;
                data.type = typeof data.type === "undefined" ? '{{ \Vermal\Ecommerce\Modules\Orders\Entities\OrderItem::PRODUCT }}' : data.type;
                let total = typeof data.formattedPriceTotal === "undefined" ? data.formattedPrice : data.formattedPriceTotal;
                let item = $('<tr>').append(
                    $('<td>').html('<strong>' + data.title + '<strong>'),
                    $('<td>').html(
                        '<input class="form-control item-qty text-right px-0" type="text" value="' + qty + '" name="item[qty][]" style="max-width: 70px;">' +
                        '<input type="hidden" value="' + data.id + '" name="item[id][]">' +
                        '<input type="hidden" value="' + data.title + '" name="item[title][]">' +
                        '<input type="hidden" value="' + data.price.price + '" name="item[price][]">' +
                        '<input type="hidden" value="' + data.price.currency + '" name="item[currency][]">' +
                        '<input type="hidden" value="' + data.type + '" name="item[type][]">'
                    ),
                    $('<td class="price">').data('price', data.price.price)
                        .data('currency', data.price.currency)
                        .html(data.formattedPrice),
                    $('<td class="total-price">').html(total),
                    $('<td class="text-right">').html(
                        '<a href="#" class="btn btn-sm btn-danger delete-item">X</a>'
                    )
                ).attr('data-id', data.id).data('id', data.id);
                $('#items-wrapper').append(item);
            }

            @if(!empty($orderItems))
                @foreach($orderItems as $item)
                    <?php
                        $it = json_encode([
                                'id' => $item['id'],
                                'title' => $item['title'],
                                'qty' => $item['qty'],
                                'price' => [
                                    'price' => $item['price'],
                                    'currency' => $item['currency']
                                ],
                                'formattedPrice' => $item['price'] . ' ' . $item['currency'],
                                'formattedPriceTotal' => ($item['price'] * $item['qty']) . ' ' . $item['currency'],
                                'type' => $item['type']
                        ], JSON_PRETTY_PRINT);
                    ?>
                    buildItem({!! $it !!});
                @endforeach
            @endif

            $(document).on('keyup change', '#items-wrapper .item-qty', function() {
                let row = $(this).parent().parent();
                let price = row.find('.price').data('price');
                let currency = row.find('.price').data('currency');
                row.find('.total-price').html( (price * Number($(this).val())) + ' ' + currency);
            });

            $(document).on('click', '#items-wrapper .delete-item', function(e) {
                e.preventDefault();
                $(this).closest('tr').remove();
            });
        });
    </script>

@endsection
