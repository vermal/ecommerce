<?php

namespace Vermal\Ecommerce\Modules\Orders;


use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Router;

trait ChangeStatus
{

	/**
	 * Order change status
	 *
	 * @param $status
	 * @param $post
	 */
	private function changeStatus($status, $post)
	{
		foreach ($post as $id) {
			$order = Database::Model('Order')->find(Sanitizer::applyFilter('trim|strip_tags|escape|digit', $id));

			$isInExpedition = Database::Model('ExpeditionItem')
				->where('e.order', $order->id)->count();
			if ($isInExpedition < 1) {
				$order->status = $status;
				Database::saveAndFlush($order);
			} else {
				$this->alert(sprintf($this->_('expedition.in_expedition_status_error'), $order->id), 'danger');
			}
		}
		Router::redirectToURL(Router::$prevLink);
	}

	public function changeStatusNew($post)
	{
		$this->changeStatus(self::NEW_, $post);
	}

	public function changeStatusNew_payed($post)
	{
		$this->changeStatus(self::NEW_PAYED, $post);
	}

	public function changeStatusFailed($post)
	{
		$this->changeStatus(self::FAILED, $post);
	}

	public function changeStatusProcessing($post)
	{
		$this->changeStatus(self::PROCESSING, $post);
	}

	public function changeStatusCancelled($post)
	{
		$this->changeStatus(self::CANCELLED, $post);
	}

	public function changeStatusPackaged($post)
	{
		$this->changeStatus(self::PACKAGED, $post);
	}

	public function changeStatusDispatched($post)
	{
		$this->changeStatus(self::DISPATCHED, $post);
	}

	public function changeStatusRefunded($post)
	{
		$this->changeStatus(self::REFUNDED, $post);
	}

	public function changeStatusFailed_to_deliver($post)
	{
		$this->changeStatus(self::FAILED_TO_DELIVER, $post);
	}

	public function changeStatusCompleted($post)
	{
		$this->changeStatus(self::COMPLETED, $post);
	}

}
