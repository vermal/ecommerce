<?php

namespace Vermal\Ecommerce\Modules\Orders\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Customers\Entities\Address;
use Vermal\Ecommerce\Modules\Orders\Entities\Customer\Billing;
use Vermal\Ecommerce\Modules\Orders\Entities\Customer\Shipping;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_order")
 * @ORM\HasLifecycleCallbacks
 **/
class Order extends Model
{
    use Billing;
    use Shipping;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $status;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_tax_amount;

    /**
     * @ORM\Column(type="float")
     */
    protected $price_items;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_items_tax_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $discount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $discount_tax;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $discount_tax_amount;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $shipping_title;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $shipping_amount;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $shipping_tax_amount;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $shipping_tax;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $payment_title;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $payment_amount;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $payment_tax_amount;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $payment_tax;

    /**
     * @ORM\Column(length=64)
     */
    protected $currency;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $note;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_completed;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $date_paid;

	/**
	 * @ORM\ManyToMany(targetEntity="\OrderItem", cascade={"persist", "remove"}, orphanRemoval=true)
	 * @ORM\JoinTable(name="ecommerce_order_order_item",
	 *      joinColumns={@ORM\JoinColumn(name="order_id", referencedColumnName="id")},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="order_item_id", referencedColumnName="id", unique=true)}
	 * )
	 */
    protected $items;

	/**
	 * @ORM\ManyToOne(targetEntity="\ShippingMethod")
	 * @ORM\JoinColumn(name="shipping_method", referencedColumnName="id")
	 */
	protected $shipping;

	/**
	 * @ORM\ManyToOne(targetEntity="\PaymentMethod")
	 * @ORM\JoinColumn(name="payment_method", referencedColumnName="id")
	 */
	protected $payment;

    /**
     * @ORM\ManyToOne(targetEntity="\Customer", cascade={"persist"})
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

	/**
	 * @ORM\OneToOne(targetEntity="OrderInvoice")
	 * @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
	 */
	protected $invoice;

	/**
	 * @ORM\Column(type="boolean", name="soft_delete")
	 */
	protected $softDelete;

	/**
	 * @ORM\Column(type="boolean")
	 */
	protected $payed;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $payment_id;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $predicted_delivery;

    public function __construct()
	{
		parent::__construct();
		$this->items = new ArrayCollection();
	}

	/**
	 * @param OrderItem $item
	 */
	public function addItem($item)
	{
		$this->items[] = $item;
    }

	public function clearItems()
	{
		$this->items->clear();
    }

	/**
	 * Get formatted price
	 *
	 * @param int|float $value
	 * @param bool $currency
	 * @param bool $showDecimal
	 * @return string
	 */
	public function formatPrice($value, $showDecimal = true, $currency = false)
	{
		$decimals = 0;
		if ((is_numeric( $value ) && floor( $value ) != $value) || $showDecimal)
			$decimals = 2;
		$price = number_format($value, $decimals,',',' ');
		if ($currency)
			$price .= ' ' . $this->currency;
		return $price;
	}


	/**
	 * Get formatted invoice number
	 *
	 * @param int $pad
	 * @return mixed
	 */
	public function getInvoiceNumber($pad = 5)
	{
		return $this->invoice->getNumber($pad);
	}

	/**
	 * Get full shipping address
	 *
	 * @return string
	 */
	public function getFullShippingAddress()
	{
		return $this->shipping_name . ' ' . $this->shipping_surname . ' - ' .
			$this->shipping_address . ', ' . $this->shipping_city . ', ' . $this->shipping_postcode . ', ' . $this->shipping_country;
	}

	/**
	 * Get full shipping address
	 *
	 * @return string
	 */
	public function getFullBillingAddress()
	{
		return $this->billing_name . ' ' . $this->billing_surname . ' - ' .
			$this->billing_address . ', ' . $this->billing_city . ', ' . $this->billing_postcode . ', ' . $this->billing_country;
	}

}
