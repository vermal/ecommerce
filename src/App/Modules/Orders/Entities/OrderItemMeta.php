<?php

namespace Vermal\Ecommerce\Modules\Orders\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Database\MagicAccessor;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_order_item_meta")
 * @ORM\HasLifecycleCallbacks
 **/
class OrderItemMeta
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string", name="meta_key") **/
    protected $key;

    /**
     * @ORM\Column(type="text", name="meta_value")
     */
    protected $value;

    /**
     * @ORM\OneToMany(targetEntity="\OrderItemMeta", mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\OrderItemMeta", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\ManyToMany(targetEntity="\OrderItem", mappedBy="meta", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $item;

    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * Check if has child
     *
     * @param $id
     * @return bool
     */
    public function hasChild($id)
    {
        foreach ($this->children as $child) {
            if ($child->id == $id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $child
     */
    public function addChild($child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove children
     */
    public function removeAllChildren() {
        $this->children->clear();
    }
}
