<?php

namespace Vermal\Ecommerce\Modules\Orders\Entities;

use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_order_item")
 * @ORM\HasLifecycleCallbacks
 **/
class OrderItem extends Model
{
    const PRODUCT = 'product';
    const DISCOUNT = 'discount';

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $te;

    /**
     * @ORM\Column(type="string", name="short_te", nullable=true)
     */
    protected $shortTe;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    // INFO
    /** @ORM\Column(type="float", nullable=true) **/
    protected $ean;

    // SHIPPING
    /** @ORM\Column(type="string", nullable=true) **/
    protected $weight;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $length;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $width;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $height;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $qty;

    /**
     * @ORM\Column(type="float")
     */
    protected $price;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_tax;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_tax_amount;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_per_unit;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_per_unit_tax_amount;

    /**
     * @ORM\Column(length=64)
     */
    protected $currency;

    /**
     * @ORM\ManyToOne(targetEntity="\Multimedia")
     * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
     */
    protected $simpleMultimedia;

    /**
     * @ORM\ManyToMany(targetEntity="\OrderItemMeta", inversedBy="product", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinTable(name="ecommerce_order_item_order_item_meta")
     */
    protected $meta;

	/**
	 * @ORM\ManyToOne(targetEntity="\Product", inversedBy="items", cascade={"persist"})
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $product;

	/**
	 * @ORM\ManyToOne(targetEntity="\Coupon", cascade={"persist"})
	 * @ORM\JoinColumn(name="coupon_id", referencedColumnName="id", onDelete="SET NULL")
	 */
	protected $coupon;

	/**
	 * @ORM\ManyToMany(targetEntity="\Order", mappedBy="items")
	 */
	protected $orders;

	/**
	 * Get formatted price
	 *
	 * @param string $key
	 * @param bool $currency
	 * @param bool $showDecimal
	 * @return string
	 */
	public function getFormattedPrice($key, $showDecimal = true, $currency = true)
	{
		$value = $this->{$key};
		$decimals = 0;
		if ((is_numeric( $value ) && floor( $value ) != $value) || $showDecimal)
			$decimals = 2;
		$price = number_format($value, $decimals,',',' ');
		if ($currency)
			$price .= ' ' . Currencies::getSymbol($this->currency);
		return $price;
	}

}
