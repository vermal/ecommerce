<?php

namespace Vermal\Ecommerce\Modules\Orders\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_order_invoice")
 * @ORM\HasLifecycleCallbacks
 **/
class OrderInvoice extends Model
{

	/**
	 * @ORM\Column(type="string")
	 */
	protected $prefix;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $number;

	/**
	 * @param int $pad
	 * @return string
	 */
	public function getNumber($pad = 3)
	{
		$prefix = !empty($this->prefix) ? $this->prefix : '';
 		return $prefix . str_pad($this->number, $pad, '0', STR_PAD_LEFT);
	}

}
