<?php

namespace Vermal\Ecommerce\Modules\Orders\Entities\Customer;

trait Billing
{
    /**
     * @ORM\Column(type="string")
     */
    protected $billing_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $billing_surname;

    /**
     * @ORM\Column(type="string")
     */
    protected $billing_address;

    /**
     * @ORM\Column(type="string")
     */
    protected $billing_city;

    /**
     * @ORM\Column(type="string")
     */
    protected $billing_postcode;

    /**
     * @ORM\Column(type="string")
     */
    protected $billing_country;

    /**
     * @ORM\Column(type="string")
     */
    protected $billing_phone;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $billing_company;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $billing_ico;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $billing_dic;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $billing_ic_dph;
}
