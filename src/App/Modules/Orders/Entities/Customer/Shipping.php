<?php

namespace Vermal\Ecommerce\Modules\Orders\Entities\Customer;

trait Shipping
{
    /**
     * @ORM\Column(type="string")
     */
    protected $shipping_name;

    /**
     * @ORM\Column(type="string")
     */
    protected $shipping_surname;

    /**
     * @ORM\Column(type="string")
     */
    protected $shipping_address;

    /**
     * @ORM\Column(type="string")
     */
    protected $shipping_city;

    /**
     * @ORM\Column(type="string")
     */
    protected $shipping_postcode;

    /**
     * @ORM\Column(type="string")
     */
    protected $shipping_country;
}
