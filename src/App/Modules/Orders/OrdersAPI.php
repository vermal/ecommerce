<?php

namespace Vermal\Ecommerce\Modules\Orders;

use PHPMailer\PHPMailer\Exception;
use Vermal\Admin\Upload;
use Vermal\Admin\View;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Expeditions\ExpeditionsModel;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Form\Form;
use Vermal\Router;

class OrdersAPI extends Controller
{

	public function order($id)
	{
		/** @var Order $order*/
		$order = Database::Model('Order')
			->find($id, true);
		if (empty($order)) echo json_encode([]);

		$items_ = Database::Model('OrderItem')
			->join('o.orders', 'orders')
			->where('orders.id', $order['id'])
			->get();
		$items = [];
		foreach ($items_ as $key => $item) {
			$items[$key] = $item->toArray();
			$items[$key]['categories'] = !empty($item->product) ? '(' . $item->product->getCategoriesAsString() . ')' : null;
		}


		$customer = Database::Model('Customer')
			->join('c.orders', 'orders')
			->where('orders.id', $order['id'])
			->first(true);

		echo json_encode([
			'order' => $order,
			'items' => $items,
			'customer' => $customer
		]);
	}

}
