<?php

namespace Vermal\Ecommerce\Modules\Orders;

use Mpdf\Output\Destination;
use PHPMailer\PHPMailer\Exception;
use Vermal\Admin\Upload;
use Vermal\Admin\View;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Expeditions\ExpeditionsModel;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Form\Form;
use Vermal\Router;

class Orders extends Controller
{

	use ChangeStatus;

	const NEW_ = 'new';
	const NEW_PAYED = 'new_payed';
	const FAILED = 'failed';
	const PROCESSING = 'PROCESSING';
	const CANCELLED = 'cancelled';
	const PACKAGED = 'packaged';
	const DISPATCHED = 'dispatched';
	const REFUNDED = 'refunded';
	const FAILED_TO_DELIVER = 'failed_to_deliver';
	const COMPLETED = 'completed';

	/** INVOICE DIR */
	const INVOICE_DIR = BASE_PATH . '/public/uploads/invoices/';

    public $defaultViewPath = false;
    private $orderStatuses = [];

    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('orders', $this->CRUDAction);
        $this->addToBreadcrumb('order', 'admin.order.index');
        $this->setFluidLayoutContainer();
		$this->orderStatuses = apply_filters('ecommerce_orderStatuses', [
			self::NEW_ => $this->_('order.statuses.new_'),
			self::NEW_PAYED => $this->_('order.statuses.new_payed'),
			self::FAILED => $this->_('order.statuses.failed'),
			self::PROCESSING => $this->_('order.statuses.processing'),
			self::CANCELLED => $this->_('order.statuses.cancelled'),
			self::PACKAGED => $this->_('order.statuses.packaged'),
			self::DISPATCHED => $this->_('order.statuses.dispatched'),
			self::REFUNDED => $this->_('order.statuses.refunded'),
			self::FAILED_TO_DELIVER => $this->_('order.statuses.failed_to_deliver'),
			self::COMPLETED => $this->_('order.statuses.completed')
		]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    	$paymentMethods = Database::Model('PaymentMethod')->setPair('id', 'title')->get();
		$shippingMethods = Database::Model('ShippingMethod')->setPair('id', 'title')->get();

        $this->setDefaultViewPath();

        $data = Database::Model('Order')
			->order('o.created_at', 'DESC');
        $datagrid = new DataGrid($data);
        $datagrid->addDefaultFilter('softDelete', '0');

        $datagrid->addColumn('id', '')->setRenderer(function($entity) {
			return "<a href='" . routerLink('admin.order.edit', ['id' => $entity->id]) . "'>" .
				'#' . $entity->id . ' ' . $entity->billing_name . ' ' . $entity->billing_surname .
				'</a>';
		});
		$datagrid->addColumn('created_at', $this->_('order.date'))->setRenderer(function ($entity) {
			return $entity->getFormattedCreatedAt('d.m.Y H:i');
		});
		$datagrid->addColumn('predicted_delivery', $this->_('order.predictedDelivery'))->setRenderer(function ($entity) {
			if (!empty($entity->predicted_delivery))
				return date('d.m.Y', $entity->predicted_delivery->getTimestamp());
		});
		$datagrid->addColumn('shipping_city', $this->_('order.address'))->setRenderer(function ($entity) {
			return $entity->shipping_address . ', ' . $entity->shipping_city . ', ' . $entity->shipping_postcode;
		});
		$datagrid->addColumn('payment_title', $this->_('order.payment'));
		$datagrid->addColumn('shipping_title', $this->_('order.shipping'));
		$datagrid->addColumn('status', $this->_('order.status'))->setRenderer(function ($entity) {
			$bg = 'transparent';
			$color = 'initial';
			if ($entity->status == Orders::NEW_) {
				$bg = '#8e44ad';
				$color = '#fff';
			} else if ($entity->status == Orders::NEW_PAYED) {
				$bg = '#cd6133';
				$color = '#fff';
			} else if ($entity->status == Orders::PACKAGED) {
				$bg = '#f8dda7';
				$color = '#94660c';
			} else if ($entity->status == Orders::DISPATCHED) {
				$bg = '#f1c40f';
			} else if ($entity->status == Orders::FAILED_TO_DELIVER || $entity->status == Orders::FAILED) {
				$bg = '#c23616';
				$color = '#fff';
			} else if ($entity->status == Orders::COMPLETED || $entity->status == Orders::REFUNDED || $entity->status == Orders::CANCELLED) {
				$bg = '#27ae60';
				$color = '#fff';
			}
			return "<div class='alert d-inline-block p-1 font-weight-bold' style='background-color: " . $bg . "; color: " . $color . "; border: none;'>" .
				$this->_('order.statuses.' . $entity->status) . "</div>";
		});
		$datagrid->addColumn('total', $this->_('order.total'))->setRenderer(function ($entity) {
			return $entity->price . Currencies::getSymbol($entity->currency);
		});
		$datagrid->addColumn('invoiceno', 'Faktúra')->setRenderer(function($entity) {
			if (!empty($entity->invoice)) {
				return $entity->invoice->number;
			}
		});


        // Add actions
		$datagrid->addAction('invoice', ['admin.order.invoice', ['id']], 'fas fa-file-invoice', '', 'btn btn-sm btn-info');
        $datagrid->addEdit(['admin.order.edit', ['id']]);
        $datagrid->addDelete(['admin.order.destroy_', ['id']]);

		$datagrid->addAction('payed1', ['admin.order.payed', ['id']], 'fas fa-toggle-off', 'Nezaplatená',
			'btn btn-danger btn-sm mt-2', [['payed', '=', 0]]);
		$datagrid->addAction('payed0', ['admin.order.payed', ['id']], 'fas fa-toggle-on', 'Zaplatená',
			'btn btn-success btn-sm mt-2', [['payed', '=', 1]]);

		$datagrid->addAction('preview', ['custom' => '#'], 'fas fa-eye', 'Náhľad',
			'btn btn-primary btn-sm mt-2 order-show-preview');

        $datagrid->addSearch('id', $this->_('order.id'), 'col-lg-1');
        $datagrid->filterRenderer(function($value, $data) {
			/** @var Database $data */
			$data = $data->where('o.id', $value);
			return $data;
		});

		$datagrid->addSearch('invoice_id', 'Invoice ID', 'col-lg-1');
		$datagrid->filterRenderer(function($value, $data) {
			/** @var Database $data */
			$data = $data
				->join('o.invoice', 'oi')
				->where('oi.number', $value);
			return $data;
		});

		$datagrid->addFilter('customer', [null => $this->_('order.customer')], $this->_('order.customer'),
			'col-lg-3', 'js-select2 search', ['data-controller' => self::class, 'data-method' => 'searchCustomer']);
		$datagrid->addFilter('status', $this->orderStatuses, $this->_('order.status'), 'col-lg-1');
		$datagrid->addFilter('payment', $paymentMethods, $this->_('order.payment'), 'col-lg-2');
		$datagrid->addFilter('shipping', $shippingMethods, $this->_('order.shipping'), 'col-lg-2');
		$datagrid->addInputFilter('created_at', 'daterange', $this->_('order.date'), 'col-lg-2', 'daterange');
		$datagrid->addInputFilter('predicted_delivery', 'daterange', $this->_('order.predictedDelivery'), 'col-lg-3', 'daterange');
		$datagrid->addFilter('softDelete', [1 => 'Zmazané', 0 => 'Aktívne'], '', 'col-lg-2');

		$datagrid->addBulkAction('createExpedition', 'Expedovať objednávky', [self::class, 'createExpedition']);
		foreach ($this->orderStatuses as $key => $status) {
			$datagrid->addBulkAction('changeStatus' . ucfirst($key), 'Zmena stavu: ' . $status, [self::class, 'changeStatus' . ucfirst($key)]);
		}

		$datagrid->addQuickFilter(routerLink('admin.order.index', ['filter__softDelete' => 1]), 'Zmazané objednávky');
		$datagrid->addAction('renewOrder', ['admin.order.renew', ['id']], '', 'Obnoviť',
			'btn btn-outline-primary btn-sm mt-2', [['softDelete', '=', 1]]);

		$ordersGridPreview = View::singleView('ordersGridPreview', __DIR__ . '/templates/', [], true);
		$datagrid->addHtml($ordersGridPreview);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        $this->getPersistedItems();
        View::view('order', [
            "form" => $form->build(),
            "formName" => $form->getName()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
		$form = $this->form();
		if ($form->hasError()) {
			$this->persistItems($post);
			Router::redirect('admin.order.create');
		}

		// Set post items ids
		$post->items = $this->handleItems($post);
		$post->currency = 'EUR';

		(new OrderModel((array)$post, $form))->store();
		Router::redirect('admin.order.index');
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param int $id
	 */
    public function edit($id)
    {
		$form = $this->form();
		$form->setAjax(true);
		$form->setMethod('PUT');
		$form->setAction(routerLink('admin.order.update', ['id' => $id]));
		$this->getPersistedItems();

		/** @var Order $order */
		$order = Database::Model('Order')->find($id);

		$form->setValues($order, ['items', 'email', 'billing', 'shipping', 'shippingMethod', 'paymentMethod', 'customer']);

		$form->getComponent('customer')->setSelect([
			$order->customer->id => $order->customer->name . ' ' . $order->customer->surname
		])->setValue($order->customer->id);
		$form->getComponent('shippingMethod')->setValue($order->shipping->id);
		$form->getComponent('paymentMethod')->setValue($order->payment->id);
		$form->getComponent('email')
			->addAttribute('disabled', 'disabled')
			->setValue($order->customer->email);

		$items = [];
		foreach ($order->items as $item) {
			$id_ = null;
			if (!empty($item->product)) {
				$id_ = $item->product->id;
			} else if (!empty($item->coupon)) {
				$id_ = $item->coupon->id;
			}
			$items[] = [
				'id' => $id_,
				'title' => $item->title,
				'qty' => $item->qty,
				'price' => $item->price_per_unit,
				'currency' => $item->currency,
				'type' => $item->type
			];
		}
		View::setVariable('orderItems', $items);

		View::view('order', [
			"form" => $form->build(),
			"formName" => $form->getName(),
			"order" => $order
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
		$form = $this->form();
		if ($form->hasError()) {
			$this->alert($this->_('error.form.empty'), 'danger');
			die();
		}

		// Set post items ids
		$post->items = $this->handleItems($post);
		$post->currency = 'EUR';

		$orderModel = new OrderModel((array)$post, $form);
		$order = $orderModel->save((int)$id);
		if (!$order) {
			foreach ($orderModel->form->getErrors() as $error) {
				$this->alert($this->_($error['msg']), $error['type']);
				break;
			}
		} else {
			$this->alert($this->_('order.edited'));
		}
    }

	/**
	 * Send new order email
	 *
	 * @param $order
	 * @param $settings
	 *
	 * @throws Exception
	 * @throws \Mpdf\MpdfException
	 * @throws \Whoops\Exception\ErrorException
	 */
	public static function sendNewOrderEmail($order, $settings)
	{
		$mailer = self::mailer($settings);

		// Subject
		$mailer->subject('Nová objednávka');

		// Create template
		$template = apply_filters('ecommerce_order_email_new_order', '', $order);
		if (empty($template)){
			$oldViewPath = View::getViewPath();
			View::setViewPath(__DIR__ . '/templates/');
			$template = \Vermal\View::view('email.new-order', ['order' => $order], true);
			// Set view path back to original
			View::setViewPath($oldViewPath);
		}

		// Setup
		$mailer->body($template);
		$mailer->addAddress($order->customer->email, $order->customer->name . ' ' . $order->customer->surname);

		// Generate invoice if it doesn't exist as file
		if (!is_file(self::INVOICE_DIR . 'magu-order-' . $order->invoice->getNumber() . '.pdf')) {
			self::generateInvoice($order->id, $settings, true);
		}
		// Add invoice
		$mailer->addAttachment(self::INVOICE_DIR . 'magu-order-' . $order->invoice->getNumber() . '.pdf', 'magu-order-' . $order->invoice->getNumber() . '.pdf');

		// Send
		$mailer->send();
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
		$order = Database::Model('Order')->find($id);
		$order->softDelete = true;
		Database::saveAndFlush($order);
//    	$order = Database::Model('Order')->find($id);
//    	$order->clearItems();
//    	Database::saveAndFlush($order);
//		Database::Model('Order')->delete($id);
//
//		// Redirect
		$this->alert($this->_('order.deleted') .
			'. <a class="btn btn-primary btn-sm ml-3" href="' . routerLink('admin.order.renew', ['id' => $id]) . '">'
			. $this->_('order.renew') . '</a>');
		Router::redirectToURL(Router::$prevLink);
    }

	/**
	 * @param $id
	 */
	public function rendewOrder($id)
	{
		$order = Database::Model('Order')->find($id);
		$order->softDelete = false;
		Database::saveAndFlush($order);

		$this->alert($this->_('order.renewed'));
		Router::redirectToURL(Router::$prevLink);
    }

	/**
	 * Create array of items from post object
	 *
	 * @param $post
	 * @return array
	 */
    private function handleItems($post)
	{
		$items = [];
		if (!empty($post->item)) {
			foreach ($post->item->qty as $key => $qty) {
				$items[] = [
					'qty' => $qty,
					'id' => $post->item->id->{$key},
					'title' => $post->item->title->{$key},
					'price' => $post->item->price->{$key},
					'currency' => $post->item->currency->{$key},
				];
			}
		}
		return $items;
	}

	/**
	 * Persist items in session
	 *
	 * @param $post
	 */
    private function persistItems($post)
	{
		$_SESSION['orderPersistedItems'] = $this->handleItems($post);
	}

	/**
	 * Get items from session
	 */
	private function getPersistedItems()
	{
		$items = [];
		if (!empty($_SESSION['orderPersistedItems'])) {
			$items = $_SESSION['orderPersistedItems'];
			unset($_SESSION['orderPersistedItems']);
		}
		View::setVariable('orderItems', $items);
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('order', routerLink('admin.order.store'));

        // Customer
        $form->addSelect('customer', ['guest' => 'Guest'], $this->_('order.customer'));
        $form->addEmail('email', 'E-Mail')->setParentClass('customer-email');
		$form->addTextArea('note', 'Note');

        $form->addSelect('status', $this->orderStatuses, $this->_('order.status'));

        // Billing/Shipping data
        $GLOBALS['billing_fields'] = ['name', 'surname', 'address', 'city', 'postcode', 'country', 'phone', 'company', 'ico', 'dic', 'ic_dph'];
        $GLOBALS['shipping_fields'] = ['name', 'surname', 'address', 'city', 'postcode', 'country'];
        for ($i = 0; $i < 2; $i++) {
            if ($i == 0) $type = 'billing';
            else $type = 'shipping';

            $form->addText($type . '_name', $this->_('customer.name'))->setCol('col-md-6')->required();
            $form->addText($type . '_surname', $this->_('customer.surname'))->setCol('col-md-6')->required();
            $form->addText($type . '_address', $this->_('address.address'))->required();
            $form->addText($type . '_city', $this->_('address.city'))->setCol('col-md-8')->required();
            $form->addText($type . '_postcode', $this->_('address.psc'))->setCol('col-md-4')->required();
            $form->addText($type . '_country', $this->_('address.country'))->required();
        }
		$form->addText('billing_company', $this->_('address.companyName'));
		$form->addText('billing_ico', $this->_('address.ico'))->setCol('col-lg-4');
		$form->addText('billing_dic', $this->_('address.dic'))->setCol('col-lg-4');
		$form->addText('billing_ic_dph', $this->_('address.ic_dph'))->setCol('col-lg-4');
        $form->addText('billing_phone', $this->_('address.phone'))->required();
        $form->addSelect('billing', ['new' => 'New'], $this->_('order.billing_address'));
        $form->addSelect('shipping', ['new' => 'New'], $this->_('order.shipping_address'));

        // Items
        $form->addSelect('items', ['0' => $this->_('order.selectItem')], $this->_('order.items'));

        $form->addSelect('coupons',
			['' => 'Coupon'] + Database::Model('Coupon')->setPair('id', 'code')->get(),
			'Coupons');

        // Shipping methods
	    $form->addSelect('shippingMethod',
		    Database::Model('ShippingMethod')->setPair('id', 'title')->get(), $this->_('order.shipping'))
	        ->setClass('js-select2')
			->required();

		// Shipping methods
		$form->addSelect('paymentMethod',
			Database::Model('PaymentMethod')->setPair('id', 'title')->get(), $this->_('order.payment'))
			->setClass('js-select2')
			->required();

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))
			->setParentClass('mb-0')
			->setClass('btn btn-success btn-block');

        return $form;
    }

	/**
	 * @param $orderId
	 * @throws \Mpdf\MpdfException
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function invoice($orderId)
	{
		self::generateInvoice($orderId, $this->settings, false);
    }

	/**
	 * @param $token
	 * @throws \Mpdf\MpdfException
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function invoiceTokenized($token)
	{
		$order = Database::Model('Order')
			->where('o.token', $token)->first();
		self::generateInvoice($order->id, $this->settings, false);
	}

	/**
	 * @param $orderId
	 * @param $settings
	 * @param bool $output
	 * @throws \Mpdf\MpdfException
	 * @throws \Whoops\Exception\ErrorException
	 */
    public static function generateInvoice($orderId, $settings, $output = false)
	{
		$order = Database::Model('Order')->find($orderId);
		$vars = [
			'order' => $order,
			'settings' => $settings
		];

		$mpdf = new \Mpdf\Mpdf();;

		$mpdf->SetTopMargin(30);

		if (!empty($settings->invoice_logo))
			$mpdf->imageVars['logo'] = file_get_contents(Upload::$uploadPath . $settings->invoice_logo->getMiniFile());

		$oldViewPath = View::getViewPath();
		View::setViewPath(__DIR__ . '/templates/');
		$mpdf->SetHTMLHeader(
			View::view('invoice.header', $vars, true)
		);
		$mpdf->WriteHTML(
			View::view('invoice.body', $vars, true)
		);
		$mpdf->SetHTMLFooter(
			View::view('invoice.footer', $vars, true)
		);
		// Set view path back to original
		View::setViewPath($oldViewPath);

		$companyName = self::$appSettings->invoice_company;
		$companyName = str_replace(['s.r.o.', 'a.s.'], '', $companyName);
		$companyName = self::slugify($companyName);
		if (!$output) {
			$mpdf->Output($companyName . '-order-' . $order->invoice->getNumber() . '.pdf', Destination::INLINE);
		} else {
			if (!file_exists(self::INVOICE_DIR)) {
				mkdir(self::INVOICE_DIR, 0777, true);
			}
			$mpdf->Output(self::INVOICE_DIR . $companyName . '-order-' . $order->invoice->getNumber() . '.pdf', Destination::FILE);
		}
	}

	public static function searchCustomer($query)
	{
		return [
			Database::Model('Customer')
				->where('c.name', '%', $query)
				->orWhere('c.surname', '%', $query)
				->orWhere('c.email', '%', $query),
			['id', 'fullNameAndEmail']
		];
    }

	/**
	 * @param $id
	 */
	public static function payed($id)
	{
		$entity = Database::Model('Order')->find($id);
		if (!empty($entity)) {
			$entity->payed = !$entity->payed;
			Database::saveAndFlush($entity);
		}
		Router::redirectToURL(Router::$prevLink);
	}

	/**
	 * @param $ids
	 */
	public function createExpedition($ids)
	{
		$expedition = new ExpeditionsModel();
		$response = $expedition->createExpedition((array)$ids);
		if (!empty($response['msg'])) {
			$this->alert($response['msg'], $response['type']);
		}
		Router::redirectToURL(Router::$prevLink);
	}

	public function orderDataApi($id)
	{

		$order = Database::Model('Order')->find($id);
		if (empty($data)) echo json_encode([]);
		echo json_encode([
			'id' => $order->id
		]);
	}

}
