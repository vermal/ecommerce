<?php

namespace Vermal\Ecommerce\Modules\Orders;


use PhpOffice\PhpSpreadsheet\Calculation\DateTime;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\EcommerceServiceProvider;
use Vermal\Ecommerce\Modules\Cart\Cart;
use Vermal\Ecommerce\Modules\Customers\Addresses;
use Vermal\Ecommerce\Modules\Customers\Models\AddressModel;
use Vermal\Ecommerce\Modules\Customers\Models\CustomerModel;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Ecommerce\Modules\Orders\Entities\OrderItem;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductPrice;
use Vermal\Ecommerce\Modules\Settings\Entities\Setting;
use Vermal\Ecommerce\Modules\Tax\Repositories\TaxRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\View;

class OrderModel
{

	/** @var array $values */
	private $values = [];

	/**
	 * @var Form $form
	 */
	public $form;

	/** @var string $currency */
	private $currency;

	private $totalPrice = 0;
	private $totalPriceTax = 0;
	private $priceItems = 0;
	private $priceItemsTax = 0;
	private $discount = 0;

	public function __construct($values, $form)
	{
		$this->values = $values;
		$this->form = $form;
		$this->currency = $this->values['currency'];
	}

	/**
	 * Create new order
	 *
	 * @return bool
	 */
	public function store()
	{
		$order = Entity::getEntity('Order');
		$order->status = Orders::NEW_;
		$order->softDelete = false;
		$order->payed = false;
		return $this->hydrate($order);
	}

	/**
	 * Edit existing order
	 *
	 * @param int|Order $order
	 * @return bool
	 */
	public function save($order)
	{
		if (is_int($order))
			$order = Database::Model('Order')->find($order);
		if (empty($order)) {
			$this->form->setGlobalError("This order doesn't exist", 'danger');
			return false;
		}
		$this->currency = $order->currency;
		if (
			in_array($order->status, [Orders::CANCELLED, Orders::REFUNDED, Orders::FAILED, Orders::COMPLETED, Orders::PACKAGED, Orders::DISPATCHED])
			&& !Auth::hasRole('developer')
		) {
			$this->form->setGlobalError('order.locked');
			return false;
		}
		if (isset($this->values['status'])) {
			$order->status = $this->values['status'];
		}

		// Clear products
//		foreach ($order->items as $item) {
//			if ($item->type == OrderItem::PRODUCT && $order->items->contains($item))
//				$order->items->removeElement($item);
//		}
		$order->clearItems();
		Database::saveAndFlush($order);
		return $this->hydrate($order);
	}


	/**
	 * Hydrate object
	 *
	 * @param Order $entity
	 * @return bool
	 */
	private function hydrate($entity)
	{
		if (!$this->handleCustomer($entity))
			return false;
		if (!$this->handleAddresses($entity))
			return false;
		if (!$this->handleProducts($entity))
			return false;
		if (!$this->handleDiscounts($entity))
			return false;

		// Recalculate prices
		$this->priceItems = 0;
		$this->priceItemsTax = 0;
		foreach ($entity->items as $item) {
			$this->priceItems += $item->price;
			$this->priceItemsTax += $item->price_tax_amount;
		}
		$this->totalPrice = $this->priceItems;
		$this->totalPriceTax = $this->priceItemsTax;


		if (!$this->handleShipping($entity))
			return false;
		if (!$this->handlePayment($entity))
			return false;

		$entity->price = $this->totalPrice;
		$entity->price_tax_amount = $this->totalPriceTax;
		$entity->price_items = $this->priceItems;
		$entity->price_items_tax_amount = $this->priceItemsTax;
		$entity->currency = $this->currency;

		$entity->discount = -$this->discount;

		if (!empty($this->values['predicted_delivery']))
			$entity->predicted_delivery = new \DateTime(date('Y-m-d H:i:s', strtotime($this->values['predicted_delivery'])));

		// Create new invoice number after everything is completed
		if ($entity->invoice == null) {
			$lastInvoice = Database::Model('OrderInvoice')
				->order('o.number', 'DESC')
				->limit(1)
				->get();
			if (!empty($lastInvoice)) $lastInvoice = $lastInvoice[0]->number + 1;
			else $lastInvoice = 1;
			$mask = Controller::$appSettings->invoice_number_mask;
			$mask = str_replace('Y', date('Y'), $mask);
			$mask = str_replace('m', date('m'), $mask);
			$mask = str_replace('d', date('d'), $mask);
			$invoice = Entity::getEntity('OrderInvoice');
			$invoice->prefix = $mask;
			$invoice->number = $lastInvoice;
			$entity->invoice = $invoice;
			Database::save($invoice);
			$entity = Database::saveAndFlush($entity);

			// Generate invoice
			Orders::generateInvoice($entity->id, Controller::$appSettings, true);
		}

		if (!empty($this->values['note'])) {
			$entity->note = $this->values['note'];
		}

		$entity = Database::saveAndFlush($entity);

		return $entity;
	}

	/**
	 * Handle customer
	 *
	 * @param $entity
	 * @return bool
	 */
	private function handleCustomer(&$entity)
	{
		$values = $this->values;
		if (array_key_exists('customer', $values)) {
			$customer = $values['customer'];

			// Check if customer is set to new and email is not empty
			if ($customer == 'guest' && empty($values['email'])) {
				$this->form->setError('email', 'Please enter correct email address');
				return false;
			}

			// Check if customer already exist by email
			if ($customer == 'guest' && !empty($values['email'])) {
				$customerEntity = Database::Model('Customer')->where('c.email', $values['email'])->find();
				if (empty($customerEntity)) {
					unset($customerEntity);
				} else {
					$customer = $customerEntity->id;
				}
			}

			// If customer is guest create new one
			if ($customer == 'guest') {
				// Create new customer with addresses
				$customerEntity = (new CustomerModel([
					'name' => $values['billing_name'],
					'surname' => $values['billing_surname'],
					'email' => $values['email'],
					'billing' => [
						'name' => $values['billing_name'],
						'surname' => $values['billing_surname'],
						'companyName' => $values['billing_company'],
						'street' => $values['billing_address'],
						'city' => $values['billing_city'],
						'psc' => $values['billing_postcode'],
						'country' => $values['billing_country'],
						'phone' => $values['billing_phone'],
					],
					'shipping' => [
						'name' => $values['shipping_name'],
						'surname' => $values['shipping_surname'],
						'street' => $values['shipping_address'],
						'city' => $values['shipping_city'],
						'psc' => $values['shipping_postcode'],
						'country' => $values['shipping_country'],
					]
				], $this->form))->store();
				$customerEntity->last_order_at = new \DateTime();
				$entity->customer = $customerEntity;
			} else {
				/** @var Customer $customerEntity */
				if (empty($customerEntity)) {
					$customerEntity = Database::Model('Customer')->find($customer);
				}

				if (isset($values['billing']) && $values['billing'] == 'new') {
					$billingAddress = (new AddressModel([
						'type' => Addresses::BILLING,
						'name' => $values['billing_name'],
						'surname' => $values['billing_surname'],
						'street' => $values['billing_address'],
						'city' => $values['billing_city'],
						'psc' => $values['billing_postcode'],
						'country' => $values['billing_country'],
						'phone' => $values['billing_phone'],
						'companyName' => $values['billing_company'],
						'ico' => $values['billing_ico'],
						'dic' => $values['billing_dic'],
						'ic_dph' => $values['billing_ic_dph'],
					]))->store();
					$billingAddress->customer = $customerEntity;
					Database::save($billingAddress);
					$customerEntity->addAddress($billingAddress);
					if (empty($customerEntity->billing_address)) {
						$customerEntity->billing_address = $billingAddress;
					}
				}

				if (isset($values['shipping']) && $values['shipping'] == 'new') {
					$shippingAddress = (new AddressModel([
						'type' => Addresses::SHIPPING,
						'name' => $values['shipping_name'],
						'surname' => $values['shipping_surname'],
						'street' => $values['shipping_address'],
						'city' => $values['shipping_city'],
						'psc' => $values['shipping_postcode'],
						'country' => $values['shipping_country'],
					]))->store();
					$shippingAddress->customer = $customerEntity;
					Database::save($shippingAddress);
					$customerEntity->addAddress($shippingAddress);
					if (empty($customerEntity->shipping_address)) {
						$customerEntity->shipping_address = $shippingAddress;
					}
				}

				// Find customer
				$customerEntity->last_order_at = new \DateTime();
				$entity->customer = $customerEntity;
			}
		}
		return true;
	}

	/**
	 * Assign values to address fields
	 *
	 * @param $entity
	 * @return  bool
	 */
	private function handleAddresses(&$entity)
	{
		$values = $this->values;
		if (array_key_exists('billing_name', $values)) {
			$entity->billing_name = Sanitizer::applyFilter('trim|strip_tags', $values['billing_name']);
			$entity->billing_surname = Sanitizer::applyFilter('trim|strip_tags', $values['billing_surname']);
			$entity->billing_address = Sanitizer::applyFilter('trim|strip_tags', $values['billing_address']);
			$entity->billing_city = Sanitizer::applyFilter('trim|strip_tags', $values['billing_city']);
			$entity->billing_postcode = Sanitizer::applyFilter('trim|strip_tags', $values['billing_postcode']);
			$entity->billing_country = Sanitizer::applyFilter('trim|strip_tags', $values['billing_country']);
			$entity->billing_phone = Sanitizer::applyFilter('trim|strip_tags', $values['billing_phone']);
			$entity->billing_company = Sanitizer::applyFilter('trim|strip_tags', $values['billing_company']);
			$entity->billing_ico = Sanitizer::applyFilter('trim|strip_tags', $values['billing_ico']);
			$entity->billing_dic = Sanitizer::applyFilter('trim|strip_tags', $values['billing_dic']);
			$entity->billing_ic_dph = Sanitizer::applyFilter('trim|strip_tags', $values['billing_ic_dph']);
		}
		if (array_key_exists('shipping_name', $values) &&
			( !empty($values['shipping_name']) && !empty(!empty($values['shipping_surname'])))) {
			$entity->shipping_name = Sanitizer::applyFilter('trim|strip_tags', $values['shipping_name']);
			$entity->shipping_surname = Sanitizer::applyFilter('trim|strip_tags', $values['shipping_surname']);
			$entity->shipping_address = Sanitizer::applyFilter('trim|strip_tags', $values['shipping_address']);
			$entity->shipping_city = Sanitizer::applyFilter('trim|strip_tags', $values['shipping_city']);
			$entity->shipping_postcode = Sanitizer::applyFilter('trim|strip_tags', $values['shipping_postcode']);
			$entity->shipping_country = Sanitizer::applyFilter('trim|strip_tags', $values['shipping_country']);
		} else {
			$entity->shipping_name = Sanitizer::applyFilter('trim|strip_tags', $values['billing_name']);
			$entity->shipping_surname = Sanitizer::applyFilter('trim|strip_tags', $values['billing_surname']);
			$entity->shipping_address = Sanitizer::applyFilter('trim|strip_tags', $values['billing_address']);
			$entity->shipping_city = Sanitizer::applyFilter('trim|strip_tags', $values['billing_city']);
			$entity->shipping_postcode = Sanitizer::applyFilter('trim|strip_tags', $values['billing_postcode']);
			$entity->shipping_country = Sanitizer::applyFilter('trim|strip_tags', $values['billing_country']);
		}
		return true;
	}

	/**
	 * Assign products
	 *
	 * @param Order $entity
	 * @return bool
	 */
	private function handleProducts(&$entity)
	{
		// todo: do not delete products that doesn't exist anymore
		$values = $this->values;
		$items = $values['items'];
		$ids = [];
		$qtys = [];
		if (!empty($items)) {
			foreach ($items as $key => $item) {
				if (empty($item['id']) || (!empty($values['item']) && $values['item']->type->{$key} !== OrderItem::PRODUCT)) continue;
				$ids[] = $item['id'];
				$qtys[] = $item['qty'];
			}
		}
		usort($items, function ($a, $b){
			if ($a == $b)
				return 0;
			return ($a['id'] < $b['id']) ? -1 : 1;
		});

		$products = Database::Model('Product')
			->where('p.id', $ids)
			->order('p.id', 'ASC')
			->get();
		foreach ($products as $key => $product) {
			$qty = $qtys[$key];
			/** @var Product $product */
			/** @var OrderItem $orderItem */
			$orderItem = Entity::getEntity('OrderItem');

			$orderItem->type = OrderItem::PRODUCT;
			$orderItem->title = $product->title;
			$orderItem->te = $product->te;
			$orderItem->shortTe = $product->shortTe;
			$orderItem->ean = $product->ean;
			$orderItem->weight = $product->weight;
			$orderItem->length = $product->length;
			$orderItem->width = $product->width;
			$orderItem->height = $product->height;
			$orderItem->qty = $qty;
			$orderItem->product = $product;

			$orderItem->currency = $this->currency;
			$orderItem->simpleMultimedia = $product->simpleMultimedia;

			// Add meta todo

			// Get tax
			$categories = [];
			foreach ($product->getCategories() as $category) {
				$categories[] = $category->id;
			}
			// todo: better solution for product tax rate select
			$taxRate = TaxRepository::getTaxRate($values['billing_country'], $values['billing_postcode'], $categories);
			if (empty($taxRate)) {
				$this->form->setGlobalError('No tax rate available for this product', 'warning');
				return false;
			}
			$taxRate = $taxRate[0];
			$price = $this->getProductPrice($product->getPrice($orderItem->currency));
			$tax = EcommerceServiceProvider::calculateTax($price, $taxRate->rate);
			$orderItem->price = ((float)($price - ($tax < 0 ? $tax : 0)) * $qty);
			$orderItem->price_tax = (float)$taxRate->rate;
			$orderItem->price_tax_amount = (float)$tax * $qty;
			$orderItem->price_per_unit = (float)($price - ($tax < 0 ? $tax : 0));
			$orderItem->price_per_unit_tax_amount = (float)$tax;
			$this->totalPrice += $orderItem->price;
			$this->totalPriceTax += $orderItem->price_tax_amount;
			$this->priceItems += $orderItem->price;
			$this->priceItemsTax += $orderItem->price_tax_amount;

			// Save item in order
			Database::save($orderItem);
			$orderItem->order = $entity;
			$entity->addItem($orderItem);
		}
		return true;
	}

	/**
	 * Assign coupons
	 *
	 * @param Order $entity
	 * @return bool
	 */
	private function handleDiscounts(&$entity)
	{
		$cart = Cart::getInstance()->getCart();

		// get coupons from cart if not admin
		$coupons = [];
		if (Router::$currentName !== 'admin.order.update' && Router::$currentName !== 'admin.order.store') {
			foreach ($cart->coupons as $coupon) {
				$coupons[] = $coupon;
			}
		}

		// get coupons from form
		$values = $this->values;
		if (!empty($values['items']) && !empty($values['item'])) {
			foreach ($values['items'] as $key => $item) {
				if (empty($item['id']) || $values['item']->type->{$key} !== OrderItem::DISCOUNT) continue;
				$ids[] = $item['id'];
			}
			if (!empty($ids)) {
				$coupons_ = Database::Model('Coupon')->where('c.id', $ids)->get();
				foreach ($coupons_ as $coupon) {
					$coupons[] = $coupon;
				}
			}
		}

		$couponPriceEntity = $cart;
		if (Router::$currentName === 'admin.order.update' || Router::$currentName === 'admin.order.store') {
			$couponPriceEntity = $entity;
		}
		foreach ($coupons as $key => $coupon) {
			/** @var OrderItem $orderItem */
			$orderItem = Entity::getEntity('OrderItem');

			$orderItem->type = OrderItem::DISCOUNT;
			$orderItem->title = 'Kupón - ' . $coupon->code;
			$orderItem->qty = 1;
			$orderItem->currency = $this->currency;

			list($discount, $tax) = Cart::calculateCouponPrice($coupon, $couponPriceEntity, $values['billing_country'], $values['billing_postcode']);

			// Set discount
			$cart->discount = -$discount;
			$cart->discount_tax = 0;
			$cart->discount_tax_amount = 0;

			// Get tax
			$orderItem->price = -$discount;
			$orderItem->price_tax = 0;
			$orderItem->price_tax_amount = 0;
			$orderItem->price_per_unit = -$discount;
			$orderItem->price_per_unit_tax_amount = 0;
			$orderItem->coupon = $coupon;

			$this->totalPrice += $orderItem->price;
			$this->totalPriceTax += $orderItem->price_tax_amount;
			$this->priceItems += $orderItem->price;
			$this->priceItemsTax += $orderItem->price_tax_amount;

			$this->discount += $discount;

			// Save item in order
			Database::save($orderItem);
			$orderItem->order = $entity;
			$entity->addItem($orderItem);
		}
		return true;
	}

	/**
	 * @param ProductPrice $productPrice
	 * @return mixed
	 */
	private function getProductPrice($productPrice)
	{
		return $productPrice->getProductPrice();
	}

	/**
	 * @param $entity
	 * @return bool
	 */
	private function handleShipping(&$entity)
	{
		$shipping = Database::Model('ShippingMethod')->find($this->values['shippingMethod']);
		$shippingPrice = $shipping->getPrice($this->currency)->price;
		$shippingTaxRate = Controller::$appSettings->shipping_tax;
		$shippingTaxAmount = EcommerceServiceProvider::calculateTax($shippingPrice, $shippingTaxRate);
		$shippingPrice = (float)($shippingPrice - ($shippingTaxAmount < 0 ? $shippingTaxAmount : 0));
		$entity->shipping = $shipping;
		$entity->shipping_title = $shipping->title;
		$entity->shipping_amount = $shippingPrice;
		$entity->shipping_tax_amount = $shippingTaxAmount;
		$entity->shipping_tax = $shippingTaxRate;
		$this->totalPrice += $shippingPrice;
		$this->totalPriceTax += $shippingTaxAmount;
		return true;
	}

	/**
	 * @param $entity
	 * @return bool
	 */
	private function handlePayment(&$entity)
	{
		$payment = Database::Model('PaymentMethod')->find($this->values['paymentMethod']);
		$paymentPrice = $payment->getPrice($this->currency)->price;
		$paymentTaxRate = Controller::$appSettings->payment_tax;
		$paymentTaxAmount = EcommerceServiceProvider::calculateTax($paymentPrice, $paymentTaxRate);
		$paymentPrice = (float)($paymentPrice - ($paymentTaxAmount < 0 ? $paymentTaxAmount : 0));
		$entity->payment = $payment;
		$entity->payment_title = $payment->title;
		$entity->payment_amount = $paymentPrice;
		$entity->payment_tax_amount = $paymentTaxAmount;
		$entity->payment_tax = $paymentTaxRate;
		$this->totalPrice += $paymentPrice;
		$this->totalPriceTax += $paymentTaxAmount;
		return true;
	}

}
