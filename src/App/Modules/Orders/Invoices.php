<?php

namespace Vermal\Ecommerce\Modules\Orders;


use Vermal\Admin\Defaults\ControllerExtension;
use Vermal\Controller;
use Vermal\Database\Database;

class Invoices extends Controller
{

	use ControllerExtension;

	public function __construct()
	{
		parent::__construct();
		$this->settingsSetup();
	}

	/**
	 * @param $token
	 * @throws \Mpdf\MpdfException
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function invoiceTokenized($token)
	{
		$order = Database::Model('Order')
			->where('o.token', $token)->first();
		Orders::generateInvoice($order->id, $this->settings, false);
	}

}
