<?php

namespace Vermal\Ecommerce\Modules\Product\Models;

use Vermal\Admin\Defaults\AbstractModel;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\Upload;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductAttribute;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Router;
use Vermal\Traits\DataGridSort;

class ProductModel extends AbstractModel
{

	use DataGridSort;

	/**
	 * Hydrate database object
	 *
	 * @param Product $product
	 * @param $post
	 * @param $update
	 * @return object
	 * @throws \Exception
	 */
	public function hydrate($product, $post, $update = false)
	{
		$product->ean = Sanitizer::applyFilter('trim|digit', $post->ean ?? 0);
		$product->status = $post->status ?? 1;

		$product->weight = Sanitizer::applyFilter('trim|float', $post->weight ?? null);
		$product->length = Sanitizer::applyFilter('trim|float', $post->length ?? null);
		$product->width = Sanitizer::applyFilter('trim|float', $post->width ?? null);
		$product->height = Sanitizer::applyFilter('trim|float', $post->height ?? null);

		$product->stock = Sanitizer::applyFilter('trim|float', $post->stock ?? 0);

		if (!$update) {
			$product->order = $this->getMaxValue('Product');
			$product->active = $post->active ?? 1;
		}
		if (isset($post->active))
			$product->active = $post->active;

		// Add translation to content
		if ($update) {
			$product->clearTr();
			Database::saveAndFlush($product);
		}
		foreach (Languages::get() as $lang)
		{
			// Content
			$title = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});

			$entity = 'ProductTr';
			Database::translate($product, $entity, 'title', $lang, $title);
			Database::translate($product, $entity, 'te', $lang, $this->sanitize('trim', $post->{$lang . '_te'}));
			Database::translate($product, $entity, 'shortTe', $lang, $this->sanitize('trim', $post->{$lang . '_shortTe'}));
			Database::translate($product, $entity, 'slug', $lang, self::slugify($title));

			// SEO
			Database::translate($product, $entity, 'keywords', $lang, $this->sanitize('trim|escape|strip_tags', $post->{$lang . '_keywords'}));
		}

		// Add categories to product
		if ($update) $product->clearCategories();
		if (!empty($post->categories)) {
			foreach ($post->categories as $category) {
				$category = Sanitizer::applyFilter('trim|digit', $category);
				$newCategory = Database::Model('ProductCategory')->find($category);

				// Check if category exist
				if ($newCategory === null) continue;

				$product->addCategory($newCategory);
				Database::save($newCategory);
			}
		}

		// Add tags to product
		if ($update) $product->clearTags();
		if (!empty($post->tags)) {
			foreach ($post->tags as $tag) {

				if (is_numeric($tag)) {
					$tag = Sanitizer::applyFilter('trim|digit', $tag);
					$newTag = Database::Model('ProductMeta')->find($tag);
				} else {
					$newTag = ProductMetaRepository::addMeta('tag', ['value' => $tag], null, 'id');
				}

				// Check if category exist
				if ($newTag === null) continue;

				$product->addTag($newTag);
				Database::save($newTag);
			}
		}

		// Add attributes to product
		if ($update) $product->clearAttributes();
		if (!empty($post->attribute)) {
			foreach ($post->attribute as $attribute) {
				if (!is_array($attribute) && !is_object($attribute)) $attribute = [$attribute];
				foreach ($attribute as $attr) {
					$attr = Sanitizer::applyFilter('trim|digit', $attr);
					$newAttribute = Database::Model('ProductAttribute')->find($attr);

					// Check if category exist
					if ($newAttribute === null) continue;

					$product->addAttribute($newAttribute);
					Database::save($newAttribute);
				}
			}
		}

		// Add prices
		if ($update) $product->clearPrices();
		foreach (Currencies::get() as $curency) {
			if (!isset($post->{$curency . '_price'})) continue;
			$productPrice = Entity::getEntity('ProductPrice');

			// Currency
			$productPrice->currency = $curency;

			// Price settings
			$productPrice->price = Sanitizer::applyFilter('trim|float', $post->{$curency . '_price'});
			$productPrice->salePrice = Sanitizer::applyFilter('trim|float', $post->{$curency . '_salePrice'} ?? null);
			$productPrice->isSale = Sanitizer::applyFilter('cast:bool', $post->{$curency . '_isSale'} ?? false);

			// Get sale start & sale end
			$range = $post->{$curency . '_saleRange'} ?? null;
			if (!empty($range)) {
				$range = explode(' - ', $range);
				$saleStart = new \DateTime($range[0]);
				$saleEnd = new \DateTime($range[1]);

				if($saleEnd < new \DateTime()) {
					$productPrice->saleStart = null;
					$productPrice->saleEnd = null;
				} else {
					$productPrice->saleStart = $saleStart;
					$productPrice->saleEnd = $saleEnd;
				}
			} else {
				$productPrice->saleStart = null;
				$productPrice->saleEnd = null;
			}

			// Save
			$productPrice->product = $product;
			$product->addPrice($productPrice);
			Database::save($productPrice);
		}

		// Remove image
		if (empty($post->featured_image_hidden_image)) {
			$product->simpleMultimedia = null;
		}
		// Upload file
		if (!empty($_FILES['featured_image']['name'])) {
			$files = Upload::uploadFile('featured_image', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$product->simpleMultimedia = Database::Model('Multimedia')->find($file);
			}
		}

		// Store custom fields
		$product = apply_filters('ecommerce_product_hydrate', $product, $post);

		return $product;
	}

	/**
	 * Generate product variation
	 *
	 * @param $post
	 * @param $id
	 * @param $redirect
	 * @throws \Exception
	 */
	public function productVariation($post, $id, $redirect)
	{
		/** @var Product $product */
		$product = Database::Model('Product')->find($id);

		if ($post->submit == 'all') {
			$variations = [];
			$attributes = $product->getAttributes();
			foreach ($attributes as $attr) {
				if (!$attr->obj->allowVariation) continue;
				$variations_ = [];
				foreach ($attr->children as $a) {
					$variations_[] = $a->id;
				}
				$variations[] = $variations_;
			}
			$variations = generate_combinations($variations);
		} else if ($post->submit == 'single') {
			$variations = [(array)$post->variation];
		} else if ($post->submit == 'remove-all') {
			foreach ($product->children as $child) {
				$child->clearTr();
				$child->clearMeta();
				$child->clearPrices();
				$child->clearCategories();
				Database::saveAndFlush($child);
				Database::Model('Product')->delete($child->id);
			}
			$this->alert($this->_('product.deletedVariation'));
			Router::redirect($redirect, ['id' => $product->id]);
		}

		foreach ($variations as $variation) {
			// Product variation validation
			if ($this->productHasVariation($id, $variation)) {
				if ($post->submit != 'all') {
					$this->alert($this->_('product.variationExist'), 'danger');
					Router::redirect($redirect, ['id' => $product->id]);
				} else continue;
			}

			/** @var Product $childProduct */
			$childProduct = Entity::getEntity('Product');
			$productTitleByAttribute = [];
			foreach ($variation as $attribute) {

				/** @var ProductAttribute $attribute */
				$attribute = Database::Model('ProductAttribute')->find(Sanitizer::applyFilter('trim|digit', $attribute));
				if ($attribute === null || empty($attribute->parent)) continue;

				// Skip if attribute doesn't allow variations
				if (empty($attribute->parent->allowVariation)) continue;

				foreach (Languages::get() as $lang) {
					if (!isset($productTitleByAttribute[$lang])) $productTitleByAttribute[$lang] = [];
					$productTitleByAttribute[$lang][] = $attribute->getTr($lang, 'value');
				}

				$childProduct->addAttribute($attribute);
				Database::save($attribute);
			}

			// Copy values from parent product
			foreach (Languages::get() as $lang) {
				$title = $product->getTr($lang, 'title') . ' -  ' . implode(', ', $productTitleByAttribute[$lang]);
				$post->{$lang . '_title'} = $title;
				$post->{$lang . '_te'} = '';
				$post->{$lang . '_shortTe'} = '';
				$post->{$lang . '_slug'} = '';
				$post->{$lang . '_keywords'} = '';
			}

			$post->ean = $product->ean;
			$post->status = 1;
			$post->sort = 1;

			$post->weight = $product->weight;
			$post->length = $product->length;
			$post->width = $product->width;
			$post->height = $product->height;

			$post->stock = $product->stock;

			// Hydrate child
			$childProduct = $this->hydrate($childProduct, $post);

			$childProduct->parent = $product;
			$product->addChild($childProduct);
			Database::save($childProduct);
		}
		Database::saveAndFlush($product);

		$this->alert($this->_('product.createdVariations'), 'success');
		Router::redirect($redirect, ['id' => $product->id]);
	}

	/**
	 * Check if product has variation
	 *
	 * @param $id
	 * @param $variation
	 * @return bool
	 */
	private function productHasVariation($id, $variation)
	{
		// Check if variation already exist
		$qb = Database::Model('Product')
			->where('p.parent', '=', $id);

		foreach (array_values((array)$variation) as $key => $value) {
			$alias = 'pm' . $key;
			$qb
				->join('p.attributes', $alias)
				->where($alias . '.id', $value);
		}

		return empty($qb->get()) ? false : true;
	}

}
