<?php

namespace Vermal\Ecommerce\Modules\Product;


use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\App;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMetaTr;
use Vermal\Ecommerce\Modules\Product\Entities\ProductTr;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Admin\Upload;
use Vermal\Database\Database;


class ProductTags extends Controller
{

    /**
     * ProductCategories constructor.
     */
    public function __construct()
    {
        parent::__construct();
	 	$this->requiredPermission('products', $this->CRUDAction);
        $this->addToBreadcrumb('product_category', 'admin.product-tag.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('ProductMeta')->where('p.key', 'tag');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('value', $this->_('product.title'));

        // Add actions
        $datagrid->addEdit(['admin.product-tag.edit', ['id']]);
        $datagrid->addDelete(['admin.product-tag.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.createWithTr', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => ['title']
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.product-tag.create');
        }

        // Hydrate data
        $this->hydrate($post);

        // Redirect with message
        $this->alert($this->_('productTag.created'));
        Router::redirect('admin.product-tag.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.product-tag.update', ['id' => $id]));

        /** @var ProductMeta $meta */
        $meta = ProductMetaRepository::getMeta('tag', $id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.product-tag.edit', ['id' => $id], [$meta->value]);

        // Set values
        foreach (Languages::get() as $lang) {
            $form->getComponent($lang . '_title')->setValue($meta->getTr($lang, 'value'));
        }

        View::view('crud.createWithTr', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => ['title']
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        // Hydrate data
        $this->hydrate($post, $id);

        // Send message
        $this->alert($this->_('productTag.edited'));
    }

    /**
     * @param int $id
     * @param object $post
     */
    private function hydrate($post, $id = null)
    {
        $data = [];
        foreach (Languages::get() as $lang) {
            $data[$lang] = [
                'value' => Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'})
            ];
        }
        ProductMetaRepository::addMeta('tag', $data, $id, 'id');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        ProductMetaRepository::deleteMeta($id);
        $this->alert($this->_('productTag.deleted'));
        Router::redirect('admin.product-tag.index');
    }

    /**
     * Create product category form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('product-tag', routerLink('admin.product-tag.store'));

        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', 'Tag name')->min(2);
        }

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
