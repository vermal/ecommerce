<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\App;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Tags;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_product_review")
 * @ORM\HasLifecycleCallbacks
 **/
class ProductReview extends Model
{

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="text")
     */
    protected $te;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $review;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $name;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $active;

    /**
     * @ORM\ManyToOne(targetEntity="\Product", inversedBy="reviews")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

}
