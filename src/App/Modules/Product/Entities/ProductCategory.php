<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Database\MagicAccessor;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_product_category")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\ProductCategoryTr")
 **/
class ProductCategory
{
    use MagicAccessor;
    use Translatable;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $te;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="string", length=255)
	 */
	protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="\ProductCategory", mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\ProductCategory", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\ManyToMany(targetEntity="\Product", mappedBy="categories")
     */
    protected $products;

    /**
     * @ORM\OneToMany(
     *   targetEntity="\ProductCategoryTr",
     *   mappedBy="object",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     */
    protected $translations;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * Check if has child
     *
     * @param $id
     * @return bool
     */
    public function hasChild($id)
    {
        foreach ($this->children as $child) {
            if ($child->id == $id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $child
     */
    public function addChild($child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove children
     */
    public function removeAllChildren() {
        $this->children->clear();
    }

	/**
	 * @param $product
	 */
	public function addProduct($product)
	{
		$this->products[] = $product;
    }


	public function getLevel($level = 1)
	{
		if (!empty($this->parent)) {
			$level++;
			$this->parent->getLevel($level);
		}
		return $level;
    }

	public function getIdsToParent($category, $ids = "")
	{
		if (empty($ids)) $ids = [];
		if (!empty($category->parent)) {
			$ids[] = $category->parent->id;
			$ids = $this->getIdsToParent($category->parent, $ids);
		}
		return $ids;
    }
}
