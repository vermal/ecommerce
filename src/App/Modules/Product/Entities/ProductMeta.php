<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Database\MagicAccessor;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_productmeta")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\ProductMetaTr")
 **/
class ProductMeta
{
    use MagicAccessor;
    use Translatable;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string", name="meta_key") **/
    protected $key;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="text", name="meta_value")
     */
    protected $value;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", name="meta_slug", nullable=true)
	 */
	protected $slug;

    /**
     * @ORM\OneToMany(targetEntity="\ProductMeta", mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\ProductMeta", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

    /**
     * @ORM\ManyToMany(targetEntity="\Product", mappedBy="meta")
     */
    protected $products;

    /**
     * @ORM\OneToMany(
     *   targetEntity="\ProductMetaTr",
     *   mappedBy="object",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     */
    protected $translations;

    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->translations = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

    /**
     * Check if has child
     *
     * @param $id
     * @return bool
     */
    public function hasChild($id)
    {
        foreach ($this->children as $child) {
            if ($child->id == $id) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param $child
     */
    public function addChild($child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove children
     */
    public function removeAllChildren() {
        $this->children->clear();
    }

	/**
	 * @param $product
	 */
	public function addProduct($product)
	{
		$this->products[] = $product;
    }

    /**
     * To array
     *
     * @return array
     */
    public function toArray()
    {
        $data = [
            'id' => $this->id,
            'key' => $this->key,
            'value' => $this->value,
            'children' => []
        ];
        foreach ($this->children as $child) {
            $data['children'][] = $child->toArray();
        }
        return $data;
    }

	/** SELECT2 SUPPORT */
	public function getText()
	{
		return $this->value;
	}
}
