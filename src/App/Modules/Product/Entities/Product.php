<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\App;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Tags;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_product")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\ProductTr")
 **/
class Product extends Model
{
    use Translatable;
    use Categories;
    use Attributes;
    use Tags;
    public $metaData = [];

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $title;

    /** @Gedmo\Translatable
     * @ORM\Column(type="text")
     */
    protected $te;

    /** @Gedmo\Translatable
     * @ORM\Column(type="text", name="short_te")
     */
    protected $shortTe;

    /** @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $keywords;

    /** @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $slug;

    // INFO
    /** @ORM\Column(type="string", nullable=true) **/
    protected $ean;

    // SHIPPING
    /** @ORM\Column(type="string", nullable=true) **/
    protected $weight;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $length;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $width;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $height;

    /** @ORM\Column(type="integer", nullable=true) **/
    protected $stock;

    // OTHERS
    /** @ORM\Column(type="string") **/
    protected $status;

    /** @ORM\Column(type="integer", name="sort") **/
    protected $order;

    /**
     * @ORM\OneToMany(targetEntity="\ProductPrice", mappedBy="product", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToMany(targetEntity="\ProductMeta", inversedBy="product")
     * @ORM\JoinTable(name="ecommerce_product_productmeta")
     */
    protected $meta;

	/**
	 * @ORM\ManyToMany(targetEntity="\ProductCategory", inversedBy="products")
	 * @ORM\JoinTable(name="ecommerce_product_product_category")
	 */
	protected $categories;

	/**
	 * @ORM\ManyToMany(targetEntity="\ProductAttribute", inversedBy="products")
	 * @ORM\JoinTable(name="ecommerce_product_product_attribute")
	 */
	protected $attributes;

    /**
     * @ORM\ManyToOne(targetEntity="\Multimedia")
     * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
     */
    protected $simpleMultimedia;

    /**
     * @ORM\ManyToMany(targetEntity="\Multimedia")
     * @ORM\JoinTable(name="ecommerce_product_multimedia",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $multimedia;

    /**
     * @ORM\OneToMany(
     *   targetEntity="\ProductTr",
     *   mappedBy="object",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     */
    protected $translations;

    /**
     * @ORM\OneToMany(targetEntity="\Product", mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $children;

    /**
     * @ORM\ManyToOne(targetEntity="\Product", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    protected $parent;

	/**
	 * @ORM\OneToMany(targetEntity="\ProductReview", mappedBy="product", cascade={"persist", "remove"}, orphanRemoval=true)
	 */
	protected $reviews;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $active;

    /**
     * Product constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->translations = new ArrayCollection();
        $this->meta = new ArrayCollection();
        $this->multimedia = new ArrayCollection();
        $this->price = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->reviews = new ArrayCollection();
		$this->categories = new ArrayCollection();
		$this->attributes = new ArrayCollection();
    }

    /**
     * Clear prices
     */
    public function clearPrices()
    {
        $this->price->clear();
    }

    /**
     * @param ProductPrice $productPrice
     */
    public function addPrice(ProductPrice $productPrice)
    {
        $this->price[] = $productPrice;
    }

    /**
     * @param $currency
     * @return ProductPrice
     */
    public function getPrice($currency)
    {
        foreach ($this->price as $price) {
            if ($price->currency == $currency) return $price;
        }
		if (!empty($this->parent))
			return $this->parent->getPrice($currency);
    }

    /**
     * @param $child
     */
    public function addChild($child)
    {
        $this->children[] = $child;
    }

    /**
     * Remove children
     */
    public function removeAllChildren() {
        $this->children->clear();
    }

	/**
	 * @param $review
	 */
	public function addReview($review)
	{
		$this->reviews[] = $review;
	}

	/**
	 * Remove review
	 *
	 * @param $review
	 */
	public function removeReview($review) {
		if ($this->reviews->contains($review))
			$this->reviews->removeElement($review);
	}

	/**
	 * Remove reviews
	 */
	public function clearReviews() {
		$this->reviews->clear();
	}

	/**
	 * get reviews score
	 *
	 * @param int $default
	 * @return float
	 */
	public function getReviewsScore($default = 5)
	{
		$reviews = [];
		foreach ($this->reviews as $review) {
			$reviews[] = (float)$review->review;
		}
		if (!empty($reviews))
			return (float)array_sum($reviews) / count($reviews);
		else
			return (float)$default;
	}

	/**
	 * Get best review
	 *
	 * @return mixed|null
	 */
	public function getBestReview()
	{
		$bestReviewScore = 0;
		$bestReview = null;
		foreach ($this->reviews as $review) {
			if ((float)$review->review > $bestReviewScore) {
				$bestReviewScore = $review->review;
				$bestReview = $review;
			}
		}
		return $bestReview;
	}

    /**
     * Clear Meta data
     *
     * @param $key
     */
    public function clearMetaKeys($key)
    {
        foreach ($this->meta as $meta) {
            if (stripos($meta->key, $key) === 0) {
                $this->meta->removeElement($meta);
            }
        }
    }

	/**
	 * Clear meta data
	 */
	public function clearMeta()
	{
		$this->meta->clear();
	}

    /**
     * @param ProductMeta $productMeta
     */
    public function addMeta($productMeta)
    {
        $this->meta[] = $productMeta;
    }

    /**
     * Get meta data
     *
     * @param $key
     * @return array $metas
     */
    public function getMeta($key)
    {
        $metas = [];
        if (isset($this->metaData[$key])) return $this->metaData[$key];
        foreach ($this->meta as $meta) {
            if (stripos($meta->key, $key) === 0) {
                $this->metaData[$key][] = $meta;
                $metas[] = $meta;
            }
        }
        return $metas;
    }

    /**
     * Convert to array
     *
     * @return array
     */
    public function toArray()
    {
        $array = get_object_vars($this);
        // Get price
        $price = $this->getPrice(App::get('currency'));

        if (!empty($price)) {
			$array['formattedPrice'] = $price->getFormattedPrice();
			$array['price'] = $price->toArray();
		}
        // unset
        unset($array['meta']);
        unset($array['multimedia']);
        unset($array['translations']);
        unset($array['created_at']);
        unset($array['updated_at']);

        return $array;
    }

    /** SELECT2 SUPPORT */
	public function getText()
	{
		return $this->title;
    }

}
