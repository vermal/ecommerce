<?php

namespace Vermal\Ecommerce\Modules\Product\Entities\Extensions;

trait Categories
{

    /**
     * Remove all categories
     */
    public function clearCategories()
    {
        $this->categories->clear();
    }

    /**
     * Return all categories for article
     *
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

	/**
	 * @return string
	 */
	public function getCategoriesAsString()
	{
		$o = '';
		foreach ($this->categories as $category) {
			$o .= $category->title . ', ';
		}
		return rtrim($o, ', ');
    }

	/**
	 * Search category
	 *
	 * @param $search
	 * @param string $by
	 *
	 * @return bool
	 */
	public function hasCategory($search, $by = 'slug')
	{
		foreach ($this->getCategories() as $category) {
			if ($category->{$by} == $search) {
				return true;
			}
		}
		return false;
    }

    /**
     * Add new category
     *
     * @param $category
     */
    public function addCategory($category)
    {
        $this->categories[] = $category;
    }

}
