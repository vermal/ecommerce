<?php

namespace Vermal\Ecommerce\Modules\Product\Entities\Extensions;

trait Tags
{

    /**
     * Remove all tags
     */
    public function clearTags()
    {
        $this->clearMetaKeys('tag');
    }

    /**
     * Return all tags for product
     *
     * @return mixed
     */
    public function getTags()
    {
        return $this->getMeta('tag');
    }

    /**
     * Add new tag
     *
     * @param $tag
     */
    public function addTag($tag)
    {
        $this->addMeta($tag);
    }

}
