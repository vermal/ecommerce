<?php

namespace Vermal\Ecommerce\Modules\Product\Entities\Extensions;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Database\Database;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Router;

trait Attributes
{

    /**
     * Remove all attributes
     */
    public function clearAttributes()
    {
		$this->attributes->clear();
    }

    /**
 * Return all categories for article
 *
 * @param boolean $array
 * @return mixed
 */
	public function getAttributes($array = false)
	{
		$attributes_ = $this->getSimpleAttributes();
		$attributes = [];
		foreach ($attributes_ as $attr) {
			if ($attr->parent !== null) {
				if (!isset($attributes[$attr->parent->id])) {
					$attributes[$attr->parent->id] = new \stdClass();
					$attributes[$attr->parent->id]->obj = !$array ? $attr->parent : $attr->parent->toArray();
					$attributes[$attr->parent->id]->children = [];
				}
				$attributes[$attr->parent->id]->children[$attr->id] = !$array ? $attr : $attr->toArray();
			}
		}

		return !$array ? $attributes : json_decode(json_encode($attributes), true);
	}

	/**
	 * Return all categories for article
	 *
	 * @param string $key
	 * @param boolean $array
	 * @return mixed
	 */
	public function getAttributesByKey($key, $array = false)
	{
		$attributes_ = $this->getSimpleAttributes();
		$attributes = [];
		$id = 0;
		foreach ($attributes_ as $attr) {
			if ($attr->parent !== null) {
				if ($attr->parent->key !== $key) continue;
				if (!isset($attributes[$attr->parent->id])) {
					$id = $attr->parent->id;
					$attributes[$attr->parent->id] = new \stdClass();
					$attributes[$attr->parent->id]->obj = !$array ? $attr->parent : $attr->parent->toArray();
					$attributes[$attr->parent->id]->children = [];
				}
				$attributes[$attr->parent->id]->children[$attr->id] = !$array ? $attr : $attr->toArray();
			}
		}

		if (!isset($attributes[$id]->children)) return [];
		$attributes = $attributes[$id]->children;
		return !$array ? $attributes : json_decode(json_encode($attributes), true);
	}

	/**
	 * Get first attribbute
	 *
	 * @param $key
	 * @return mixed
	 */
	public function getFirstAttributeByKey($key)
	{
		$attributes = $this->getAttributesByKey($key);
		foreach ($attributes as $attr) {
			$attribute = $attr;
			break;
		}
		return $attribute;
	}

	/**
	 * @return ArrayCollection
	 */
	public function getSimpleAttributes()
	{
		return $this->attributes;
    }

    /**
     * Add new attribute
     *
     * @param $attribute
     */
    public function addAttribute($attribute)
    {
		$this->attributes[] = $attribute;
    }

}
