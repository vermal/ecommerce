<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Database\MagicAccessor;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_product_attribute")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\ProductAttributeTr")
 **/
class ProductAttribute
{
    use MagicAccessor;
    use Translatable;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true, name="key_")
	 */
	protected $key;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string", length=255)
     */
    protected $value;

	/**
	 * @ORM\Column(type="integer", name="allow_variation", nullable=true)
	 */
	protected $allowVariation;

	/**
	 * @ORM\OneToMany(targetEntity="\ProductAttribute", mappedBy="parent", cascade={"persist", "remove"}, orphanRemoval=true)
	 */
	protected $children;

	/**
	 * @ORM\ManyToOne(targetEntity="\ProductAttribute", inversedBy="children")
	 * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
	 */
	protected $parent;

    /**
     * @ORM\ManyToMany(targetEntity="\Product", mappedBy="attributes")
     */
    protected $products;

    /**
     * @ORM\OneToMany(
     *   targetEntity="\ProductAttributeTr",
     *   mappedBy="object",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     */
    protected $translations;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->products = new ArrayCollection();
    }

	/**
	 * @param $child
	 */
	public function addChild($child)
	{
		$this->children[] = $child;
	}

	/**
	 * Remove children
	 */
	public function removeAllChildren() {
		$this->children->clear();
	}

	/**
	 * @param $product
	 */
	public function addProduct($product)
	{
		$this->products[] = $product;
    }

	/**
	 * To array
	 *
	 * @return array
	 */
	public function toArray()
	{
		$data = [
			'id' => $this->id,
			'key' => $this->key,
			'value' => $this->value,
			'children' => []
		];
		foreach ($this->children as $child) {
			$data['children'][] = $child->toArray();
		}
		return $data;
	}
}
