<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Vermal\Database\MagicAccessor;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_productprice")
 * @ORM\HasLifecycleCallbacks
 **/
class ProductPrice
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string") **/
    protected $currency;

    /** @ORM\Column(type="string") **/
    protected $price;

    /** @ORM\Column(type="string", name="sale_price", nullable=true) **/
    protected $salePrice;

    /** @ORM\Column(type="boolean", name="is_sale") **/
    protected $isSale;

    /** @ORM\Column(type="datetime", name="sale_start", nullable=true) **/
    protected $saleStart;

    /** @ORM\Column(type="datetime", name="sale_end", nullable=true) **/
    protected $saleEnd;

    /**
     * @ORM\ManyToOne(targetEntity="\Product", inversedBy="price", cascade={"persist"})
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;

	/**
	 * Get price
	 *
	 * @return mixed
	 */
	public function getProductPrice()
	{
		if ($this->isSale ||
			(
				!empty($this->saleStart) && !empty($this->saleEnd) &&
				time() >= $this->saleStart->getTimestamp() && time() <= $this->saleEnd->getTimestamp()
			)
		) {
			return $this->salePrice;
		} else {
			return $this->price;
		}
	}

    /**
     * Get formatted price
     *
     * @return string
     */
    public function getFormattedPrice()
    {
        return $this->price . ' ' . Currencies::getSymbol($this->currency);
    }

    /**
     * Convert to array
     *
     * @return array
     */
    public function toArray()
    {
        $array = get_object_vars($this);
        unset($array['product']);
        return $array;
    }
}
