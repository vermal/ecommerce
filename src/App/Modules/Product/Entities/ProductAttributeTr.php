<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_product_attribute_tr",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })})
 **/
class ProductAttributeTr extends AbstractPersonalTranslation
{

    /**
     * Convenient constructor
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($locale, $field, $value)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }

    /**
     * @ORM\ManyToOne(targetEntity="\ProductAttribute", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;

}
