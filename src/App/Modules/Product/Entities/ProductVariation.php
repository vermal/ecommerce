<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\App;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * TODO: not supported yet
 * @ORM\Entity @ORM\Table(name="ecommerce_product_variation")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\ProductVariationTr")
 **/
class ProductVariation extends Model
{
    use Translatable;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $title;

    // INFO
    /** @ORM\Column(type="float") **/
    protected $ean;

    // SHIPPING
    /** @ORM\Column(type="string", nullable=true) **/
    protected $weight;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $length;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $width;

    /** @ORM\Column(type="string", nullable=true) **/
    protected $height;

    /** @ORM\Column(type="integer", nullable=true) **/
    protected $storage;

    // OTHERS
    /** @ORM\Column(type="string") **/
    protected $status;

    /**
     * @ORM\OneToMany(targetEntity="\ProductPrice", mappedBy="product", orphanRemoval=true, cascade={"persist", "remove"})
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="\Multimedia")
     * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
     */
    protected $simpleMultimedia;

    /**
     * @ORM\ManyToMany(targetEntity="\Multimedia")
     * @ORM\JoinTable(name="ecommerce_product_variation_multimedia",
     *      joinColumns={@ORM\JoinColumn(name="product_variation_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", unique=true)}
     *      )
     */
    protected $multimedia;

    /**
     * @ORM\OneToMany(
     *   targetEntity="\ProductVariationTr",
     *   mappedBy="object",
     *   cascade={"persist", "remove"},
     *   orphanRemoval=true
     * )
     */
    protected $translations;

    /**
     * Product constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->translations = new ArrayCollection();
        $this->multimedia = new ArrayCollection();
        $this->price = new ArrayCollection();
    }

    /**
     * Clear prices
     */
    public function clearPrices()
    {
        $this->price->clear();
    }

    /**
     * @param ProductPrice $productPrice
     */
    public function addPrice(ProductPrice $productPrice)
    {
        $this->price[] = $productPrice;
    }

    /**
     * @param $currency
     * @return ProductPrice
     */
    public function getPrice($currency)
    {
        foreach ($this->price as $price) {
            if ($price->currency == $currency) return $price;
        }
    }

    /**
     * Convert to array
     *
     * @return array
     */
    public function toArray()
    {
        $array = get_object_vars($this);
        $array['price'] = $this->getPrice(App::get('currency'))->toArray();
        // unset
        unset($array['meta']);
        unset($array['multimedia']);
        unset($array['translations']);
        unset($array['created_at']);
        unset($array['updated_at']);

        return $array;
    }

}
