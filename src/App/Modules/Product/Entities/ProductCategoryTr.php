<?php

namespace Vermal\Ecommerce\Modules\Product\Entities;

use Gedmo\Translatable\Entity\MappedSuperclass\AbstractPersonalTranslation;
use Vermal\Database\MagicAccessor;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_product_category_tr",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_idx", columns={
 *         "locale", "object_id", "field"
 *     })})
 **/
class ProductCategoryTr extends AbstractPersonalTranslation
{

    /**
     * Convenient constructor
     *
     * @param string $locale
     * @param string $field
     * @param string $value
     */
    public function __construct($locale, $field, $value)
    {
        $this->setLocale($locale);
        $this->setField($field);
        $this->setContent($value);
    }

    /**
     * @ORM\ManyToOne(targetEntity="\ProductCategory", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $object;

}
