<?php

namespace Vermal\Ecommerce\Modules\Product;

use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Upload;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Models\ProductModel;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Traits\DataGridSort;


class Products extends Controller
{

	use DataGridSort;

    public $defaultViewPath = false;
    private $fields = [];
    private $fileFields = ['featured_image'];
    private $translatableFields = ['title', 'te', 'shortTe', 'keywords'];

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('products', $this->CRUDAction);
        $this->addToBreadcrumb('product', 'admin.product.index');
        View::setVariable('currencies', Currencies::get());

        $this->translatableFields = apply_filters('ecommerce_product_translatable_fields', $this->translatableFields);
		$this->fields = apply_filters('ecommerce_product_fields', $this->fields);
		$this->fileFields = apply_filters('ecommerce_product_file_fields', $this->fileFields);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->setDefaultViewPath();

		$categories_ = Database::Model('ProductMeta')->where('p.key', 'category')->get();
		$categories = [];
		foreach ($categories_ as $category) {
			$categories[$category->id] = $category->value;
		}

        $data = Database::Model('Product')
			->order('p.order', 'ASC')
			->where('p.parent', '=', null);
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
		$datagrid->addColumn('ean', $this->_('product.ean'));
		$datagrid->addColumn('title', $this->_('product.title'));
        $datagrid->addColumn('categories', $this->_('product.categories'))->setRenderer(function($product) {
            $str = '';
            foreach ($product->getCategories() as $category) {
                $str .= '<a href="' . routerLink('admin.product-category.edit', ['id' => $category->id]) . '" target="_blank">' .
                    $category->title . '</a>, ';
            }
            return rtrim($str, ', ');
        });
		$datagrid->addColumn('reviews', $this->_('product.reviews'))->setRenderer(function($product) {
			return "<a target='_blank' href='" .
				routerLink('admin.product-review.index', ['filter__product' => $product->id]) . "'>" .
				$product->reviews->count() . "</a>";
		});


        // Add actions
		$datagrid->addSortable(['admin.product.sort']);
		$datagrid->addEdit(['admin.product.edit', ['id']]);
		$datagrid->addDelete(['admin.product.destroy_', ['id']]);
		$datagrid->addAction('reviews',
			['admin.product-review.index', ['filter__product' => 'id']], 'fas fa-star', $this->_('product.reviews'), 'btn btn-sm btn-outline-info');
		$datagrid->addSwitch(['admin.switch', ['id'], ['c' => str_replace('\\', '__', self::class)]], 'active');

		// Add bulk actions
		$datagrid->addBulkAction('activate', $this->_('datagrid.bulk.activate'), [self::class, 'activate']);
		$datagrid->addBulkAction('deactivate', $this->_('datagrid.bulk.deactivate'), [self::class, 'deactivate']);

        // Add filters
//		$datagrid->addFilter('meta', $categories, $this->_('product.categories'));
//		$datagrid->addFilter('meta', [null => $this->_('product.categories')], $this->_('product.categories'),
//			'col-lg-3', 'js-select2 search', ['data-controller' => self::class, 'data-method' => 'searchCategories']);

		// Datagrid
		$datagrid = apply_filters('ecommerce_product_datagrid', $datagrid);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('product', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => $this->fields,
			"translatableFields" => $this->translatableFields,
			"fileFields" => $this->fileFields,
			"productAttributes" => [],
			"attributes" => $attributes = Database::Model('ProductAttribute')
				->where('p.parent', '=')
				->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.product.create');
        }

        $product = Entity::getEntity('Product');
        $product = $this->hydrate($product, $post);

        Database::saveAndFlush($product);

        // Redirect with message
        $this->alert($this->_('product.created'));
        Router::redirect('admin.product.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.product.update', ['id' => $id]));

		$form = apply_filters('ecommerce_product_form_queries', $form);

        /** @var Product $product */
        $product = Database::Model('Product')->find($id);
        $attributes = Database::Model('ProductAttribute')
			->where('p.parent', '=')->get();

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.product.edit', ['id' => $id], [$product->title]);

        // Set values
        $form->getComponent('ean')->setValue($product->ean);
        $form->getComponent('weight')->setValue($product->weight);
        $form->getComponent('length')->setValue($product->length);
        $form->getComponent('width')->setValue($product->width);
        $form->getComponent('height')->setValue($product->height);
	    $form->getComponent('stock')->setValue($product->stock);

        // Set values content
        foreach (Languages::get() as $lang) {
            $form->getComponent($lang . '_title')->setValue($product->getTr($lang, 'title'));
            $form->getComponent($lang . '_te')->setValue($product->getTr($lang, 'te'));
            $form->getComponent($lang . '_shortTe')->setValue($product->getTr($lang, 'shortTe'));
            $form->getComponent($lang . '_keywords')->setValue($product->getTr($lang, 'keywords'));
        }

        // Set values price
        foreach (Currencies::get() as $currency) {
            $price = $product->getPrice($currency);
            if (empty($price)) continue;

            $form->getComponent($currency . '_price')->setValue($price->price);
            $form->getComponent($currency . '_salePrice')->setValue($price->salePrice);
            $form->getComponent($currency . '_isSale')->setValue($price->isSale);

            if (!is_null($price->saleStart) && !is_null($price->saleEnd)) {
                $form->getComponent($currency . '_saleRange')
                    ->setValue(date('d.m.Y', $price->saleStart->getTimestamp()) . ' - ' . date('d.m.Y', $price->saleEnd->getTimestamp()));
            }
        }

        // Categories
        $categories = [];
        foreach ($product->getCategories() as $category) {
            $categories[$category->id] = $category->title;
        }
        $form->getComponent('categories')
			->setSelect($categories)
			->setValue($categories);

		// Tags
		$tags = [];
		foreach ($product->getTags() as $tag) {
			$tags[$tag->id] = $tag->id;
		}
		$form->getComponent('tags')->setValue($tags);

		/** @var Multimedia $multimedia */
		$multimedia = $product->simpleMultimedia;
		if (!empty($multimedia))
			$form->getComponent('featured_image')->setValue($multimedia->getMiniFile());

		// Set values
		$form = apply_filters('ecommerce_product_form_values', $form, $product);

        View::view('product', [
            "form" => $form->build(),
            "formName" => $form->getName(),
			"fields" => $this->fields,
			"translatableFields" => $this->translatableFields,
			"fileFields" => $this->fileFields,
            "productAttributes" => $product->getAttributes(),
            "attributes" => $attributes,
			"product" => $product
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        $product = Database::Model('Product')->find($id);
        $this->hydrate($product, $post, true);

        $this->alert($this->_('product.edited'));
        Database::saveAndFlush($product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
	 * @param bool $redirect
     */
    public function destroy($id, $redirect = true)
    {
        $product = Database::Model('Product')->find($id);
        $hasParent = empty($product->parent) ? false : $product->parent->id;
        $product->clearTr();
        $product->clearMeta();
        $product->clearPrices();
        $product->clearCategories();
        Database::saveAndFlush($product);
        Database::Model('Product')->delete($id);

        // Redirect
        if ($redirect) {
			$this->alert($this->_('product.deleted'));
			if (!$hasParent)
				Router::redirect('admin.product.index');
			else {
				Router::redirect('admin.product.edit', ['id' => $hasParent]);
			}
		}
    }

    /**
     * Hydrate database object
     *
     * @param Product $product
     * @param $post
     * @param $update
     * @return object
     * @throws \Exception
     */
    private function hydrate($product, $post, $update = false)
    {
        $productModel = new ProductModel();
        return $productModel->hydrate($product, $post, $update);
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('product', routerLink('admin.product.store'));

        // Add categories to form
        $form->addSelect('categories', [], $this->_('product.categories'))
            ->addAttribute('multiple', 'true')
			->addAttribute('data-controller', self::class)
			->addAttribute('data-method', 'searchCategories')
			->setClass('js-select2 search');

        // Add tags to form
		$tags = [];
		foreach (Database::Model('ProductMeta')->where('p.key', 'tag')->get() as $tag) {
			$tags[$tag->id] = $tag->value;
		}
		$form->addSelect('tags', $tags, $this->_('product.tags'))
			->addAttribute('multiple', 'true')
			->setClass('js-select2 allow-tags');

        // Default
        $form->addText('ean', $this->_('product.ean'));

        // Price
        View::setVariable('currencies', $this->settings->currencies);
        foreach (Currencies::get() as $currency) {
            $form->addText($currency . '_price', $this->_('product.price'))->setCol('col-md-6')->int();
            $form->addText($currency . '_salePrice', $this->_('product.salePrice'))->setCol('col-md-6');

            $form->addCheckboxSwitch($currency . '_isSale', $this->_('product.isSale'));
            $form->addText($currency . '_saleRange', $this->_('product.saleRange'))->setClass('daterange');
        }

        // Shipping
        $form->addText('weight', $this->_('product.weight'))->setCol('col-md-6');
        $form->addText('length', $this->_('product.length'))->setCol('col-md-6');
        $form->addText('width', $this->_('product.width'))->setCol('col-md-6');
        $form->addText('height', $this->_('product.height'))->setCol('col-md-6');
        $form->addText('stock', $this->_('product.storage'))->setCol('col-12');


        // Product Data
        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', $this->_('product.title'))->min(2);
            $form->addTextEditor($lang . '_te', $this->_('product.te'))->min(2);
            $form->addTextEditor($lang . '_shortTe', $this->_('product.shortTe'));
            $form->addTextArea($lang . '_keywords', $this->_('product.keywords'));
        }

        $form->addInput('featured_image', 'file', 'Featured image');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

		// Allow form extension
		$form = apply_filters('ecommerce_product_form', $form);

        return $form;
    }

    /**
     * Get attribute values
     *
     * @param $id
     */
    public function attributeValues($id)
    {
        $attribute = Database::Model('ProductAttribute')->find($id);
        $output = [];
        foreach ($attribute->children as $attribute) {
            $output[] = [
                'id' => $attribute->id,
                'val' => $attribute->value
            ];
        }
        echo json_encode($output);
    }

	/**
	 * Generate product variation
	 *
	 * @param $post
	 * @param $id
	 * @throws \Exception
	 */
	public function productVariation($post, $id)
	{
		$productModel = new ProductModel();
		$productModel->productVariation($post, $id, 'admin.product.edit');
    }

	/**
	 * Remove product variation
	 *
	 * @param $id
	 */
    public function removeProductVariation($id)
	{
		$this->destroy($id);
	}

	public function sort($post)
	{
		$prev_id = isset($post->prev_id) ? $post->prev_id : false;
		$next_id = isset($post->next_id) ? $post->next_id : false;
		$this->sortHelper('Product', $post->item_id, $prev_id, $next_id);
	}

	/**
	 * Search categories
	 *
	 * @param $query
	 * @return array
	 */
	public static function searchCategories($query)
	{
		return [
			Database::Model('ProductCategory')
				->where('p.title', '%', $query),
			['id', 'title']
		];
	}

	/**
	 * @param $id
	 */
	public static function switchItem($id)
	{
		$entity = Database::Model('Product')->find($id);
		if (!empty($entity)) {
			$entity->active = !$entity->active;
			Database::saveAndFlush($entity);
		}
		Router::redirect('admin.product.index');
	}

	/**
	 * Activate
	 *
	 * @param $keys
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function activate($keys)
	{
		$entity = Database::Model('Product')
			->where('p.id', (array)$keys)->get();
		foreach ($entity as $row) {
			$row->active = 1;
			Database::save($row);
		}
		Database::$em->flush();
		Router::redirectToURL(Router::prevLink());
	}

	/**
	 * Deactivate
	 *
	 * @param $keys
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function deactivate($keys)
	{
		$entity = Database::Model('Product')
			->where('p.id', (array)$keys)->get();
		foreach ($entity as $row) {
			$row->active = 0;
			Database::save($row);
		}
		Database::$em->flush();
		Router::redirectToURL(Router::prevLink());
	}
}
