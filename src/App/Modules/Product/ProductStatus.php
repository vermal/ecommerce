<?php
/**
 * Created by PhpStorm.
 * User: patrik
 * Date: 2020-08-04
 * Time: 12:19
 */

namespace Vermal\Ecommerce\Modules\Product;


use Vermal\Ecommerce\Defaults\Controller;

class ProductStatus
{

	const ACTIVE = 1;
	const UNACTIVE = 0;
	const SOLD_OUT = 2;
	const RESERVED = 3;

	public static function get()
	{
		return [
			self::ACTIVE => self::ACTIVE,
			self::UNACTIVE => self::UNACTIVE,
			self::SOLD_OUT => self::SOLD_OUT,
			self::RESERVED => self::RESERVED
		];
	}

	public static function getTranslated()
	{
		$statuses = [];
		foreach (self::get() as $status) {
			$statuses[$status] = Controller::__('product.active.' . $status);
		}
		return $statuses;
	}

}
