<?php

namespace Vermal\Ecommerce\Modules\Product;

use Gregwar\Image\Source\Data;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Defaults\Select2Trait;
use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Upload;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Entities\ProductReview;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Traits\DataGridSort;


class ProductReviews extends Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('products', $this->CRUDAction);
        $this->addToBreadcrumb('productReviews', 'admin.product-reviews.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('ProductReview');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
		$datagrid->addColumn('review', $this->_('productReview.review'));
		$datagrid->addColumn('title', $this->_('productReview.title'));
		$datagrid->addColumn('te', $this->_('productReview.title'));
		$datagrid->addColumn('product', $this->_('productReview.product'))->setRenderer(function($entity) {
			if (empty($entity->product)) return null;
			return "<a href='" . routerLink('admin.product.edit', ['id' => $entity->product->id]) . "' target='_blank'>" .
				$entity->product->title . "</a>";
		});

        // Add actions
		$datagrid->addEdit(['admin.product-review.edit', ['id']]);
		$datagrid->addDelete(['admin.product-review.destroy_', ['id']]);
		$datagrid->addDelete(['admin.product-review.destroy_', ['id']]);

		// Add bulk actions
		$datagrid->addBulkAction('delete', $this->_('datagrid.bulk.delete'), [self::class, 'deleteAll']);
		$datagrid->addBulkAction('activate', $this->_('datagrid.bulk.activate'), [self::class, 'activate']);
		$datagrid->addBulkAction('deactivate', $this->_('datagrid.bulk.deactivate'), [self::class, 'deactivate']);

		$datagrid->addSwitch(['admin.switch', ['id'], ['c' => str_replace('\\', '__', self::class)]], 'active');

		// Add filter
		$datagrid->addFilter('product', [null => $this->_('productReview.product')], $this->_('productReview.product'),
			'col-lg-3', 'js-select2 search', ['data-controller' => self::class]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.product-review.create');
        }

		$product = Database::Model('Product')->find($post->product);

		$productReview = Entity::getEntity('ProductReview');
		$productReview->active = 1;
		$productReview = $form->hydrate($productReview, ['product']);
		$productReview->product = $product;

		Database::saveAndFlush($productReview);

        // Redirect with message
        $this->alert($this->_('productReview.created'));
        Router::redirect('admin.product-review.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.product-review.update', ['id' => $id]));

        /** @var ProductReview $productReview */
        $productReview = Database::Model('ProductReview')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.product-review.edit', ['id' => $id], [$productReview->title]);

        // Set values
		if (!empty($productReview->product)) {
			$form->getComponent('product')->setSelect([
				$productReview->product->getId() => $productReview->product->title
			]);
		}
        $form->setValues($productReview, ['product']);

        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

		$product = Database::Model('Product')->find($post->product);

        $productReview = Database::Model('ProductReview')->find($id);
		$productReview = $form->hydrate($productReview, ['product']);
		$productReview->product = $product;

        $this->alert($this->_('productReview.edited'));
        Database::saveAndFlush($productReview);
    }

    /**
     * @param int $id
     */
    public function destroy($id)
    {
        Database::Model('ProductReview')->delete($id);
		Router::redirect('admin.product-review.index');
    }
    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('product', routerLink('admin.product-review.store'));

        $form->addInput('review', 'number', $this->_('productReview.review'))
			->setCol('col-lg-4')->required();
		$form->addSelect('product', [null => $this->_('productReview.product')], $this->_('productReview.product'))
			->addAttribute('data-controller', self::class)
			->setClass('js-select2 search')->setCol('col-lg-8')
			->required();
		$form->addText('title', $this->_('productReview.title'))->required();
		$form->addTextArea('te', $this->_('productReview.te'))->required();
		$form->addText('name', 'Meno')->required();

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

	/**
	 * Select 2 support
	 *
	 * @param $query
	 * @return Database
	 */
	public static function select2($query)
	{
		return Database::Model('Product')
			->where('p.title', '%', $query);
    }

	/**
	 * @param $id
	 */
	public static function switchItem($id)
	{
		$review = Database::Model('ProductReview')->find($id);
		if (!empty($review)) {
			$review->active = !$review->active;
			Database::saveAndFlush($review);
		}
		Router::redirect('admin.product-review.index');
    }

	/**
	 * Delete all
	 *
	 * @param $keys
	 */
	public function deleteAll($keys)
	{
		$productReviews = Database::Model('ProductReview')
			->where('p.id', (array)$keys)->get();
		foreach ($productReviews as $productReview) {
			Database::Model('ProductReview')->delete($productReview->id);
		}
		Router::redirectToURL(Router::prevLink());
    }

	/**
	 * Activate
	 *
	 * @param $keys
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function activate($keys)
	{
		$productReviews = Database::Model('ProductReview')
			->where('p.id', (array)$keys)->get();
		foreach ($productReviews as $productReview) {
			$productReview->active = 1;
			Database::save($productReview);
		}
		Database::$em->flush();
		Router::redirectToURL(Router::prevLink());
    }

	/**
	 * Activate
	 *
	 * @param $keys
	 * @throws \Doctrine\ORM\ORMException
	 * @throws \Doctrine\ORM\OptimisticLockException
	 */
	public function deactivate($keys)
	{
		$productReviews = Database::Model('ProductReview')
			->where('p.id', (array)$keys)->get();
		foreach ($productReviews as $productReview) {
			$productReview->active = 0;
			Database::save($productReview);
		}
		Database::$em->flush();
		Router::redirectToURL(Router::prevLink());
	}
}
