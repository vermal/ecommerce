<?php

namespace Vermal\Ecommerce\Modules\Product;


use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;


class ProductAttributes extends Controller
{
    public $defaultViewPath = false;

    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('products', $this->CRUDAction);
        $this->addToBreadcrumb('product_attribute', 'admin.product-attribute.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->setDefaultViewPath();

        $data = Database::Model('ProductAttribute')->where('p.parent', '=');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('value', $this->_('product.title'));

        // Add actions
        $datagrid->addEdit(['admin.product-attribute.edit', ['id']]);
        $datagrid->addDelete(['admin.product-attribute.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('attribute', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => ['title'],
			"fieldsAfter" => ['key', 'allowVariation'],
            "values" => []
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.product.create');
        }

        // Hydrate and save attribute
        $this->hydrate($post);

        // Redirect with message
        $this->alert($this->_('productAttribute.created'));
        Router::redirect('admin.product-attribute.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
//        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.product-attribute.update', ['id' => $id]));

        $attribute = Database::Model('ProductAttribute')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.product-attribute.edit', ['id' => $id], [$attribute->value]);

        // Set values
        foreach (Languages::get() as $lang) {
            $form->getComponent($lang . '_title')->setValue($attribute->getTr($lang, 'value'));
        }
        $form->getComponent('key')->setValue($attribute->key);
		$form->getComponent('allowVariation')->setValue($attribute->allowVariation);

        View::view('attribute', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => ['title'],
            "fieldsAfter" => ['key', 'allowVariation'],
            "values" => $attribute->children
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        // Hydrate and save attribute
        $meta = Database::Model('ProductAttribute')->find($id);
        if ($meta === null) {
            $this->alert($this->_('error.not-found'), 'danger');
            die();
        }
        $this->hydrate($post, $id);

        $this->alert($this->_('productAttribute.edited'));
		Router::redirect('admin.product-attribute.edit', ['id' => $id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $meta = Database::Model('ProductAttribute')->find($id);
        $meta->clearTr();
        $meta->removeAllChildren();
        Database::saveAndFlush($meta);
        Database::Model('ProductAttribute')->delete($id);

        // Redirect
        $this->alert('Attribute was deleted sucesfully');
        Router::redirect('admin.product-attribute.index');
    }

    /**
     * Hydrate attribute
     *
     * @param $post
     * @param $id
     */
    public function hydrate($post, $id = null)
    {
		if (!empty($id)) {
			$entity = Database::Model('ProductAttribute')->find((int)$id);
			$entity->clearTr();
			Database::saveAndFlush($entity);
		} else
			$entity = Entity::getEntity('ProductAttribute');

		$entity->key = Sanitizer::applyFilter('trim|escape|strip_tags', $post->key);
		$entity->allowVariation = Sanitizer::applyFilter('trim|escape|strip_tags', $post->allowVariation);

		// Base attribute
		foreach (Languages::get() as $lang) {
			$tr = 'ProductAttributeTr';
			$value = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});
			Database::translate($entity, $tr, 'value', $lang, $value);
		}

		// Child values
		$data = [];
		foreach ($post->value as $k => $value) {
			$update = false;
			foreach (Languages::get() as $lang) {
				if (empty($value) || empty($value->{$lang})) {
					if (!empty($post->meta_id->{$k})) {
						// Delete meta if empty value
						Database::Model('ProductAttribute')->delete($post->meta_id->{$k});
					}
					continue(2);
				}
				$data[$lang] = [
					'value' => Sanitizer::applyFilter('trim|escape|strip_tags', $value->{$lang})
				];
				// If meta is created update
				if (!empty($post->meta_id->{$k})) {
					$update = $post->meta_id->{$k};
				}
			}

			if ($update) {
				$child = Database::Model('ProductAttribute')->find($update);
				$child->clearTr();
				Database::saveAndFlush($child);
			} else $child = Entity::getEntity('ProductAttribute');

			foreach ($data as $lang => $v) {
				$tr = 'ProductAttributeTr';
				Database::translate($child, $tr, 'value', $lang, $v['value']);
			}
			$child->parent = $entity;
			Database::save($child);
		}

		Database::saveAndFlush($entity);
    }

    /**
     * Create product attribute form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('product-attribute', routerLink('admin.product-attribute.store'));

        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', 'Attribute name')->min(2);
        }
        $form->addText('key', 'Attribute key')->min(1);
        $form->addCheckboxSwitch('allowVariation', 'Allow variations');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
