@extends('layout')

@section('body')

    @form($formName)

        {{-- Content --}}
        <div class="col-md-8">
            <div class="card shadow-lg m-b-30 p-4">

                @if(isset($product) && !empty($product->parent))
                    <div class="mb-3">
                        <a href="{{ routerLink('admin.product.edit', ['id' => $product->parent->id]) }}"
                           class="btn btn-outline-info btn-sm">
                            <i class="fas fa-arrow-left mr-2"></i>
                            Back to main product
                        </a>
                    </div>
                @endif

                <div class="row mb-3">
                    @label("ean")
                    @control("ean")
                </div>

                <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                    @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                                {{ strtoupper($lang) }}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content pt-4" id="myTabContent">
                    @foreach($languages as $lang)

                        <div class="tab-pane row fade {{ $loop->first ? "show active" : "" }}" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                            @foreach($translatableFields as $field)
                                @label("{$lang}_{$field}")
                                @control("{$lang}_{$field}")
                            @endforeach
                        </div>

                    @endforeach
                </div>

                <div class="row text-right mt-3">
                    @control("submit")
                </div>

            </div>

        </div>

        <div class="col-md-4">
            {{-- Categories --}}
            <div class="card shadow-lg m-b-30 p-4">
                <h3>@__(product.categories)</h3>
                <div class="row">
                    @label("categories")
                    @control("categories")
                </div>
            </div>

            {{-- Price --}}
            <div class="card shadow-lg m-b-30 p-4">
                <h3>@__(product.price)</h3>

                <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                    @foreach($currencies as $currency)
                        <li class="nav-item">
                            <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $currency }}" data-toggle="tab" href="#{{ $currency }}" role="tab" aria-controls="{{ $currency }}" aria-selected="true">
                                {{ $currency }}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content pt-4" id="currencies-tab">
                    @foreach($currencies as $currency)

                        <div class="tab-pane fade {{ $loop->first ? "show active" : "" }}" id="{{ $currency }}" role="tabpanel" aria-labelledby="tab-{{ $currency }}">
                            <div class="row">
                                @label("{$currency}_price")
                                @control("{$currency}_price")

                                @label("{$currency}_salePrice")
                                @control("{$currency}_salePrice")

                                {{-- Sale --}}
                                @label("{$currency}_isSale")
                                @control("{$currency}_isSale")

                                @label("{$currency}_saleRange")
                                @control("{$currency}_saleRange")
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>

            {{-- Tags --}}
            <div class="card shadow-lg m-b-30 p-4">
                <h3>@__(product.tags)</h3>
                <div class="row">
                    @label("tags")
                    @control("tags")
                </div>
            </div>

            {{-- Shipping --}}
            <div class="card shadow-lg m-b-30 p-4">
                <h3>@__(product.shipping)</h3>

                <div class="row">
                    @label("weight")
                    @control("weight")

                    @label("length")
                    @control("length")

                    @label("width")
                    @control("width")

                    @label("height")
                    @control("height")
                </div>
            </div>

            {{-- Storage --}}
            <div class="card shadow-lg m-b-30 p-4">
                <h3>@__(product.storage)</h3>

                <div class="row">
                    @label("stock")
                    @control("stock")
                </div>
            </div>

            {{-- Image --}}
            @foreach($fileFields as $field)
                <div class="card shadow-lg m-b-30 p-4">
                    <div class="row">
                        @label("$field")
                        @control("$field")
                    </div>
                </div>
            @endforeach
        </div>

        @if(!empty($fields))
            <div class="col-12">
                <div class="card shadow-lg m-b-30 p-4">
                    <div class="row">
                        @foreach($fields as $field)
                            @label("{$field}")
                            @control("{$field}")
                        @endforeach
                    </div>
                </div>
            </div>
        @endif

        @if(!isset($product) || empty($product->parent))
            <div class="col-12">
                <div class="card shadow-lg m-b-30 p-4">
                    <h4>Atribúty</h4>
                    <div class="input-group mb-3">
                        <select class="js-select2 form-control">
                            @foreach($attributes as $attribute)
                                <option value="{{ $attribute->id }}">{{ $attribute->value }}</option>
                            @endforeach
                        </select>
                        <div class="input-group-append">
                            <a href="#" class="btn btn-info" id="add-attribute">Pridať</a>
                        </div>
                    </div>

                    <div id="attributes">
                        @foreach($attributes as $attribute)
                            @if(array_key_exists($attribute->id, $productAttributes))
                                @php($productAttribute = $productAttributes[$attribute->id])
                                <div class="form-group">
                                    <label for="attribute-{{ $attribute->id }}">{{ $attribute->value }}</label>
                                    <select name="attribute[]" id="attribute-{{ $attribute->id }}" class="form-control js-select2" multiple="true">
                                        @foreach($attribute->children as $key => $child)
                                            @php($selected = isset($productAttribute->children[$child->id]) ? 'selected' : '')
                                            <option value="{{ $child->id }}" {{ $selected }}>{{ $child->value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('#add-attribute').click(function(e) {
                        e.preventDefault();
                        let id = $(this).parent().parent().find('select').val();
                        let label = $(this).parent().parent().find('option[value="' + id + '"]').text();

                        // If attribute select already exist
                        if ($('#attributes #attribute-' + id).length > 0) return;

                        // Get attributes
                        $.ajax({
                            url: '{{ routerLink('admin.product.attribute') }}/' + id,
                            method: 'GET',
                            success: function(resp) {
                                try {
                                    let obj = JSON.parse(resp);

                                    // Create new item
                                    let select = $('<select name="attribute[]" id="attribute-' + id + '" class="form-control js-select2" multiple="true">');
                                    for (let i = 0; i < obj.length; i++) {
                                        let attribute = obj[i];
                                        select.append(
                                                $('<option value="' + attribute.id + '">').text(attribute.val)
                                        )
                                    }
                                    let newItem = $('<div class="form-group">').append(select);
                                    newItem.prepend($('<label for="attribute-' + id + '">').text(label));

                                    // Append new item
                                    $('#attributes').append(newItem);
                                    initPlugins();
                                } catch (e) {}
                            }
                        });
                    });
                });
            </script>
        @endif

    @endform

    @if(!empty($product) && empty($product->parent))
        <div class="card shadow-lg m-b-30 p-4">
            <h4>Varianty</h4>

            <div id="variations">
                <form action="{{ routerLink('admin.product.variation', ['id' => $product->id]) }}" method="POST">
                    @include('variations', ['productAttributes' => $productAttributes])
                    <button name="submit" value="single" class="btn btn-success btn-sm">Generovať</button>
                    <button name="submit" value="all" class="btn btn-outline-info btn-sm">Generovať všetky varianty</button>
                    <button name="submit" value="remove-all" class="btn btn-outline-danger btn-sm">Zmazať všetky varianty</button>
                </form>
            </div>

            <div class="mt-3">
                <table class="table table-striped">
                    @foreach($product->children as $child)
                        <tr>
                            <td>#{{ $child->id }}</td>
                            <td>
                                @php($attributes = $child->getSimpleAttributes())
                                @foreach($attributes as $attribute)
                                    {{ $attribute->parent->value }}:
                                    {{ $attribute->value }};
                                @endforeach
                            </td>
                            <td class="text-right">
                                <a href="{{ routerLink('admin.product.edit', ['id' => $child->id]) }}"
                                   class="btn btn-primary btn-sm">
                                    <i class="fas fa-pencil-alt"></i>
                                </a>
                                <a href="{{ routerLink('admin.product.remove-variation', ['id' => $child->id]) }}"
                                   class="btn btn-danger btn-sm">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

        </div>
    @endif

@endsection
