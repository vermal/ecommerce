@extends('layout')

@section('body')

    @form($formName)
        <div class="col-12 card shadow-lg m-b-30 p-4">

            @if(isset($fieldsBefore))
                <div class="row">
                    @foreach($fieldsBefore as $field)
                        @label("{$field}")
                        @control("{$field}")
                    @endforeach
                </div>
            @endif

            <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                @foreach($languages as $lang)
                    <li class="nav-item">
                        <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                            {{ strtoupper($lang) }}
                        </a>
                    </li>
                @endforeach
            </ul>

            <div class="tab-content pt-4" id="myTabContent">
                @foreach($languages as $lang)

                    <div class="tab-pane row {{ $loop->first ? "show active" : "" }}" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                        @foreach($fields as $field)
                            @label("{$lang}_{$field}")
                            @control("{$lang}_{$field}")
                        @endforeach
                    </div>

                @endforeach
            </div>

            @if(isset($fieldsAfter))
                <div class="row">
                    @foreach($fieldsAfter as $field)
                        @label("{$field}")
                        @control("{$field}")
                    @endforeach
                </div>
            @endif

        </div>
        <div class="col-12 card shadow-lg m-b-30 p-4">

            <div class="mt-2" id="values">
                <h2>Hodnoty</h2>

                @php($calc = (12 / count($languages)))
                @php($col = ($calc > 2.9) ? $calc : 3)

                <div class="form-group template d-none">
                    <div class="row">
                    @foreach($languages as $lang)
                        <div class="col-md-{{ $col }}">
                            <label for="value_0_{{ $lang }}" class="mt-1 label-{{ $lang }}">{{ strtoupper($lang) }}</label>
                            <input type="text" name="value[0][{{ $lang }}]" id="value_0_{{ $lang }}" class="form-control input-{{ $lang }}">
                        </div>
                    @endforeach
                    </div>
                    <hr>
                </div>

                @foreach($values as $key => $value)
                    <div class="form-group">
                        <div class="row">
                            <input type="hidden" name="meta_id[{{ $key + 1 }}]" value="{{ $value->id }}">
                            @foreach($languages as $lang)
                                <div class="col-md-{{ $col }}">
                                    <label for="value_{{ $key }}_{{ $lang }}" class="mt-1 label-{{ $lang }}">{{ strtoupper($lang) }}</label>
                                    <input type="text" name="value[{{ ($key + 1) }}][{{ $lang }}]" id="value_{{ $key }}_{{ $lang }}"
                                           class="form-control input-{{ $lang }}" value="{{ $value->getTr($lang, 'value') }}">
                                </div>
                            @endforeach
                        </div>
                        <hr>
                    </div>
                @endforeach

            </div>
            <a href="#" id="add-value" class="btn btn-info">Pridať hodnotu</a>

            <div class="text-right mt-4">
                @control("submit")
            </div>
        </div>
    @endform

    <script>
        $(document).ready(function() {
            $('.nav-tabs .nav-link').click(function() {
                setTimeout(function() {
                    initPlugins();
                }, 10);
            });

            // Add value
            $('#add-value').click(function(e) {
                e.preventDefault();

                let values = $('#values');
                let newItemId = values.find('.form-group').length + 1;
                let template = values.find('.template');
                let item = template.clone();

                // Replace values
                @foreach($languages as $lang)
                item.find('.label-{{ $lang }}').attr('for', item.find('.label-{{ $lang }}').attr('for').replace('0', newItemId));
                item.find('.input-{{ $lang }}').attr('name', item.find('.input-{{ $lang }}').attr('name').replace('0', newItemId));
                item.find('.input-{{ $lang }}').attr('id', item.find('.input-{{ $lang }}').attr('id').replace('0', newItemId));
                @endforeach

                // Make real item from template
                item.removeClass('template').removeClass('d-none');
                values.append(item);
            });
        });
    </script>

@endsection
