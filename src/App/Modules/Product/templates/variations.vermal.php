@foreach($productAttributes as $attribute)
    @if(empty($attribute->obj->allowVariation))
        @continue
    @endif
    <select name="variation[{{ $attribute->obj->id }}]" id="variation-{{ $attribute->obj->id }}" class="form-control mb-2">
        @foreach($attribute->children as $child)
            <option value="{{ $child->id }}">{{ $child->value }}</option>
        @endforeach
    </select>
@endforeach
