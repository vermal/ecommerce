<?php

namespace Vermal\Ecommerce\Modules\Product\Repositories;

use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Database\Database;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Tax\Repositories\TaxRepository;


class ProductRepository
{
	/**
	 * Get products
	 *
	 * @return Database
	 */
	public function getProducts()
	{
		return Database::Model('Product')
			->where('p.parent', '=', null);
	}

    /**
     * Find product by id
     *
     * @param $id
     */
    public function findById($id)
    {
        $product = Database::Model('Product')->find($id);
        echo json_encode($product->toArray());
    }

    /**
     * Find product by id with tax rate
     *
     * @param $id
     */
    public function findByIdWithTax($id)
    {
        $product = Database::Model('Product')->find($id);
        $product = $product->toArray();
        $tax = TaxRepository::getTaxRate('*',  '*', $product['categories']);

        echo json_encode($product->toArray());
    }

    /**
     * Search customers
     *
     * @param $q
     * @param $page
     */
    public function search($q, $page)
    {
        $products = Database::Model('Product')
            ->where('p.title', '%', $q)
            ->orWhere('p.ean', '%', $q)
            ->paginate(8);
        $output = [
            'results' => [],
            'pagination' => [
                'more' => (ceil($products->count() / 8) - 1) == $page ? true : false
            ]
        ];
        foreach ($products as $product) {
            /** @var Product $product */
            $output['results'][] = [
                'id' => $product->id,
                'text' => $product->title
            ];
        }
        echo json_encode($output);
    }
}
