<?php

namespace Vermal\Ecommerce\Modules\Product\Repositories;


use Doctrine\Common\Collections\ArrayCollection;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;

class ProductMetaRepository
{
    /**
     * Current locale
     *
     * @var string $locale
     */
    private static $locale = '';

    /**
     * Add product meta
     *
     * @param $meta_key
     * @param $data
     * @param $id integer
     * @param $updateBy
     * @param string $locale
     *
     * @return ProductMeta
     */
    public static function addMeta($meta_key, $data, $id = null, $updateBy = 'key', $locale = null)
    {
        $meta = self::findMeta($meta_key, $id, $updateBy);
        $meta->key = $meta_key;
        if (isset($data[App::get('locale')])) {
            foreach ($data as $locale => $d) {
                self::$locale = $locale;
                self::hydrateData($meta, $d);
            }
        } else {
            if (empty($locale)) $locale = App::get('locale');
            self::$locale = $locale;
            self::hydrateData($meta, $data);
        }

        return Database::saveAndFlush($meta);
    }

    /**
     * Update product meta
     *
     * @param $id
     * @param $meta_key
     * @param $data
     * @param $updateBy
     * @param null $locale
     */
    public static function updateMeta($id, $meta_key, $data, $updateBy = 'key', $locale = null)
    {
        self::addMeta($meta_key, $data, $id, $updateBy, $locale);
    }

    /**
     * @param $meta_key
     * @param $id
     * @param $updateBy
     * @return ProductMeta
     */
    private static function findMeta($meta_key, $id, $updateBy)
    {
        $meta = Database::Model('ProductMeta');
        if ($updateBy == 'key') $meta->where('p.key', $meta_key);
        else if ($updateBy == 'id') $meta->where('p.id', $id);
        $meta = $meta->first();
        if (empty($meta) || !in_array($updateBy, ['key', 'id'])) {
            $meta = Entity::getEntity('ProductMeta');
        } else {
            $meta->clearTr();
            Database::saveAndFlush($meta);
        }
        return $meta;
    }

    /**
     * @param ProductMeta $meta
     * @param $data
     */
    private static function hydrateData(&$meta, $data)
    {
        foreach ($data as $key => $value) {

            if ($key != 'parent') {
                // Save default value
                if (self::$locale == App::get('locale')) {
                    $meta->{$key} = $value;
                }

                // Save value to translation table
                Database::translate($meta, 'ProductMetaTr', $key, self::$locale, $value);
            } else if ($key == 'parent') {
                // Assign parent
                $meta->parent = $value;
                $value->addChild($meta);
                Database::save($value);
            }
        }
    }

    /**
     * Get product meta
     *
     * @param $meta_key
     * @param bool|integer $id|$single
     *
     * @return ProductMeta|ArrayCollection $meta
     */
    public static function getMeta($meta_key, $id = true)
    {
        $meta = Database::Model('ProductMeta')->where('p.key', $meta_key);
        if (is_numeric($id)) $meta = $meta->where('p.id', $id)->first();
        else if (is_bool($id) && $id) $meta = $meta->first();
        else $meta = $meta->get();
        return $meta;
    }

    /**
     * Delte meta by id
     *
     * @param integer $id
     */
    public static function deleteMeta($id)
    {
        /** @var ProductMeta $meta */
        $meta = Database::Model('ProductMeta')->find($id);
        $meta->clearTr();
        Database::saveAndFlush($meta);
        Database::Model('ProductMeta')->delete($id);
    }

}
