<?php

namespace Vermal\Ecommerce\Modules\Product;


use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\App;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductCategory;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMetaTr;
use Vermal\Ecommerce\Modules\Product\Entities\ProductTr;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Admin\Upload;
use Vermal\Database\Database;


class ProductCategories extends Controller
{

	private $translatableFields = ['title', 'te'];
	private $fieldsAfter = ['parent'];

    /**
     * ProductCategories constructor.
     */
    public function __construct()
    {
        parent::__construct();
        // $this->requiredPermission('product', $this->CRUDAction);
        $this->addToBreadcrumb('product_category', 'admin.product-category.index');

		$this->translatableFields = apply_filters('ecommerce_product_category_translatable_fields', $this->translatableFields);
		$this->fieldsAfter = apply_filters('ecommerce_product_category_fields_after', $this->fieldsAfter);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('ProductCategory');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('product.title'));

        // Add actions
        $datagrid->addEdit(['admin.product-category.edit', ['id']]);
        $datagrid->addDelete(['admin.product-category.destroy_', ['id']]);

		// Datagrid
		$datagrid = apply_filters('ecommerce_product_category_datagrid', $datagrid);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.createWithTr', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => $this->translatableFields,
            'fieldsAfter' => $this->fieldsAfter,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.product.create');
        }

        // Hydrate data
        $this->hydrate($post);

        // Redirect with message
        $this->alert($this->_('productCategory.created'));
        Router::redirect('admin.product-category.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.product-category.update', ['id' => $id]));

        $form->getComponent('parent')
			->addAttribute('data-fields', htmlspecialchars(json_encode(['id' => $id])));

        /** @var ProductCategory $meta */
        $entity = Database::Model('ProductCategory')->find($id);

        // Add to breadcrumb
        $this->addToBreadcrumb('dyn', 'admin.product-category.edit', ['id' => $id], [$entity->title]);

        // Set values
        foreach (Languages::get() as $lang) {
            $form->getComponent($lang . '_title')->setValue($entity->getTr($lang, 'title'));
			$form->getComponent($lang . '_te')->setValue($entity->getTr($lang, 'te'));
        }

        if (!empty($entity->parent)) {
        	$form->getComponent('parent')
				->setSelect([$entity->parent->id => $entity->parent->title]);
		}

		// Set values
		$form = apply_filters('ecommerce_product_category_form_values', $form, $entity);

        View::view('crud.createWithTr', [
            "form" => $form->build(),
            "formName" => $form->getName(),
            "fields" => $this->translatableFields,
			"fieldsAfter" => $this->fieldsAfter,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        // Hydrate data
        $this->hydrate($post, $id);

        // Send message
        $this->alert($this->_('productCategory.edited'));
    }

    /**
     * @param int $id
     * @param object $post
     */
    private function hydrate($post, $id = null)
    {
    	if (!empty($id)) {
			$entity = Database::Model('ProductCategory')->find((int)$id);
			$entity->clearTr();
			Database::saveAndFlush($entity);
		} else
    		$entity = Entity::getEntity('ProductCategory');

		// Set parent
		if (!empty($post->parent)) {
			$entity->parent = Database::Model('ProductCategory')->find($post->parent);
		}

		// Get Entity from storage
//		foreach (Languages::get() as $lang) {
//			echo $entity->parent->getTr($lang, 'title');
//		}

        foreach (Languages::get() as $lang) {
        	$tr = 'ProductCategoryTr';

        	$value = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});
        	Database::translate($entity, $tr, 'title', $lang, $value);
			Database::translate($entity, $tr, 'te', $lang, Sanitizer::applyFilter('trim', $post->{$lang . '_te'}));
//			Database::translate($entity, $tr, 'slug', $lang, implode('/', $slug[$lang]));
        }

		$slug = $this->getFullSlugToParent($entity);
		foreach (Languages::get() as $lang) {
			$tr = 'ProductCategoryTr';
			Database::translate($entity, $tr, 'slug', $lang, implode('/', array_reverse($slug[$lang])));
		}

		// Store custom fields
		$entity = apply_filters('ecommerce_product_category_hydrate', $entity, $post);

        Database::saveAndFlush($entity);
    }

	/**
	 * Get full path to parent by lang
	 *
	 * @param $entity
	 * @param array $slug
	 * @return array
	 */
	private function getFullSlugToParent($entity, $slug = [])
	{
		if (!empty($entity->parent)) {
			foreach (Languages::get() as $lang) {
				if (empty($slug[$lang])) $slug[$lang] = [];
				$slug[$lang][] = self::slugify($entity->getTr($lang, 'title'));
			}
			$slug = $this->getFullSlugToParent($entity->parent, $slug);
		} else {
			foreach (Languages::get() as $lang) {
				if (empty($slug[$lang])) $slug[$lang] = [];
				$slug[$lang][] = self::slugify($entity->getTr($lang, 'title'));
			}
		}
		return $slug;
	}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Database::Model('ProductCategory')->delete((int)$id);
        $this->alert('Category was deleted sucesfully');
        Router::redirect('admin.product-category.index');
    }

    /**
     * Create product category form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('product-category', routerLink('admin.product-category.store'));

        $form->addSelect('parent', [null => 'Parent'], 'Parent')
			->addAttribute('data-controller', self::class)
			->setClass('js-select2 search');

        foreach (Languages::get() as $lang) {
            $form->addText($lang . '_title', 'Category name')->min(2);
			$form->addTextEditor($lang . '_te', 'Description');
        }

		// Allow form extension
		$form = apply_filters('ecommerce_product_category_form', $form);

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

	/**
	 * Select 2 support
	 *
	 * @param $query
	 * @return array<Database,array>
	 */
	public static function select2($query)
	{
		$query = Database::Model('ProductCategory')
			->where('p.title', '%', $query);

		// remove current id from select
		if (!empty($_POST['fields']['id']))
			$query = $query->where('p.id', '!=', $_POST['fields']['id']);

		return [
			$query,
			['id', 'title']
		];
	}
}
