<?php

namespace Vermal\Ecommerce\Modules\Coupon;

class CouponTypes
{
	const PERCENTAGE_CART_DISCOUNT = 'percentage_cart_discount';
	const FIXED_CART_DISCOUNT = 'fixed_cart_discount';
	const PERCENTAGE_PRODUCT_DISCOUNT = 'percentage_product_discount';
	const FIXED_PRODUCT_DISCOUNT = 'fixed_product_discount';
}
