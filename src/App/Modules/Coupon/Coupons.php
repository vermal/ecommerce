<?php

namespace Vermal\Ecommerce\Modules\Coupon;


use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Coupon\Entities\Coupon;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Customers\Repositories\CustomerRepository;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;


class Coupons extends Controller
{
	private $couponTypes = [];

    public function __construct()
    {
        parent::__construct();
//        $this->requiredPermission('coupon', $this->CRUDAction);
        $this->addToBreadcrumb('coupon', 'admin.coupon.index');
        $this->couponTypes = apply_filters('ecommerce_couponTypes', [
        	CouponTypes::FIXED_CART_DISCOUNT => $this->_('coupon.types.fixed_cart_discount'),
			CouponTypes::PERCENTAGE_CART_DISCOUNT => $this->_('coupon.types.percentage_cart_discount'),
			CouponTypes::FIXED_PRODUCT_DISCOUNT => $this->_('coupon.types.fixed_product_discount'),
			CouponTypes::PERCENTAGE_PRODUCT_DISCOUNT => $this->_('coupon.types.percentage_product_discount'),
		]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('Coupon');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('code', $this->_('coupon.code'));

        // Add actions
        $datagrid->addEdit(['admin.coupon.edit', ['id']]);
        $datagrid->addDelete(['admin.coupon.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
	    $form = $this->form();
	    View::view('crud.create', [
		    "form" => $form->build()
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.coupon.create');
        }

        $coupon = Entity::getEntity('Coupon');
        $this->hydrate($coupon, $post);

        Database::saveAndFlush($coupon);

        // Redirect with message
        $this->alert($this->_('coupon.created'));
        Router::redirect('admin.coupon.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.coupon.update', ['id' => $id]));

        /** @var Coupon $address */
        $coupon = Database::Model('Coupon')->find($id);

        $form->setValues($coupon);

		// Set values price
		foreach (Currencies::get() as $currency) {
			$price = $coupon->getPrice($currency);
			if (empty($price)) continue;
			$form->getComponent('price_' . $currency)->setValue($price->price);
		}

		// Set customer
		if (!empty($coupon->customer)) {
			$form->getComponent('customer')->setSelect([
				$coupon->customer->id => $coupon->customer->getFullNameAndEmail()
			])->setValue($coupon->customer->id);
		}

        View::view('crud.create', [
	        "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

	    $coupon = Database::Model('Coupon')->find($id);
		$this->hydrate($coupon, $post, true);

        $this->alert($this->_('coupon.edited'));
        Database::saveAndFlush($coupon);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
    	$coupon = Database::Model('Coupon')->find($id);
    	$coupon->clearPrices();
    	Database::saveAndFlush($coupon);
        Database::Model('Coupon')->delete($id);

        // Redirect
        $this->alert($this->_('coupon.deleted'));
        Router::redirect('admin.coupon.index');
    }

	/**
	 * Hydrate database object
	 *
	 * @param Coupon $entity
	 * @param $post
	 * @param $update
	 * @return object
	 * @throws \Exception
	 */
	private function hydrate($entity, $post, $update = false)
	{
		$entity->code = self::slugify($this->sanitize('trim|escape|strip_tags', $post->code));
		$entity->type = $this->sanitize('trim|escape|strip_tags', $post->type);

		$entity->minimumSpend = $this->sanitize('trim|float', $post->minimumSpend);
		$entity->maximumSpend = $this->sanitize('trim|float', $post->maximumSpend);
		$entity->individual = $this->sanitize('trim|digit', $post->individual);
		$entity->excludeSaleItems = $this->sanitize('trim|digit', $post->excludeSaleItems);

		// Customer
		if (!empty($post->customer)) {
			$customer = Database::Model('Customer')->find($this->sanitize('digit|strip_tags', $post->customer));
			if (!empty($customer)) {
				$entity->customer = $customer;
			}
		}

		// Add prices
		if ($update) $entity->clearPrices();
		foreach (Currencies::get() as $curency) {
			$entityPrice = Entity::getEntity('CouponPrice');

			// Currency
			$entityPrice->currency = $curency;

			// Price settings
			$entityPrice->price = Sanitizer::applyFilter('trim|float', $post->{'price_' . $curency});

			// Save
			$entityPrice->coupon = $entity;
			$entity->addPrice($entityPrice);
			Database::save($entityPrice);
		}
		$entity->discountAmount = $this->sanitize('trim|float', $post->discountAmount);


		return $entity;
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('coupon', routerLink('admin.coupon.store'));

	    $form->addText('code', $this->_('coupon.code'))->required();
		$form->addSelect('type', $this->couponTypes, $this->_('coupon.type'))
			->required()
			->toggle('currencies_coupon', 'IN', [CouponTypes::FIXED_PRODUCT_DISCOUNT, CouponTypes::FIXED_CART_DISCOUNT])
			->toggle('discountAmountContainer_coupon', 'IN', [CouponTypes::PERCENTAGE_PRODUCT_DISCOUNT, CouponTypes::PERCENTAGE_CART_DISCOUNT]);

		// Rules
		$form->addText('minimumSpend', $this->_('coupon.minimumSpend'))
			->setCol('col-lg-6');
		$form->addText('maximumSpend', $this->_('coupon.maximumSpend'))
			->setCol('col-lg-6');
		$form->addCheckboxSwitch('individual', $this->_('coupon.individual'))
			->setValue(1)->setCol('col-lg-6');
		$form->addCheckboxSwitch('excludeSaleItems', $this->_('coupon.excludeSaleItems'))
			->setValue(1)->setCol('col-lg-6');

		$form->addSelect('customer', [null => $this->_('coupon.customer')], $this->_('coupon.customer'))
			->setClass('js-select2 search')
			->addAttribute('data-controller', self::class)->addAttribute('data-method', 'searchCustomers');

		// Discount fixed price
		$form->startGroup('currencies', 'col-12');
			foreach ($this->settings->currencies as $currency) {
				$name = 'price_' . $currency;
				$this->fieldsAfter[] = $name;
				$form->addInput($name, 'text', $this->_('coupon.price') . ' ' . $currency)
					->setCol('col-md-2 col-sm-3 col-6');
			}
	    $form->endGroup();

		// Discount amount
		$form->startGroup('discountAmountContainer', 'col-12');
	    	$form->addText('discountAmount', $this->_('coupon.discountAmount'));
	    $form->endGroup();

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

	/**
	 * Select 2 support
	 *
	 * @param $query
	 * @return array<array, Database>
	 */
	public static function searchCustomers($query)
	{
		return [
			Database::Model('Customer')
				->where('c.name', '%', $query)
				->orWhere('c.surname', '%', $query)
				->orWhere('c.email', '%', $query),
			['id', 'fullNameAndEmail']
		];
	}
}
