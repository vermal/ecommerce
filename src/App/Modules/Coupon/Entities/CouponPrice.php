<?php

namespace Vermal\Ecommerce\Modules\Coupon\Entities;

use Vermal\Database\MagicAccessor;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_coupon_price")
 * @ORM\HasLifecycleCallbacks
 **/
class CouponPrice
{
	use MagicAccessor;

	/** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
	protected $id;

	/** @ORM\Column(type="string") **/
	protected $currency;

	/** @ORM\Column(type="float") **/
	protected $price;

	/**
	 * @ORM\ManyToOne(targetEntity="\Coupon", inversedBy="price", cascade={"persist"})
	 * @ORM\JoinColumn(name="coupon_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $coupon;

	/**
	 * Get formatted price
	 *
	 * @return string
	 */
	public function getFormattedPrice()
	{
		return $this->price . ' ' . Currencies::getSymbol($this->currency);
	}
}
