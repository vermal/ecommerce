<?php

namespace Vermal\Ecommerce\Modules\Coupon\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_coupon")
 * @ORM\HasLifecycleCallbacks
 **/
class Coupon extends Model
{

	use Translatable;

    /**
     * @ORM\Column(type="string")
     */
    protected $code;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $type;



	// RULES
	/**
	 * @ORM\Column(type="float", name="minimum_spend")
	 */
	protected $minimumSpend;

	/**
	 * @ORM\Column(type="float", name="maximum_spend")
	 */
	protected $maximumSpend;

	/**
	 * @ORM\Column(type="integer", nullable=true)
	 */
	protected $individual;

	/**
	 * @ORM\Column(type="integer", name="exclude_sale_items", nullable=true)
	 */
	protected $excludeSaleItems;

	/**
	 * @ORM\ManyToOne(targetEntity="\Customer")
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
	 */
	protected $customer;
	// .RULES



	/**
	 * @ORM\OneToMany(targetEntity="\CouponPrice", mappedBy="coupon", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $discountPrice;

	/**
	 * @ORM\Column(type="float", name="discount_amount", nullable=true)
	 */
	protected $discountAmount;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $expiry;

	public function __construct()
	{
		parent::__construct();
		$this->discountPrice = new ArrayCollection();
	}

	/**
	 * @param $currency
	 * @return CouponPrice
	 */
	public function getPrice($currency = null)
	{
		if (is_null($currency)) return $this->discountPrice->first();
		foreach ($this->discountPrice as $discountPrice) {
			if ($discountPrice->currency == $currency) return $discountPrice;
		}
	}

	/**
	 * @param $discountPrice
	 */
	public function addPrice($discountPrice)
	{
		$this->discountPrice[] = $discountPrice;
	}

	/**
	 * Clear discountPrices
	 */
	public function clearPrices()
	{
		$this->discountPrice->clear();
	}
}
