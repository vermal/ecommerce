<?php

namespace Vermal\Ecommerce\Modules\Payment\Repositories;

use Vermal\Database\Database;


class PaymentMethodRepository
{
    /**
     * Find product by id
     *
     * @param $id
     */
    public function findById($id)
    {
        $product = Database::Model('PaymentMethod')->find($id);
        echo json_encode($product->toArray());
    }


}
