<?php

namespace Vermal\Ecommerce\Modules\Payment;


use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\Upload;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Payment\Entities\PaymentMethod;
use Vermal\Ecommerce\Modules\Payment\Entities\PaymentMethodRule;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;


class PaymentMethods extends Controller
{

	public $defaultViewPath = false;

	// Payment options
	const COD = 'cod';
	const BACS = 'bacs';

	private $availableFields = ['title', 'description'];
	private $fieldsAfter = ['type'];
	private $paymentTypes = [];

    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('payment-methods', $this->CRUDAction);
        $this->addToBreadcrumb('paymentMethod', 'admin.paymentMethod.index');
		$this->paymentTypes = apply_filters('ecommerce_paymentMethodTypes', [
			self::COD => $this->_('paymentMethod.types.cod'),
			self::BACS => $this->_('paymentMethod.types.bacs'),
		]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
		$this->setDefaultViewPath();

        $data = Database::Model('PaymentMethod');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('paymentMethod.title'));
	    $datagrid->addColumn('description', $this->_('paymentMethod.description'))->setRenderer(function($entity) {
	    	return substr($entity->getTr($this->locale, 'description'), 0, 100) . ' ...';
	    });
		$datagrid->addColumn('price', $this->_('paymentMethod.price'))->setRenderer(function($entity) {
			return $entity->getPrice(self::$appCurrency)->getFormattedPrice();
		});

        // Add actions
        $datagrid->addEdit(['admin.paymentMethod.edit', ['id']]);
        $datagrid->addDelete(['admin.paymentMethod.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
		// Set default rule
		\Vermal\View::setVariable('rules', [[]]);
		\Vermal\View::setVariable('rulesEmpty', true);
	    $form = $this->form();
	    View::view('paymentMethods', [
		    "form" => $form->build(),
		    "formName" => $form->getName(),
		    "fields" => apply_filters('ecommerce_paymentMethod_fields', $this->availableFields),
		    "fieldsAfter" => apply_filters('ecommerce_paymentMethod_fields_after', $this->fieldsAfter),
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.paymentMethod.create');
        }

        $paymentMethod = Entity::getEntity('PaymentMethod');
        $this->hydrate($paymentMethod, $post);

        Database::saveAndFlush($paymentMethod);

        // Redirect with message
        $this->alert($this->_('paymentMethod.created'));
        Router::redirect('admin.paymentMethod.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.paymentMethod.update', ['id' => $id]));

        /** @var PaymentMethod $address */
        $paymentMethod = Database::Model('PaymentMethod')->find($id);

	    // Set values content
	    foreach (Languages::get() as $lang) {
		    $form->getComponent($lang . '_title')->setValue($paymentMethod->getTr($lang, 'title'));
		    $form->getComponent($lang . '_description')->setValue($paymentMethod->getTr($lang, 'description'));
	    }
		$form->getComponent('type')->setValue($paymentMethod->type);

	    // Set values price
	    foreach (Currencies::get() as $currency) {
		    $price = $paymentMethod->getPrice($currency);
		    if (empty($price)) continue;
		    $form->getComponent('price_' . $currency)->setValue($price->price);
	    }

		if (!empty($paymentMethod->icon))
			$form->getComponent('icon')->setValue($paymentMethod->icon->getFile());

		// Set rules
		$rules = $paymentMethod->rules;
		if ($rules->isEmpty()) $rules = [[]];
		\Vermal\View::setVariable('rules', $rules);
		\Vermal\View::setVariable('rulesEmpty', $paymentMethod->rules->isEmpty());
		$values = [];
		foreach ($paymentMethod->rules as $rule) {
			$values['condition'][] = $rule->condition;
			$values['operator'][] = $rule->operator;
			$values['conditionValue'][] = $rule->value;
			$values['conditionValueShipping'][] = $rule->value;
		}
		$form->setValues($values);

        View::view('paymentMethods', [
	        "form" => $form->build(),
	        "formName" => $form->getName(),
	        "fields" => apply_filters('ecommerce_paymentMethod_fields', $this->availableFields),
	        "fieldsAfter" => apply_filters('ecommerce_paymentMethod_fields_after', $this->fieldsAfter)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

	    $paymentMethod = Database::Model('PaymentMethod')->find($id);
	    $this->hydrate($paymentMethod, $post, true);

        $this->alert($this->_('paymentMethod.edited'));
        Database::saveAndFlush($paymentMethod);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
    	$paymentMethod = Database::Model('PaymentMethod')->find($id);
    	$paymentMethod->clearTr();
    	$paymentMethod->clearPrices();
    	Database::saveAndFlush($paymentMethod);
        Database::Model('PaymentMethod')->delete($id);

        // Redirect
        $this->alert($this->_('paymentMethod.deleted'));
        Router::redirect('admin.paymentMethod.index');
    }

	/**
	 * Hydrate database object
	 *
	 * @param PaymentMethod $entity
	 * @param $post
	 * @param $update
	 * @return object
	 * @throws \Exception
	 */
	private function hydrate($entity, $post, $update = false)
	{
		$entity->type = $this->sanitize('trim', $post->type);

		// Add translation to content
		if ($update) {
			$entity->clearTr();
			Database::saveAndFlush($entity);
		}
		foreach (Languages::get() as $lang)
		{
			$entityTr = 'PaymentMethodTr';
			Database::translate($entity, $entityTr, 'title', $lang, $this->sanitize('trim|escape|strip_tags', $post->{$lang . '_title'}));
			Database::translate($entity, $entityTr, 'description', $lang, $this->sanitize('trim', $post->{$lang . '_description'}));
		}

		// Add prices
		if ($update) $entity->clearPrices();
		foreach (Currencies::get() as $curency) {
			$entityPrice = Entity::getEntity('PaymentMethodPrice');

			// Currency
			$entityPrice->currency = $curency;

			// Price settings
			$entityPrice->price = Sanitizer::applyFilter('trim|float', $post->{'price_' . $curency});

			// Save
			$entityPrice->paymentMethod = $entity;
			$entity->addPrice($entityPrice);
			Database::save($entityPrice);
		}

		// Remove image
		if (empty($post->icon_hidden_image)) {
			$entity->icon = null;
		}
		// Add image
		if (!empty($_FILES['icon']['name'])) {
			$files = Upload::uploadFile('icon', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$entity->icon = Database::Model('Multimedia')->find($file);
			}
		}

		// Rules
		if ($update) $entity->clearRules();
		if (!empty($post->condition) && (!empty($post->conditionValue->{0}) || !empty($post->conditionValueShipping->{0}))) {
			foreach ($post->condition as $key => $condition) {
				$value = $post->conditionValue->{$key};
				if ($condition == 'shipping')
					$value = $post->conditionValueShipping->{$key};
				$operator = $post->operator->{$key};

				/** @var PaymentMethodRule $rule */
				$rule = Entity::getEntity('PaymentMethodRule');

				$rule->condition = $condition;
				$rule->value = $value;
				$rule->operator = $operator;

				$rule->paymentMethod = $entity;
				$entity->addRule($rule);
				Database::save($rule);
			}
		}

		return $entity;
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
    	$shippingMethods = [];
    	foreach (Database::Model('ShippingMethod')->get() as $shippingMethod) {
			$shippingMethods[$shippingMethod->id] = $shippingMethod->title . ' - ' .
				$shippingMethod->getPrice(self::$appCurrency)->getFormattedPrice();
    	}


        $form = new Form('paymentMethod', routerLink('admin.paymentMethod.store'));

	    // Product Data
	    foreach (Languages::get() as $lang) {
		    $form->addText($lang . '_title', $this->_('paymentMethod.title'))->min(1);
		    $form->addTextArea($lang . '_description', $this->_('paymentMethod.description'));
	    }

		$form->addSelect('type', $this->paymentTypes, $this->_('paymentMethod.type'))->required();

	    foreach ($this->settings->currencies as $currency) {
	    	$name = 'price_' . $currency;
			$this->fieldsAfter[] = $name;
	    	$form->addInput($name, 'text', $this->_('paymentMethod.price') . ' ' . $currency)
			    ->setCol('col-md-2 col-sm-3 col-6')
			    ->int();
	    }

		$form->addInput('icon', 'file', $this->_('paymentMethod.icon'));
		$this->fieldsAfter[] = 'icon';

		// Rules
		$form->addSelect('condition', PaymentMethodRules::getRules(), 'Condition')
			->setMultiple()->setCol('col-lg-3');
		$form->addSelect('operator', getOperators(), 'Operator')
			->setMultiple()->setCol('col-lg-2');
		$form->addText('conditionValue', 'Value')
			->setMultiple();
		$form->addSelect('conditionValueShipping', $shippingMethods, 'Value')
			->setMultiple();

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
