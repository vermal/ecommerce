@extends('layout')

@section('body')

    <div class="container">
        @form($formName)
            <div class="col-12 card shadow-lg m-b-30 p-4">

                @if(isset($fieldsBefore))
                    <div class="row">
                        @foreach($fieldsBefore as $field)
                            @label("{$field}")
                            @control("{$field}")
                        @endforeach
                    </div>
                @endif

                <ul class="nav nav-tabs tab-line" id="myTab" role="tablist">
                    @foreach($languages as $lang)
                        <li class="nav-item">
                            <a class="nav-link {{ $loop->first ? "show active" : "" }}" id="tab-{{ $lang }}" data-toggle="tab" href="#{{ $lang }}" role="tab" aria-controls="{{ $lang }}" aria-selected="true">
                                {{ strtoupper($lang) }}
                            </a>
                        </li>
                    @endforeach
                </ul>

                <div class="tab-content pt-4" id="myTabContent">
                    @foreach($languages as $lang)

                        <div class="tab-pane row {{ $loop->first ? "show active" : "" }}" id="{{ $lang }}" role="tabpanel" aria-labelledby="tab-{{ $lang }}">
                            @foreach($fields as $field)
                                @label("{$lang}_{$field}")
                                @control("{$lang}_{$field}")
                            @endforeach
                        </div>

                    @endforeach
                </div>

                @if(isset($fieldsAfter))
                    <div class="row">
                        @foreach($fieldsAfter as $field)
                            @label("{$field}")
                            @control("{$field}")
                        @endforeach
                    </div>
                @endif
            </div>

            <div class="col-12 card shadow-lg m-b-30 p-4">
                <div class="rules">
                    @foreach($rules as $rule)

                        <div class="rule row align-items-center {{ $rulesEmpty ? 'd-none' : '' }}">

                            @label("condition")
                            @control("condition")

                            @label("operator")
                            @control("operator")

                            <div class="value conditionValue col-lg-6">
                                <div class="row">
                                    @label("conditionValue")
                                    @control("conditionValue")
                                </div>
                            </div>

                            <div class="value conditionValueShipping col-lg-6">
                                <div class="row">
                                    @label("conditionValueShipping")
                                    @control("conditionValueShipping")
                                </div>
                            </div>

                            <div class="col-lg-1 text-right" style="margin-top: 15px;">
                                <a href="#" class="btn btn-danger btn-sm delete-rule">
                                    <i class="fas fa-trash"></i>
                                </a>
                            </div>
                        </div>

                    @endforeach
                </div>
                <div>
                    <input type="hidden" name="rules" value="{{ $rulesEmpty ? '0' : '1' }}">
                    <a href="#" class="btn btn-primary btn-sm add-rule">Add rule</a>
                </div>
            </div>

            <div class="col-12 card shadow-lg m-b-30 p-4">
                <div class="text-right mt-4">
                    @control("submit")
                </div>
            </div>
        @endform
    </div>

    <script>
        $(document).ready(function() {
            $('.nav-tabs .nav-link').click(function() {
                setTimeout(function() {
                    initPlugins();
                }, 10);
            });

            $('.add-rule').click(function(e) {
                e.preventDefault();
                let rules = $(this).parent().parent().find('.rules');
                let rule = rules.find('.rule:first');

                if (!rule.hasClass('d-none')) {
                    rule = rule.clone();
                    rule.find('.delete-rule').parent().removeClass('d-none');
                    rules.append(rule);
                } else {
                    $('input[name="rules"]').val(1);
                    rule.removeClass('d-none');
                }
            });

            $(document).on('click', '.delete-rule', function(e) {
                e.preventDefault();
                $(this).closest('.rule').remove();
            });

            $(document).on('change', '.rules .rule select[name="condition[]"]', function() {
                showCorrectValueInput($(this).closest('.rule'));
            });
            $('.rules .rule').each(function() {
                showCorrectValueInput($(this));
            });
            function showCorrectValueInput(rule) {
                let val = rule.find('select[name="condition[]"]').val();
                if (val === 'shipping') {
                    rule.find('.value').hide();
                    rule.find('.conditionValueShipping').show();
                } else {
                    rule.find('.value').hide();
                    rule.find('.conditionValue').show();
                }
            }
        });
    </script>

@endsection
