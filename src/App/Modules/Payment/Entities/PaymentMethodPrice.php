<?php

namespace Vermal\Ecommerce\Modules\Payment\Entities;

use Vermal\Database\MagicAccessor;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_payment_method_price")
 * @ORM\HasLifecycleCallbacks
 **/
class PaymentMethodPrice
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string") **/
    protected $currency;

    /** @ORM\Column(type="float") **/
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="\PaymentMethod", inversedBy="price", cascade={"persist"})
     * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $paymentMethod;

    /**
     * Get formatted price
     *
     * @return string
     */
    public function getFormattedPrice()
    {
        return $this->price . ' ' . Currencies::getSymbol($this->currency);
    }
}
