<?php

namespace Vermal\Ecommerce\Modules\Payment\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_payment_method")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\PaymentMethodTr")
 **/
class PaymentMethod extends Model
{
	use Translatable;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $title;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @ORM\OneToMany(targetEntity="\PaymentMethodPrice", mappedBy="paymentMethod", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $price;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $type;

	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
	 */
	protected $icon;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="\PaymentMethodTr",
	 *   mappedBy="object",
	 *   cascade={"persist", "remove"},
	 *   orphanRemoval=true
	 * )
	 */
	protected $translations;

	/**
	 * @ORM\OneToMany(targetEntity="\PaymentMethodRule", mappedBy="paymentMethod", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $rules;

	public function __construct()
	{
		parent::__construct();
		$this->price = new ArrayCollection();
		$this->translations = new ArrayCollection();
		$this->rules = new ArrayCollection();
	}

	/**
	 * @param $currency
	 * @return PaymentMethodPrice
	 */
	public function getPrice($currency)
	{
		foreach ($this->price as $price) {
			if ($price->currency == $currency) return $price;
		}
	}

	/**
	 * @param $price
	 */
	public function addPrice($price)
	{
		$this->price[] = $price;
	}

	/**
	 * Clear prices
	 */
	public function clearPrices()
	{
		$this->price->clear();
	}

	/**
	 * @return ArrayCollection
	 */
	public function getRules()
	{
		return $this->rules;
	}

	/**
	 * @param $rule
	 */
	public function addRule($rule)
	{
		$this->rules[] = $rule;
	}

	/**
	 * Clear rules
	 */
	public function clearRules()
	{
		$this->rules->clear();
	}
}
