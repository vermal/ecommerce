<?php

namespace Vermal\Ecommerce\Modules\Payment;

class PaymentMethodRules
{

	/**
	 * @var array $rules
	 */
	private static $rules = [];

	public static function getRules()
	{
		return [
			'Shipping address' => [
				'zip' => 'ZIP / Postal Code',
				'country' => 'Country'
			],
			'Cart' => [
				'subtotal' => 'Subtotal',
				'number-of-items' => 'Number of items',
			],
			'shipping' => 'Shipping method'
//			'Package measurements' => [
//				'total-weight' => 'Total weight',
//				'total-height' => 'Total height',
//				'total-width' => 'Total width',
//				'total-length' => 'Total length'
//			]
		];
	}

	/**
	 * Add rule
	 *
	 * @param $group
	 * @param $rule
	 * @param $value
	 */
	public static function addRule($group, $rule, $value)
	{
		if (!isset(self::$rules[$group]))
			self::$rules[$group] = [];
		self::$rules[$group][$rule] = $value;
	}

}
