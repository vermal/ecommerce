<?php

namespace Vermal\Ecommerce\Modules\Tax\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_tax")
 * @ORM\HasLifecycleCallbacks
 **/
class Tax extends Model
{
    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="string", name="country_code")
     */
    protected $countryCode;

    /**
     * @ORM\Column(type="string")
     */
    protected $postcode;

    /**
     * @ORM\Column(type="float")
     */
    protected $rate;

    /**
     * @ORM\Column(type="integer")
     */
    protected $priority;

    /**
     * @ORM\ManyToMany(targetEntity="\ProductCategory")
     * @ORM\JoinTable(name="ecommerce_tax_product_category",
     *      joinColumns={@ORM\JoinColumn(name="tax_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="product_category_id", referencedColumnName="id")}
     *      )
     */
    protected $productCategories;

    public function __construct()
    {
        parent::__construct();
        $this->productCategories = new ArrayCollection();
    }

    /**
     * Clear prices
     */
    public function clearProductCategories()
    {
        $this->productCategories->clear();
    }

    /**
     * @param $productMeta
     */
    public function addProductCategory($productMeta)
    {
        $this->productCategories[] = $productMeta;
    }

}
