<?php

namespace Vermal\Ecommerce\Modules\Tax;

use Vermal\Admin\Defaults\Countries;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Ecommerce\Modules\Tax\Repositories\TaxRepository;
use Vermal\Form\Form;
use Vermal\Router;

class Taxes extends Controller
{
    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('tax', $this->CRUDAction);
        $this->addToBreadcrumb('tax', 'admin.tax.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('Tax')->order('t.priority', 'DESC');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('tax.title'));
        $datagrid->addColumn('countryCode', $this->_('tax.countryCode'));
        $datagrid->addColumn('postcode', $this->_('tax.postcode'));
        $datagrid->addColumn('priority', $this->_('tax.priority'));
        $datagrid->addColumn('rate', $this->_('tax.rate'));


        // Add actions
        $datagrid->addEdit(['admin.tax.edit', ['id']]);
        $datagrid->addDelete(['admin.tax.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     */
    public function store($post)
    {
        $this->hydrate(Entity::getEntity('Tax'), $post);
        $this->alert($this->_('tax.created'), 'success');
        Router::redirect('admin.tax.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.tax.update', ['id' => $id]));

        $tax = Database::Model('Tax')->find($id);

        // Set simple values
        $form->setValues($tax, ['productCategories']);

        // Set product categories
        $productCategories = [];
        foreach ($tax->productCategories as $productCategory) {
            $productCategories[$productCategory->id] = $productCategory->title;
        }
        $form->getComponent('productCategories')->setValue($productCategories);

        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
        $this->hydrate(Database::Model('Tax')->find($id), $post, true);
        $this->alert($this->_('tax.edited'), 'success');
    }

    /**
     * Prepare data for save
     *
     * @param $tax
     * @param $post
     * @param bool $update
     */
    private function hydrate($tax, $post, $update = false)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.tax.create');
            die();
        }

        // Hydrate
        $form->hydrate($tax, ['productCategories', 'countryCode']);
        $tax->countryCode = Sanitizer::applyFilter('strip_tags|trim|lowercase', $post->countryCode);

        // Add categories to tax
        if ($update) $tax->clearProductCategories();
        if (!empty($post->productCategories)) {
            foreach ($post->productCategories as $productCategory) {

                // Validate category ID and try to find it
                $productCategory = Sanitizer::applyFilter('trim|digit', $productCategory);
                $newproductCategory = Database::Model('ProductCategory')->find($productCategory);

                // Check if category exist
                if ($newproductCategory === null) continue;

                $tax->addProductCategory($newproductCategory);
                Database::save($newproductCategory);
            }
        }

        Database::saveAndFlush($tax);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        $tax = Database::Model('Tax')->find($id);
        $tax->clearProductCategories();
        Database::saveAndFlush($tax);
        Database::Model('Tax')->delete($id);

        // Redirect
        $this->alert($this->_('tax.deleted'));
        Router::redirect('admin.tax.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('tax', routerLink('admin.tax.store'));

        $form->addText('title', $this->_('tax.title'))->required();
        $form->addSelect('countryCode', ['*' => '*'] + Countries::available(), $this->_('tax.countryCode'))
            ->setCol('col-lg-6')
            ->setClass('js-select2')
            ->setValue('*');
        $form->addText('postcode', $this->_('tax.postcode'))
            ->setCol('col-lg-6')
            ->setValue('*');
        $form->addText('rate', $this->_('tax.rate'))
            ->setCol('col-lg-6')
            ->required();
        $form->addText('priority', $this->_('tax.priority'))
            ->setCol('col-lg-6');

        // Items
        $categories = [];
        foreach (Database::Model('ProductCategory')->get() as $category) {
            $categories[$category->id] = $category->title;
        }
        $form->addSelect('productCategories', $categories, 'Product categories')
            ->setClass('js-select2')
            ->addAttribute('multiple', 'multiple');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

}
