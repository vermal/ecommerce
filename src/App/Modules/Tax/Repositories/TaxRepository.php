<?php

namespace Vermal\Ecommerce\Modules\Tax\Repositories;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\QueryBuilder;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;

class TaxRepository
{

    public static function getTaxRate($countryCode = '', $postcode = '', $productCategories = [])
    {
        $qb = Database::raw()
            ->select('t')
            ->from(Entity::getNamespace('Tax'), 't');

        // Join product categories
        $i = 1;
        // todo: product categories
//        $qb->leftJoin('t.productCategories', 'pc');
//        if (!empty($productCategories)) {
//            self::where($qb, 'id', $productCategories, $i, [], 'pc', 'in');
//        }
        self::where($qb, 'countryCode', $countryCode, $i);
        self::where($qb, 'postcode', $postcode, $i);


        $qb->orderBy('t.priority', 'DESC');
//        $qb->setMaxResults(1);
        return $qb->getQuery()->getResult();
    }

    /**
     * Where
     *
     * @param QueryBuilder $qb
     * @param $column
     * @param $value
     * @param $defaultValue
     * @param $i
     * @param string $alias
     * @param string $method
     */
    private static function where(&$qb, $column, $value, &$i, $defaultValue = '*', $alias = 't', $method = 'eq')
    {
        if (!empty($value)) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->{$method}($alias . '.' . $column, '?' . $i),
                $qb->expr()->{$method}($alias . '.' . $column, '?' . ($i + 1))
            ));
            $qb->setParameter($i, $value);
            $qb->setParameter($i + 1, $defaultValue);
            $i += 2;
        } else {
            $qb->andWhere($qb->expr()->{$method}($alias . '.' . $column, '?' . $i));
            $qb->setParameter($i, $defaultValue);
            $i++;
        }
    }

}
