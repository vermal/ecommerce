<?php

namespace Vermal\Ecommerce\Modules\Mailer;

use Vermal\Database\Database;
use Vermal\DataGrid;


class Mailer
{
    public function start()
    {
        $customers = Database::Model('Customer')->setPair('id', 'name', 'surname')->get();
        $this->index($customers);
        $this->store();
    }

    public function index($customers)
    {
        add_filter('mailer_preview_from', function($email, $inbox) {
            return $inbox->customer !== null
                ? '<a href="' . routerLink('admin.customer.edit', ['id' => $inbox->customer->id]) . '">' . $email . '</a>'
                : $email;
        });
        add_filter('mailer_datagrid', function(DataGrid $daragrid) use ($customers) {
            $daragrid->addFilter('customer', $customers, 'Customer', 'col-lg-3');
        });
    }

    public function store()
    {
        add_filter('mailer_save_mail', function($inbox) {
            $customer = Database::Model('Customer')->where('c.email', $inbox->to_email)->first();
            if ($customer !== null) {
                $inbox->customer = $customer;
            }
            return $inbox;
        });
    }
}
