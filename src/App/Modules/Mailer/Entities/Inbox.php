<?php

namespace Vermal\Ecommerce\Modules\Mailer\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity
 * @ORM\Table(name="mailer_inbox")
 * @ORM\HasLifecycleCallbacks
 **/
class Inbox extends Model
{

    /**
     * @ORM\ManyToOne(targetEntity="\Customer", inversedBy="messages")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

}
