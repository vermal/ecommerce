<?php

namespace Vermal\Ecommerce\Modules\PrivateMessages\Entities;

use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="private_message")
 * @ORM\HasLifecycleCallbacks
 **/
class PrivateMessage extends Model
{

	/**
	 * @ORM\Column(type="text")
	 */
	protected $message;

	/**
	 * @ORM\ManyToOne(targetEntity="\Customer", cascade={"persist"})
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
	 */
	protected $customer;

	/**
	 * @ORM\ManyToOne(targetEntity="\PrivateMessageConversation", cascade={"persist"})
	 * @ORM\JoinColumn(name="conversation_id", referencedColumnName="id")
	 */
	protected $conversation;

}
