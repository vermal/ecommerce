<?php

namespace Vermal\Ecommerce\Modules\PrivateMessages\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="private_message_conversation")
 * @ORM\HasLifecycleCallbacks
 **/
class PrivateMessageConversation extends Model
{

	/**
	 * @ORM\ManyToOne(targetEntity="\Customer", cascade={"persist"})
	 * @ORM\JoinColumn(name="customer_from_id", referencedColumnName="id")
	 */
	protected $from;

	/**
	 * @ORM\ManyToOne(targetEntity="\Customer", cascade={"persist"})
	 * @ORM\JoinColumn(name="customer_to_id", referencedColumnName="id")
	 */
	protected $to;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $subject;

	/**
	 * @ORM\Column(type="integer", name="soft_delete_receiver")
	 */
	protected $softDeleteReceiver;

	/**
	 * @ORM\Column(type="integer", name="soft_delete_sender")
	 */
	protected $softDeleteSender;

	/**
	 * @ORM\OneToMany(targetEntity="\PrivateMessage", mappedBy="conversation")
	 * @ORM\OrderBy({"created_at" = "DESC"})
	 */
	protected $messages;

	public function __construct()
	{
		parent::__construct();
		$this->softDeleteSender = 0;
		$this->softDeleteReceiver = 0;
		$this->messages = new ArrayCollection();
	}

	public function addMessage($message)
	{
		$this->messages[] = $message;
	}

	public function clearMessages()
	{
		$this->messages->clear();
	}
}
