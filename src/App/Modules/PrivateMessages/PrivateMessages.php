<?php

namespace Vermal\Ecommerce\Modules\PrivateMessages;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\PrivateMessages\Entities\PrivateMessage;
use Vermal\Ecommerce\Modules\PrivateMessages\Entities\PrivateMessageConversation;
use Vermal\Ecommerce\Modules\Orders\Orders;
use Vermal\Ecommerce\Modules\Shipping\ShippingMethods;
use Vermal\Form\Form;
use Vermal\Router;

class PrivateMessages extends Controller
{

    public function __construct()
    {
        parent::__construct();
	 	//$this->requiredPermission('privateMessages', $this->CRUDAction);
        $this->addToBreadcrumb('privateMessage', 'admin.privateMessage.index');
        $this->setFluidLayoutContainer();
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('PrivateMessageConversation')
			->order('p.created_at', 'DESC');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
		$datagrid->addColumn('from', $this->_('privateMessage.from'))->setRenderer(function($entity) {
			return $entity->from->email;
		});
		$datagrid->addColumn('to', $this->_('privateMessage.to'))->setRenderer(function ($entity) {
			return $entity->to->email;
		});
		$datagrid->addColumn('subject', $this->_('privateMessage.subject'));

        // Add actions
		$datagrid->addEdit(['admin.privateMessage.edit', ['id']]);
		//$datagrid->addDelete(['admin.privateMessage.destroy_', ['id']]);

		$datagrid = apply_filters('ecommerce_privateMessages_datagrid', $datagrid);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

	/**
	 * Show the form for createing the specified resource.
	 *
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function create()
	{
		$form = $this->form();
		View::view('crud.create', [
			"form" => $form->build()
		]);
	}

	/**
	 * Store the specified resource in storage.
	 *
	 * @param $post
	 */
	public function store($post)
	{
		$form = $this->form();
		if ($form->hasError()) {
			Router::redirect('admin.privateMessage.create');
		}

		/** @var PrivateMessageConversation $entity */
		$entity = Entity::getEntity('PrivateMessageConversation');

		$this->hydrate($entity, $post);
		Database::saveAndFlush($entity);

		// Redirect with message
		$this->alert($this->_('privateMessage.created'));
		Router::redirect('admin.privateMessage.index');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @throws \Whoops\Exception\ErrorException
	 */
    public function edit($id)
    {
		/** @var PrivateMessage $entity */
		$entity = Database::Model('PrivateMessageConversation')->find($id);

		$form = $this->form();
		$form->setAjax(true);
		$form->setMethod('PUT');
		$form->setAction(routerLink('admin.privateMessage.update', ['id' => $id]));

		$form->setValues($entity, ['from', 'to']);

		if (!empty($entity->from)) {
			$form->getComponent('from')
				->addAttribute('disabled', 'disabled')
				->setSelect([$entity->from->id => $entity->from->getFullNameAndEmail()])
				->setValue($entity->from->id);
		}
		if (!empty($entity->to)) {
			$form->getComponent('to')
				->addAttribute('disabled', 'disabled')
				->setSelect([$entity->to->id => $entity->to->getFullNameAndEmail()])
				->setValue($entity->to->id);
		}

		View::view('crud.create', [
			"form" => $form->build()
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
		$form = $this->form();
		if ($form->hasError()) {
			$this->alert($this->_('error.form.empty'), 'danger');
			die();
		}

		/** @var PrivateMessageConversation $entity */
		$entity = Database::Model('PrivateMessageConversation')->find($id);

		$this->hydrate($entity, $post);
		Database::saveAndFlush($entity);

		$this->alert($this->_('privateMessage.edited'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
		$order = Database::Model('Order')->delete($id);
		$order->status = self::CANCELLED;
		Database::saveAndFlush($order);

		// Redirect
		$this->alert($this->_('privateMessage.deleted'));
		Router::redirect('admin.privateMessage.index');
    }

	/**
	 * Hydrate
	 *
	 * @param PrivateMessageConversation $entity
	 * @param $post
	 */
	public function hydrate(&$entity, $post)
	{
		$entity->from = Database::Model('Customer')->find($post->from);
		$entity->to = Database::Model('Customer')->find($post->to);
		$entity->subject = $post->subject;
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('privateMessage', routerLink('admin.privateMessage.store'));

		$form->addText('subject', $this->_('privateMessage.subject'));

		$form->addSelect('from', [null => $this->_('privateMessage.from')], $this->_('privateMessage.from'))
			->setClass('js-select2 search')
			->addAttribute('data-controller', self::class)->addAttribute('data-method', 'searchCustomers');

		$form->addSelect('to', [null => $this->_('privateMessage.to')], $this->_('privateMessage.to'))
			->setClass('js-select2 search')
			->addAttribute('data-controller', self::class)->addAttribute('data-method', 'searchCustomers');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))
			->setClass('btn btn-success');

        return $form;
    }

	/**
	 * Select 2 support
	 *
	 * @param $query
	 * @return array<array, Database>
	 */
	public static function searchCustomers($query)
	{
		return [
			Database::Model('Customer')
				->where('c.name', '%', $query)
				->orWhere('c.surname', '%', $query)
				->orWhere('c.email', '%', $query),
			['id', 'fullNameAndEmail']
		];
	}

}
