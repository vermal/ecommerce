<?php

namespace Vermal\Ecommerce\Modules\Shop;

use Vermal\Controller;

class Shop extends Controller
{


	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		echo 'shop';
	}

}
