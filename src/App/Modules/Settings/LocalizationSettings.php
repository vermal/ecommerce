<?php

namespace Vermal\Ecommerce\Modules\Settings;


use Vermal\Ecommerce\Defaults\Currencies;

class LocalizationSettings
{

    /**
     * Script that is executed when LocalizationSettings constructor is fired
     */
    public function start()
    {
        $this->index();
        $this->store();
        $this->form();
    }

    /**
     * Index page
     */
    private function index()
    {
        add_filter('localization_settings_fields', function($fields) {
            $fields[] = 'currency';
            return $fields;
        });
    }

    /**
     * Store settings
     */
    private function store()
    {
        add_filter('localization_settings_store', function($settings, $post) {
            $settings->currency = $post->currency;
            return $settings;
        });
    }

    /**
     * Customize form
     */
    private function form()
    {
        add_filter('localization_settings_form', function($form) {
            $form->addSelect('currency', Currencies::getWithLabel(), 'Currencies')
                ->setClass('js-select2');
            return $form;
        });
    }

}
