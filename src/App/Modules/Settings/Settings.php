<?php

namespace Vermal\Ecommerce\Modules\Settings;


use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\Upload;
use Vermal\Database\Database;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Form\Form;

class Settings
{

    /**
     * Script that is executed when LocalizationSettings constructor is fired
     */
    public function start()
    {
        $this->index();
        $this->store();
        $this->form();
    }

    /**
     * Index page
     */
    private function index()
    {
        add_filter('settings_tabs', function($tabs) {
            $tabs['tax'] = 'Tax';
			$tabs['invoice'] = 'Invoice';
			$tabs['checkout'] = 'Checkout';
            return $tabs;
        }, 1);
        add_filter('settings_fields', function($fields) {
            $fields['default'][] = 'currencies';
            $fields['tax'][] = 'include_tax';
			$fields['tax'][] = 'payment_tax';
			$fields['tax'][] = 'shipping_tax';

			// Invoice
			$fields['invoice'][] = 'invoice_company';
			$fields['invoice'][] = 'invoice_logo';
			$fields['invoice'][] = 'invoice_address';
			$fields['invoice'][] = 'invoice_country';
			$fields['invoice'][] = 'invoice_ico';
			$fields['invoice'][] = 'invoice_dic';
			$fields['invoice'][] = 'invoice_ic_dph';
			$fields['invoice'][] = 'invoice_company_note';
			$fields['invoice'][] = 'invoice_phone';
			$fields['invoice'][] = 'invoice_bank_account';
			$fields['invoice'][] = 'invoice_iban';
			$fields['invoice'][] = 'invoice_swift';
			$fields['invoice'][] = 'invoice_email';
			$fields['invoice'][] = 'invoice_note';
			$fields['invoice'][] = 'invoice_show_payment';
			$fields['invoice'][] = 'invoice_show_shipping';
			$fields['invoice'][] = 'invoice_color';
			$fields['invoice'][] = 'invoice_number_mask';
			$fields['invoice'][] = 'invoice_number_zeros';
			$fields['invoice'][] = 'invoice_dph_payer';

			$fields['invoice'][] = 'invoice_due_date_increment';


			// Checkout
			$fields['checkout'][] = 'checkout_brand_title';
			$fields['checkout'][] = 'checkout_brand_image';

            return $fields;
        });
        add_filter('settings_fields_exclude', function($fields) {
        	$fields[] = 'invoice_logo';
			$fields[] = 'checkout_brand_image';
        	return $fields;
		});
		add_filter('settings_form_values', function($form, $settings) {
			/** @var Form $form */
			/** @var Multimedia $multimedia */
			$multimedia = $settings->invoice_logo;
			if (!empty($multimedia))
				$form->getComponent('invoice_logo')->setValue($multimedia->getMiniFile());

			/** @var Multimedia $multimedia */
			$multimedia = $settings->checkout_brand_image;
			if (!empty($multimedia))
				$form->getComponent('checkout_brand_image')->setValue($multimedia->getMiniFile());
			return $form;
		});
    }

    /**
     * Store settings
     */
    private function store()
    {
        add_filter('settings_store', function($settings, $post) {
        	if (!empty($post->currencies)) {
				$settings->currencies = (array)$post->currencies;
			}
            $settings->include_tax = $post->include_tax;
			$settings->payment_tax = !empty($post->payment_tax) ? $post->payment_tax : 0;
			$settings->shipping_tax = !empty($post->shipping_tax) ? $post->shipping_tax : 0;

			// Invoice
			$settings->invoice_company = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_company);
			$settings->invoice_address = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_address);
			$settings->invoice_country = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_country);
			$settings->invoice_ico = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_ico);
			$settings->invoice_dic = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_dic);
			$settings->invoice_ic_dph = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_ic_dph);
			$settings->invoice_company_note = Sanitizer::applyFilter('trim', $post->invoice_company_note);
			$settings->invoice_bank_account = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_bank_account);
			$settings->invoice_iban = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_iban);
			$settings->invoice_swift = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_swift);
			$settings->invoice_phone = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_phone);
			$settings->invoice_email = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_email);
			$settings->invoice_note = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_note);
			$settings->invoice_show_payment = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_show_payment);
			$settings->invoice_show_shipping = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_show_shipping);
			$settings->invoice_color = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_color);

			$settings->invoice_number_mask = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_number_mask);
			$settings->invoice_number_zeros = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_number_zeros);
			$settings->invoice_dph_payer = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_dph_payer);

			$settings->invoice_due_date_increment = Sanitizer::applyFilter('strip_tags|trim', $post->invoice_due_date_increment);


			// Remove image
			if (empty($post->invoice_logo_hidden_image)) {
				$settings->invoice_logo = null;
			}
			// Upload file
			if (!empty($_FILES['invoice_logo']['name'])) {
				// todo: separate folder for settings upload
				$files = Upload::uploadFile('invoice_logo', 1);
				Upload::cropImages($files);
				foreach ($files as $file) {
					$settings->invoice_logo = Database::Model('Multimedia')->find($file);
				}
			}


			// Checkout
			$settings->checkout_brand_title = Sanitizer::applyFilter('strip_tags|trim', $post->checkout_brand_title);
			// Remove image
			if (empty($post->checkout_brand_image_hidden_image)) {
				$settings->checkout_brand_image = null;
			}
			// Upload file
			if (!empty($_FILES['checkout_brand_image']['name'])) {
				$files = Upload::uploadFile('checkout_brand_image', 1);
				Upload::cropImages($files);
				foreach ($files as $file) {
					$settings->checkout_brand_image = Database::Model('Multimedia')->find($file);
				}
			}
            return $settings;
        });
    }

    /**
     * Customize form
     */
    private function form()
    {
        add_filter('settings_form', function($form) {
            /** @var $form Form */
            $form->addSelect('currencies', Currencies::available(), 'Currencies')
                ->addAttribute('multiple', true)
                ->setClass('js-select2');

            // Tax
            $form->addCheckboxSwitch('include_tax', 'Prices entered with tax');

            // Payment tax
            $form->addText('payment_tax', 'Payment tax in %');
			// Shipping tax
			$form->addText('shipping_tax', 'Shipping tax in %');

			// Invoice
			$form->addText('invoice_company', 'Company name');
			$form->addInput('invoice_logo', 'file', 'Company logo');
			$form->addText('invoice_address', 'Address');
			$form->addText('invoice_country', 'Country');
			$form->addText('invoice_ico', 'IČO');
			$form->addText('invoice_dic', 'DIČ');
			$form->addText('invoice_ic_dph', 'IČ DPH');
			$form->addText('invoice_company_note', 'Company note');
			$form->addText('invoice_bank_account', 'Bank account');
			$form->addText('invoice_iban', 'IBAN');
			$form->addText('invoice_swift', 'Swift');
			$form->addText('invoice_phone', 'Phone');
			$form->addText('invoice_email', 'E-Mail');
			$form->addText('invoice_note', 'Note');
			$form->addCheckboxSwitch('invoice_show_payment', 'Show payment in invoice');
			$form->addCheckboxSwitch('invoice_show_shipping', 'Show shipping in invoice');
			$form->addText('invoice_color', 'Invoice color');
			$form->addCheckboxSwitch('invoice_dph_payer', 'DPH Payers');

			$form->addText('invoice_due_date_increment', 'Due date increment (default 14 days)');

			$form->addText('invoice_number_mask', 'Invoice number mask');
			$form->addText('invoice_number_zeros', 'Invoice count of zeros');

			$form->addText('checkout_brand_title', 'Brand title');
			$form->addInput('checkout_brand_image', 'file', 'Brand image');

            return $form;
        });
    }

}
