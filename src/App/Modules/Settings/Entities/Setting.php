<?php

namespace Vermal\Ecommerce\Modules\Settings\Entities;

/**
 * @ORM\Entity @ORM\Table(name="setting")
 * @ORM\HasLifecycleCallbacks
 **/
class Setting
{
    /** @ORM\Column(type="array", nullable=true) **/
    protected $currencies;

    /** @ORM\Column(type="integer", nullable=true) **/
    protected $include_tax;

	/** @ORM\Column(type="float", nullable=true) **/
	protected $payment_tax;

	/** @ORM\Column(type="float", nullable=true) **/
	protected $shipping_tax;

	// INVOICE
	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_company;

	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="invoice_logo", referencedColumnName="id", nullable=true)
	 */
	protected $invoice_logo;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_address;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_country;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_ico;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_dic;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_ic_dph;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_company_note;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_bank_account;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_iban;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_swift;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_phone;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_email;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_note;

	/** @ORM\Column(type="integer", nullable=true) **/
	protected $invoice_show_payment;

	/** @ORM\Column(type="integer", nullable=true) **/
	protected $invoice_show_shipping;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_color;

	/** @ORM\Column(type="string", nullable=true) **/
	protected $invoice_number_mask;

	/** @ORM\Column(type="integer", nullable=true) **/
	protected $invoice_number_zeros;

	/** @ORM\Column(type="integer", nullable=true) **/
	protected $invoice_dph_payer;

	/** @ORM\Column(type="integer", nullable=true) **/
	protected $invoice_due_date_increment;


	/** @ORM\Column(type="string", nullable=true) **/
	protected $checkout_brand_title;
	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="checkout_brand_image", referencedColumnName="id", nullable=true)
	 */
	protected $checkout_brand_image;
}
