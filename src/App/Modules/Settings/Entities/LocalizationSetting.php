<?php

namespace Vermal\Ecommerce\Modules\Settings\Entities;

/**
 * @ORM\Entity @ORM\Table(name="localization_setting")
 * @ORM\HasLifecycleCallbacks
 **/
class LocalizationSetting
{
    /** @ORM\Column(type="string", nullable=true) **/
    protected $currency;
}
