<?php

namespace Vermal\Ecommerce\Modules\Dashboard;


use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Ecommerce\Modules\Orders\Entities\OrderItem;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Router;

class Dashboard
{

	public function __construct()
	{
		add_filter('admin_dashboard_template', function() {
			$defaultViewPath = View::getViewPath();
			View::setViewPath(__DIR__ . '/');

			$filter = [
				routerLink('admin.dashboard', ['period' => 'whole']) => 'Whole period',
				routerLink('admin.dashboard') => 'Last 7 days',
				routerLink('admin.dashboard', ['period' => 'thismonth']) => 'This month',
				routerLink('admin.dashboard', ['period' => 'lastmonth']) => 'Last month',
				routerLink('admin.dashboard', ['period' => 'thisyear']) => 'This year',
				routerLink('admin.dashboard', ['period' => 'lastyear']) => 'Last year',
			];

			$view = View::view('dashboard', [
				'quickFilter' => $filter,
				'orders' => $this->getOrdersAnalytics()
			]);
			View::setViewPath($defaultViewPath);
			return $view;
		});
	}

	/**
	 * @return array
	 */
	private function getOrdersAnalytics()
	{
		$period = $this->calculatePeriod();
		$daysBetween = $this->daysBetween($period[0], $period[1]);

		$orders = Database::Model('Order')
			->where('o.created_at', '>=' , date('Y-m-d H:i:s', $period[0]))
			->where('o.created_at', '<=' , date('Y-m-d H:i:s', $period[1]))
			->where('o.softDelete', '!=', true)
			->get();

		$products_ = Database::Model('Product')
			->get();
		$products = [];
		foreach ($products_ as $product) {
			$cats = [];
			foreach ($product->categories as $cat) {
				$cats[] = $cat->title;
			}
			$products[$product->id] = [
				'orders' => 0,
				'title' => $product->title,
				'categories' => implode(', ', $cats)
			];
		}

		$salesPrice = 0;
		$salesCount = 0;
		$salesItems = 0;
		$salesShippingPrice = 0;
		$salesPaymentPrice = 0;
		$salesTaxPrice = 0;
		$currency = Controller::$appCurrencySymbol;
		$customers = [];
		$newCustomers = 0;
		$packagesOldCustomers = 0;
		$packagesNewCustomers = 0;
		$lastOrdersForWindow = [];
        $lastOrdersForWindow['last30'] = false;
        $lastOrdersForWindow['last60'] = false;
        $lastOrdersForWindow['last90'] = false;
        $lastOrdersForWindow['last120'] = false;
		foreach ($orders as $order) {
			/** @var Order $order */

			$salesPrice += $order->price;
			$salesCount++;
			$salesShippingPrice += $order->shipping_amount;
			$salesPaymentPrice += $order->payment_amount;
			$salesTaxPrice += $order->price_tax_amount;
			$items = 0;

			// Get product stats
			foreach ($order->items as $item) {
				/** @var OrderItem $item */
				/** @var Product $product */
				$product = $item->product;
				if (!empty($product->id) && !empty($products[$product->id])) {
					$products[$product->id]['orders'] += $item->qty;
					$items += $item->qty;
				}
			}
			$salesItems += $items;

			if (!empty($order->customer)) {
				if (!isset($customers[$order->customer->email])) {
					$newCustomer = $order->customer->orders->count() < 2;
					if (!$newCustomer) {
						foreach ($order->customer->orders as $o) {
							if (empty($o->created_at)) continue;
							if (strtotime('-30 days') < $o->created_at->getTimestamp()) {
								$last30Days = true;
							} else if (strtotime('-60 days') < $o->created_at->getTimestamp()) {
								$last60Days = true;
							} else if (strtotime('-90 days') < $o->created_at->getTimestamp()) {
								$last90Days = true;
							} else if (strtotime('-120 days') < $o->created_at->getTimestamp()) {
								$last120Days = true;
							}
						}
					}
					$customers[$order->customer->email] = [
						'customer' => $order->customer,
						'email' => $order->customer->email,
						'orders' => 1,
						'totalOrders' => $order->customer->orders->count(),
						'date' => $order->customer->getFormattedCreatedAt('d.m.Y'),
						'newCustomer' => $newCustomer ? 1 : 0,
						'lastOrderAt' => $order->customer->orders->first()->created_at->getTimestamp(),
						'firstOrderAt' => $order->customer->orders->last()->created_at->getTimestamp(),
						'last30' => $last30Days ?? false,
						'last60' => $last60Days ?? false,
						'last90' => $last90Days ?? false,
						'last120' => $last120Days ?? false,
					];
				} else {
					$customers[$order->customer->email]['orders']++;
				}

				$lastOrdersForWindow['last30'] = $last30Days ?? false;
				$lastOrdersForWindow['last60'] = $last60Days ?? false;
				$lastOrdersForWindow['last90'] = $last90Days ?? false;
				$lastOrdersForWindow['last120'] = $last120Days ?? false;

				if ($customers[$order->customer->email]['newCustomer']) {
					$newCustomers++;
					$packagesNewCustomers += $items;
				} else {
					$packagesOldCustomers += $items;
				}
			}
		}
		usort($customers, function ($a, $b) {
			return $b['lastOrderAt'] <=> $a['lastOrderAt'];
		});

//		$last30DaysOrderCustomers = 0;
//		$last60DaysOrderCustomers = 0;
//		$last90DaysOrderCustomers = 0;
//		foreach ($customers as $customer) {
//			$lastOrder = $customer['lastOrderAt'];
//			$customer_ = $customer['customer'];
//			if ($customer['newCustomer']) continue;
//			$dates = [];
//			foreach ($customer_->orders as $k => $o) {
//				if ($k == 0) continue;
//				if (strtotime('-30 days'))
//				$dates[] = $o->created_at->getTimestamp();
//			}
//		}

		$netSales = $salesPrice - $salesShippingPrice - $salesPaymentPrice - $salesTaxPrice;
		return [
			'gross-sales' => number_format($salesPrice, 2, ',', ' ') . $currency,
			'gross-sales-daily' => number_format($salesPrice / $daysBetween, 2, ',', ' ') . $currency,
			'net-sales' => number_format($netSales, 2, ',', ' ') . $currency,
			'net-sales-daily' => number_format($netSales / $daysBetween, 2, ',', ' ') . $currency,
			'shipping' => number_format($salesShippingPrice, 2, ',', ' ') . $currency,
			'payment' => number_format($salesPaymentPrice / $daysBetween, 2, ',', ' ') . $currency,
			'tax' => number_format($salesTaxPrice, 2, ',', ' ') . $currency,
			'orders' => $salesCount,
			'items' => $salesItems,
			'products' => $products,
			'customers'=> $customers,
			'newCustomers' => $newCustomers,
			'packagesNewCustomers' => $packagesNewCustomers,
			'packagesOldCustomers' => $packagesOldCustomers,
			'lastOrdersForWindow' => $lastOrdersForWindow
		];
	}


	/**
	 * Calulate period
	 *
	 * @return array
	 */
	private function calculatePeriod()
	{
		if (!empty(Controller::$params['period'])) {
			$period = Controller::$params['period'];
			if ($period == 'thismonth') {
				$firstDayUTS = mktime (0, 0, 0, date("m"), 1, date("Y"));
				$lastDayUTS = mktime (0, 0, 0, date("m"), date('t'), date("Y"));
				return [
					$firstDayUTS,
					$lastDayUTS
				];
			} else if ($period == 'lastmonth') {
				return [
					mktime(0, 0, 0, date("m")-1, 1),
					mktime(0, 0, 0, date("m"), 0)
				];
			} else if ($period == 'thisyear') {
				return [
					mktime(0, 0, 0, 1, 1, date("y")),
					mktime(0, 0, 0, 12, 31, date("y"))
				];
			} else if ($period == 'lastyear') {
				return [
					mktime(0, 0, 0, 1, 1, date("y") - 1),
					mktime(0, 0, 0, 12, 31, date("y") - 1)
				];
			} else if ($period == 'whole') {
				return [
					mktime(0, 0, 0, 1, 1, date("y") - 20),
					mktime(0, 0, 0, 12, 31, date("y"))
				];
			}
		} else if (!empty(Controller::$params['custom'])) {
			$period = Controller::$params['custom'];
			$date = explode(' - ', $period);
			return [
				strtotime(trim($date[0])),
				strtotime(trim($date[1]))
			];
		} else {
			return [
				strtotime('-7days'),
				strtotime('now')
			];
		}
	}

	/**
	 * Get days between dates
	 *
	 * @param $date1
	 * @param $date2
	 * @return float
	 */
	private function daysBetween($date1, $date2)
	{
		$datediff = $date2 - $date1;
		return round($datediff / (60 * 60 * 24));
	}
}
