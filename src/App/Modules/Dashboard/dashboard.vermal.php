@extends('layout')

@section('body')

	<div class="container pull-up">

		<div class="row">
			<div class="col-12  m-b-30">
				<div class="card">
					<div class="card-body">
						<form action="{{ routerLink('admin.dashboard') }}" method="GET">
							@foreach($quickFilter as $url => $value)
								<a href="{{ $url }}" class="btn btn-outline-primary {{ (currentLink(true) === $url ? 'active' : '') }}">{{ $value }}</a>
							@endforeach
							<div class="input-group mt-3">
								<input type="text" name="custom" class="daterange form-control"
									   placeholder="d.m.Y - d.m.Y" value="{{ !empty(\Vermal\Router::$params['custom']) ? \Vermal\Router::$params['custom'] : '' }}">
								<div class="input-group-append">
									<button class="btn btn-primary">Custom</button>
								</div>
							</div>
						</form>
					</div>
					<div class="">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['gross-sales'] }}</h2>
								<p class="fw-600">Gross sales</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['gross-sales-daily'] }}</h2>
								<p class="fw-600">Average gross sales daily</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['net-sales'] }}</h2>
								<p class="fw-600">Net sales</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['net-sales-daily'] }}</h2>
								<p class="fw-600">Average net sales daily</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['shipping'] }}</h2>
								<p class="fw-600">Charged for shipping</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['payment'] }}</h2>
								<p class="fw-600">Charged for payments</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['tax'] }}</h2>
								<p class="fw-600">Charged for tax</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['orders'] }}</h2>
								<p class="fw-600">Orders placed</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['items'] }}</h2>
								<p class="fw-600">Items purchased</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ count($orders['customers']) }}</h2>
								<p class="fw-600">Customers</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['newCustomers'] }}</h2>
								<p class="fw-600">New customers</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['packagesOldCustomers'] }}</h2>
								<p class="fw-600">Old customers packages</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['packagesNewCustomers'] }}</h2>
								<p class="fw-600">New customers packages</p>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['lastOrdersForWindow']['last30'] }}</h2>
								<p class="fw-600">Last 30 days</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['lastOrdersForWindow']['last60'] }}</h2>
								<p class="fw-600">Last 60 days</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['lastOrdersForWindow']['last90'] }}</h2>
								<p class="fw-600">Last 90 days</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 m-b-30">
				<div class="card card-hover">
					<div class="card-body">
						<div class="text-center">
							<div class="text-center">
								<h2 class="fw-600 p-t-20">{{ $orders['lastOrdersForWindow']['last120'] }}</h2>
								<p class="fw-600">Last 120 days</p>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<h3 class="mt-5">Products</h3>
		<hr>

		<div class="row">
			@foreach($orders['products'] as $product)
				<div class="col-lg-3 col-md-4 m-b-30">
					<div class="card card-hover">
						<div class="card-body">
							<div class="text-center">
								<div class="text-center">
									<h2 class="fw-600 p-t-20">{{ $product['orders'] }}</h2>
									<p class="fw-600">
										{{ $product['title'] }}
										<br>
										<small>{{ $product['categories'] }}</small>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endforeach
		</div>

		<h3 class="mt-5">Customers</h3>
		<hr>

		<div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<th>Email</th>
					<th>Orders</th>
					<th>Total orders</th>
					<th>Created at</th>
					<th>Last order at</th>
					<th>First order at</th>
					<th>New customer</th>
				</tr>
				@foreach($orders['customers'] as $customer)
					<tr>
						<td>{{ $customer['email'] }}</td>
						<td>{{ $customer['orders'] }}</td>
						<td>{{ $customer['totalOrders'] }}</td>
						<td>{{ $customer['date'] }}</td>
						<td>{{ $customer['lastOrderAt'] > 0 ? date('d.m.Y', $customer['lastOrderAt']) : '' }}</td>
						<td>{{ $customer['firstOrderAt'] > 0 ? date('d.m.Y', $customer['firstOrderAt']) : '' }}</td>
						<td>{{ $customer['newCustomer'] ? 'Yes' : 'No' }}</td>
					</tr>
				@endforeach
			</table>
		</div>

		{{--<div class="row">--}}
			{{--<div class="col-12  m-b-30">--}}
				{{--<div class="card">--}}
					{{--<div class="card-header">--}}
						{{--<div class="card-title">Distribution by number of followers</div>--}}
					{{--</div>--}}
					{{--<div class="card-body">--}}
						{{--<canvas id="chart-17"></canvas>--}}
					{{--</div>--}}
					{{--<div class="">--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}
		{{--</div>--}}

	</div>


	{{--<script v:admin:public:src="assets/vendor/chartjs/Chart.bundle.min.js"></script>--}}
	{{--<script>--}}

		{{--let ctx = $('#chart-17')[0].getContext('2d');--}}
		{{--var mixedChart = new Chart(ctx, {--}}
			{{--type: 'bar',--}}
			{{--data: {--}}
				{{--datasets: [{--}}
					{{--label: 'Bar Dataset',--}}
					{{--data: [10, 20, 30, 40],--}}
					{{--// this dataset is drawn below--}}
					{{--order: 1--}}
				{{--}, {--}}
					{{--label: 'Line Dataset',--}}
					{{--data: [10, 10, 10, 10],--}}
					{{--type: 'line',--}}
					{{--// this dataset is drawn on top--}}
					{{--order: 2--}}
				{{--}],--}}
				{{--labels: ['January', 'February', 'March', 'April']--}}
			{{--},--}}
			{{--options: {}--}}
		{{--});--}}

	{{--</script>--}}

@endsection
