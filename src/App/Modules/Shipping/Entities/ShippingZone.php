<?php

namespace Vermal\Ecommerce\Modules\Shipping\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_shipping_zone")
 * @ORM\HasLifecycleCallbacks
 **/
class ShippingZone extends Model implements \Gedmo\Translatable\Translatable
{
	use Translatable;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $title;

	/**
	 * @ORM\OneToOne(targetEntity="\ShippingZoneRegion", mappedBy="shippingZone", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $regions;

	public function __construct()
	{
		$this->regions = new ArrayCollection();
	}

	/**
	 * @param ShippingZoneRegion $region
	 */
	public function addRegion(ShippingZoneRegion $region)
	{
		$this->regions[] = $region;
	}

	/**
	 * Clear prices
	 */
	public function clearRegions()
	{
		$this->regions->clear();
	}
}
