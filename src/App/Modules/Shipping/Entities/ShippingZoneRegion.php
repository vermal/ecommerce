<?php

namespace Vermal\Ecommerce\Modules\Shipping\Entities;

use Vermal\Admin\Defaults\Model;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_shipping_zone_region")
 * @ORM\HasLifecycleCallbacks
 **/
class ShippingZoneRegion extends Model
{
    /**
     * @ORM\Column(type="string")
     */
    protected $region;

	/**
	 * @ORM\ManyToOne(targetEntity="\ShippingZone", inversedBy="regions", cascade={"persist"})
	 * @ORM\JoinColumn(name="shipping_zone_id", referencedColumnName="id")
	 */
    protected $shippingZone;
}
