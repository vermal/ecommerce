<?php

namespace Vermal\Ecommerce\Modules\Shipping\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Database\MagicAccessor;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_shipping_method_rule")
 * @ORM\HasLifecycleCallbacks
 **/
class ShippingMethodRule
{

	use MagicAccessor;

	/** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
	protected $id;

    /**
     * @ORM\Column(type="string", name="condition_")
     */
    protected $condition;

	/**
	 * @ORM\Column(type="string", name="operator_")
	 */
	protected $operator;

	/**
	 * @ORM\Column(type="string", name="value_")
	 */
	protected $value;

	/**
	 * @ORM\ManyToOne(targetEntity="\ShippingMethod", inversedBy="rules")
	 * @ORM\JoinColumn(name="shipping_method_id", referencedColumnName="id", onDelete="CASCADE")
	 */
	protected $shippingMethod;
}
