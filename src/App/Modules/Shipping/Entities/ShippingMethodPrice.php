<?php

namespace Vermal\Ecommerce\Modules\Shipping\Entities;

use Vermal\Database\MagicAccessor;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_shipping_method_price")
 * @ORM\HasLifecycleCallbacks
 **/
class ShippingMethodPrice
{
    use MagicAccessor;

    /** @ORM\Id @ORM\Column(type="integer") @ORM\GeneratedValue **/
    protected $id;

    /** @ORM\Column(type="string") **/
    protected $currency;

    /** @ORM\Column(type="float") **/
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="\ShippingMethod", inversedBy="price", cascade={"persist"})
     * @ORM\JoinColumn(name="shipping_method_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $shippingMethod;

    /**
     * Get formatted price
     *
     * @return string
     */
    public function getFormattedPrice()
    {
        return $this->price . ' ' . Currencies::getSymbol($this->currency);
    }
}
