<?php

namespace Vermal\Ecommerce\Modules\Shipping\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_shipping_method")
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\TranslationEntity(class="\ShippingMethodTr")
 **/
class ShippingMethod extends Model
{

	use Translatable;

    /**
     * @Gedmo\Translatable
     * @ORM\Column(type="string")
     */
    protected $title;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description;

	/**
	 * @Gedmo\Translatable
	 * @ORM\Column(type="text", nullable=true, name="short_description")
	 */
	protected $shortDescription;

//	/**
//	 * @ORM\Column(type="string")
//	 */
//	protected $country;
//
//	/**
//	 * @ORM\Column(type="string")
//	 */
//	protected $zip;

	/**
	 * @ORM\OneToMany(targetEntity="\ShippingMethodPrice", mappedBy="shippingMethod", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $price;

	/**
	 * @ORM\OneToMany(targetEntity="\ShippingMethodRule", mappedBy="shippingMethod", orphanRemoval=true, cascade={"persist", "remove"})
	 */
	protected $rules;

//	/**
//	 * @ORM\Column(type="float", name="price_min")
//	 */
//	protected $priceMin;
//
//	/**
//	 * @ORM\Column(type="float", name="price_max")
//	 */
//	protected $priceMax;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $type;

	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
	 */
	protected $icon;

	/**
	 * @ORM\OneToMany(
	 *   targetEntity="\ShippingMethodTr",
	 *   mappedBy="object",
	 *   cascade={"persist", "remove"},
	 *   orphanRemoval=true
	 * )
	 */
	protected $translations;

	public function __construct()
	{
		parent::__construct();
		$this->price = new ArrayCollection();
		$this->translations = new ArrayCollection();
		$this->rules = new ArrayCollection();
	}

	/**
	 * @param $currency
	 * @return ShippingMethodPrice
	 */
	public function getPrice($currency)
	{
		foreach ($this->price as $price) {
			if ($price->currency == $currency) return $price;
		}
	}

	/**
	 * @param $price
	 */
	public function addPrice($price)
	{
		$this->price[] = $price;
	}

	/**
	 * Clear prices
	 */
	public function clearPrices()
	{
		$this->price->clear();
	}

	/**
	 * @return ArrayCollection
	 */
	public function getRules()
	{
		return $this->rules;
	}

	/**
	 * @param $rule
	 */
	public function addRule($rule)
	{
		$this->rules[] = $rule;
	}

	/**
	 * Clear rules
	 */
	public function clearRules()
	{
		$this->rules->clear();
	}
}
