<?php

namespace Vermal\Ecommerce\Modules\Shipping\Repositories;

use Vermal\Database\Database;


class ShippingMethodRepository
{
    /**
     * Find product by id
     *
     * @param $id
     */
    public function findById($id)
    {
        $product = Database::Model('ShippingMethod')->find($id);
        echo json_encode($product->toArray());
    }


}
