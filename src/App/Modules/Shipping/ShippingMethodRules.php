<?php

namespace Vermal\Ecommerce\Modules\Shipping;

class ShippingMethodRules
{

	public static function getRules()
	{
		return [
			'Shipping address' => [
				'zip' => 'ZIP / Postal Code',
				'country' => 'Country'
			],
			'Cart' => [
				'subtotal' => 'Subtotal',
				'number-of-items' => 'Number of items',
			],
//			'Package measurements' => [
//				'total-weight' => 'Total weight',
//				'total-height' => 'Total height',
//				'total-width' => 'Total width',
//				'total-length' => 'Total length'
//			]
		];
	}

}
