<?php

namespace Vermal\Ecommerce\Modules\Shipping;


use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Sanitizer;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingZone;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingZoneRegion;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;


class Shippings extends Controller
{
	public $defaultViewPath = false;
	private $availableFields = ['title'];

    public function __construct()
    {
        parent::__construct();
        // $this->requiredPermission('product', $this->CRUDAction);
        $this->addToBreadcrumb('shipping', 'admin.shipping.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('ShippingZone');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('shipping.title'));

        // Add actions
        $datagrid->addEdit(['admin.address.edit', ['id']]);
        $datagrid->addDelete(['admin.address.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
	    $form = $this->form();
	    View::view('shipping', [
		    "form" => $form->build(),
		    "formName" => $form->getName(),
		    "fields" => apply_filters('ecommerce_shipping_fields', $this->availableFields)
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.shipping.create');
        }

        var_dump($post);

	    $shipping = Entity::getEntity('ShippingMethod');


        die();

        $form->hydrate($shipping);

        Database::saveAndFlush($shipping);

        // Redirect with message
        $this->alert($this->_('shipping.created'));
        Router::redirect('admin.shipping.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.shipping.update', ['id' => $id]));

        /** @var Address $address */
        $address = Database::Model('ShippingMethod')->find($id);
        $form->setValues($address);

        View::view('crud.edit', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        $address = Database::Model('ShippingMethod')->find($id);
        $form->hydrate($address);

        $this->alert($this->_('shipping.edited'));
        Database::saveAndFlush($address);
    }

	/**
	 * @param ShippingZone $entity
	 * @param $post
	 * @param bool $update
	 */
	private function hydrate($entity, $post, $update = false)
	{

		// Remove data and replace with new
		if ($update) {
			$entity->clearTr();
			$entity->clearRegions();
			Database::saveAndFlush($entity);
		}

		// Save multilangual data
		foreach (Languages::get() as $lang) {
			// Content
			$title = Sanitizer::applyFilter('trim|escape|strip_tags', $post->{$lang . '_title'});

			$entityTr = 'ShippingMethodTr';
			Database::translate($entity, $entityTr, 'title', $lang, $title);
		}

		// Save regions
		foreach ($post->regions as $region) {
			$region_ = new ShippingZoneRegion();
			$region_->region = $region;
			$entity->addRegion($region_);
		}

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Database::Model('ShippingMethod')->delete($id);

        // Redirect
        $this->alert($this->_('shipping.deleted'));
        Router::redirect('admin.shipping.index');
    }

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('shipping', routerLink('admin.shipping.store'));

	    // Product Data
	    foreach (Languages::get() as $lang) {
		    $form->addText($lang . '_title', $this->_('shipping.title'))->min(2);
	    }
        $form->addSelect('regions', ['*' => $this->_('shipping.allLanguages')] + Languages::available(), $this->_('shipping.regions'))
	        ->addAttribute('multiple', 'multiple')
            ->setClass('js-select2');

	    // Price
	    View::setVariable('currencies', $this->settings->currencies);

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
