<?php

namespace Vermal\Ecommerce\Modules\Shipping;


use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Multimedia\Entities\Multimedia;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\Upload;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingMethod;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingMethodRule;
use Vermal\Form\Form;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Router;


class ShippingMethods extends Controller
{
	public $defaultViewPath = false;

	// Shipping options
	const FLAT_RATE = 'flat_rate';
	const LOCAL_PICKUP = 'local_pickup';

	private $availableFields = ['title', 'description', 'shortDescription'];
	private $fieldsAfter = ['type'];
	private $shippingTypes = [];

    public function __construct()
    {
        parent::__construct();
        $this->requiredPermission('shipping-methods', $this->CRUDAction);
        $this->addToBreadcrumb('shippingMethod', 'admin.shippingMethod.index');
        $this->shippingTypes = apply_filters('ecommerce_shippingMethodTypes', [
        	self::FLAT_RATE => $this->_('shippingMethod.types.flat_rate'),
			self::LOCAL_PICKUP => $this->_('shippingMethod.types.local_pickup'),
		]);
    }

	/**
	 * Get shipping types
	 *
	 * @return mixed
	 */
	public static function getShippingTypes()
	{
		return apply_filters('ecommerce_shippingMethodTypes', [
			self::FLAT_RATE => self::__('shippingMethod.types.flat_rate'),
			self::LOCAL_PICKUP => self::__('shippingMethod.types.local_pickup'),
		]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
    	$this->setDefaultViewPath();

        $data = Database::Model('ShippingMethod');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('shippingMethod.title'));
	    $datagrid->addColumn('description', $this->_('shippingMethod.description'))->setRenderer(function($entity) {
	    	return substr($entity->description, 0, 100) . ' ...';
	    });
		$datagrid->addColumn('price', $this->_('shippingMethod.price'))->setRenderer(function($entity) {
			return $entity->getPrice(self::$appCurrency)->getFormattedPrice();
		});

        // Add actions
        $datagrid->addEdit(['admin.shippingMethod.edit', ['id']]);
        $datagrid->addDelete(['admin.shippingMethod.destroy_', ['id']]);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
		// Set default rule
		\Vermal\View::setVariable('rules', [[]]);
		\Vermal\View::setVariable('rulesEmpty', true);
	    $form = $this->form();
	    View::view('shippingMethods', [
		    "form" => $form->build(),
		    "formName" => $form->getName(),
		    "fields" => apply_filters('ecommerce_shippingMethod_fields', $this->availableFields),
		    "fieldsAfter" => apply_filters('ecommerce_shippingMethod_fields_after', $this->fieldsAfter),
	    ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.shippingMethod.create');
        }

        $shippingMethod = Entity::getEntity('ShippingMethod');
        $this->hydrate($shippingMethod, $post);

        Database::saveAndFlush($shippingMethod);

        // Redirect with message
        $this->alert($this->_('shippingMethod.created'));
        Router::redirect('admin.shippingMethod.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.shippingMethod.update', ['id' => $id]));

        /** @var ShippingMethod $address */
        $shippingMethod = Database::Model('ShippingMethod')->find($id);

	    // Set values content
	    foreach (Languages::get() as $lang) {
		    $form->getComponent($lang . '_title')->setValue($shippingMethod->getTr($lang, 'title'));
		    $form->getComponent($lang . '_description')->setValue($shippingMethod->getTr($lang, 'description'));
			$form->getComponent($lang . '_shortDescription')->setValue($shippingMethod->getTr($lang, 'shortDescription'));
	    }
//	    $form->getComponent('country')->setValue($shippingMethod->country);
//	    $form->getComponent('zip')->setValue($shippingMethod->zip);
//	    $form->getComponent('priceMin')->setValue($shippingMethod->priceMin);
//	    $form->getComponent('priceMax')->setValue($shippingMethod->priceMax);
		$form->getComponent('type')->setValue($shippingMethod->type);

		if (!empty($shippingMethod->icon))
			$form->getComponent('icon')->setValue($shippingMethod->icon->getFile());

	    // Set values price
	    foreach (Currencies::get() as $currency) {
		    $price = $shippingMethod->getPrice($currency);
		    if (empty($price)) continue;
		    $form->getComponent('price_' . $currency)->setValue($price->price);
	    }

	    // Set rules
		$rules = $shippingMethod->rules;
		if ($rules->isEmpty()) $rules = [[]];
		\Vermal\View::setVariable('rules', $rules);
		\Vermal\View::setVariable('rulesEmpty', $shippingMethod->rules->isEmpty());
		$values = [];
		foreach ($shippingMethod->rules as $rule) {
			$values['condition'][] = $rule->condition;
			$values['operator'][] = $rule->operator;
			$values['conditionValue'][] = $rule->value;
		}
		$form->setValues($values);

        View::view('shippingMethods', [
	        "form" => $form->build(),
	        "formName" => $form->getName(),
	        "fields" => apply_filters('ecommerce_shippingMethod_fields', $this->availableFields),
	        "fieldsAfter" => apply_filters('ecommerce_shippingMethod_fields_after', $this->fieldsAfter)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

	    $shippingMethod = Database::Model('ShippingMethod')->find($id);
		$this->hydrate($shippingMethod, $post, true);

        $this->alert($this->_('shippingMethod.edited'));
        Database::saveAndFlush($shippingMethod);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
    	$shippingMethod = Database::Model('ShippingMethod')->find($id);
    	$shippingMethod->clearTr();
    	$shippingMethod->clearPrices();
    	Database::saveAndFlush($shippingMethod);
        Database::Model('ShippingMethod')->delete($id);

        // Redirect
        $this->alert($this->_('shippingMethod.deleted'));
        Router::redirect('admin.shippingMethod.index');
    }

	/**
	 * Hydrate database object
	 *
	 * @param ShippingMethod $entity
	 * @param $post
	 * @param $update
	 * @return object
	 * @throws \Exception
	 */
	private function hydrate($entity, $post, $update = false)
	{
//		$country = $this->sanitize('trim|escape|strip_tags', $post->country);
//		$zip = $this->sanitize('trim|escape|strip_tags', $post->zip);
//		$entity->country = !empty($country) ? $country : '*';
//		$entity->zip = !empty($zip) ? $zip : '*';

//		$entity->priceMin = $this->sanitize('trim|float', $post->priceMin);
//		$entity->priceMax = $this->sanitize('trim|float', $post->priceMax);

		$entity->type = $this->sanitize('trim', $post->type);

		// Add translation to content
		if ($update) {
			$entity->clearTr();
			Database::saveAndFlush($entity);
		}
		foreach (Languages::get() as $lang) {
			$entityTr = 'ShippingMethodTr';
			Database::translate($entity, $entityTr, 'title', $lang, $this->sanitize('trim|escape|strip_tags', $post->{$lang . '_title'}));
			Database::translate($entity, $entityTr, 'description', $lang, $this->sanitize('trim', $post->{$lang . '_description'}));
			Database::translate($entity, $entityTr, 'shortDescription', $lang, $this->sanitize('trim', $post->{$lang . '_shortDescription'}));
		}

		// Add prices
		if ($update) $entity->clearPrices();
		foreach (Currencies::get() as $curency) {
			$entityPrice = Entity::getEntity('ShippingMethodPrice');

			// Currency
			$entityPrice->currency = $curency;

			// Price settings
			$entityPrice->price = Sanitizer::applyFilter('trim|float', $post->{'price_' . $curency});

			// Save
			$entityPrice->shippingMethod = $entity;
			$entity->addPrice($entityPrice);
			Database::save($entityPrice);
		}

		// Remove image
		if (empty($post->icon_hidden_image)) {
			$entity->icon = null;
		}
		// Add image
		if (!empty($_FILES['icon']['name'])) {
			$files = Upload::uploadFile('icon', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$entity->icon = Database::Model('Multimedia')->find($file);
			}
		}

		// Rules
		if ($update) $entity->clearRules();
		if (!empty($post->condition) && !empty($post->conditionValue->{0})) {
			foreach ($post->condition as $key => $condition) {
				$value = $post->conditionValue->{$key};
				$operator = $post->operator->{$key};

				/** @var ShippingMethodRule $rule */
				$rule = Entity::getEntity('ShippingMethodRule');

				$rule->condition = $condition;
				$rule->value = $value;
				$rule->operator = $operator;

				$rule->shippingMethod = $entity;
				$entity->addRule($rule);
				Database::save($rule);
			}
		}

		return $entity;
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('shippingMethod', routerLink('admin.shippingMethod.store'));

	    // Product Data
	    foreach (Languages::get() as $lang) {
		    $form->addText($lang . '_title', $this->_('shippingMethod.title'))->min(1);
		    $form->addTextArea($lang . '_description', $this->_('shippingMethod.description'));
			$form->addTextArea($lang . '_shortDescription', $this->_('shippingMethod.shortDescription'));
	    }

		$form->addSelect('type', $this->shippingTypes, $this->_('shippingMethod.type'))->required();

	    foreach ($this->settings->currencies as $currency) {
	    	$name = 'price_' . $currency;
			$this->fieldsAfter[] = $name;
	    	$form->addInput($name, 'text', $this->_('shippingMethod.price') . ' ' . $currency)
			    ->setCol('col-md-2 col-sm-3 col-6')
			    ->int();
	    }

	    // Rules
		$form->addSelect('condition', ShippingMethodRules::getRules(), 'Condition')
			->setMultiple()->setCol('col-lg-3');
		$form->addSelect('operator', getOperators(), 'Operator')
			->setMultiple()->setCol('col-lg-2');
		$form->addText('conditionValue', 'Value')
			->setMultiple()->setCol('col-lg-6');

	    $form->addInput('icon', 'file', $this->_('shippingMethod.icon'));
		$this->fieldsAfter[] = 'icon';

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
