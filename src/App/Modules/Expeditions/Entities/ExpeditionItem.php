<?php

namespace Vermal\Ecommerce\Modules\Expeditions\Entities;

use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Defaults\Currencies;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_expedition_item")
 * @ORM\HasLifecycleCallbacks
 **/
class ExpeditionItem extends Model
{

	/**
	 * @ORM\Column(type="string")
	 */
	protected $title;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $status;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price_tax_amount;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $currency;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $shipment_id;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $note;

	/**
	 * @ORM\ManyToOne(targetEntity="\Order")
	 * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
	 */
	protected $order;

	/**
	 * @ORM\ManyToOne(targetEntity="\Expedition", inversedBy="items")
	 * @ORM\JoinColumn(name="expedition_id", referencedColumnName="id")
	 */
	protected $expedition;

	/**
	 * Get formatted price
	 *
	 * @param string $key
	 * @param bool $currency
	 * @param bool $showDecimal
	 * @return string
	 */
	public function getFormattedPrice($key, $showDecimal = true, $currency = true)
	{
		$value = $this->{$key};
		$decimals = 0;
		if ((is_numeric( $value ) && floor( $value ) != $value) || $showDecimal)
			$decimals = 2;
		$price = number_format($value, $decimals,',',' ');
		if ($currency)
			$price .= ' ' . Currencies::getSymbol($this->currency);
		return $price;
	}

}
