<?php

namespace Vermal\Ecommerce\Modules\Expeditions\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_expedition")
 * @ORM\HasLifecycleCallbacks
 **/
class Expedition extends Model
{
	/**
	 * @ORM\ManyToOne(targetEntity="\ShippingMethod")
	 * @ORM\JoinColumn(name="shipping_method", referencedColumnName="id")
	 */
	protected $shipping;

	/**
	 * @ORM\Column(type="string")
	 */
	protected $status;

	/**
	 * @ORM\OneToMany(targetEntity="\ExpeditionItem", mappedBy="expedition")
	 */
	protected $items;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	protected $completed_at;

	public function __construct()
	{
		parent::__construct();
		$this->items = new ArrayCollection();
	}

	/**
	 * @param ExpeditionItem $item
	 */
	public function addItem($item)
	{
		$this->items[] = $item;
	}

	/**
	 * @param string $format
	 * @return false|string
	 */
	public function getFormattedCompletedAt($format = 'd.m.Y H:i:s')
	{
		if (!empty($this->completed_at))
			return date($format, $this->completed_at->getTimestamp());
		else return null;
	}

}
