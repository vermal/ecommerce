<?php

namespace Vermal\Ecommerce\Modules\Expeditions;

use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Expeditions\Entities\Expedition;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Ecommerce\Modules\Orders\Orders;
use Vermal\Form\Form;
use Vermal\Router;

class ExpeditionsModel
{

	/**
	 * Create new expedition
	 *
	 * @param array<integer> $orders
	 * @return array|bool
	 */
	public function createExpedition($orders)
	{
		$items = [];

		// Filter orders by shipping method
		foreach ($orders as $order)
		{
			/** @var Order $order */
			$order = Database::Model('Order')->find($order);

			// Check if order is packaged
			if (!in_array($order->status, [Orders::NEW_PAYED, Orders::NEW_, Orders::PACKAGED, Orders::PROCESSING])) {
				return ['msg' => Controller::__('expedition.not_packaged'), 'type' => 'danger'];
			}

			// In expedition
			$isInExpedition = Database::Model('ExpeditionItem')
				->where('e.order', $order->id)->count();
			if ($isInExpedition > 0) {
				return ['msg' => Controller::__('expedition.in_expedition'), 'type' => 'danger'];
			}

			// Set status to dispatched
			$order->status = Orders::DISPATCHED;

			if (!isset($items[$order->shipping->type]))
				$items[$order->shipping->type] = [
					'shipping' => $order->shipping,
					'items' => []
				];

			$items[$order->shipping->type]['items'][] = [
				'title' => $order->getFullShippingAddress(),
				'price' => $order->price,
				'price_tax_amount' => $order->price_tax_amount,
				'currency' => $order->currency,
				'order' => $order
			];
		}

		// Store expeditions
		foreach ($items as $shipping => $item)
		{
			/** @var Expedition $expedition */
			$expedition = Entity::getEntity('Expedition');

			// Set defaults
			$expedition->shipping = $item['shipping'];
			$expedition->status = Expeditions::CREATED;

			// Add items
			foreach ($item['items'] as $row)
			{
				$expeditionItem = Entity::getEntity('ExpeditionItem');
				$expeditionItem->status = Expeditions::CREATED;
				foreach ($row as $key => $value) {
					$expeditionItem->{$key} = $value;
				}
				$expeditionItem->expedition = $expedition;
				$expedition->addItem($expeditionItem);
				Database::save($expeditionItem);
			}
			Database::saveAndFlush($expedition);
		}

		return ['msg' => Controller::__('expedition.created'), 'type' => 'success'];
	}

}
