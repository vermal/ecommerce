<?php

namespace Vermal\Ecommerce\Modules\Expeditions;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\View;
use Vermal\Database\Database;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Modules\Customers\Auth\Facebook;
use Vermal\Ecommerce\Modules\Expeditions\Entities\Expedition;
use Vermal\Ecommerce\Modules\Expeditions\Entities\ExpeditionItem;
use Vermal\Ecommerce\Modules\Orders\Orders;
use Vermal\Ecommerce\Modules\Shipping\ShippingMethods;
use Vermal\Form\Form;
use Vermal\Router;

class Expeditions extends Controller
{

	const CREATED = 'created';
	const SENT = 'sent';
	const COMPLETED = 'completed';
	const CANCELLED = 'cancelled';

	/** INVOICE DIR */
	const INVOICE_DIR = BASE_PATH . '/public/uploads/invoices/';

	/**
	 * @var array $expeditionStatuses
	 */
    private $expeditionStatuses = [];

    public function __construct()
    {
        parent::__construct();
	 	$this->requiredPermission('expeditions', $this->CRUDAction);
        $this->addToBreadcrumb('expedition', 'admin.expedition.index');
        $this->setFluidLayoutContainer();
		$this->expeditionStatuses = apply_filters('ecommerce_expeditionStatus', [
			self::CREATED => $this->_('expedition.statuses.created'),
			self::SENT => $this->_('expedition.statuses.sent'),
			self::COMPLETED => $this->_('expedition.statuses.completed'),
			self::CANCELLED => $this->_('expedition.statuses.cancelled'),
		]);
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = Database::Model('Expedition')
			->order('e.created_at', 'DESC');
        $datagrid = new DataGrid($data);
        $datagrid->addDefaultFilter('status', [self::CREATED, self::SENT]);

        $datagrid->addColumn('id', 'ID');
		$datagrid->addColumn('shipping', $this->_('expedition.shipping'))->setRenderer(function($entity) {
			return $entity->shipping->title;
		});
		$datagrid->addColumn('items', $this->_('expedition.items'))->setRenderer(function ($entity) {
			/** @var Expedition $entity */
			$data = [];
			foreach ($entity->items as $expeditionItem) {
				foreach ($expeditionItem->order->items as $item) {
					if (empty($data[$item->product->id])) {
						$data[$item->product->id] = [
							'title' => $item->title,
							'category' => !empty($item->product) ? $item->product->getCategoriesAsString() : null,
							'qty' => 0
						];
					}
					$data[$item->product->id]['qty'] += $item->qty;
				}
			}
			$output = "";
			foreach ($data as $row) {
				$output .= $row['qty'] . 'x - ' . $row['title'] . ' (' . $row['category'] . ')' . '<br>';
			}
			return $output;
		});
		$datagrid->addColumn('created_at', $this->_('expedition.date'))->setRenderer(function ($entity) {
			return $entity->getFormattedCreatedAt();
		});
		$datagrid->addColumn('status', $this->_('expedition.status'))->setRenderer(function ($entity) {
			$bg = 'transparent';
			$color = 'initial';
			if ($entity->status == self::CREATED) {
				$bg = '#8e44ad';
				$color = '#fff';
			} else if ($entity->status == self::SENT) {
				$bg = '#f1c40f';
			} else if ($entity->status == self::COMPLETED || $entity->status == self::CANCELLED) {
				$bg = '#27ae60';
				$color = '#fff';
			}
			return "<div class='alert d-inline-block p-1 font-weight-bold' style='background-color: " . $bg . "; color: " . $color . "; border: none;'>" .
				$this->_('expedition.statuses.' . $entity->status) . "</div>";
		});
		$datagrid->addColumn('completed_at', $this->_('expedition.date_completed'))->setRenderer(function ($entity) {
			return $entity->getFormattedCompletedAt();
		});


        // Add actions
		$datagrid->addAction('edit', ['admin.expedition.edit', ['id']], 'fas fa-eye', '', 'btn btn-sm btn-primary');
		$datagrid->addAction('sent', ['admin.expedition.sent', ['id']], '', $this->_('expedition.sent'), 'btn btn-info btn-sm', [
			['status', '=', self::CREATED]
		]);
        $datagrid->addAction('finish', ['admin.expedition.finish', ['id']], '', $this->_('expedition.finish'), 'btn btn-success btn-sm', [
        	['status', '=', self::SENT]
		]);
		$datagrid->addAction('cancel', ['admin.expedition.cancel', ['id']], '', $this->_('expedition.cancel'), 'btn btn-outline-danger btn-sm', [
			['status', '=', self::CREATED]
		]);
//		$datagrid->addDelete(['admin.expedition.destroy_', ['id']]);

		// Shipping type
		$datagrid->addFilter('e.shipping.type', ShippingMethods::getShippingTypes(), 'Doručenie', 'col-md-2');
		$datagrid->addFilter('status', $this->expeditionStatuses, 'Status', 'col-md-2');

		$datagrid = apply_filters('ecommerce_expeditions_datagrid', $datagrid);

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param $id
	 * @throws \Whoops\Exception\ErrorException
	 */
    public function edit($id)
    {
		/** @var Expedition $entity */
		$entity = Database::Model('Expedition')->find($id);

		$form = $this->form($entity->items);
		$form->setAjax(true);
		$form->setMethod('PUT');
		$form->setAction(routerLink('admin.expedition.update', ['id' => $id]));

		$form->setValues($entity, ['items']);

		if ($entity->status == self::COMPLETED) {
			$form->getComponent('status')
				->addAttribute('disabled', 'disabled');
		}

		View::view('crud.create', [
			"form" => $form->build()
		]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     */
    public function update($post, $id)
    {
		/** @var Expedition $entity */
		$entity = Database::Model('Expedition')->find($id);

		$form = $this->form($entity->items);
		if ($form->hasError()) {
			$this->alert($this->_('error.form.empty'), 'danger');
			die();
		}

		// save item statuses
		foreach ($post->item_status as $itemId => $status) {
			foreach ($entity->items as $item) {
				if ($item->id == $itemId) {
					do_action('ecommerce_expeditions_update_item_status', [$item, $status]);
					if ($entity->status !== self::COMPLETED) {
						$item->status = $status;
						Database::save($item);
					}
					// Set order status
					if ($item->status === self::COMPLETED) {
						$item->order->status = Orders::COMPLETED;
					}
				}
			}
		}

		$this->alert($this->_('expedition.edited'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
		$order = Database::Model('Order')->find($id);
		$order->status = self::CANCELLED;
		Database::saveAndFlush($order);

		// Redirect
		$this->alert($this->_('expedition.deleted'));
		Router::redirect('admin.expedition.index');
    }

    /**
     * Create register form
     *
	 * @param ArrayCollection<ExpeditionItem> $items
     * @return Form
     */
    private function form($items)
    {
        $form = new Form('expedition', routerLink('admin.expedition.store'));

		$form->addSelect('status', $this->expeditionStatuses, 'Status');

		foreach ($items as $key => $item) {
			$form->addText('item_title[' . $item->id . ']', $this->_('expedition.status'))
				->addAttribute('disabled', 'disabled')
				->setValue($item->title)
				->setCol('col-md-5');
			$form->addText('item_price[' . $item->id . ']', $this->_('expedition.price'))
				->addAttribute('disabled', 'disabled')
				->setValue($item->getFormattedPrice('price'))
				->setCol('col-md-2');
			$form->addText('item_price_tax_amount[' . $item->id . ']', $this->_('expedition.price_tax_amount'))
				->addAttribute('disabled', 'disabled')
				->setValue($item->getFormattedPrice('price_tax_amount'))
				->setCol('col-md-2');
			$form->addSelect('item_status[' . $item->id . ']', $this->expeditionStatuses, $this->_('expedition.status'))
				->setValue($item->status)
				->setCol('col-md-1');
			if ($item->status == self::COMPLETED) {
				$form->getComponent('item_status[' . $item->id . ']')
					->addAttribute('disabled', 'disabled');
			}
			$form->addHtml('item_note[' . $item->id .  ']', 'p', apply_filters('ecommerce_expedition_item_note', '', $item))
				->setCol('col-md-2');

			// Form extend
			$form = apply_filters('ecommerce_expedition_item_extend', $form, $item);

			$data = [];
			foreach ($item->order->items as $item) {
				if (empty($data[$item->product->id])) {
					$data[$item->product->id] = [
						'title' => $item->title,
						'category' => !empty($item->product) ? $item->product->getCategoriesAsString() : null,
						'qty' => 0
					];
				}
				$data[$item->product->id]['qty'] += $item->qty;
			}
			$output = "";
			foreach ($data as $row) {
				$output .= $row['qty'] . 'x - ' . $row['title'] . ' (' . $row['category'] . ')' . '<br>';
			}
			$form->addHtml('items[' . $item->id . ']', 'div', $output)
				->addAttribute('style', 'margin-top: -10px')
				->setCol('col-12');
			$form->addHtml('divider[' . $item->id . ']', 'hr', '', false)
				->setCol('col-12');

		}

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))
			->setClass('btn btn-success');

        return $form;
    }

	/**
	 * @param $id
	 */
	public function finish($id)
	{
		$this->changeStatus($id, self::COMPLETED);
    }

	/**
	 * @param $id
	 */
	public function sent($id)
	{
		$this->changeStatus($id, self::SENT);
	}

	/**
	 * @param $id
	 */
	public function cancel($id)
	{
		$this->changeStatus($id, self::CANCELLED);
	}

	/**
	 * @param $id
	 * @param $status
	 */
	private function changeStatus($id, $status)
	{
		do_action('ecommerce_expedition_status_' . strtolower($status), $id);
		$expedition = Database::Model('Expedition')->find($id);
		foreach ($expedition->items as $item) {
			$item->status = $status;
			if ($status == self::COMPLETED) {
				$order = $item->order;
				$order->status = Orders::COMPLETED;
				Database::save($order);
			}
			Database::save($item);
		}
		$expedition->status = $status;
		if ($status == self::COMPLETED)
			$expedition->completed_at = new \DateTime();
		Database::saveAndFlush($expedition);
		Router::redirectToURL(Router::$prevLink);
	}

}
