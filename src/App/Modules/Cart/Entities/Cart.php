<?php

namespace Vermal\Ecommerce\Modules\Cart\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Ecommerce\Modules\Coupon\Entities\Coupon;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_cart")
 * @ORM\HasLifecycleCallbacks
 **/
class Cart extends Model
{

	/**
	 * @ORM\Column(type="string", name="session_id")
	 */
	protected $sessionId;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $price;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $price_tax_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $price_items;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $price_items_tax_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $discount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $discount_tax;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $discount_tax_amount;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $shipping_title;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $shipping_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $shipping_tax_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $shipping_tax;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $payment_title;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $payment_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $payment_tax_amount;

	/**
	 * @ORM\Column(type="float", nullable=true)
	 */
	protected $payment_tax;

	/**
	 * @ORM\Column(length=64, nullable=true)
	 */
	protected $currency;

	/**
	 * @ORM\ManyToOne(targetEntity="\Customer")
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id", nullable=true)
	 */
	protected $customer;

	/**
	 * @ORM\ManyToOne(targetEntity="\ShippingMethod")
	 * @ORM\JoinColumn(name="shipping_method_id", referencedColumnName="id", nullable=true)
	 */
	protected $shipping;

	/**
	 * @ORM\ManyToOne(targetEntity="\PaymentMethod")
	 * @ORM\JoinColumn(name="payment_method_id", referencedColumnName="id", nullable=true)
	 */
	protected $payment;

	/**
	 * @ORM\OneToMany(targetEntity="\CartItem", mappedBy="cart", cascade={"persist", "remove"}, orphanRemoval=true)
	 */
	protected $items;

	/**
	 * @ORM\ManyToMany(targetEntity="\Coupon", cascade={"persist"})
	 * @ORM\JoinTable(name="ecommerce_cart_coupon",
	 * 		joinColumns={@ORM\JoinColumn(name="cart_id", referencedColumnName="id")},
	 * 		inverseJoinColumns={@ORM\JoinColumn(name="coupon_id", referencedColumnName="id")}
	 * )
	 */
	protected $coupons;

	public function __construct()
	{
		parent::__construct();
		$this->items = new ArrayCollection();
		$this->coupons = new ArrayCollection();
	}

	/**
	 * @param CartItem $item
	 */
	public function addItem($item)
	{
		if (!$this->items->contains($item))
			$this->items[] = $item;
	}

	/**
	 * Remove cart item
	 *
	 * @param $item
	 */
	public function removeItem($item)
	{
		if ($this->items->contains($item))
			$this->items->removeElement($item);
	}

	/**
	 * Clear items
	 */
	public function clearItems()
	{
		foreach ($this->items as $item) {
			$this->removeItem($item);
		}
	}

	/**
	 * @param Coupon $coupon
	 */
	public function addCoupon($coupon)
	{
		if (!$this->coupons->contains($coupon))
			$this->coupons[] = $coupon;
	}

	/**
	 * Remove coupon
	 *
	 * @param $coupon
	 */
	public function removeCoupon($coupon)
	{
		if ($this->coupons->contains($coupon))
			$this->coupons->removeElement($coupon);
	}

	/**
	 * Clear coupons
	 */
	public function clearCoupons()
	{
		foreach ($this->coupons as $coupon) {
			$this->removeCoupon($coupon);
		}
	}

	/**
	 * Get formatted price
	 *
	 * @param $key
	 * @return string
	 */
	public function getFormattedPrice($key)
	{
		$value = $this->{$key};
		if ($key == 'price' || $key == 'price_tax_amount') {
			$value += $this->discount;
		}
		return number_format($value, 2, ',', '.') . ' ' .
			Currencies::getSymbol($this->currency);
	}

}
