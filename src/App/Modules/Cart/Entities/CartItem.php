<?php

namespace Vermal\Ecommerce\Modules\Cart\Entities;

use Vermal\Admin\Defaults\Model;

/**
 * @ORM\Entity
 * @ORM\Table(name="ecommerce_cart_item")
 * @ORM\HasLifecycleCallbacks
 **/
class CartItem extends Model
{

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $qty;

	/**
	 * @ORM\ManyToOne(targetEntity="\Cart", inversedBy="items")
	 * @ORM\JoinColumn(name="cart_id", referencedColumnName="id")
	 */
	protected $cart;

	/**
	 * @ORM\ManyToOne(targetEntity="\Product")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 */
	protected $product;

}
