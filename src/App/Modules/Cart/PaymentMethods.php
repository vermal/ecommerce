<?php

namespace Vermal\Ecommerce\Modules\Cart;

use Vermal\App;
use Vermal\Database\Database;

trait PaymentMethods
{

	/**
	 * Get shipping methods
	 *
	 * @return array
	 */
	public function getPaymentMethods()
	{
		$paymenthMethods_ = Database::Model('PaymentMethod')->get();
		$paymentMethods = [];
		foreach ($paymenthMethods_ as $paymentMethod) {
			if ($paymentMethod->rules->isEmpty()) {
				// Add shipping method if rules are empty
				$paymentMethods[] = $paymentMethod;
			} else {
				foreach ($paymentMethod->rules as $rule) {
					$valid = true;
					switch ($rule->condition) {
						case ('subtotal'):
							$valid = $this->subtotal($rule->operator, $rule->value);
							break;
						case ('number-of-items'):
							$valid = $this->numberOfItems($rule->operator, $rule->value);
							break;
						case ('zip'):
							$valid = $this->zip($rule->operator, $rule->value);
							break;
						case ('country'):
							$valid = $this->country($rule->operator, $rule->value);
							break;
						case ('shipping'):
							$valid = $this->shippingMethod($rule->operator, $rule->value);
							break;
					}
					if (!$valid) continue(2);
				}
				$paymentMethods[] = $paymentMethod;
			}
		}
		return $paymentMethods;
	}

	/**
	 * Compare country
	 *
	 * @param $opreator
	 * @param $value
	 * @return bool
	 */
	private function shippingMethod($opreator, $value) {
		$checkout = App::sessionGet('checkout');
		if (empty($checkout) || empty($checkout['shipping_method'])) return true;
		return $this->compare($checkout['shipping_method'], $opreator, $value);
	}

}
