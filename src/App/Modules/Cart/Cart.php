<?php

namespace Vermal\Ecommerce\Modules\Cart;

use Gregwar\Image\Source\Data;
use Vermal\Admin\Defaults\ControllerExtension;
use Vermal\Admin\Sanitizer;
use Vermal\Admin\View;
use Vermal\App;
use Vermal\Controller;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\EcommerceServiceProvider;
use Vermal\Ecommerce\Modules\Cart\Entities\CartItem;
use Vermal\Ecommerce\Modules\Checkout\Checkout;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Tax\Repositories\TaxRepository;
use Vermal\Form\Form;

class Cart extends Controller
{

	use ControllerExtension;
	use Compare;
	use ShippingMethods;
	use PaymentMethods;
	use Coupon;

	/**
	 *
	 * @var $instance
	 */
	private static $instance;

	private $cart;

	public function __construct()
	{
		parent::__construct();

		// Setup settings
		$this->settingsSetup();

		// Cart
		$this->cart = $this->getCart();
	}

	public static function getInstance()
	{
		if (is_null( self::$instance )) {
			self::$instance = new self();
		}
		return self::$instance;
	}

	/**
     * Add item
	 *
	 * @param $post
     */
    public function addItem($post)
    {
    	$cart = $this->getCart();

    	/** @var Product $product */
    	$product = Database::Model('Product')
			->find(Sanitizer::applyFilter('strip_tags|trim|digit', $post->id));

    	if (empty($product)) {
    		// Product doesn't exist
    		die();
		}

		$cartItem = Database::Model('CartItem')
			->where('c.cart', $cart->id)
			->where('c.product', $product->id)
			->first();
    	if (empty($cartItem)) {
    		// Create new cart item
			/** @var $cartItem CartItem */
			$cartItem = Entity::getEntity('CartItem');
			$cartItem->product = $product;
			$cartItem->qty = $post->qty;
			$cartItem->cart = $cart;
			$cart->addItem($cartItem);
		} else {
			// Cart item already exist so inscrease quantity
			$cartItem->qty = (int)$cartItem->qty + $post->qty;
		}
		Database::save($cartItem);
		Database::saveAndFlush($cart);

		// Update cart
		$this->updateCart();

		// Output json data
		if (self::isAjax())
			$this->outputCartJson('add-to-cart');
    }

	/**
	 * Add coupon
	 *
	 * @param $post
	 */
	public function addCoupon($post)
	{
		$cart = $this->getCart();

		$coupon = Database::Model('Coupon')
			->where('c.code', Sanitizer::applyFilter('trim|escape|strip_tags', $post->code))
			->first();

		// Add coupon if exist
		if (!empty($coupon)) {
			$cart->addCoupon($coupon);
			Database::saveAndFlush($cart);
			// Update cart
			$this->updateCart();
		}

		// Output json data
		if (self::isAjax())
			$this->outputCartJson('add-coupon');
    }

	/**
	 * Add coupon
	 *
	 * @param $post
	 */
	public function removeCoupon($post)
	{
		$cart = $this->getCart();

		foreach ($cart->coupons as $coupon) {
			if ($coupon->id == $post->id) {

				// Remove coupon
				$cart->removeCoupon($coupon);
				Database::saveAndFlush($coupon);

				// Update cart
				$this->updateCart();
				break;
			}
		}

		// Output json data
		if (self::isAjax())
			$this->outputCartJson('add-coupon');
	}

	/**
	 * Decrease qty
	 *
	 * @param $post
	 */
	public function removeItem($post)
	{
		$cart = $this->getCart();

		$cartItem = Database::Model('CartItem')
			->where('c.cart', $cart->id)
			->where('c.id', $post->id)
			->first();
		$cart->removeItem($cartItem);
		Database::saveAndFlush($cart);

		// Update cart
		$this->updateCart();

		// Output json data
		if (self::isAjax())
			$this->outputCartJson('remove-item');
	}

	/**
	 * Change qty
	 *
	 * @param $post
	 */
	public function changeQty($post)
	{
		$cart = $this->getCart();

		$cartItem = Database::Model('CartItem')
			->where('c.cart', $cart->id)
			->where('c.id', $post->id)
			->first();
		$cartItem->qty = $post->qty;
		if ($cartItem->qty < 1) $cartItem->qty = 1;
		Database::saveAndFlush($cartItem);

		// Update cart
		$this->updateCart();

		// Output json data
		if (self::isAjax())
			$this->outputCartJson('change-qty');
    }

	/**
	 * Set shipping method
	 *
	 * @param $value
	 */
	public function setShipping($value = null)
	{
		if (!empty($value))
			$value = Database::Model('ShippingMethod')->find($value);
		$cart = $this->getCart();
		$cart->shipping = $value;
		Database::saveAndFlush($cart);
		$this->updateCart();
    }

	/**
	 * Set payment method
	 *
	 * @param $value
	 */
	public function setPayment($value = null)
	{
		if (!empty($value))
			$value = Database::Model('PaymentMethod')->find($value);
		$cart = $this->getCart();
		$cart->payment = $value;
		Database::saveAndFlush($cart);
		$this->updateCart();
	}

	/**
	 * Update cart prices
	 */
	public function updateCart()
	{
		$cart = $this->getCart();

		$cart->currency = $this->currency;

		$price = 0;
		$priceTax = 0;

		// Calculate items prices
		$priceItems = 0;
		$priceItemsTax = 0;
		$checkoutData = App::sessionGet('checkout');
		foreach ($cart->items as $item) {
			/** @var $product Product */
			$product = $item->product;

			// Get tax
			$categories = [];
			foreach ($product->getCategories() as $category) {
				$categories[] = $category->id;
			}
			$country = !empty($checkoutData['billing_country']) ? $checkoutData['billing_country'] : '*';
			$postcode = !empty($checkoutData['billing_postcode']) ? $checkoutData['billing_postcode'] : '*';
			$taxRate = TaxRepository::getTaxRate($country, $postcode, $categories);
			$taxRate = $taxRate[0];

			$productPrice = $product->getPrice($this->currency)->getProductPrice();
			$tax = EcommerceServiceProvider::calculateTax($productPrice, $taxRate->rate);

			$priceItems += ((float)($productPrice - ($tax < 0 ? $tax : 0)) * $item->qty);
			$priceItemsTax += (float)$tax * $item->qty;
		}
		$price += $priceItems;
		$priceTax += $priceItemsTax;
		$cart->price_items = $priceItems;
		$cart->price_items_tax_amount = $priceItemsTax;

		if (!empty($cart->shipping)) {
			// Calculate shipping prices
			$shipping = $cart->shipping;
			$shippingPrice = $shipping->getPrice($this->currency)->price;
			$shippingTaxRate = $this->settings->shipping_tax;
			$shippingTaxAmount = EcommerceServiceProvider::calculateTax($shippingPrice, $shippingTaxRate);
			$shippingPrice = (float)($shippingPrice - ($shippingTaxAmount < 0 ? $shippingTaxAmount : 0));
			$cart->shipping = $shipping;
			$cart->shipping_title = $shipping->title;
			$cart->shipping_amount = $shippingPrice;
			$cart->shipping_tax_amount = $shippingTaxAmount;
			$cart->shipping_tax = $shippingTaxRate;
			$price += $shippingPrice;
			$priceTax += $shippingTaxAmount;
		}

		if (!empty($cart->payment)) {
			$payment = $cart->payment;
			$paymentPrice = $payment->getPrice($this->currency)->price;
			$paymentTaxRate = $this->settings->payment_tax;
			$paymentTaxAmount = EcommerceServiceProvider::calculateTax($paymentPrice, $paymentTaxRate);
			$paymentPrice = (float)($paymentPrice - ($paymentTaxAmount < 0 ? $paymentTaxAmount : 0));
			$cart->payment = $payment;
			$cart->payment_title = $payment->title;
			$cart->payment_amount = $paymentPrice;
			$cart->payment_tax_amount = $paymentTaxAmount;
			$cart->payment_tax = $paymentTaxRate;
			$price += $paymentPrice;
			$priceTax += $paymentTaxAmount;
		}

		// Save prices
		$cart->price = $price;
		$cart->price_tax_amount = $priceTax;

		// Get coupons and calculate dicsount
		$discount = 0;
		$tax = 0;
		if (!$cart->coupons->isEmpty()) {
			foreach ($cart->coupons as $coupon) {
				list($couponPrice, $tax) = self::calculateCouponPrice($coupon, $cart, $checkoutData['billing_country'], $checkoutData['billing_postcode']);
				$discount += $couponPrice;
				$tax += $tax;
			}
		}

		// Set discount
		$cart->discount = -$discount;
		$cart->discount_tax = 0;
		$cart->discount_tax_amount = -$tax;

		$this->cart = Database::saveAndFlush($cart);
    }

	/**
	 * @return \Vermal\Ecommerce\Modules\Cart\Entities\Cart
	 */
	public function getCart()
	{
		if (!empty($this->cart)) {
			return $this->cart;
		} else if (!isset($_COOKIE['v_cart'])) {
			$cartId = uniqid();
			setcookie('v_cart', $cartId, time() + (86400 * 30), '/'); // 86400 = 1 day

			// Create cart
			$cart = Entity::getEntity('Cart');
			$cart->sessionId = $cartId;
			$cart->currency = $this->currency;
			Database::saveAndFlush($cart);
		} else {
			$cart = Database::Model('Cart')->where('c.sessionId', $_COOKIE['v_cart'])
				->first();
			if (empty($cart)) {
				unset($_COOKIE['v_cart']);
				$this->getCart();
			}
		}
		return $cart;
    }

	/**
	 * Output cart data
	 *
	 * @param string $action
	 * @param array $data
	 */
    public function outputCartJson($action, $data = [])
	{
		$shipping = '-';
		if (!empty($this->cart->shipping))
			$shipping = $this->cart->getFormattedPrice('shipping_amount');

		$payment = '-';
		if (!empty($this->cart->payment))
			$payment = $this->cart->getFormattedPrice('payment_amount');

		$data = [
			'fields' => [
				'price-shipping' => $shipping,
				'price-payment' => $payment,
				'price-total' => $this->cart->getFormattedPrice('price'),
				'price-subtotal' => $this->cart->getFormattedPrice('price_items'),
				'price-discount' => $this->cart->getFormattedPrice('discount'),
				'cart-items' => $this->cart->items->count(),
				'shipping-methods' => Checkout::getShippingMethodsView(),
				'payment-methods' => Checkout::getPaymentMethodsView(),
				'summary-coupons' => Checkout::getCouponsView()
			],
			'data' => $data
		];
		$data = apply_filters('cart_ajax_output', $data, $action);

		echo json_encode($data);
	}

}
