<?php

namespace Vermal\Ecommerce\Modules\Cart;

use Vermal\App;

trait Compare
{

	/**
	 * Compare subtotal
	 *
	 * @param $opreator
	 * @param $value
	 * @return bool
	 */
	private function subtotal($opreator, $value) {
		return $this->compare($this->cart->price, $opreator, $value);
	}

	/**
	 * Compare number of items
	 *
	 * @param $opreator
	 * @param $value
	 * @return bool
	 */
	private function numberOfItems($opreator, $value) {
		return $this->compare($this->cart->items->count(), $opreator, $value);
	}

	/**
	 * Compare zip
	 *
	 * @param $opreator
	 * @param $value
	 * @return bool
	 */
	private function zip($opreator, $value) {
		$checkout = App::sessionGet('checkout');
		if (empty($checkout)) return true;
		if (empty($checkout['shipping_postcode']))
			$checkout['shipping_postcode'] = $checkout['billing_postcode'];
		return $this->compare($checkout['shipping_postcode'], $opreator, $value);
	}

	/**
	 * Compare country
	 *
	 * @param $opreator
	 * @param $value
	 * @return bool
	 */
	private function country($opreator, $value) {
		$checkout = App::sessionGet('checkout');
		if (empty($checkout)) return true;
		if (empty($checkout['shipping_country']))
			$checkout['shipping_country'] = $checkout['billing_country'];
		return $this->compare($checkout['shipping_country'], $opreator, $value);
	}

	/**
	 * Compare values
	 *
	 * @param $value1
	 * @param $operator
	 * @param $value2
	 * @return bool
	 */
	private function compare($value1, $operator, $value2) {
		switch ($operator) {
			case '=':
				return $value1 == $value2;
			case '!=':
				return $value1 != $value2;
			case '>':
				return $value1 > $value2;
			case '>=':
				return $value1 >= $value2;
			case '<':
				return $value1 < $value2;
			case '<=':
				return $value1 <= $value2;
			case 'range':
				list($min, $max) = explode('-', $value2);
				return ((int)trim($min) <= $value1) && ($value1 <= (int)trim($max));
			case 'in':
				return in_array($value1, $value2);
			case 'notin':
				return !in_array($value1, $value2);
			default:
				return true;
		}
	}

}
