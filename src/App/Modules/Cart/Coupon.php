<?php

namespace Vermal\Ecommerce\Modules\Cart;

use Vermal\App;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\EcommerceServiceProvider;
use Vermal\Ecommerce\Modules\Coupon\CouponTypes;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Ecommerce\Modules\Orders\Entities\OrderItem;
use Vermal\Ecommerce\Modules\Tax\Repositories\TaxRepository;

trait Coupon
{

	/**
	 * @param $coupon
	 * @param \Vermal\Ecommerce\Modules\Cart\Entities\Cart|Order $entity
	 * @param string $country
	 * @param string $postcode
	 * @return array
	 */
	public static function calculateCouponPrice($coupon, $entity, $country = '*', $postcode = '*')
	{
		$type = $coupon->type;
		$price = $entity->price;
		$discount = 0;
		$tax = 0;

		if ($type == CouponTypes::PERCENTAGE_CART_DISCOUNT) {
			// Caluclate percentage discount for whole cart
			$discount = $price * ($coupon->discountAmount / 100);
		} else if ($type == CouponTypes::PERCENTAGE_PRODUCT_DISCOUNT) {
			// Caluclate percentage discount for products
			$cartEntity = Entity::getEntity('Cart');
			foreach ($entity->items as $item) {
				if (!empty($item->type) && $item->type !== OrderItem::PRODUCT) continue;
				$value = (($item->product->getPrice($entity->currency)->price * $item->qty) * ($coupon->discountAmount / 100));

				if ($entity instanceof $cartEntity) {
					$discount += $value;
				} else {
					$item->price -= $value;
					$item->price_per_unit = $item->price / $item->qty;
					$item->price_tax_amount = EcommerceServiceProvider::calculateTax($item->price, $item->price_tax);
					$item->price_per_unit_tax_amount = $item->price_tax_amount / $item->qty;
					Database::save($item);
				}
			}
		} else if ($type == CouponTypes::FIXED_CART_DISCOUNT) {
			// Caluclate fixed cart dicsount
			$discount = $coupon->getPrice($entity->currency)->price;
		} else if ($type == CouponTypes::FIXED_PRODUCT_DISCOUNT) {
			// Caluclate fixed discount for each product
			$cartEntity = Entity::getEntity('Cart');
			foreach ($entity->items as $item) {
				if (!empty($item->type) && $item->type !== OrderItem::PRODUCT) continue;
				$value = $coupon->getPrice($entity->currency)->price;

				if ($entity instanceof $cartEntity) {
					$discount += $price * $item->qty;
				} else {
					$item->price -= $value * $item->qty;
					$item->price_per_unit = $item->price / $item->qty;
					$item->price_tax_amount = EcommerceServiceProvider::calculateTax($item->price, $item->price_tax);
					$item->price_per_unit_tax_amount = $item->price_tax_amount / $item->qty;

					Database::save($item);
				}
			}
		}

		Database::flush();
		return [$discount, $tax];
	}

	private static function calculateItemTax($product, $price, $country = '*', $postcode = '*')
	{
		// Get tax
		$categories = [];
		foreach ($product->getCategories() as $category) {
			$categories[] = $category->id;
		}
		$taxRate = TaxRepository::getTaxRate($country, $postcode, $categories);
		$taxRate = $taxRate[0];
		$tax = EcommerceServiceProvider::calculateTax($price, $taxRate->rate);
		return $tax;
	}

}
