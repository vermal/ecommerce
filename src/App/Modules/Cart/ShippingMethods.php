<?php

namespace Vermal\Ecommerce\Modules\Cart;

use Vermal\Database\Database;

trait ShippingMethods
{

	/**
	 * Get shipping methods
	 *
	 * @return array
	 */
	public function getShippingMethods()
	{
		$shippingMethods_ = Database::Model('ShippingMethod')->get();
		$shippingMethods = [];
		foreach ($shippingMethods_ as $shippingMethod) {
			if ($shippingMethod->rules->isEmpty()) {
				// Add shipping method if rules are empty
				$shippingMethods[] = $shippingMethod;
			} else {
				foreach ($shippingMethod->rules as $rule) {
					$valid = true;
					switch ($rule->condition) {
						case ('subtotal'):
							$valid = $this->subtotal($rule->operator, $rule->value);
							break;
						case ('number-of-items'):
							$valid = $this->numberOfItems($rule->operator, $rule->value);
							break;
						case ('zip'):
							$valid = $this->zip($rule->operator, $rule->value);
							break;
						case ('country'):
							$valid = $this->country($rule->operator, $rule->value);
							break;
					}
					if (!$valid) continue(2);
				}
				$shippingMethods[] = $shippingMethod;
			}
		}
		// Filter shipping methods
		$shippingMethods = apply_filters('ecommerce_checkout_shipping_method', $shippingMethods);
		// Return shipping mehtods
		return $shippingMethods;
	}

}
