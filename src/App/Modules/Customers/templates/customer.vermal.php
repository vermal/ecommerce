@extends('layout')

@section('body')

    <div class="card shadow-lg m-b-30">
        <div class="card-body">
            {!! $form !!}
        </div>
    </div>

    @if(isset($formNotes))
        <div class="card shadow-lg m-b-30">
            <div class="card-body">

                <h3>@__(note.notes)</h3>

                <div id="notes">
                    @foreach($notes as $note)
                        <div class="show fade alert alert-{{ $note->type }} alert-dismissible" data-id="{{ $note->id }}">
                            {{ $note->note }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        </div>
                    @endforeach
                </div>

                {!! $formNotes !!}
            </div>
        </div>
    @endif

    <script type="text/javascript">
        $(document).ready(function() {
            // Add note
            $('#form-customer-note').on('success', function(item, data) {
                let response = data.response;
                $('#form-customer-note')[0].reset();
                let obj = JSON.parse(response);
                if (obj.note !== undefined) {
                    let note = JSON.parse(obj.note);
                    $('#notes').prepend(
                        $('<div class="show fade alert alert-' + note.type + '" data-id="' + note.id + '">').text(note.note).append(
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>'
                        )
                    )
                }
            });
            // Remove note
            $('#notes .alert-dismissible').click(function() {
                let id = $(this).data('id');
                $.ajax({
                    url: "{{ routerLink('admin.customer.remove-note') }}",
                    method: 'POST',
                    data: {id: id}
                })
            });
        });
    </script>

@endsection
