<?php

namespace Vermal\Ecommerce\Modules\Customers\Repositories;

use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Database\Database;


class CustomerRepository
{
    /**
     * Search customers
     *
     * @param $q
     * @param $page
     * @param boolean $address
     */
    public function search($q, $page, $address = false)
    {
        $users = Database::Model('Customer')
            ->where('c.name', '%', $q)
            ->orWhere('c.surname', '%', $q)
            ->orWhere('c.email', '%', $q)
            ->paginate(8);
        $output = [
            'results' => [],
            'pagination' => [
                'more' => (ceil($users->count() / 8) - 1) == $page ? true : false
            ]
        ];
        foreach ($users as $user) {
            /** @var Customer $user */
            $output['results'][] = [
                'id' => $user->id,
                'text' => $user->getFullName() . ' (' . $user->email . ')'
            ];
        }
        echo json_encode($output);
    }
}
