<?php

namespace Vermal\Ecommerce\Modules\Customers\Repositories;

use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Database\Database;


class AddressRepository
{
    /**
     * Get address for custom
     *
     * @param $customerID
     * @param $type
     */
    public function findByCustomer($customerID, $type = 'all')
    {
        /** @var Customer $user */
        $user = Database::Model('Customer')->find($customerID);
        $output = [];
        if ($type == 'billing') {
            $output['billing'] = $user->getAddressAsArray('billing');
        }
        if ($type == 'shipping') {
            $output['shipping'] = $user->getAddressAsArray('shipping');
        }
        if ($type == 'all') {
            $output = [];
            foreach ($user->addresses as $address) {
                $output[] = $address->toArray();
            }
        }
        echo json_encode($output);
    }
}
