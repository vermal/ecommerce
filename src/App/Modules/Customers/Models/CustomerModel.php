<?php

namespace Vermal\Ecommerce\Modules\Customers\Models;


use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Customers\Addresses;
use Vermal\Ecommerce\Modules\Customers\Customers;
use Vermal\Ecommerce\Modules\Customers\Entities\Address;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Form\Form;

class CustomerModel
{

	/** @var array $values */
	private $values = [];

	/**
	 * @var Form $form
	 */
	private $form;

	public function __construct($values, $form = null)
	{
		$this->values = $values;
		$this->form = $form;
	}

	/**
	 * Create new customer
	 */
	public function store()
	{
		$entity = Database::Model('Customer')->where('c.email', $this->values['email'])
			->first();
		if (empty($entity)) {
			$entity = Entity::getEntity('Customer');
		}
		$this->hydrate($entity);
		Database::saveAndFlush($entity);
		return $entity;
	}

	/**
	 * Edit existing customer
	 *
	 * @param int|Customer $entity
	 * @return bool
	 */
	public function save($entity)
	{
		if (is_int($entity))
			$entity = Database::Model('Customer')->find($entity);
		if (empty($entity)) {
			if (!empty($this->form)) {
				$this->form->setGlobalError("This customer doesn't exist", 'danger');
			}
			return false;
		}
		$this->hydrate($entity, true);
		Database::save($entity);
		return $entity;
	}


	/**
	 * Hydrate object
	 *
	 * @param Customer $entity
	 * @param $edit
	 */
	private function hydrate(&$entity, $edit = false)
	{
		$values = $this->values;
		$entity->name = Sanitizer::applyFilter('trim|strip_tags', $values['name']);
		$entity->surname = Sanitizer::applyFilter('trim|strip_tags', $values['surname']);
		$entity->email = Sanitizer::applyFilter('trim|strip_tags', $values['email']);
		// By default customer gets inactive status ... that means the actual profile doesn't exist
		$entity->status = Customers::INACTIVE;

		// Add billing address
		if (array_key_exists('billing', $values)) {
			$billing = $values['billing'];
			$billing['type'] = Addresses::BILLING;
			$billingAddress = (new AddressModel($billing, $this->form))->store();
			// Add address
			$billingAddress->customer = $entity;
			$entity->addAddress($billingAddress);
			// Select address
			$entity->billing_address = $billingAddress;
			Database::save($billingAddress);
		}

		// Add shipping address
		if (array_key_exists('shipping', $values)) {
			$shipping = $values['shipping'];
			$shipping['type'] = Addresses::SHIPPING;
			$shippingAddress = (new AddressModel($shipping, $this->form))->store();
			// Add address
			$shippingAddress->customer = $entity;
			$entity->addAddress($shippingAddress);
			// Select address
			$entity->shipping_address = $shippingAddress;
			Database::save($shippingAddress);
		}

		// Unfinished order
		if (array_key_exists('unfinishedOrder', $values)) {
			$entity->unfinishedOrder = $values['unfinishedOrder'];
		}
	}

}
