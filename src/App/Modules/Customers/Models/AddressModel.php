<?php

namespace Vermal\Ecommerce\Modules\Customers\Models;


use Vermal\Admin\Sanitizer;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Customers\Entities\Address;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Form\Form;

class AddressModel
{

	/** @var array $values */
	private $values = [];

	/**
	 * @var Form $form
	 */
	private $form;

	public function __construct($values, $form = null)
	{
		$this->values = $values;
		$this->form = $form;
	}

	/**
	 * Create new order
	 */
	public function store()
	{
		$entity = Entity::getEntity('Address');
		$this->hydrate($entity);
		Database::save($entity);
		return $entity;
	}

	/**
	 * Edit existing order
	 *
	 * @param int|Customer $entity
	 * @return bool
	 */
	public function save($entity)
	{
		if (is_int($entity))
			$entity = Database::Model('Address')->find($entity);
		if (empty($entity)) {
			if (!empty($this->form)) {
				$this->form->setGlobalError("This address doesn't exist", 'danger');
			}
			return false;
		}
		$this->hydrate($entity, true);
		Database::save($entity);
		return $entity;
	}


	/**
	 * Hydrate object
	 *
	 * @param Address $entity
	 * @param $edit
	 */
	private function hydrate(&$entity, $edit = false)
	{
		$values = $this->values;
		$entity->type = $values['type'];

		if (array_key_exists('name', $values)) {
			$entity->name = Sanitizer::applyFilter('trim|strip_tags', $values['name']);
		}
		if (array_key_exists('surname', $values)) {
			$entity->surname = Sanitizer::applyFilter('trim|strip_tags', $values['surname']);
		}

		if (array_key_exists('phone', $values)) {
			$entity->phone = Sanitizer::applyFilter('trim|strip_tags', $values['phone']);
		}
		if (array_key_exists('street', $values)) {
			$entity->street = Sanitizer::applyFilter('trim|strip_tags', $values['street']);
		}
		if (array_key_exists('city', $values)) {
			$entity->city = Sanitizer::applyFilter('trim|strip_tags', $values['city']);
		}
		if (array_key_exists('psc', $values)) {
			$entity->psc = Sanitizer::applyFilter('trim|strip_tags', $values['psc']);
		}
		if (array_key_exists('country', $values)) {
			$entity->country = Sanitizer::applyFilter('trim|strip_tags', $values['country']);
		}

		// Company
		if (array_key_exists('companyName', $values)) {
			$entity->companyName = Sanitizer::applyFilter('trim|strip_tags', $values['companyName']);
		}
		if (array_key_exists('ico', $values)) {
			$entity->ico = Sanitizer::applyFilter('trim|strip_tags', $values['ico']);
		}
		if (array_key_exists('dic', $values)) {
			$entity->dic = Sanitizer::applyFilter('trim|strip_tags', $values['dic']);
		}
		if (array_key_exists('ic_dph', $values)) {
			$entity->ic_dph = Sanitizer::applyFilter('trim|strip_tags', $values['ic_dph']);
		}

		if (array_key_exists('customer', $values)) {
			$entity->customer = $values['customer'];
		}

		$title = !empty($values['title']) ? $values['title'] : $entity->getFullAddress();
		$entity->title = $title;
	}

}
