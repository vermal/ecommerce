<?php

namespace Vermal\Ecommerce\Modules\Customers;


use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Tests\Common\Annotations\Fixtures\Annotation\Route;
use Vermal\Admin\Defaults\Languages;
use Vermal\Admin\Modules\Auth\Entities\User\User;
use Vermal\Admin\Modules\Extensions\DatagridExcelExport;
use Vermal\Admin\Upload;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Defaults\Imap;
use Vermal\Ecommerce\Defaults\MailInbox;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;


class Customers extends Controller
{
    public $defaultViewPath = false;

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const BLOCKED = 'blocked';
    const STATUSES = [
        self::ACTIVE => 'Active',
        self::INACTIVE => 'Inactive',
        self::BLOCKED => 'Blocked'
    ];

    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('customers', $this->CRUDAction);
        $this->addToBreadcrumb('customer', 'admin.customer.index');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $this->setDefaultViewPath();
        $this->setFluidLayoutContainer();

        // Return view
        View::view('crud.read', [
            'datagrid' => self::datagrid()->build()
        ]);
    }

	/**
	 * @return DataGrid
	 */
    private static function datagrid() {
		$data = Database::Model('Customer')
			->join('c.billing_address', 'billing_address')
			->order('c.last_order_at');

		$data = apply_filters('ecommerce_customer_datagrid_data', $data);

		$datagrid = new DataGrid($data);

		$datagrid->addColumn('id', 'ID');
		$datagrid->addColumn('name', self::__('customer.name'))->setRenderer(function($customer) {
			return $customer->getFullName();
		});
		$datagrid->addColumn('email', self::__('customer.email'));
		$datagrid->addColumn('phone', self::__('customer.phone'));

		$datagrid->addColumn('orders', self::__('customer.orders'))->setRenderer(function($customer) {
			return '<a href="' . routerLink('admin.order.index', ['filter__customer' => $customer->id]) .
				'">' . self::__('customer.showOrders') . ': ' . $customer->orders->count() . '<a>';
		});
		$datagrid->addColumn('notes', self::__('note.notes'))->setRenderer(function($customer) {
			return $customer->getLastNote();
		});

		// Add actions
		$datagrid->addAction('address', ['admin.address.index', ['filter__customer' => 'id']],'', self::__('customer.addresses'), 'btn btn-sm btn-secondary');
		$datagrid->addAction('messages', ['admin.mailer.index', ['filter__customer' => 'id']],'', self::__('customer.messages'), 'btn btn-sm btn-secondary');
		$datagrid->addEdit(['admin.customer.edit', ['id']]);
		$datagrid->addDelete(['admin.customer.destroy_', ['id']]);

		$datagrid->addBulkAction('delete', 'Zmazať', [self::class, 'removeAll']);

		// Competition
		$datagrid->addFilter('unfinishedOrder', [
			1 => 'Zobraziť'
		], self::__('customer.unfinishedOrder'), 'col-md-2 d-none');

		// Objecnávky
		$datagrid->addFilter('orders', [
			1 => 'Zobraziť'
		], self::__('customer.haveOrders'), 'col-md-2 d-none');
		$datagrid->filterRenderer(function($value, $data) {
			return $data
				->join('c.orders','o')
				->having('COUNT(o.id) > 0')
				->groupBy('c.id');
		});

		$datagrid->addSearch('email', self::__('customer.email'), 'col-lg-2');
		$datagrid->addSearch('name', self::__('customer.name'), 'col-lg-2');
		$datagrid->addSearch('surname', self::__('customer.surname'), 'col-lg-2');
		$datagrid->addSearch('phone', self::__('customer.phone'), 'col-lg-2');
		$datagrid->addSearch('billing_address.city', self::__('address.city'),  'col-lg-2');
		$datagrid->addSearch('billing_address.street', self::__('address.street'),  'col-lg-2');
		$datagrid->addSearch('billing_address.psc', self::__('address.psc'),  'col-lg-1');

		$datagrid->addQuickFilter(routerLink('admin.customer.index', ['filter__unfinishedOrder' => 1]),
			self::__('customer.unfinishedOrder'));
		$datagrid->addQuickFilter(routerLink('admin.customer.index', ['filter__orders' => 1]),
			self::__('customer.haveOrders'));

		// Extend
		$datagrid = apply_filters('ecommerce_customer_datagrid', $datagrid);

		return $datagrid;
	}

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('customer', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.customer.create');
        }

        $customer = Entity::getEntity('Customer');
        $form->hydrate($customer);

		// Upload file
		if (!empty($_FILES['avatar']['name'])) {
			$files = Upload::uploadFile('avatar', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$customer->avatar = Database::Model('Multimedia')->find($file);
			}
		}

		// Extend
		$customer = apply_filters('ecommerce_customer_hydrate', $customer, $post);

        Database::saveAndFlush($customer);

        // Redirect with message
        $this->alert($this->_('customer.created'));
        Router::redirect('admin.customer.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.customer.update', ['id' => $id]));

        /** @var Customer $customer */
        $customer = Database::Model('Customer')->find($id);
        $form->setValues($customer, ['password', 'avatar']);

		// Extend
		$form = apply_filters('ecommerce_customer_form_values', $form, $customer);

        View::view('customer', [
            "form" => $form->build(),
            "notes" => Notes::getNotes($id),
            "formNotes" => Notes::form($id)->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        $customer = Database::Model('Customer')->find($id);
        $form->hydrate($customer);

		// Upload file
		if (!empty($_FILES['avatar']['name'])) {
			$files = Upload::uploadFile('avatar', 1);
			Upload::cropImages($files);
			foreach ($files as $file) {
				$customer->avatar = Database::Model('Multimedia')->find($file);
			}
		}

		// Extend
		$customer = apply_filters('ecommerce_customer_hydrate', $customer, $post);

        $this->alert($this->_('customer.edited'));
        Database::saveAndFlush($customer);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        Database::Model('Customer')->delete($id);

        // Redirect
        $this->alert($this->_('customer.deleted'));
		Router::redirectToURL(Router::$prevLink);
    }

	/**
	 * @param $ids
	 */
	public function removeAll($ids)
	{
		foreach ($ids as $id) {
			Database::Model('Customer')->delete($id);
		}
		Router::redirectToURL(Router::$prevLink);
	}

    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('customer', routerLink('admin.customer.store'));

        // Default
        $form->addText('name', $this->_('customer.name'))->setCol('col-lg-6')->required();
        $form->addText('surname', $this->_('customer.surname'))->setCol('col-lg-6')->required();
        $form->addText('email', $this->_('customer.email'))->setCol('col-lg-6')->email();
        $form->addPassword('password', $this->_('customer.password'))->setCol('col-lg-6');

        // Phone
		$form->addText('phone', $this->_('customer.phone'));

        // Status
        $form->addSelect('status', self::STATUSES, $this->_('customer.status'))->required();

        // Avatar
        $form->addInput('avatar', 'file', 'Avatar');

		// Extend
		$form = apply_filters('ecommerce_customer_form', $form);

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }

    /**
     * Search customers
     *
     * @param $q
     * @param $page
     * @param boolean $address
     */
    public function search($q, $page, $address = false)
    {
        $users = Database::Model('Customer')
            ->where('c.name', '%', $q)
            ->orWhere('c.surname', '%', $q)
            ->orWhere('c.email', '%', $q)
            ->paginate(8);
        $output = [
            'results' => [],
            'pagination' => [
                'more' => (ceil($users->count() / 8) - 1) == $page ? true : false
            ]
        ];
        foreach ($users as $user) {
            /** @var Customer $user */
            $output['results'][] = [
                'id' => $user->id,
                'text' => $user->getFullName() . ' (' . $user->email . ')'
            ];
        }
        echo json_encode($output);
    }

	/**
	 * Export customers to excel
	 */
	public static function dataGridExportExcelHandler()
	{
		$datagrid = self::datagrid();
		$datagrid->addColumn('orders', 'Orders')->setRenderer(function($customer) {
			return $customer->orders->count();
		});
		$datagrid->addColumn('last_order', 'Last order date')->setRenderer(function($customer) {
			if (!empty($customer->orders->last())) {
				return date('Y-m-d H:i:s', $customer->orders->last()->created_at->getTimestamp());
			} else if (!empty($customer->last_order_at)) {
				return date('Y-m-d H:i:s', $customer->last_order_at->getTimestamp());
			} else return null;
		});

		$export = new DatagridExcelExport($datagrid);

		$export->skipColumns(['notes']);
		$export->setColumnDimensions([
			'name' => 20,
			'email' => 25,
			'phone' => 15
		]);

		$export->doExport();
    }
}
