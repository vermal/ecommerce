<?php

namespace Vermal\Ecommerce\Modules\Customers;


use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Tests\Common\Annotations\Fixtures\Annotation\Route;
use Entities\Address;
use Vermal\Admin\Defaults\Languages;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;


class Notes extends Controller
{
    public static $NOTES = [
        'info' => 'INFO',
        'warning' => 'WARNING',
        'danger' => 'DANGER'
    ];

    /**
     * Get customer notes
     *
     * @param int|Customer $customer
     * @return mixed
     */
    public static function getNotes($customer)
    {
        return Database::Model('Note')->where('n.customer', $customer)
            ->order('n.created_at', 'DESC')
            ->get();
    }

    /**
     * @param $post
     */
    public function addNote($post)
    {
        $note = Entity::getEntity('Note');
        $note->note = $this->sanitize('strip_tags|trim', $post->note);
        $note->type = $this->sanitize('strip_tags|trim', $post->type);
        $note->customer = Database::Model('Customer')->find($post->customer);

        $note = Database::saveAndFlush($note);
        $this->alert($this->_('note.added'), 'success', [
            'note' => json_encode([
                'id' => $note->id,
                'note' => $note->note,
                'type' => $note->type
            ])
        ]);
    }

    /**
     * Delete note
     *
     * @param $post
     */
    public function removeNote($post)
    {
        Database::Model('Note')->delete($post->id);
    }

    /**
     * Add customer note form
     *
     * @param int $customer
     * @return Form
     */
    public static function form($customer)
    {
        $form = new Form('customer-note', routerLink('admin.customer.add-note'));
        $form->setAjax(true);

        // Default
        $form->addSelect('type', self::$NOTES, self::__('note.type'))->required();
        $form->addTextArea('note')->required();
        $form->addHidden('customer', $customer);

        // Add submit button
        $form->addButton('submit', self::__('global.form.save'))->setClass('btn btn-info');

        return $form;
    }

}
