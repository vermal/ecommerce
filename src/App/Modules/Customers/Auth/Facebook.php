<?php

namespace Vermal\Ecommerce\Modules\Customers\Auth;

use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Vermal\Admin\Modules\Auth\Auth;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Ecommerce\Modules\Customers\Authorization;

class Facebook
{

	private $facebook;
	private $helper;

	public function __construct()
	{
		$this->facebook = new \Facebook\Facebook([
			'app_id' => App::get('facebookAppId'),
			'app_secret' => App::get('facebookAppSecret'),
			'default_graph_version' => App::get('facebookGraphVersion')
		]);

		// Get redirect login helper
		$this->helper = $this->facebook->getRedirectLoginHelper();
	}

	/**
	 * Get facebook redirect url
	 *
	 * @param string $redirectTo
	 * @param array $data
	 * @return string
	 */
	public function getLoginUrl($redirectTo = '', $data = ['email'])
	{
		if (empty($redirectTo)) $redirectTo = routerLink('ecommerce.customer.auth-fb');
		return $this->helper->getLoginUrl($redirectTo, $data);
	}


	/**
	 * Facebook callback
	 *
	 * @throws FacebookSDKException
	 */
	public function callback()
	{
		$accessToken = null;
		try {
			$accessToken = $this->helper->getAccessToken();
			$success = true;
		} catch (FacebookResponseException $e) {
			$success = false;
		} catch (FacebookSDKException $e) {
			$success = false;
		}

		if (!$success || empty($accessToken)) {
			do_action('facebook_auth_callback', [$accessToken, [], false]);
			return;
		}

		$oAuth2Client = $this->facebook->getOAuth2Client();
		if (!$accessToken->isLongLived())
			$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);

		$response = $this->facebook->get('/me?fields=id,first_name,last_name,email', $accessToken);
		$userData = $response->getGraphNode()->asArray();

		// Try to find user or create new user
		$customer = Database::Model('Customer')
			->where('c.email', $userData['email'])
			->orWhere('c.facebook_id', $userData['id'])
			->first();
		$customer = !empty($customer) ? $customer : null;
		Authorization::storeCustomer($userData['email'], $userData['first_name'], $userData['last_name'], null, $customer, [
			'facebook_id' => $userData['id']
		]);

		do_action('facebook_auth_callback', [$accessToken, $userData, $success]);
	}

}
