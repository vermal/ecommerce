<?php

namespace Vermal\Ecommerce\Modules\Customers\Auth;

use Vermal\App;
use Vermal\Database\Database;
use Vermal\Ecommerce\Modules\Customers\Authorization;

class Google
{

	private $google;

	public function __construct()
	{
		//Make object of Google API Client for call Google API
		$this->google = new \Google_Client();

		//Set the OAuth 2.0 Client ID
		$this->google->setClientId(App::get('googleClientId'));

		//Set the OAuth 2.0 Client Secret key
		$this->google->setClientSecret(App::get('googleClientSecret'));
		$this->google->setRedirectUri(routerLink('ecommerce.customer.auth-google'));

		$this->google->addScope('email');
		$this->google->addScope('profile');
	}

	/**
	 * Get facebook redirect url
	 *
	 * @param string $redirectTo
	 * @return string
	 */
	public function getLoginUrl($redirectTo = '')
	{
		if (empty($redirectTo)) {
			$redirectTo = routerLink('ecommerce.customer.auth-google');
			$this->google->setRedirectUri($redirectTo);
		}
		return $this->google->createAuthUrl();
	}


	/**
	 * Callback
	 */
	public function callback()
	{
		if (empty($_GET['code'])) {
			do_action('google_auth_callback', [null, [], false]);
			return;
		}

		$token = $this->google->fetchAccessTokenWithAuthCode($_GET['code']);
		$this->google->setAccessToken($token['access_token']);

		// get profile info
		$google_oauth = new \Google_Service_Oauth2($this->google);
		$google_account_info = $google_oauth->userinfo->get();
		$id = $google_account_info->id;
		$email = $google_account_info->email;
		$name = $google_account_info->getGivenName();
		$surname = $google_account_info->getFamilyName();

		// Try to find user or create new user
		$customer = Database::Model('Customer')
			->where('c.email', $email)
			->orWhere('c.google_id', $id)
			->first();
		$customer = !empty($customer) ? $customer : null;
		Authorization::storeCustomer($email, $name, $surname, null, $customer, [
			'google_id' => $id
		]);

		do_action('google_auth_callback', [$token['access_token'], $google_account_info, true]);
	}

}
