<?php

namespace Vermal\Ecommerce\Modules\Customers;


use Doctrine\ORM\Query\Expr\Join;
use Vermal\Admin\Defaults\Languages;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\Currencies;
use Vermal\Admin\Sanitizer;
use Vermal\DataGrid;
use Vermal\Ecommerce\Modules\Customers\Entities\Address;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Repositories\ProductMetaRepository;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\Admin\View;
use Vermal\Database\Database;


class Addresses extends Controller
{
	const BILLING = 'billing';
	const SHIPPING = 'shipping';
	private $addressTypes = [];

    public function __construct()
    {
        parent::__construct();
         $this->requiredPermission('addresses', $this->CRUDAction);
        $this->addToBreadcrumb('address', 'admin.address.index');
        $this->addressTypes = [
        	self::BILLING => 'Billing',
			self::SHIPPING => 'Shipping'
		];
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $customers = Database::Model('Customer')->setPair('id', 'name', 'surname')->get();
        $data = Database::Model('Address');
        $datagrid = new DataGrid($data);

        $datagrid->addColumn('id', 'ID');
        $datagrid->addColumn('title', $this->_('address.title'));
        $datagrid->addColumn('name', $this->_('customer.name'))->setRenderer(function($address) {
            return '<a href="' . routerLink('admin.customer.edit', ['id' => $address->customer->id]) . '">' .
                $address->name . ' ' . $address->surname . '</a>';
        });
        $datagrid->addColumn('address', $this->_('customer.address'))->setRenderer(function($address) {
            return $address->getFullAddress();
        });


        // Add actions
        $datagrid->addEdit(['admin.address.edit', ['id']]);
        $datagrid->addDelete(['admin.address.destroy_', ['id']]);


        $datagrid->addFilter('customer', $customers, $this->_('address.customer'));

        // Return view
        View::view('crud.read', [
            'datagrid' => $datagrid->build()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $form = $this->form();
        View::view('crud.create', [
            "form" => $form->build()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \stdClass $post
     * @throws
     */
    public function store($post)
    {
        $form = $this->form();
        if ($form->hasError()) {
            Router::redirect('admin.address.create');
        }

        $address = Entity::getEntity('Address');
        $form->hydrate($address, ['customer']);
        $address->customer = Database::Model('Customer')->find($post->customer);

        Database::saveAndFlush($address);

        // Redirect with message
        $this->alert($this->_('address.created'));
        Router::redirect('admin.address.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @throws
     */
    public function edit($id)
    {
        $form = $this->form();
        $form->setAjax(true);
        $form->setMethod('PUT');
        $form->setAction(routerLink('admin.address.update', ['id' => $id]));

        /** @var Address $address */
        $address = Database::Model('Address')->find($id);
        $form->setValues($address, ['customer']);
        $form->getComponent('customer')->setValue($address->customer->id);

        View::view('crud.edit', [
            "form" => $form->build()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \stdClass $post
     * @param  int  $id
     * @throws
     */
    public function update($post, $id)
    {
        $form = $this->form();
        if ($form->hasError()) {
            $this->alert($this->_('error.form.empty'), 'danger');
            die();
        }

        $address = Database::Model('Address')->find($id);
        $form->hydrate($address, ['customer']);
        $address->customer = Database::Model('Customer')->find($post->customer);

        $this->alert($this->_('address.edited'));
        Database::saveAndFlush($address);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
//        $product = Database::Model('Product')->find($id);
//        $product->clearTr();
//        Database::saveAndFlush($product);
        Database::Model('Address')->delete($id);

        // Redirect
        $this->alert($this->_('address.deleted'));
        Router::redirect('admin.address.index');
    }
    /**
     * Create register form
     *
     * @return Form
     */
    private function form()
    {
        $form = new Form('address', routerLink('admin.address.store'));

        $form->addText('title', $this->_('address.title'))->required();
		$form->addSelect('type', $this->addressTypes, $this->_('address.type'))->required();

        // Personal
        $form->addText('name', $this->_('customer.name'))->setCol('col-lg-4')->required();
        $form->addText('surname', $this->_('customer.surname'))->setCol('col-lg-4')->required();
        $form->addText('phone', $this->_('customer.phone'))->setCol('col-lg-4')->required();

        // Address
        $form->addText('street', $this->_('address.street'))->setCol('col-lg-3')->required();
        $form->addText('city', $this->_('address.city'))->setCol('col-lg-3')->required();
        $form->addText('psc', $this->_('address.psc'))->setCol('col-lg-3')->required();
        $form->addText('country', $this->_('address.country'))->setCol('col-lg-3')->required();

        // Company
        $form->addText('companyName', $this->_('address.companyName'))->setCol('col-lg-3');
        $form->addText('ico', $this->_('address.ico'))->setCol('col-lg-3');
        $form->addText('dic', $this->_('address.dic'))->setCol('col-lg-3');
        $form->addText('ic_dph', $this->_('address.ic_dph'))->setCol('col-lg-3');

        $customers = Database::Model('Customer')->setPair('id', 'name', 'surname')->get();
        $form->addSelect('customer', $customers, $this->_('address.customer'))
            ->setClass('js-select2');

        // Add submit button
        $form->addButton('submit', $this->_('global.form.save'))->setClass('btn btn-success');

        return $form;
    }
}
