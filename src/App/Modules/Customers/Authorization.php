<?php

namespace Vermal\Ecommerce\Modules\Customers;

use Translatable\Fixture\Type\Custom;
use Vermal\Admin\Defaults\Controller;
use Vermal\Admin\Defaults\Mailer;
use Vermal\Admin\Sanitizer;
use Vermal\App;
use Vermal\Database\Database;
use Vermal\Database\Entity;
use Vermal\Ecommerce\Modules\Customers\Auth\Facebook;
use Vermal\Ecommerce\Modules\Customers\Auth\Google;
use Vermal\Ecommerce\Modules\Customers\Customers;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Form\Form;
use Vermal\Router;
use Vermal\View;

class Authorization
{
	public static $signIn = '';
	public static $signInRequest = '';
	public static $signInView = '';
	public static $signUp = '';
	public static $signUpRequest = '';
	public static $signUpView = '';
	public static $lostPassword = '';
	public static $lostPasswordRequest = '';
	public static $lostPasswordView = '';
	public static $renewPassword = '';
	public static $renewPasswordRequest = '';
	public static $renewPasswordView = '';
	public static $profile = '';
	public static $auths = [];

	/**
	 * @var bool $recaptcha
	 */
	public static $recaptcha = false;

	/**
	 * Logout
	 */
	public function logout()
	{
		$id = App::sessionGet('customer')['id'];
		$customer = Database::Model('Customer')->find($id);
		if (!empty($customer->facebook_id) && !empty(self::$auths['facebook'])) {

		}

		App::sessionSet('customer', null);
		Router::redirect(self::$signIn);
	}


	/**
	 * Sign in
	 *
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function signIn()
	{
		$form = $this->signInForm();
		view(self::$signInView, [
			'form' => $form->build(),
			'errors' => $form->getErrors()
		]);
	}

	/**
	 * Sign in request method
	 *
	 * @param $post
	 */
	public function signInRequest($post)
	{
		$form = $this->signInForm();
		if ($form->hasError()) {
			Router::redirect(self::$signIn);
		}

		// ReCaptcha
		if (!empty($post->{'g-recaptcha-response'}))
			$recaptcha = Form::validateRecaptcha($post->{'g-recaptcha-response'});
		else
			$recaptcha = false;
		if (!$recaptcha) {
			$this->captchaError($form);
			Router::redirect(self::$signIn);
		}

		/**
		 * @var Customer $user
		 */
		$user = Database::Model('Customer')->where('c.email', Sanitizer::applyFilter('strip_tags|trim|escape', $post->email))->first();
		if (empty((array)$user)) {
			$form->setGlobalError($this->_('auth.login.wrong_credentials'),'danger');
			Router::redirect(self::$signIn);
		}

		if (!empty($user->lastLoginAttempt)) {
			// Reset login attempts if time exceeded
			$lastLoginAttempt = strtotime("+" . App::get('login.timeout', 15) . " minutes", $user->lastLoginAttempt->getTimestamp());
			if ($lastLoginAttempt < strtotime("now")) {
				$user->loginAttempts = 0;
			}
			// Check if login attempts are OK
			if($user->loginAttempts > App::get('login.attempts', 4)) {
				$form->setGlobalError(sprintf($this->_('auth.login.tooManyAttempts'), App::get('login.attempts', 4)),'danger');
				$this->incorrectLogin($user);
				Router::redirect(self::$signIn);
			}
		}

		if (!password_verify($post->password, $user->password)) {
			$form->setGlobalError($this->_('auth.login.wrong_credentials'),'danger');
			$this->incorrectLogin($user);
			Router::redirect(self::$signIn);
		}

		// Save user login
		$user = $this->correctLogin($user);

		// Store user
		self::storeUserInSession($user);

		// Redirect to profile
		Router::redirect(self::$profile);
	}

	/**
	 * @param Customer $customer
	 * @param $time
	 * @throws \Exception
	 */
	private function incorrectLogin($customer, $time = true)
	{
		if (!empty($customer)) {
			if ($time)
				$customer->lastLoginAttempt = new \DateTime();
			$customer->loginAttempts = (int) $customer->loginAttempts + 1;
			Database::saveAndFlush($customer);
		}
	}

	/**
	 * @param Customer $customer
	 * @throws \Exception
	 * @return Customer
	 */
	private function correctLogin($customer)
	{
		if (!empty($customer)) {
			if (!empty($customer->lastLoginAttempt)) {
				$customer->lastLogin = $customer->lastLoginAttempt;
			}
			$customer->lastLoginAttempt = new \DateTime();
			$customer->loginAttempts = 0;
			Database::saveAndFlush($customer);
		}
		return $customer;
	}

	/**
	 * Sign in form
	 *
	 * @return Form
	 */
	public function signInForm()
	{
		$form = new Form('sign-in', routerLink(self::$signInRequest));

		$form->addEmail('email', 'E-Mail')->email();
		$form->addPassword('password', 'Heslo')->setValue(null)->required();
		$form->addCheckboxSwitch('keepLoggedIn', 'Neodhlasovať');

		foreach (self::$auths as $auth => $data) {
			if ($auth === 'facebook') $clazz = new Facebook();
			else if ($auth === 'google') $clazz = new Google();
			if (!empty($clazz))
				$this->addAuthButton($clazz, $auth, $form);
		}

		if (self::$recaptcha)
			$form->addRecpatcha();

		$form->addButton('submit', 'Prihlásiť')
			->setCol('col-auto mb-0')
			->setClass('btn btn-primary');

		return $form;
	}



	/**
	 * Sign up
	 *
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function signUp()
	{
		$form = $this->signUpForm();
		view(self::$signUpView, [
			'form' => $form->build(),
			'errors' => $form->getErrors()
		]);
	}

	/**
	 * Sign in request method
	 *
	 * @param $post
	 */
	public function signUpRequest($post)
	{
		$form = $this->signUpForm();
		if ($form->hasError()) {
			$form->setGlobalError('Akciu sa nepodarilo dokončiť!');
			Router::redirect(self::$signUp);
		}

		// ReCaptcha
		if (!empty($post->{'g-recaptcha-response'}))
			$recaptcha = Form::validateRecaptcha($post->{'g-recaptcha-response'});
		else
			$recaptcha = false;
		if (!$recaptcha) {
			$this->captchaError($form);
			Router::redirect(self::$signUp);
		}

		// Check if customer already exist
		$user = Database::Model('Customer')->where('c.email', Sanitizer::applyFilter('strip_tags|trim|escape', $post->email))->first();
		if (!empty((array)$user)) {
			$form->setGlobalError(
				'Použivateľ s týmto emailom už existuje, chcete sa <a href="' . routerLink(self::$signIn) . '">prihlásiť?</a>',
				'danger');
			Router::redirect(self::$signUp);
		}

		// Check if passwords are same
		if ($post->password !== $post->passwordCheck) {
			$form->setGlobalError('Heslá sa nezhodujú', 'danger');
			Router::redirect(self::$signUp);
		}

		// Create customer
		self::storeCustomer($post->email, $post->name ?? '', $post->surname ?? '', $post->password);

		// Redirect to profile
		Router::redirect(self::$profile);
	}

	/**
	 * Store customer
	 *
	 * @param $email
	 * @param $name
	 * @param $surname
	 * @param $password
	 * @param Customer $customer
	 * @param array $aditional
	 */
	public static function storeCustomer($email, $name, $surname, $password, $customer = null, $aditional = [])
	{
		if (empty($customer))
			$customer = Entity::getEntity('Customer');

		$customer->email = Sanitizer::applyFilter('strip_tags|trim|escape', $email);
		$customer->name = Sanitizer::applyFilter('strip_tags|trim|escape', $name);
		$customer->surname = Sanitizer::applyFilter('strip_tags|trim|escape', $surname);

		// Set active status
		$customer->status = Customers::ACTIVE;

		if (!empty($password))
			$customer->setPassword($password);

		// Set aditional data
		foreach ($aditional as $key => $value) {
			$customer->{$key} = $value;
		}

		// Save customer
		$customer = Database::saveAndFlush($customer);
		self::storeUserInSession($customer);
	}

	/**
	 * Sign in form
	 *
	 * @return Form
	 */
	public function signUpForm()
	{
		$form = new Form('sign-up', routerLink(self::$signUpRequest));

		$form->addText('name', 'Meno')->setCol('col-sm-6')->required();
		$form->addText('surname', 'Priezvisko')->setCol('col-sm-6')->required();

		$form->addEmail('email', 'E-Mail')->email();
		$form->addPassword('password', 'Heslo')->setValue(null)->required();
		$form->addPassword('passwordCheck', 'Kontrola hesla')->setValue(null)->required();

		$form->addCheckboxSwitch('agreement', 'Súhlasim s <a href="#">pravidlami</a>')->required();

		if (self::$recaptcha)
			$form->addRecpatcha();

		foreach (self::$auths as $auth => $data) {
			if ($auth === 'facebook') $clazz = new Facebook();
			else if ($auth === 'google') $clazz = new Google();
			if (!empty($clazz))
				$this->addAuthButton($clazz, $auth, $form);
		}

		$form->addButton('submit', 'Registrovať')
			->setClass('btn btn-primary');

		$form = apply_filters('authorization_signup_form', $form);

		return $form;
	}

	/**
	 * Sign up
	 *
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function lostPassword()
	{
		$form = $this->lostPasswordForm();
		view(self::$lostPasswordView, [
			'form' => $form->build(),
			'errors' => $form->getErrors()
		]);
	}

	/**
	 * Sign in request method
	 *
	 * @param $post
	 */
	public function lostPasswordRequest($post)
	{
		$form = $this->lostPasswordForm();
		if ($form->hasError()) {
			Router::redirect(self::$lostPassword);
		}

		// ReCaptcha
		if (!empty($post->{'g-recaptcha-response'}))
			$recaptcha = Form::validateRecaptcha($post->{'g-recaptcha-response'});
		else
			$recaptcha = false;
		if (!$recaptcha) {
			$this->captchaError($form);
			Router::redirect(self::$lostPassword);
		}

		// Get customer
		$customer = Database::Model('Customer')->where('c.email', $post->email)->first();
		if ($customer !== null) {
			$token = uniqid();
			$customer->resetToken = $token;
			$customer->resetTokenValidUntil = (new \DateTime())->modify('+30 minutes');

			$settings = Controller::$appSettings;
			$mail = Controller::mailer();
			$mail->body($this->resetPasswordGenerateMail($customer))
				->from($settings->mailer_username, $settings->mailer_name)
				->addAddress($customer->email, $customer->name)
				->subject($this->_('auth.mail.passwordReset.title'));

			try {
				$mail->send();
			} catch (\Exception $e) {
				$form->setGlobalError($this->_('auth.mail.passwordReset.error'), 'danger');
				Router::redirect(self::$lostPassword);
			}

			// Save user
			Database::saveAndFlush($customer);
		}

		// Finish
		$form->setGlobalError('Na Vašu emailovú adresu sme zaslali link na obnovenie hesla.', 'success');
		Router::redirect(self::$lostPassword);
	}

	/**
	 * Generate password reset email
	 *
	 * @param $user
	 * @return string
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function resetPasswordGenerateMail($user)
	{
		View::setViewPath(App::get('admin.base_path') . '/Modules/Auth/templates/');
		$mail = View::view('mail.password-reset', [
			'resetLink' => routerLink(self::$renewPassword, ['token' => $user->resetToken]),
			'user' => $user
		], true);
		return $mail;
	}

	/**
	 * Lost password form
	 *
	 * @return Form
	 */
	public function lostPasswordForm()
	{
		$form = new Form('lost-password', routerLink(self::$lostPasswordRequest));

		$form->addEmail('email', 'E-Mail')->email();

		if (self::$recaptcha)
			$form->addRecpatcha();

		$form->addButton('submit', 'Obnoviť heslo')
			->setClass('btn btn-primary');

		return $form;
	}

	/**
	 * Renew password
	 *
	 * @throws \Whoops\Exception\ErrorException
	 */
	public function renewPassword($key)
	{
		$form = $this->renewPasswordForm($key);

		$customer = $this->getCustomerWithValidToken($key);
		if (empty($customer)) {
			$form = $this->lostPasswordForm();
			$form->setGlobalError('Helso sa nepodarilo obnoviť skúste to znovu prosím!', 'danger');
			Router::redirect(self::$lostPassword);
		}

		view(self::$renewPasswordView, [
			'form' => $form->build(),
			'errors' => $form->getErrors()
		]);
	}

	/**
	 * Sign in request method
	 *
	 * @param $post
	 */
	public function renewPasswordRequest($post)
	{
		$form = $this->renewPasswordForm($post->key);
		if ($form->hasError()) {
			Router::redirect(self::$renewPassword);
		}

		// ReCaptcha
		if (!empty($post->{'g-recaptcha-response'}))
			$recaptcha = Form::validateRecaptcha($post->{'g-recaptcha-response'});
		else
			$recaptcha = false;
		if (!$recaptcha) {
			$this->captchaError($form);
			Router::redirect(self::$renewPassword, ['key' => $post->key]);
		}

		// Get customer
		$user = $this->getCustomerWithValidToken($post->key);
		if (empty($user->id)) {
			$form->setGlobalError(
				'Helso sa nepodarilo obnoviť skúste to znovu prosím!',
				'warning');
			Router::redirect(self::$lostPassword);
		}

		if ($post->password !== $post->password_check) {
			$form->setGlobalError('Heslá sa nezhodujú', 'danger');
			Router::redirect(self::$renewPassword, ['key' => $post->key]);
		}

		$user->reset_token = null;
		$user->reset_token_valid_until = null;
		$user->setPassword($post->password);
		Database::saveAndFlush($user);

		// Finish
		$form = $this->signInForm();
		$form->setGlobalError(
			'Heslo bolo úspešne aktualizované, teraz sa môžete prihlásiť.',
			'success');
		Router::redirect(self::$signIn);
	}

	/**
	 * Get customer with valid token
	 *
	 * @param $token
	 * @return mixed
	 * @throws \Exception
	 */
	private function getCustomerWithValidToken($token)
	{
		return Database::Model('Customer')
			->where('c.resetToken', Sanitizer::applyFilter('strip_tags|trim|escape', $token))
			->where('c.resetTokenValidUntil', '>', (new \DateTime())->format('Y-m-d H:i:s'))
			->first();
	}

	/**
	 * Lost password form
	 *
	 * @param $key
	 * @return Form
	 */
	public function renewPasswordForm($key = null)
	{
		$form = new Form('renew-password', routerLink(self::$renewPasswordRequest));

		$form->addHidden('key', Sanitizer::applyFilter('stip_tags|escape', $key));
		$form->addPassword('password', 'Heslo')->required();
		$form->addPassword('password_check', 'Overenie hesla')->required();

		if (self::$recaptcha)
			$form->addRecpatcha();

		$form->addButton('submit', 'Obnoviť heslo')
			->setClass('btn btn-primary');

		return $form;
	}


	/**
	 * @param Google|Facebook $clazz
	 * @param $type
	 * @param Form $form
	 */
	private function addAuthButton($clazz, $type, &$form)
	{
		$googleSettings = self::$auths[$type];
		$form->addHtml($type, 'a', $googleSettings['label'])
			->addAttribute('href', $clazz->getLoginUrl());
		if (!empty($googleSettings['class'])) {
			$form->getComponent($type)->setClass($googleSettings['class']);
		}
		if (!empty($googleSettings['attributes'])) {
			foreach ($googleSettings['attributes'] as $key => $value) {
				$form->getComponent($type)->addAttribute($key, $value);
			}
		}
	}


	/**
	 * Store user in session
	 *
	 * @param $user
	 */
	public static function storeUserInSession($user)
	{
		App::sessionSet('customer', apply_filters('authorization_store_user_in_session', [
			'id' => $user->id,
			'email' => $user->email,
			'name' => $user->name,
			'surname' => $user->surname,
			'fullName' => $user->name . ' ' . $user->surname,
			'oAuth' => !empty($user->google_id) || !empty($user->facebook_id) ? true : false,
			'miniAvatar' => !empty($user->avatar) ? $user->avatar->getMiniFile() : null,
			'avatar' => !empty($user->avatar) ? $user->avatar->getFile() : null
		], $user));
	}

	/**
	 * Get customer data
	 *
	 * @return array|null
	 */
	public static function getCustomer()
	{
		return App::sessionGet('customer');
	}

	/**
	 * Check if user is logged in
	 *
	 * @return bool
	 */
	public static function customerLoggedIn()
	{
		$user = App::sessionGet('customer');
		return !empty($user);
	}

	/**
	 * Translate
	 *
	 * @param $key
	 * @return array|mixed
	 */
	private function _($key) {
		return Controller::__($key);
	}

	/**
	 * @param Form $form
	 */
	private function captchaError(&$form)
	{
		$form->setGlobalError($this->_('global.recaptchaError'), 'danger');
	}

}
