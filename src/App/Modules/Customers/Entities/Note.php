<?php

namespace Vermal\Ecommerce\Modules\Customers\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_customer_note")
 * @ORM\HasLifecycleCallbacks
 **/
class Note extends Model
{
    /**
     * @ORM\Column(type="string")
     */
    protected $note;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\ManyToOne(targetEntity="\Customer", inversedBy="notes")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * Product constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->addresses = new ArrayCollection();
    }

}
