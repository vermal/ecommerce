<?php

namespace Vermal\Ecommerce\Modules\Customers\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_customer")
 * @ORM\HasLifecycleCallbacks
 **/
class Customer extends Model
{
    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $surname;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $phone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $password;

    /**
     * @ORM\ManyToOne(targetEntity="\Address")
     * @ORM\JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $billing_address;

    /**
     * @ORM\ManyToOne(targetEntity="\Address")
     * @ORM\JoinColumn(name="shipping_address_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $shipping_address;

    /**
     * @ORM\OneToMany(targetEntity="\Address", mappedBy="customer")
     */
    protected $addresses;

    /**
     * @ORM\OneToMany(targetEntity="\Note", mappedBy="customer")
     */
    protected $notes;

    /**
     * @ORM\OneToMany(targetEntity="\Inbox", mappedBy="customer")
     */
    protected $messages;

    /**
     * @ORM\Column(type="string")
     */
    protected $status;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $facebook_id;

	/**
	 * @ORM\Column(type="string", nullable=true)
	 */
	protected $google_id;

	/**
	 * @ORM\Column(type="datetime", name="last_login", nullable=true)
	 */
	protected $lastLogin;

	/**
	 * @ORM\Column(type="datetime", name="last_login_attempt", nullable=true)
	 */
	protected $lastLoginAttempt;

	/**
	 * @ORM\Column(type="integer", name="login_attempts", nullable=true)
	 */
	protected $loginAttempts;

	/**
	 * @ORM\ManyToOne(targetEntity="\Multimedia")
	 * @ORM\JoinColumn(name="multimedia_id", referencedColumnName="id", nullable=true)
	 */
	protected $avatar;

	/** @ORM\Column(type="string", nullable=true, name="reset_token") **/
	protected $resetToken;

	/** @ORM\Column(type="datetime", nullable=true, name="reset_token_valid_until") **/
	protected $resetTokenValidUntil;

	/**
	 * @ORM\Column(type="integer", name="unfinished_order")
	 */
	protected $unfinishedOrder;

	/**
	 * @ORM\OneToMany(targetEntity="\Order", mappedBy="customer")
	 * @ORM\OrderBy({"created_at" = "DESC"})
	 */
	protected $orders;

	/** @ORM\Column(type="datetime", nullable=true) **/
	protected $last_order_at;

    /**
     * Product constructor.
     */
    public function __construct() {
        parent::__construct();
        $this->addresses = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->unfinishedOrder = 0;
        $this->orders = new ArrayCollection();
    }

    /**
     * Get full name
     *
     * @return string
     */
    public function getFullName()
    {
        return $this->name . ' ' . $this->surname;
    }

	/**
	 * Get full name
	 *
	 * @return string
	 */
	public function getFullNameAndEmail()
	{
		return $this->name . ' ' . $this->surname . ' - ' . $this->email;
	}

    /**
     * Get last note
     *
     * @return Note|null
     */
    public function getLastNote()
    {
        if ($this->notes->count() > 0) {
            return $this->notes->getIterator()[$this->notes->count() - 1]->note;
        } else return null;
    }

    /**
     * @return mixed|null
     */
    public function getAddress()
    {
        if (!empty($this->addresses->first())) return $this->addresses->first();
        else return null;
    }

	/**
	 * @param Address $address
	 */
	public function addAddress($address)
	{
		$this->addresses[] = $address;
    }

    /**
     * @return Address|null
     */
    public function getBillingAddress()
    {
        if (!empty($this->billing_address)) return $this->billing_address;
        else if (!empty($this->shipping_address)) return $this->shipping_address;
        else if (!empty($this->addresses->first())) return $this->addresses->first();
        else return null;
    }

    /**
     * @return Address|null
     */
    public function getShippingAddress()
    {
        if (!empty($this->shipping_address)) return $this->shipping_address;
        else if (!empty($this->billing_address)) return $this->billing_address;
        else if (!empty($this->addresses->first())) return $this->addresses->first();
        else return null;
    }

    /**
     * Get address as array
     *
     * @param null $type
     * @return array
     */
    public function getAddressAsArray($type = null)
    {
        $address = $this->{'get' . ucfirst($type) . 'Address'}();
        $address_ = [];
        if (!empty($address)) {
            /** @var Address $address */
            $address_ = $address->toArray();
        }
        return $address_;
    }

	/**
	 * @param $password
	 */
	public function setPassword($password)
	{
		$this->password = password_hash($password, PASSWORD_DEFAULT);
    }

}
