<?php

namespace Vermal\Ecommerce\Modules\Customers\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Vermal\Admin\Defaults\Entities\Translatable;
use Vermal\Admin\Defaults\Model;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Attributes;
use Vermal\Ecommerce\Modules\Product\Entities\Extensions\Categories;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity @ORM\Table(name="ecommerce_address")
 * @ORM\HasLifecycleCallbacks
 **/
class Address extends Model
{
	/**
	 * @ORM\Column(type="string")
	 */
	protected $type;

    /**
     * @ORM\Column(type="string")
     */
    protected $title;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $surname;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $phone;

    /**
     * @ORM\Column(type="string")
     */
    protected $street;

    /**
     * @ORM\Column(type="string")
     */
    protected $city;

    /**
     * @ORM\Column(type="integer")
     */
    protected $psc;

    /**
     * @ORM\Column(type="string")
     */
    protected $country;

    /**
     * @ORM\Column(type="string", name="company_name", nullable=true)
     */
    protected $companyName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $ico;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $dic;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $ic_dph;

	/**
	 * @ORM\ManyToOne(targetEntity="\Customer", inversedBy="addresses")
	 * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
	 */
	protected $customer;

    public function getFullAddress()
    {
        return $this->name . ' ' . $this->surname . ', ' . $this->street . ', ' . $this->psc . ' ' . $this->city;
    }

    /**
     * Convert to array
     *
     * @return array
     */
    public function toArray()
    {
        $address = get_object_vars($this);
        $address['fullAddress'] = $this->getFullAddress();
        unset($address['customer']);
        unset($address['created_at']);
        unset($address['updated_at']);
        return $address;
    }
}
