<?php

namespace Vermal\Ecommerce;

use Vermal\Console\Console;
use Vermal\Ecommerce\Defaults\Controller;
use Vermal\Ecommerce\Defaults\DummyProducts;
use Vermal\Ecommerce\Defaults\Imap;
use Vermal\Ecommerce\Defaults\Seeder;
use Vermal\Ecommerce\Modules\Cart\Entities\Cart;
use Vermal\Ecommerce\Modules\Cart\Entities\CartItem;
use Vermal\Ecommerce\Modules\Coupon\Entities\Coupon;
use Vermal\Ecommerce\Modules\Coupon\Entities\CouponPrice;
use Vermal\Ecommerce\Modules\Customers\Entities\Address;
use Vermal\Ecommerce\Modules\Customers\Entities\Customer;
use Vermal\Ecommerce\Modules\Customers\Entities\Note;
use Vermal\Ecommerce\Modules\Dashboard\Dashboard;
use Vermal\Ecommerce\Modules\Expeditions\Entities\Expedition;
use Vermal\Ecommerce\Modules\Expeditions\Entities\ExpeditionItem;
use Vermal\Ecommerce\Modules\Mailer\Entities\Inbox;
use Vermal\Ecommerce\Modules\Mailer\Mailer;
use Vermal\Ecommerce\Modules\Orders\Entities\OrderInvoice;
use Vermal\Ecommerce\Modules\Orders\Entities\OrderItemMeta;
use Vermal\Ecommerce\Modules\Payment\Entities\PaymentMethodRule;
use Vermal\Ecommerce\Modules\PrivateMessages\Entities\PrivateMessage;
use Vermal\Ecommerce\Modules\PrivateMessages\Entities\PrivateMessageConversation;
use Vermal\Ecommerce\Modules\Product\Entities\Product;
use Vermal\Ecommerce\Modules\Product\Entities\ProductAttribute;
use Vermal\Ecommerce\Modules\Product\Entities\ProductAttributeTr;
use Vermal\Ecommerce\Modules\Product\Entities\ProductCategory;
use Vermal\Ecommerce\Modules\Product\Entities\ProductCategoryTr;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMeta;
use Vermal\Ecommerce\Modules\Product\Entities\ProductMetaTr;
use Vermal\Ecommerce\Modules\Product\Entities\ProductPrice;
use Vermal\Ecommerce\Modules\Product\Entities\ProductReview;
use Vermal\Ecommerce\Modules\Product\Entities\ProductTr;
use Vermal\Ecommerce\Modules\Settings\Entities\LocalizationSetting;
use Vermal\Ecommerce\Modules\Orders\Entities\Order;
use Vermal\Ecommerce\Modules\Orders\Entities\OrderItem;
use Vermal\Ecommerce\Modules\Settings\Entities\Setting;
use Vermal\Ecommerce\Modules\Settings\LocalizationSettings;
use Vermal\Ecommerce\Modules\Settings\Settings;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingMethod;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingMethodRule;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingMethodTr;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingMethodPrice;
use Vermal\Ecommerce\Modules\Payment\Entities\PaymentMethod;
use Vermal\Ecommerce\Modules\Payment\Entities\PaymentMethodTr;
use Vermal\Ecommerce\Modules\Payment\Entities\PaymentMethodPrice;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingZone;
use Vermal\Ecommerce\Modules\Shipping\Entities\ShippingZoneRegion;
use Vermal\Ecommerce\Modules\Tax\Entities\Tax;
use Vermal\Router;
use Vermal\App;
use Vermal\View;

/**
 * Class EcommerceServiceProvider
 * @package Vermal\ecommerce
 */
class EcommerceServiceProvider
{

    /**
     * Init admin
     */
    public function boot()
    {
       // Register entities
        $this->registerEntities([
            // Product
            "Product" => [
                "namespace" => Product::class,
                "path" => "Modules/Product/Entities"
            ],
            "ProductTr" => [
                "namespace" => ProductTr::class,
                "path" => "Modules/Product/Entities"
            ],
            "ProductMeta" => [
                "namespace" => ProductMeta::class,
                "path" => "Modules/Product/Entities"
            ],
            "ProductMetaTr" => [
                "namespace" => ProductMetaTr::class,
                "path" => "Modules/Product/Entities"
            ],
			"ProductCategory" => [
				"namespace" => ProductCategory::class,
				"path" => "Modules/Product/Entities"
			],
			"ProductCategoryTr" => [
				"namespace" => ProductCategoryTr::class,
				"path" => "Modules/Product/Entities"
			],
			"ProductAttribute" => [
				"namespace" => ProductAttribute::class,
				"path" => "Modules/Product/Entities"
			],
			"ProductAttributeTr" => [
				"namespace" => ProductAttributeTr::class,
				"path" => "Modules/Product/Entities"
			],
            "ProductPrice" => [
                "namespace" => ProductPrice::class,
                "path" => "Modules/Product/Entities"
            ],
			"ProductReview" => [
				"namespace" => ProductReview::class,
				"path" => "Modules/Product/Entities"
			],

            // Orders
            "Order" => [
                "namespace" => Order::class,
                "path" => "Modules/Orders/Entities"
            ],
            "OrderItem" => [
                "namespace" => OrderItem::class,
                "path" => "Modules/Orders/Entities"
            ],
            "OrderItemMeta" => [
                "namespace" => OrderItemMeta::class,
                "path" => "Modules/Orders/Entities"
            ],
			"OrderInvoice" => [
				"namespace" => OrderInvoice::class,
				"path" => "Modules/Orders/Entities"
			],

			// Expeditions
			"Expedition" => [
				"namespace" => Expedition::class,
				"path" => "Modules/Expeditions/Entities"
			],
			"ExpeditionItem" => [
				"namespace" => ExpeditionItem::class,
				"path" => "Modules/Expeditions/Entities"
			],

            // Tax
            "Tax" => [
                "namespace" => Tax::class,
                "path" => "Modules/Tax/Entities"
            ],

	        // Shipping
//	        "ShippingZone" => [
//		        "namespace" => ShippingZone::class,
//		        "path" => "Modules/Shipping/Entities"
//	        ],
//	        "ShippingZoneRegion" => [
//		        "namespace" => ShippingZoneRegion::class,
//		        "path" => "Modules/Shipping/Entities"
//	        ],
	        "ShippingMethod" => [
		        "namespace" => ShippingMethod::class,
		        "path" => "Modules/Shipping/Entities"
	        ],
	        "ShippingMethodTr" => [
		        "namespace" => ShippingMethodTr::class,
		        "path" => "Modules/Shipping/Entities"
	        ],
	        "ShippingMethodPrice" => [
		        "namespace" => ShippingMethodPrice::class,
		        "path" => "Modules/Shipping/Entities"
	        ],
			"ShippingMethodRule" => [
				"namespace" => ShippingMethodRule::class,
				"path" => "Modules/Shipping/Entities"
			],

			// Payment
			"PaymentMethod" => [
				"namespace" => PaymentMethod::class,
				"path" => "Modules/Payment/Entities"
			],
			"PaymentMethodTr" => [
				"namespace" => PaymentMethodTr::class,
				"path" => "Modules/Payment/Entities"
			],
			"PaymentMethodPrice" => [
				"namespace" => PaymentMethodPrice::class,
				"path" => "Modules/Payment/Entities"
			],
			"PaymentMethodRule" => [
				"namespace" => PaymentMethodRule::class,
				"path" => "Modules/Payment/Entities"
			],

            // Settings
            "Setting" => [
                "namespace" => Setting::class,
                'path' => "Modules/Settings/Entities"
            ],
            "LocalizationSetting" => [
                "namespace" => LocalizationSetting::class,
                'path' => "Modules/Settings/Entities"
            ],

            // Customer
            "Customer" => [
                "namespace" => Customer::class,
                'path' => "Modules/Customers/Entities"
            ],
            "Address" => [
                "namespace" => Address::class,
                'path' => "Modules/Customers/Entities"
            ],
            "Note" => [
                "namespace" => Note::class,
                'path' => "Modules/Customers/Entities"
            ],
            "Inbox" => [
                "namespace" => Inbox::class,
                'path' => "Modules/Mailer/Entities"
            ],

			// Cart
			"Cart" => [
				"namespace" => Cart::class,
				'path' => "Modules/Cart/Entities"
			],
			"CartItem" => [
				"namespace" => CartItem::class,
				'path' => "Modules/Cart/Entities"
			],


			// Coupons
			"Coupon" => [
				"namespace" => Coupon::class,
				'path' => "Modules/Coupon/Entities"
			],
			"CouponPrice" => [
				"namespace" => CouponPrice::class,
				'path' => "Modules/Coupon/Entities"
			],

			// PrivateMessages
			"PrivateMessageConversation" => [
				"namespace" => PrivateMessageConversation::class,
				'path' => "Modules/PrivateMessages/Entities"
			],
			"PrivateMessage" => [
				"namespace" => PrivateMessage::class,
				'path' => "Modules/PrivateMessages/Entities"
			],
        ]);

		// Add localization file
		App::merge('localization-files', [
			__DIR__ . '/../public/lang/'
		]);

        // Register routes
        $this->routes();

        // Actions
        add_action('LocalizationSettings', [new LocalizationSettings(), 'start']);
        add_action('Settings', [new Settings(), 'start']);
        add_action('Mailer', [new Mailer(), 'start']);

		// Frontend nav items
		App::merge('frontendNavLinks', [
			'Objednávky' => routerLink('admin.order.index'),
			'Produkty' => routerLink('admin.product.index')
		]);

		App::bind('ECOMMERCE_BASE_PATH', BASE_PATH . '/vendor/vermal/ecommerce/src/');

		// Admin dashboard
		new Dashboard();
    }

    /**
     * Define routes
     */
    public function routes()
    {
		// Default namespace for all modules
		$namespace = '\\Vermal\\Ecommerce\\Modules\\';

        Router::prefix('{locale::locale}?/admin/', function() use ($namespace) {

            // Products
            Router::resource('product', $namespace . 'Product\\Products', 'product.');
            Router::get('product-attribute-values/{id}', $namespace . 'Product\\Products@attributeValues')->name('product.attribute');
			Router::post('generate-product-variation/{id}', $namespace . 'Product\\Products@productVariation')->name('product.variation');
			Router::get('remove-product-variation/{id}', $namespace . 'Product\\Products@removeProductVariation')->name('product.remove-variation');
            Router::resource('product-category', $namespace . 'Product\\ProductCategories', 'product-category.');
			Router::resource('product-tag', $namespace . 'Product\\ProductTags', 'product-tag.');
            Router::resource('product-attribute', $namespace . 'Product\\ProductAttributes', 'product-attribute.');
			Router::resource('product-review', $namespace . 'Product\\ProductReviews', 'product-review.');
			Router::post('product-sort', $namespace . 'Product\\Products@sort')->name('product.sort');

            // Product API
            Router::get('product-search', $namespace . 'Product\\Repositories\\ProductRepository@search')->name('product.search');
            Router::get('product-by-id', $namespace . 'Product\\Repositories\\ProductRepository@findById')->name('product.find-by-id');

            // Orders
            Router::resource('order', $namespace . 'Orders\\Orders', 'order.');
			Router::get('order/renew/{id}', $namespace . 'Orders\\Orders@rendewOrder')->name('order.renew');
			Router::get('order/invoice/{id}', $namespace . 'Orders\\Orders@invoice')->name('order.invoice');
			Router::get('order-payed-switch/{id}', $namespace . 'Orders\\Orders@payed')->name('order.payed');
			Router::get('api/order/{id}', $namespace . 'Orders\\OrdersAPI@order')->name('order.api.order');

			// Expeditions
			Router::resource('expedition', $namespace . 'Expeditions\\Expeditions', 'expedition.');
			Router::get('expedition/finish/{id}', $namespace . 'Expeditions\\Expeditions@finish')->name('expedition.finish');
			Router::get('expedition/sent/{id}', $namespace . 'Expeditions\\Expeditions@sent')->name('expedition.sent');
			Router::get('expedition/cancel/{id}', $namespace . 'Expeditions\\Expeditions@cancel')->name('expedition.cancel');

            // Taxes
            Router::resource('tax', $namespace . 'Tax\\Taxes', 'tax.');

	        // ShippingMethods
//	        Router::resource('shipping', $namespace . 'Shipping\\Shippings', 'shipping.');
	        Router::resource('shipping/methods', $namespace . 'Shipping\\ShippingMethods', 'shippingMethod.');

	        // Payment methods
			Router::resource('payment/methods', $namespace . 'Payment\\PaymentMethods', 'paymentMethod.');

			// Coupons
			Router::resource('coupons', $namespace . 'Coupon\\Coupons', 'coupon.');

            // Customer
            Router::resource('customer', $namespace . 'Customers\\Customers', 'customer.');
            Router::post('customer-note', $namespace . 'Customers\\Notes@addNote')->name('customer.add-note');
            Router::post('customer-remove-note', $namespace . 'Customers\\Notes@removeNote')->name('customer.remove-note');
            Router::resource('address', $namespace . 'Customers\\Addresses', 'address.');

			// Private messages
			Router::resource('private-messages', $namespace . 'PrivateMessages\\PrivateMessages', 'privateMessage.');

			// Customer API
            Router::get('customer-search', $namespace . 'Customers\\Repositories\\CustomerRepository@search')->name('customer.search');
            Router::get('address-by-customer', $namespace . 'Customers\\Repositories\\AddressRepository@findByCustomer')->name('address.find-by-customer');

        }, 'admin.', ['locale']);

		Router::prefix('{locale::locale}?/', function() use ($namespace) {
			Router::get(apply_filters('checkout_path', 'checkout'), $namespace . 'Checkout\\Checkout@index')->name('checkout');
			Router::get(apply_filters('order_received', 'checkout/order-received/{id}'), $namespace . 'Checkout\\Checkout@orderReceived')
				->name('order-received');
			Router::post(apply_filters('order_received', 'checkout/order-received/{id}'), $namespace . 'Checkout\\Checkout@orderReceived')
				->name('order-received-post');
			Router::post('checkout-make', $namespace . 'Checkout\\Checkout@make')->name('checkout.make');
			Router::get('order/invoice/{token}', $namespace . 'Orders\\Invoices@invoiceTokenized')->name('order.invoice');


			Router::post('cart/add-item', $namespace . 'Cart\\Cart@addItem')->name('cart.add');
			Router::post('cart/remove-item', $namespace . 'Cart\\Cart@removeItem')->name('cart.remove');
			Router::post('cart/change-qty', $namespace . 'Cart\\Cart@changeQty')->name('cart.change-qty');
			Router::post('cart/add-coupon', $namespace . 'Cart\\Cart@addCoupon')->name('cart.add-coupon');
			Router::post('cart/remove-coupon', $namespace . 'Cart\\Cart@removeCoupon')->name('cart.remove-coupon');

			if (empty(appGet('ecommerce.shop-path')))
				Router::get(apply_filters('shop_path', 'shop'), $namespace . 'Shop\\Shop@index')->name('shop');

			// Ecommerce oauth login
			Router::get(apply_filters('customer_fb_callback_url', 'customer-fb'), $namespace . 'Customers\\Auth\\Facebook@callback')->name('customer.auth-fb');
			Router::get(apply_filters('customer_google_callback_url', 'customer-google'), $namespace . 'Customers\\Auth\\Google@callback')->name('customer.auth-google');

		}, 'ecommerce.' , ['locale']);

		Console::registerCommand('ecommerce-seed', [Seeder::class, 'main']);
		Console::registerCommand('ecommerce-dummy-products', [DummyProducts::class, 'main']);

		View::addRegexTag("v:admin:ecommerce:(.*?):href=[\"\'](.*?)[\"\']", 'href="' . URL . 'vendor/vermal/ecommerce/src/App/Modules/$1/$2"');
		View::addRegexTag("v:admin:ecommerce:(.*?):src=[\"\'](.*?)[\"\']", 'src="' . URL . 'vendor/vermal/ecommerce/src/App/Modules/$1/$2"');
		View::addRegexTag("v:admin:ecommerce:href=[\"\'](.*?)[\"\']", 'href="' . URL . 'vendor/vermal/ecommerce/src/$1"');
		View::addRegexTag("v:admin:ecommerce:src=[\"\'](.*?)[\"\']", 'src="' . URL . 'vendor/vermal/ecommerce/src/$1"');
    }

    /**
     * Register all entites and paths mentioned in boot method
     *
     * @param $entites
     */
    private function registerEntities($entites)
    {
        $database = App::get('database');
        foreach ($entites as $key => $entity) {
            $database['entities'][$entity['namespace']] = $key;
            $database['paths'][$key] = 'vendor/vermal/ecommerce/src/' . $entity['path'];
        }
        App::bind('database', $database);
    }

	/**
	 * Calculate tax
	 *
	 * @param $price
	 * @param $rate
	 * @return float|int
	 */
    public static function calculateTax($price, $rate)
	{
		$pricesIncludingTax = Controller::$appSettings->include_tax;
		if (!empty($pricesIncludingTax)) {
			return $price - ($price / (1 + ($rate / 100)));
		} else {
			return $price - ($price + ($price * ($rate / 100)));
		}
	}

}
